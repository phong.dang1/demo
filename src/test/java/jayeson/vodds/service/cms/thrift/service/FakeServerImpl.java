package jayeson.vodds.service.cms.thrift.service;

import java.lang.management.ManagementFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.service.CMSThriftServer;
import jayeson.vodds.service.cms.service.ServiceModule;
import jayeson.vodds.service.cms.thrift.CMS;
import jayeson.vodds.service.cms.transactions.TransactionModule;
import jayeson.vodds.service.cms.transactions.TransactionProcessor;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;

/**
 * Instantiate and implement the thrift cms server to receive and process credit requests
 * @author Praveen
 *
 */
public class FakeServerImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(FakeServerImpl.class.getName());

	TServer server;
	Thread serverThread;
	ScheduledExecutorService scService;
	boolean runScheduler=true;
	
	class StartThriftServer implements Runnable{
		Injector injector;
		public StartThriftServer(Injector inj){
			injector = inj;
		}
		@Override
		public void run() {

				
				CMSThriftServer thriftService = injector.getInstance(CMSThriftServer.class);
				
				try {
					
					
			        TServerSocket serverTransport = new TServerSocket(9011);
			        CMS.Processor processor =new CMS.Processor(thriftService);
			           
			        server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));
			        
						try{
							server.serve();
							
							logger.debug("Thrift Server Stopped..");
						}
						catch(Exception ex){
							logger.error("Thrift server stopped due to error {}",ex);
						}

					
				} catch(Exception ex) {
					logger.error("Fail to start the thrift service", ex);
					System.exit(-1);
				}			
		}
		
	}
	
	
	public void startServer(){
		
		// a scheduling executor service to run shcedule repeated tasks in CMS
		scService= Executors.newScheduledThreadPool(200);
		
		Injector injector = Guice.createInjector(new FakeDBModule(),new TransactionModule());        

		serverThread = new Thread(new StartThriftServer(injector));
		// start the thrift server
		serverThread.start();
		logger.debug("cms thrift server started");
		
		// schedule the transaction queue processor for a fixed interval
		Provider<TransactionProcessor> tprocessor = injector.getProvider(TransactionProcessor.class);	
		//scService.scheduleWithFixedDelay(tprocessor.get(),1,5,TimeUnit.SECONDS);
			
		
		
		
	}
	
	public void stopServer(){
		
		runScheduler=false;
		server.stop();
		serverThread.interrupt();

	}
	
	public static void main(String args[]) throws InterruptedException{
		FakeServerImpl instance = new FakeServerImpl();
		instance.startServer();
		Thread.sleep(3000);
		instance.stopServer();
	}

}
