package jayeson.vodds.service.cms.thrift.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jayeson.vodds.service.cms.json.LeverageReserveInfo;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.thrift.*;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/***
 * Tests the thrift service calls of cms, including
 * starting up a server, submitting thrift requests, evaluating thrift results
 * @author Praveen
 *
 */
public class CMSThriftClientTest {
	
	static FakeServerImpl server;
	
	@BeforeClass
	public static void setUpServer(){
		server = new FakeServerImpl();
		server.startServer();
		
	}
	
	@AfterClass
	public static void cleanUpServer(){
		server.stopServer();
	}
	
	@Test
	public void testExecuteTransaction() {
		try {
			TTransport transport = new TSocket("localhost",9011);
			TProtocol protocol = new TBinaryProtocol(transport);
			CMS.Client thriftClient = new CMS.Client(protocol);
			transport.open();
			thriftClient.executeTransaction("RETURN", 87, 39821, 1000, "Test");
		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	// test the reserve credit function for partial success case, when user do not have sufficient funds but asks to reserve available credit
	public void testPartialReserveCredit() {


		ObjectMapper oMapper = new ObjectMapper();
		//NormalReserveInfo readInfo = oMapper.readValue(rJson, NormalReserveInfo.class);
		
		// prepare the thrift credit request
		ThriftCreditRequestInfo reqInfo = new ThriftCreditRequestInfo();
		reqInfo.setOType(ThriftOrderType.NORMAL);
		reqInfo.setTType(ThriftTransactionType.RESERVE);
		reqInfo.setServiceId("fakeSubmitter");
		reqInfo.setUserId(192);
		reqInfo.setMatchId("A");
		reqInfo.setSportType("SOCCER");
		reqInfo.setOrderStatus("CONFIRMED");
		reqInfo.setExecutionStage("OPENED");
		reqInfo.setStartTime(1000);
		

		NormalReserveInfo rInfo = new NormalReserveInfo();
		rInfo.creditAmount=5000;
		rInfo.modelId=2001;
		rInfo.useAllCredit=true;
		String rJson;
		try {
			rJson = oMapper.writeValueAsString(rInfo);
			reqInfo.setRequestJson(rJson);
			TTransport transport = new TSocket("localhost",9011);
			TProtocol protocol = new TBinaryProtocol(transport);
			CMS.Client thriftClient = new CMS.Client(protocol);
			transport.open();
			//ThriftCreditRequestResult tResult = thriftClient.processCreditRequest(reqInfo);
			//System.out.println(tResult.responseJson);
			//NormalReserveResult resInfo = oMapper.readValue(tResult.responseJson, NormalReserveResult.class);
			 
			
			
			//assertEquals(tResult.resultCode,ThriftResultCode.SUCCESS_PARTIAL);
			//assertEquals(resInfo.sCreditAmount,4166.66,0.1);


		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		}
		
		
	}
	
	// test the successful case, when user  have sufficient funds
	@Test
	public void testSuccessReserveCredit() {


		ObjectMapper oMapper = new ObjectMapper();
		//NormalReserveInfo readInfo = oMapper.readValue(rJson, NormalReserveInfo.class);
		
		// prepare the thrift credit request
		ThriftCreditRequestInfo reqInfo = new ThriftCreditRequestInfo();
		reqInfo.setOType(ThriftOrderType.POSITIONAL_CLOSE);
		reqInfo.setTType(ThriftTransactionType.RESERVE);
		reqInfo.setServiceId("fakeSubmitter");
		reqInfo.setUserId(92);
		reqInfo.setMatchId("A");
		reqInfo.setSportType("SOCCER");
		reqInfo.setOrderStatus("CONFIRMED");
		reqInfo.setExecutionStage("OPENED");
		reqInfo.setStartTime(1000);


		LeverageReserveInfo rInfo = new LeverageReserveInfo();
		rInfo.creditAmount=2000;
		rInfo.modelId=2001;
		rInfo.useAllCredit=true;
		rInfo.isMarginTopup = true;
		String rJson;
		try {
			rJson = oMapper.writeValueAsString(rInfo);
			reqInfo.setRequestJson(rJson);
			TTransport transport = new TSocket("localhost",9011);
			TProtocol protocol = new TBinaryProtocol(transport);
			CMS.Client thriftClient = new CMS.Client(protocol);
			transport.open();
			//thriftClient.processCreditRequest(reqInfo);
			//System.out.println(tResult.responseJson);
			//NormalReserveResult resInfo = oMapper.readValue(tResult.responseJson, NormalReserveResult.class);
			 
			
			
			//assertEquals(tResult.resultCode,ThriftResultCode.SUCCESS);
			//assertEquals(resInfo.sCreditAmount,2000,0.1);


		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		}
		
		
	}

	// test the failure case, when user do not have sufficient funds
	@Test
	public void testFailedReserveCredit() {


		ObjectMapper oMapper = new ObjectMapper();
		//NormalReserveInfo readInfo = oMapper.readValue(rJson, NormalReserveInfo.class);
		
		// prepare the thrift credit request
		ThriftCreditRequestInfo reqInfo = new ThriftCreditRequestInfo();
		reqInfo.setOType(ThriftOrderType.NORMAL);
		reqInfo.setTType(ThriftTransactionType.RESERVE);
		reqInfo.setServiceId("fakeSubmitter");
		reqInfo.setUserId(106);
		reqInfo.setMatchId("A");
		reqInfo.setSportType("SOCCER");
		reqInfo.setOrderStatus("CONFIRMED");
		reqInfo.setExecutionStage("OPENED");
		reqInfo.setRequestJson("Test");
		reqInfo.setStartTime(1000);


		NormalReserveInfo rInfo = new NormalReserveInfo();
		rInfo.creditAmount=5000;
		rInfo.modelId=2001;
		rInfo.useAllCredit=false;
		String rJson;
		try {
			rJson = oMapper.writeValueAsString(rInfo);
			reqInfo.setRequestJson(rJson);
			TTransport transport = new TSocket("localhost",9011);
			TProtocol protocol = new TBinaryProtocol(transport);
			CMS.Client thriftClient = new CMS.Client(protocol);
			transport.open();
			
			//ThriftCreditRequestResult tResult = thriftClient.processCreditRequest(reqInfo);
			//System.out.println(tResult.responseJson);
			//NormalReserveResult resInfo = oMapper.readValue(tResult.responseJson, NormalReserveResult.class);
			 
			
			
			//assertEquals(tResult.resultCode,ThriftResultCode.FAILED);
			//assertEquals(resInfo.sCreditAmount,-1,0.1);


		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		catch (TTransportException e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void testTopUp() {
		try {
			TTransport transport = new TSocket("localhost",9011);
			TProtocol protocol = new TBinaryProtocol(transport);
			CMS.Client thriftClient = new CMS.Client(protocol);
			transport.open();
			ThriftUpdateCreditForUser updateInfo = new ThriftUpdateCreditForUser("AP02",87,86,new Double(2000),"Test", "localhost", 0, false,0, 0);
			thriftClient.updateCreditForUser(updateInfo);
		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		}
	}
	

}
