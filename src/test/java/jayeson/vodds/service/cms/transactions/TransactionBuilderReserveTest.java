package jayeson.vodds.service.cms.transactions;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import jayeson.database.BetUser;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;








import com.google.inject.Inject;



/** Test the transaction builder component
 * 
 * @author Praveen
 *
 */

// run the orders in pre determined order
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class TransactionBuilderReserveTest {
	
	@Inject 
	TransactionBuilder builder;
	@Inject
	UserManager manager;
	
	BetUserNode node1;
	
	@Before
	public void initializeData(){
		
		node1 = manager.findBetUserNode(192);
	}
	
/**
 * Test whether the reserve credit result generates the correct number of db transactions
 */
	@Test
	public void testAPopulateReserveCreditTransactions() {

		ReserveCreditResult fakeResult = new ReserveCreditResult();
		fakeResult.setModelId(201);
		fakeResult.setMessage("reserved succesfully");
		fakeResult.setReservedCredit(500);
		fakeResult.setCommissionAmount(100);
		
		//builder.populateReserveCreditTransactions(fakeResult, node1);
		// transaction 1 - insert user credit subtract in transaction table
		// transaction 2 - insert user commission subtract in transaction table
		// transaction 3 - insert agent1 credit add in transaction table
		// trasaction 4 - insert agent2 credit add in transaction table
		
		//assertEquals(builder.tQueue.getTransQueue().size(),4);
		
		// transaction 4 - update bet user credit in user table
		//assertEquals(builder.tQueue.getUserQueue().size(),1);
		
		// transaction 5 - update agent1 credit in user table
		// transaction 6 - update agent2 credit in user table
		
		//assertEquals(builder.tQueue.getAgentQueue().size(),2);
		
		
		
		// test if the agent has commission added to their credit
		//assertEquals(node1.getAgentUsers().get(0).getUser().getCredit().doubleValue(),100050,0.01);
		//assertEquals(node1.getAgentUsers().get(1).getUser().getCredit().doubleValue(),100050,0.01);
		
		
		// test if the value for credits in transaction queue are correct
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),-500,0.01);
		//builder.tQueue.getTransQueue().poll();
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),-100,0.01);
		//builder.tQueue.getTransQueue().poll();

//		assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),50,0.01);
//		builder.tQueue.getTransQueue().poll();
//		assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),50,0.01);
//		builder.tQueue.getTransQueue().poll();

				
		
	}
	

	

}
