package jayeson.vodds.service.cms.transactions;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import jayeson.database.AgentUser;
import jayeson.database.BetUser;
import jayeson.database.BettingModel;
import jayeson.database.Transaction;
import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.database.newvodds.DarkPoolBetInfo;
import jayeson.database.newvodds.ModelTransaction;
import jayeson.database.newvodds.PositionalModel;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import jayeson.vodds.service.cms.data.TransactionWrapper;

public class TransactionTestModule extends AbstractModule {

	@Override
	protected void configure() {
	}

	@Provides
	Queue<ModelTransaction> provideTQueue() {
		
		return new ConcurrentLinkedQueue<ModelTransaction>();
	}
	
	@Provides
	Queue<AgentUser> provideAQueue() {
		
		return new ConcurrentLinkedQueue<AgentUser>();
	}

	@Provides
	Queue<BetUser> provideBQueue() {
		
		return new ConcurrentLinkedQueue<BetUser>();
	}
	
	@Provides
	Queue<BettingModel> provideOQueue() {
		
		return new ConcurrentLinkedQueue<BettingModel>();
	}
	
	@Provides
	Queue<PositionalModel> providePMQueue() {
		
		return new ConcurrentLinkedQueue<PositionalModel>();
	}
	
	@Provides
	Queue<BettingModelExtraVodds> provideEVMQueue(){
		return new ConcurrentLinkedQueue<BettingModelExtraVodds>();
	}
	
	@Provides
	Queue<TransactionWrapper> provideUserTransaction(){
		return new ConcurrentLinkedQueue<TransactionWrapper>();
	}
	
	@Provides
	Queue<DarkPoolBetInfo> provideDPoolQueue(){
		return new ConcurrentLinkedQueue<DarkPoolBetInfo>();
	}

}
