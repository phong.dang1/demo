package jayeson.vodds.service.cms.transactions;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import jayeson.database.BetUser;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;









import com.google.inject.Inject;



/** Test the transaction builder component
 * 
 * @author Praveen
 *
 */

// run the orders in pre determined order
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class TransactionBuilderRefundTest {
	
	@Inject 
	TransactionBuilder builder;
	@Inject
	UserManager manager;
	
	BetUserNode node1;
	
	@Before
	public void initializeData(){
		
		node1 = new BetUserNode();
	}
	

	/**
	 * test whether the refund credit action populates correct transactions
	 * @throws InterruptedException 
	 */
	@Test
	public void testDPopulateRefundCreditTransactions() throws InterruptedException{
		
		ReserveCreditResult fakeResult = new ReserveCreditResult();
		fakeResult.setModelId(201);
		fakeResult.setMessage("reserved succesfully");
		fakeResult.setReservedCredit(500);
		fakeResult.setCommissionAmount(100);
		TradeLimitData data = new TradeLimitData();
		
		builder.populateReserveCreditTransactions(fakeResult, node1,ThriftOrderType.NORMAL.name(),data);
	
		
		RefundCreditResult fakeRefund = new RefundCreditResult();
		fakeRefund.setModelId(201);
		fakeRefund.setRefundedCredit(100);
		fakeRefund.setRefundedCommission(20);
		
		builder.populateRefundCreditTransactions(fakeRefund, node1,ThriftOrderType.NORMAL.name(),data);
		
		// transaction 1 - insert user credit add in transaction table
		// transaction 2 - insert agent1 credit subtract in transaction table
		// trasaction 3 - insert agent2 credit subtract in transaction table
		// since the transaction are polled in testC
		//assertEquals(builder.tQueue.getTransQueue().size(),8);
		
		// transaction 4 - update bet user credit in user table
		//assertEquals(builder.tQueue.getUserQueue().size(),2);
		
		// transaction 5 - update agent1 credit in user table
		// transaction 6 - update agent2 credit in user table
		
		//assertEquals(builder.tQueue.getAgentQueue().size(),4);
		
	
		
	
		// assert the credit of agent user
		//assertEquals(node1.getAgentUsers().get(0).getUser().getCredit().doubleValue(),100040,0.01);
		//assertEquals(node1.getAgentUsers().get(1).getUser().getCredit().doubleValue(),100040,0.01);
		
		
		// test if the value for credits in transaction queue are correct
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),-500,0.01);
		builder.tQueue.getTransQueue().poll();
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),-100,0.01);
		builder.tQueue.getTransQueue().poll();
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),50,0.01);
		builder.tQueue.getTransQueue().poll();
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),50,0.01);
		builder.tQueue.getTransQueue().poll();
		
		// test if the value for credits in transaction queue are correct	
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),100,0.01);
		builder.tQueue.getTransQueue().poll();
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),20,0.01);
		builder.tQueue.getTransQueue().poll();

		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),-10,0.01);
		builder.tQueue.getTransQueue().poll();
		//assertEquals(builder.tQueue.getTransQueue().peek().getCreditAmount().doubleValue(),-10,0.01);
		builder.tQueue.getTransQueue().poll();
		


	}
	
	

}
