package jayeson.vodds.service.cms.orders;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.json.LeverageRefundInfo;
import jayeson.vodds.service.cms.order.action.RefundOpenCreditAction;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;
import jayeson.vodds.service.cms.transactions.TransactionTestModule;


import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class RefundOpenCreditActionTest {
	
	
	@Inject UserManager manager;
	@Inject TransactionBuilder builder;
	@Inject VoddsCommonUtility voddsCommonUtility; 
	ExecutorService service = Executors.newFixedThreadPool(10);
	@Test
	public void testSuccesfulCreditAction(){
		
		LeverageRefundInfo refInfo = new LeverageRefundInfo();
		refInfo.refundAmount=500;
		refInfo.modelId=2000;
		refInfo.leverageRatio=10;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		
		RefundOpenCreditAction cTask = new RefundOpenCreditAction(manager,builder, refInfo,info,voddsCommonUtility);
		Future<RefundCreditResult> fResult1 = service.submit(cTask);
		
		try {
			
			RefundCreditResult taskResult = fResult1.get();
			assertEquals(taskResult.getRefundedCredit(),500,0.001);
			//assertEquals(taskResult.getRefundedCommission(),100,0.001);
			//assertEquals(manager.findBetUserNode(106).getCredit().doubleValue(),5150,0.001);
			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}
		
		
		
	}

}