package jayeson.vodds.service.cms.orders;

import static org.junit.Assert.*;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.order.action.ReserveNormalCreditAction;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.order.worker.OutstandingHandler;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;
import jayeson.vodds.service.cms.transactions.TransactionTestModule;


import jayeson.vodds.service.cms.util.GeneralUtility;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class ReserveCreditActionTest {
	
	/**
	 * test whether the reserve credit task return the correct results and generates all required database transactions
	 * for the normal succesful use case
	 */
	
	@Inject UserManager manager;
	@Inject TransactionBuilder builder;
	@Inject VoddsCommonUtility voddsCommonUtility;
	@Inject	GeneralUtility genUtil;
	
	ExecutorService service = Executors.newFixedThreadPool(10);
	
	@Test
	public void testSuccesfulReserveCreditActionTask(){
		
		NormalReserveInfo fakeReserve = new NormalReserveInfo();
		fakeReserve.creditAmount = 4166.1;
		fakeReserve.modelId = 2001;
		fakeReserve.useAllCredit = false;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		info.setUserId(106);
		
		
		ReserveNormalCreditAction creditTask = new ReserveNormalCreditAction(manager, builder, fakeReserve, info, voddsCommonUtility, genUtil);
		Future<ReserveCreditResult> fResult1 = service.submit(creditTask);
		
		try {
			
			ReserveCreditResult taskResult = fResult1.get();
			//assertEquals(taskResult.getReservedCredit(),4166.1,0.001);
			//assertEquals(taskResult.getCommissionAmount(),833.22,0.001);
			assertEquals(taskResult.getModelId(),2001,0.001);
			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}
		
	}
	
	/**
	 * test whether the reserve credit task return correct results for normail failure case
	 */
	
	@Test
	public void testFailedReserveCreditAction(){
		
		NormalReserveInfo failReserve = new NormalReserveInfo();
		failReserve.creditAmount=5010;
		failReserve.modelId = 20002;
		failReserve.useAllCredit = false;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		info.setUserId(106);
		
		ReserveNormalCreditAction creditTask = new ReserveNormalCreditAction(manager, builder, failReserve, info, voddsCommonUtility, genUtil);
		Future<ReserveCreditResult> fResult2 = service.submit(creditTask);
		
		try {
			
			ReserveCreditResult taskResult = fResult2.get();
			assertEquals(taskResult.getReservedCredit(),-1,0.001);
			assertEquals(taskResult.getCommissionAmount(),Double.NaN,0.001);
			assertEquals(taskResult.getModelId(),20002);
			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}

		
	}
	
	/**
	 * test the use of useAllCredit flag, when there is no sufficient credit
	 */
	@Test
	public void testUseAllCreditFlag(){
		
		NormalReserveInfo failReserve = new NormalReserveInfo();
		failReserve.creditAmount=5010;
		failReserve.modelId = 20002;
		failReserve.useAllCredit = true;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		info.setUserId(106);
		
		ReserveNormalCreditAction creditTask = new ReserveNormalCreditAction(manager, builder, failReserve, info, voddsCommonUtility, genUtil);
		Future<ReserveCreditResult> fResult3 = service.submit(creditTask);
		
			
//		try {
//			
//			ReserveCreditResult taskResult = fResult3.get();
//			//assertEquals(taskResult.getReservedCredit(),4166.66,0.01);
//			//assertEquals(taskResult.getCommissionAmount(),833.33,0.01);
//			assertEquals(taskResult.getModelId(),20002);
//			
//			
//			
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//
//			e.printStackTrace();
//		}

		
	}
	
	
	

}
