package jayeson.vodds.service.cms.orders;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.json.LeverageReserveInfo;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.order.action.ReserveCloseCreditAction;
import jayeson.vodds.service.cms.order.action.ReserveNormalCreditAction;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;
import jayeson.vodds.service.cms.transactions.TransactionTestModule;


import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class ReserveCloseCreditActionTest {
	

	@Inject UserManager manager;
	@Inject TransactionBuilder builder;
	@Inject VoddsCommonUtility voddsCommonUtility; 
	
	ExecutorService service = Executors.newFixedThreadPool(10);
	
	/**
	 * test whether the reserve credit task return the correct results and generates all required database transactions
	 * for the normal succesful use case
	 */
	@Test
	public void testSuccesfulReserveCreditActionTask(){
		
		LeverageReserveInfo fakeReserve = new LeverageReserveInfo();
		fakeReserve.creditAmount = 5000;
		fakeReserve.modelId = 2001;
		fakeReserve.useAllCredit = false;
		fakeReserve.isMarginTopup = true;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		
		
		ReserveCloseCreditAction creditTask = new ReserveCloseCreditAction(manager,builder, fakeReserve,info,voddsCommonUtility);
		Future<ReserveCreditResult> fResult1 = service.submit(creditTask);
		
		try {
			
			ReserveCreditResult taskResult = fResult1.get();
			assertEquals(taskResult.getReservedCredit(),5000,0.001);
			//assertEquals(taskResult.getCommissionAmount(),1000,0.001);
			//assertEquals(manager.findBetUserNode(106).getCredit().doubleValue(),4000,0.001);

			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * test the use of useAllCredit flag, when there is no sufficient credit
	 */
	@Test
	public void testUseAllCreditFlag(){
		
		LeverageReserveInfo failReserve = new LeverageReserveInfo();
		failReserve.creditAmount=50000;
		failReserve.modelId = 20002;
		failReserve.useAllCredit = true;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		ReserveCloseCreditAction creditTask = new ReserveCloseCreditAction(manager, builder,failReserve,info,voddsCommonUtility);
		Future<ReserveCreditResult> fResult3 = service.submit(creditTask);
		
			
		try {
			
			ReserveCreditResult taskResult = fResult3.get();
			//assertEquals(taskResult.getReservedCredit(),25000,0.01);
			//assertEquals(taskResult.getCommissionAmount(),5000,0.01);
			//assertEquals(manager.findBetUserNode(108).getCredit().doubleValue(),0,0.001);

			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}

		
	}

}