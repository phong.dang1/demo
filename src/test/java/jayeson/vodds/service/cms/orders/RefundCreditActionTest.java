package jayeson.vodds.service.cms.orders;

import static org.junit.Assert.*;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.json.NormalRefundInfo;
import jayeson.vodds.service.cms.order.action.RefundNormalCreditAction;
import jayeson.vodds.service.cms.order.result.IActionResult;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.worker.OutstandingHandler;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;
import jayeson.vodds.service.cms.transactions.TransactionTestModule;


import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.google.inject.Injector;

import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})

public class RefundCreditActionTest {
	
	/*** 
	 * test whether the refund credit action succesfully adds the credit back to user node
	 */
	
	
	@Inject UserManager manager;
	@Inject TransactionBuilder builder;
	@Inject OutstandingHandler defaultHandler;
	@Inject VoddsCommonUtility voddsCommonUtility; 
	
	ExecutorService service = Executors.newFixedThreadPool(10);

	@Test
	public void testRefundCredit(){
		
		NormalRefundInfo rInfo = new NormalRefundInfo();
		rInfo.modelId = 2001;
		rInfo.refundAmount = 59.23;
		ThriftCreditRequestInfo reqInfo = new ThriftCreditRequestInfo();
		
		
		RefundNormalCreditAction creditTask = new RefundNormalCreditAction(manager, builder,rInfo,reqInfo,voddsCommonUtility);
		Future<RefundCreditResult> cResult = service.submit(creditTask);
		
		try {
			IActionResult result = cResult.get();
			assertEquals(result.getModelId(),2001);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}

}
