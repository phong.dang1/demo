package jayeson.vodds.service.cms.orders;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.json.LeverageReserveInfo;
import jayeson.vodds.service.cms.order.action.ReserveOpenCreditAction;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;
import jayeson.vodds.service.cms.transactions.TransactionTestModule;

import jayeson.vodds.service.cms.util.GeneralUtility;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class ReserveOpenCreditActionTest {
	
	@Inject UserManager manager;
	@Inject TransactionBuilder builder;
	@Inject VoddsCommonUtility voddsCommonUtility;
	@Inject	GeneralUtility genUtil;
	ExecutorService service = Executors.newFixedThreadPool(10);
	
	/**
	 * test whether the reserve credit task return the correct results for the case when user have sufficient credit to fulfil the order
	 */
	
	@Test
	public void testSuccesfulCreditAction(){
		
		LeverageReserveInfo resInfo = new LeverageReserveInfo();
		resInfo.creditAmount = 500;
		resInfo.leverageRatio = 10;
		resInfo.useAllCredit = false;
		resInfo.modelId = 21;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		
		ReserveOpenCreditAction creditTask = new ReserveOpenCreditAction(manager, builder, resInfo, info, voddsCommonUtility, genUtil);
		Future<ReserveCreditResult> fResult1 = service.submit(creditTask);
		
		try {
			
			ReserveCreditResult taskResult = fResult1.get();
			assertEquals(taskResult.getReservedCredit(),500,0.001);
			//assertEquals(taskResult.getCommissionAmount(),100,0.001);
			//assertEquals(manager.findBetUserNode(106).getCredit().doubleValue(),4850,0.001);
			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}
		
	}

	/**
	 * test whether the reserve credit task return the correct results for the case when user have insufficient credit to fulfil the order but allows
	 * to use all available credit
	 */
	
	
	@Test
	public void testUseAllCreditFlagCondition(){
		
		LeverageReserveInfo resInfo = new LeverageReserveInfo();
		resInfo.creditAmount = 50000;
		resInfo.leverageRatio = 10;
		resInfo.useAllCredit = true;
		resInfo.modelId = 21;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		
		ReserveOpenCreditAction creditTask = new ReserveOpenCreditAction(manager, builder, resInfo, info, voddsCommonUtility, genUtil);
		Future<ReserveCreditResult> fResult1 = service.submit(creditTask);
		
		try {
			
			ReserveCreditResult taskResult = fResult1.get();
			//assertEquals(taskResult.getReservedCredit(),16666.67,0.01);
			//assertEquals(taskResult.getCommissionAmount(),3333.33,0.01);
			//assertEquals(manager.findBetUserNode(107).getCredit().doubleValue(),0,0.01);
			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}
		
	}
	
	/**
	 * test the condition when the user doesn't have sufficient credit to fulfil the order
	 */
	
	@Test
	public void testFailureCondition(){
		
		LeverageReserveInfo resInfo = new LeverageReserveInfo();
		resInfo.creditAmount = 50000;
		resInfo.leverageRatio = 10;
		resInfo.useAllCredit = false;
		resInfo.modelId = 21;
		ThriftCreditRequestInfo info = new ThriftCreditRequestInfo();
		
		ReserveOpenCreditAction creditTask = new ReserveOpenCreditAction(manager, builder, resInfo, info, voddsCommonUtility, genUtil);
		Future<ReserveCreditResult> fResult1 = service.submit(creditTask);
		
		try {
			
			ReserveCreditResult taskResult = fResult1.get();
			assertEquals(taskResult.getReservedCredit(),-1,0.01);
			assertEquals(taskResult.getCommissionAmount(),Double.NaN,0.01);
			//assertEquals(manager.findBetUserNode(108).getCredit().doubleValue(),5000,0.01);
			
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		}
		
	}

}