package jayeson.vodds.service.cms.order.settlement;

import static org.junit.Assert.*;
import jayeson.vodds.service.cms.data.OrderWrapper;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;
import jayeson.vodds.service.cms.transactions.TransactionTestModule;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

/**
 * Test whether the settlement of orders are fulfilled correctly in general scenario
 * @author Praveen
 *
 */

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class,TransactionTestModule.class})
public class NormalOrderSettlementTest {
	
	@Inject UnsettledQueue orderQ;
	@Inject OrderWrapperManager oManager;
	
	
	@Test
	public void testNormalOrderSettlement(){
		//OrderWrapper oWrapper = oManager.findStoppedOrderById(12661);
		//orderQ.add(oWrapper);
		//orderQ.settleOrders();
		//assertEquals(oWrapper.getOrderSettlement(),"WIN");
		
	}
	
	@Test
	public void testPositionalOrderSettlement(){
		//OrderWrapper oWrapper = oManager.findStoppedOrderById(39208);
		//orderQ.add(oWrapper);
		//orderQ.settleOrders();
		//assertEquals(oWrapper.getOrderSettlement(),"SETTLED");
		
	}

	

}
