package jayeson.vodds.service.cms.data;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.FakeDBModule;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class})

public class UserManagerTest {

	@Inject UserManager manager;


	/**
	 * test if the user manager correctly retrieves and generates the bet user nodes
	 * 
	 */
	@Test
	public void testFindBetUserNode() {
		
		//BetUserNode node1 = manager.findBetUserNode(101); 
		//BetUserNode node2 = manager.findBetUserNode(110);
		//BetUserNode node3 = manager.findBetUserNode(111);
		
		
		//assertNull(node3);
		//assertNotNull(node1);
		//assertNotNull(node2);
		
	}
	
	/**
	 * Test if the cache of user size is correct, that is user manager is not loading from dbservice for already queried users
	 */
	@Test
	public void testUserManagerCache(){
		//BetUserNode node1 = manager.findBetUserNode(101); 
		//BetUserNode node2 = manager.findBetUserNode(110);
		
		//assertEquals(manager.getBetUserCache().size(),2);
		//assertEquals(manager.getAgentCache().size(),2);
	}

	/** Test if the user credit is correct
	 * 
	 */
	@Test
	public void testUserCredit(){
		
		//BetUserNode node1 = manager.findBetUserNode(101);
		//assertEquals(node1.getCredit(),new BigDecimal(5000));
	}
	

	public void testFindAgentUserNode() {
		fail("Not yet implemented");
	}

}
