package jayeson.vodds.service.cms.database;



import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.AgentUser;
import jayeson.database.AgentUserType;
import jayeson.database.BetUser;
import jayeson.database.Currency;
import jayeson.database.User;
import jayeson.database.newvodds.VoddsBetUser;

/**
 * Generates and manages the fake bet users, along with a predefined hierarchy of
 * user agent mapping
 * 
 * All the bet users will have the same parents, upto two levels of hierarchy
 * 
 * @author Praveen
 *
 */

@Singleton
public class FakeUserGenerator {
	
	// storage for bet and agent users
	Map<Long, VoddsBetUser> userCache;
	Map<Long, AgentUser> agentCache;
	
	@Inject
	public FakeUserGenerator(){
		agentCache = new HashMap<Long,AgentUser>();
		userCache = new HashMap<Long,VoddsBetUser>();
		populateAgentUsers();

	}
	
	// generates a bet user instance if not found in the cache, allow users only upto 110
	
	public VoddsBetUser getBetUser(long id){
		
		if(userCache.containsKey(id)){
			return userCache.get(id);
		}
		
		else{
			// return null when the user is out of range (to facilitate testing)
			if(id > 110)
				return null;
			
			
			VoddsBetUser user = new VoddsBetUser();
			user.setId(id);
			Currency uCurrency = new Currency();
			uCurrency.setCode("SGD");
			uCurrency.setIsBase((byte)1);			
			user.setCurrency(uCurrency);
			
			user.setCredit(new BigDecimal(1000));
			user.setCreditManaged((short) 1);
			user.setCashBalance(new BigDecimal(10000));
			user.setCommission(new BigDecimal(0.03));
			
			user.setParent(agentCache.get(103));
			
			user.setInitialCreditLimit(new BigDecimal(5000));
			user.setOutstandingAmount(new BigDecimal(0));
			user.setAccountGroup(null);
			user.setAccountHolderOnly((byte) 0);
			User oUser = new User();
			oUser.setUsername("test123");
			user.setUser(oUser);
			
			userCache.put(id, user);
			
			return user;
		}
		
	}
	
	
	
	// generates two agent users, one normal and one master that will act as parents for all fake bet users
	
	public void populateAgentUsers(){
		
		
		
		AgentUserType aType1 = new AgentUserType();
		aType1.setTypeName("fake normal agent");
		aType1.setLevel((short) 3);
		
		AgentUserType aType2 = new AgentUserType();
		aType2.setTypeName("fake master agent");
		aType2.setLevel((short) 2);
		
		Currency uCurrency = new Currency();
		uCurrency.setCode("SGD");
		uCurrency.setIsBase((byte)1);			

		
		
		AgentUser aUser1 = new AgentUser();
		aUser1.setId(104);
		aUser1.setParentId(103);
		aUser1.setCredit(new BigDecimal(100000));
		aUser1.setCurrency(uCurrency);
		aUser1.setCommission(new BigDecimal(0.1));
		aUser1.setAgentType(aType1);
		aUser1.setOutstandingAmount(new BigDecimal(0));

		
		AgentUser aUser2 = new AgentUser();
		aUser2.setId(103);
		aUser2.setParentId(-1);
		aUser2.setCredit(new BigDecimal(100000));
		aUser2.setCurrency(uCurrency);
		aUser2.setCommission(new BigDecimal(0.1));
		aUser2.setAgentType(aType2);
		aUser2.setOutstandingAmount(new BigDecimal(0));
		
		
		agentCache.put((long) 103, aUser2);
		agentCache.put((long) 104, aUser1);
	}

	public Map<Long, VoddsBetUser> getUserCache() {
		return userCache;
	}

	public Map<Long, AgentUser> getAgentCache() {
		AgentUser a = new AgentUser();
		a.setId(86);
		a.setParentId(85);
		a.setCredit(new BigDecimal(10000));
		a.setCashBalance(new BigDecimal(0));
		a.setCommission(new BigDecimal(0));
		agentCache.put((long) 86, a);
		return agentCache;
	}
	
	
	
	

}
