package jayeson.vodds.service.cms.database;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.mockito.Mockito;

import jayeson.database.DatabaseManager;
import jayeson.utility.JacksonConfig;
import jayeson.utility.JacksonConfigFormat;
import jayeson.vodds.common.module.VoddsCommonModule;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.util.ConfigUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;

public class FakeDBModule extends AbstractModule {

	@Override
	protected void configure() {
		
		bind(IDBService.class).to(FakeDBService.class);
		bind(FakeUserGenerator.class).toInstance(new FakeUserGenerator());		
		bind(ConfigBean.class).toInstance(JacksonConfig.readConfig("src/test/resources/conf/config.json", "NA", ConfigBean.class, JacksonConfigFormat.JSON));
		bind(ObjectMapper.class).toInstance(new ObjectMapper());
		bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(200));
		bind(ScheduledExecutorService.class).toInstance(Executors.newScheduledThreadPool(200));
		//bind(EventBus.class).asEagerSingleton();
		bind(EventBus.class).asEagerSingleton();
		install(new VoddsCommonModule());
		ConfigUtil configUtil = Mockito.mock(ConfigUtil.class);
		Mockito.when(configUtil.loadConstants()).thenReturn(new HashMap<>());
		bind(ConfigUtil.class).toInstance(configUtil);
		//bind(DatabaseManager.class).toInstance(DatabaseManager.instance());	

	}
	

}
