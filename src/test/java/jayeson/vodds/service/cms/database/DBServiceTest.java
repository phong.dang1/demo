package jayeson.vodds.service.cms.database;

import static org.junit.Assert.*;
import jayeson.database.BetUser;
import jayeson.database.newvodds.VoddsBetUser;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.database.GuiceJUnitRunner.GuiceModules;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

@RunWith(GuiceJUnitRunner.class)
@GuiceModules({FakeDBModule.class})

/***
 * Test the functionality of fake db service if the actions are retrieving expected results
 * 
 * @author Praveen
 *
 */
public class DBServiceTest {

	@Inject IDBService dbService;
	
	@Test
	/***
	 * tests the function of find bet user, and checks if the generated user node has same user id
	 */
	public void testFindBetUser() {
		
		long userId = 110;
		BetUser user = dbService.findBetUser(userId);
		
		assertEquals(110,user.getId());
		
	}

}
