package jayeson.vodds.service.cms.database;

import com.google.inject.Inject;
import jayeson.database.*;
import jayeson.database.newvodds.*;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.vodds.service.cms.data.AgentUserNode;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.data.TransactionWrapper;
import jayeson.vodds.service.cms.data.game.GameTransactionWrapper;
import jayeson.vodds.service.cms.order.settlement.MPLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.OutstandingNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PLNotificationTask;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.transactions.TransactionQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FakeDBService implements IDBService {

	private static final Logger logger = LoggerFactory.getLogger(FakeDBService.class.getName());
	FakeUserGenerator uGenerator;
	
	@Inject
	public FakeDBService(FakeUserGenerator ugen){
	
		uGenerator = ugen;
	}
	
	
	@Override
	public VoddsBetUser findBetUser(long id) {
		return uGenerator.getBetUser(id);	
	}

	@Override
	public List<AgentUser> findAllAgentOfBetUser(long betUserId) {

		return  new ArrayList<AgentUser>(uGenerator.getAgentCache().values());
	}

	@Override
	public AgentUser findAgentUser(long id) {
		return uGenerator.getAgentCache().get(id);
	}

	@Override
	public void persistResults(List transactions) {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			logger.error(" error while writing transactions to db:{}",e);
		}
		
		
		
	}


	@Override
	public Bet findBetById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BettingModel> findUnsettledOrders(int start,int end) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BettingModel> findUnsettledOrdersWithMatchNotStarted(int start,int end) {
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * returns two fake models
	 * 3001 - normal model
	 * modelType - VONormal
	 * modelStatus - STOPPED
	 * bets information -  2 bets
	 * bet1: stake: 100, return amount:150 , multiplication: 1
	 * bet2: stake: 100, return amount:200, ,multiplication: 1
	 * 
	 * 5001 - positional close model
	 * modelType - VOClose
	 * modelStatus - STOPPED
	 */
	@Override
	public BettingModel findStoppedOrderById(long id) {
		
		if(id == 7410){
			BettingModel nModel  = new BettingModel();
			nModel.setId(7410);
			nModel.setModelType("NORMAL");
			nModel.setModelStatus("STOPPED");
			BetUser owner = new BetUser();
			User user = new User();
			user.setId(106);
			user.setUsername("test123");

			owner.setUser(user);
			owner.setId(106);
			
			
			
			nModel.setOwner(owner);
			return nModel;
		}
		
		if(id == 3001){
			BettingModel nModel  = new BettingModel();
			nModel.setId(3001);
			nModel.setModelType("VONormal");
			nModel.setModelStatus("STOPPED");
			
			BetUser owner = new BetUser();
			User user = new User();
			user.setId(106);
			user.setUsername("test123");

			owner.setUser(user);
			owner.setId(106);
			
			
			nModel.setOwner(owner);
			return nModel;
		}
		
		if(id == 5001){
			BettingModel nModel  = new BettingModel();
			nModel.setId(5001);
			nModel.setModelType("VOClose");
			nModel.setModelStatus("STOPPED");
			
			BetUser owner = new BetUser();
			User user = new User();
			user.setId(108);
			user.setUsername("test321");

			owner.setUser(user);
			owner.setId(108);

			
			nModel.setOwner(owner);
			return nModel;
		}
		
		logger.error(" unsupported order id:{}",id);


		return null;
	}


	@Override
	public List<Bet> findBetsByOrderType(long orderId, String oType) {
		List<Bet> betList = new ArrayList<Bet>();
		if(orderId == 3001){
			Bet normBet1 = new Bet();
			normBet1.setId("normBet1");
			normBet1.setStatus(BetStatus.WIN);
			normBet1.setBetStake(new BigDecimal(100));
			normBet1.setMultiplication(new BigDecimal(1));
			normBet1.setReturnAmount(new BigDecimal(150));
			
			Bet normBet2 = new Bet();
			normBet2.setId("normBet2");
			normBet2.setStatus(BetStatus.WIN);
			normBet2.setBetStake(new BigDecimal(100));
			normBet2.setMultiplication(new BigDecimal(1));
			normBet2.setReturnAmount(new BigDecimal(200));
			
			betList.add(normBet2);
			betList.add(normBet1);
			
			return betList;
			
		}
		else if(orderId == 5001){
			
			Bet pClose1 = new Bet();
			pClose1.setId("pClose1");
			pClose1.setStatus(BetStatus.LOSS);
			pClose1.setBetStake(new BigDecimal(100));
			pClose1.setMultiplication(new BigDecimal(1));
			pClose1.setReturnAmount(new BigDecimal(-200));
			
			BettingModel closeModel = new BettingModel();
			closeModel.setId(5001);
			pClose1.setModel(closeModel);
			
			betList.add(pClose1);
			
			return betList;
			
		}
		else if(orderId == 4001){
			
			Bet pOpen1 = new Bet();
			pOpen1.setId("pOpen1");
			pOpen1.setStatus(BetStatus.WIN);
			pOpen1.setBetStake(new BigDecimal(100));
			pOpen1.setMultiplication(new BigDecimal(1));
			pOpen1.setReturnAmount(new BigDecimal(150));
			
			BettingModel openModel = new BettingModel();
			openModel.setId(4001);
			pOpen1.setModel(openModel);
			
			betList.add(pOpen1);
			return betList;

		}
		
		logger.error(" unsupported order id {} for bets by order",orderId);

		return null;
	}


	/**
	 * populate margin transactions of positional opening order
	 */
	@Override
	public List<ModelTransaction> findMarginTransactions(List<Long> modelIds, long userId) {
		List<ModelTransaction> modelTransactions = new ArrayList<ModelTransaction>();
		long modelId = modelIds.get(0);
		if(modelId==4001 || modelId == 5001){
			ModelTransaction resTransaction = new ModelTransaction();
			resTransaction.setCreditAmount(new BigDecimal(-500));
			
			ModelTransaction refTransaction = new ModelTransaction();
			refTransaction.setCreditAmount(new BigDecimal(50));
			
			modelTransactions.add(resTransaction);
			modelTransactions.add(refTransaction);
			

			return modelTransactions;

		}
		logger.error(" unsupported opening order id:{}",modelId);

		return null;
	}
/**
 * model id 4001 corresponding to positional open order for the positional close order of id 5001 
 */

	@Override
	public BettingModel findOpeningOrder(long orderId) {
		if(orderId == 5001){
			BettingModel oModel  = new BettingModel();
			oModel.setId(4001);
			oModel.setModelType("VOOpen");
			oModel.setModelStatus("STOPPED");
			
			return oModel;
		}
		
		logger.error(" unsupported order id:{}",orderId);
		return null;
	}


	@Override
	public ModelTransaction findLatestReturnTransaction(long orderId,long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public BigDecimal getAgentCommission(AgentUserNode aNode,
			BetUserNode bNode, BigDecimal commValue) {
		// TODO Auto-generated method stub
		return commValue;
	}


	@Override
	public BettingModel findClosingOrder(long orderId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Bet> findBetsByOrder(long orderId) {
		List<Bet> betList = new ArrayList<Bet>();
		if(orderId == 3001){
			Bet normBet1 = new Bet();
			normBet1.setId("normBet1");
			normBet1.setStatus(BetStatus.WIN);
			normBet1.setBetStake(new BigDecimal(100));
			normBet1.setMultiplication(new BigDecimal(1));
			normBet1.setReturnAmount(new BigDecimal(150));
			
			Bet normBet2 = new Bet();
			normBet2.setId("normBet2");
			normBet2.setStatus(BetStatus.WIN);
			normBet2.setBetStake(new BigDecimal(100));
			normBet2.setMultiplication(new BigDecimal(1));
			normBet2.setReturnAmount(new BigDecimal(200));
			
			betList.add(normBet2);
			betList.add(normBet1);
			
			return betList;
			
		}
		else if(orderId == 7410){
			Bet normBet1 = new Bet();
			normBet1.setId("normBet1");
			normBet1.setStatus(BetStatus.WIN);
			normBet1.setBetStake(new BigDecimal(100));
			normBet1.setMultiplication(new BigDecimal(1));
			normBet1.setReturnAmount(new BigDecimal(150));
			
			Bet normBet2 = new Bet();
			normBet2.setId("normBet2");
			normBet2.setStatus(BetStatus.WIN);
			normBet2.setBetStake(new BigDecimal(100));
			normBet2.setMultiplication(new BigDecimal(1));
			normBet2.setReturnAmount(new BigDecimal(200));
			
			betList.add(normBet2);
			betList.add(normBet1);
			
			return betList;
		}
		else if(orderId == 5001){
			
			Bet pClose1 = new Bet();
			pClose1.setId("pClose1");
			pClose1.setStatus(BetStatus.LOSS);
			pClose1.setBetStake(new BigDecimal(100));
			pClose1.setMultiplication(new BigDecimal(1));
			pClose1.setReturnAmount(new BigDecimal(-200));
			
			BettingModel closeModel = new BettingModel();
			closeModel.setId(5001);
			pClose1.setModel(closeModel);
			
			betList.add(pClose1);
			
			return betList;
			
		}
		else if(orderId == 4001){
			
			Bet pOpen1 = new Bet();
			pOpen1.setId("pOpen1");
			pOpen1.setStatus(BetStatus.WIN);
			pOpen1.setBetStake(new BigDecimal(100));
			pOpen1.setMultiplication(new BigDecimal(1));
			pOpen1.setReturnAmount(new BigDecimal(150));
			
			BettingModel openModel = new BettingModel();
			openModel.setId(4001);
			pOpen1.setModel(openModel);
			
			betList.add(pOpen1);
			return betList;

		}
		
		logger.error(" unsupported order id {} for bets by order",orderId);

		return null;
	}


	@Override
	public PositionalModel findPositionalModelByCloseId(long closeId) {
		if(closeId == 5001){
			BettingModel oModel  = new BettingModel();
			oModel.setId(4001);
			oModel.setModelType("VOOpen");
			oModel.setModelStatus("STOPPED");
			
			BettingModel cModel  = new BettingModel();
			cModel.setId(5001);
			cModel.setModelType("VOClose");
			cModel.setModelStatus("STOPPED");

			
			PositionalModel pModel = new PositionalModel();
			pModel.setOpenModelId(oModel);
			pModel.setCloseModelId(cModel);
			
			return pModel;
		}		return null;
	}


	@Override
	public BettingModelExtraVodds getModelExtraEntity(long modelId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Bet> findBetListById(List<String> idList) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int findUnsettledOrdersCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int findUnsettledOrdersWithMatchNotStartedCount() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void persistResults(Set<?> transactions) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Service getServiceById(String id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public BigDecimal getConversionRate(String currencyChildUser, String currencyUserParrent) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void persistResults(Object obj) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<DarkPoolBetInfo> findUnsettledDPoolBets() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<DPoolLiquidationMapping> fetchLiquidationMappings(String dBetId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Bet> fetchLiquidatingBets(String dBetId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void saveUserCredit(VoddsBetUser betUser) {
		// TODO Auto-generated method stub
	}


	@Override
	public void saveAgentUserCredit(Long id, BigDecimal credit) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public BigDecimal getPlOfBetUser(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void persistBMResults(Set<BettingModel> transactions, OrderWrapperManager owManger,PLNotificationTask plNotificationTask, MPLNotificationTask task) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistBUResults(Set<VoddsBetUser> setBetUser) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistAUResults(Set<AgentUser> setAgentUser) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<ModelTransaction> findLatestReturnTransactionForOperator(long orderId, long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int findVoidingOrdersCount() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public List<BettingModel> findVoidingOrders(int limitStart, int limitEnd) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<ModelTransaction> findAllTransactionByOrderId(long orderId, ModelTransactionType typeTransaction) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public BettingModel findBettingModelById(long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void updateBMResults(Set<BettingModel> transactions, OrderWrapperManager owManger) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistPMResults(Set<PositionalModel> setPositionModel, MPLNotificationTask task,NoQPersister noQ) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistBMEVResults(Set<BettingModelExtraVodds> setBMEV,OutstandingNotificationTask amtNotificationTask) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistModelTransaction(List<ModelTransaction> transactions,TransactionQueue tQueue) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistTransaction(List<TransactionWrapper> transactions) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void persistDPTransaction(List<DarkPoolBetInfo> transactions) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public ModelTransaction findLastestRefundTransaction(long orderId, long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int checkOrderIsValid(long userId, long orderId) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public ModelTransaction findLatestReturnPTPLTransaction(long orderId, long userId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Sportbook findSportbookById(String id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void persistAgentPtPl(Map<Long, List<BettingModelAgentPtPl>> agentPtPl) {

	}

	@Override
	public void persistAgentPtPl(BettingModelAgentPtPl agentPtPl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void persistBetOfAgentPtPl(Map<String, List<BetAgentPtPl>> betAgentPtPl) {
		
	}

	@Override
	public void persistBetOfAgentPtPl(BetAgentPtPl betAgentPtPl) {

	}


	@Override
	public void saveAgentUserCreditAndPtpl(Long id, BigDecimal credit,BigDecimal ptpl) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public BigDecimal calculateTotalTransactionByActualizedOrder(long openOrderId,long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int findUnsettledWithAcceptedBetCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean checkExistLastRevertTransactionsUnsettledOrder(long modelID) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public List<BettingModel> findUnsettledWithAcceptedBet() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ModelTransaction findLatestSettlementDarkpool(long orderId, long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ModelTransaction findLatestSettlementMissingBet(long orderId, long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Map<String, StakePerMatch> findStakePerMatchByUserId(long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public BetUserSetting findBetUserSettingById(long userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void persistSPMResults(Set<StakePerMatch> setStakePerMatch) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public VoddsBetUser findVoddsBetUser(long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getMatchId(long orderId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public PositionalModel findPositionalModel(long bettingModelId) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isExcludeTradeLimitCheck(long userId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public UserAction findUserAction(String actionName) {
		return null;
	}

	@Override
	public UserGameDetail findGameDetail(long gameDetailId) {
		return null;
	}

	@Override
	public UserGameTransaction findGameTransaction(long id) {
		return null;
	}

	@Override
	public void persistGameTransactions(List<GameTransactionWrapper> transactions) {

	}


	@Override
	public BigDecimal getCachedPlOfBetUser(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
