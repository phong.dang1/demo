/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package jayeson.vodds.service.cms.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (1.0.0-dev)", date = "2022-4-26")
public class ThriftTradeLimit implements org.apache.thrift.TBase<ThriftTradeLimit, ThriftTradeLimit._Fields>, java.io.Serializable, Cloneable, Comparable<ThriftTradeLimit> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ThriftTradeLimit");

  private static final org.apache.thrift.protocol.TField USER_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("userId", org.apache.thrift.protocol.TType.I64, (short)1);
  private static final org.apache.thrift.protocol.TField MAX_BET_NORMAL_FIELD_DESC = new org.apache.thrift.protocol.TField("maxBetNormal", org.apache.thrift.protocol.TType.DOUBLE, (short)2);
  private static final org.apache.thrift.protocol.TField MAX_BET_POSITIONAL_FIELD_DESC = new org.apache.thrift.protocol.TField("maxBetPositional", org.apache.thrift.protocol.TType.DOUBLE, (short)3);
  private static final org.apache.thrift.protocol.TField MAX_BET_PER_MATCH_FIELD_DESC = new org.apache.thrift.protocol.TField("maxBetPerMatch", org.apache.thrift.protocol.TType.DOUBLE, (short)4);
  private static final org.apache.thrift.protocol.TField MAX_LOSS_FIELD_DESC = new org.apache.thrift.protocol.TField("maxLoss", org.apache.thrift.protocol.TType.DOUBLE, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ThriftTradeLimitStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ThriftTradeLimitTupleSchemeFactory());
  }

  public long userId; // required
  public double maxBetNormal; // required
  public double maxBetPositional; // required
  public double maxBetPerMatch; // required
  public double maxLoss; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    USER_ID((short)1, "userId"),
    MAX_BET_NORMAL((short)2, "maxBetNormal"),
    MAX_BET_POSITIONAL((short)3, "maxBetPositional"),
    MAX_BET_PER_MATCH((short)4, "maxBetPerMatch"),
    MAX_LOSS((short)5, "maxLoss");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // USER_ID
          return USER_ID;
        case 2: // MAX_BET_NORMAL
          return MAX_BET_NORMAL;
        case 3: // MAX_BET_POSITIONAL
          return MAX_BET_POSITIONAL;
        case 4: // MAX_BET_PER_MATCH
          return MAX_BET_PER_MATCH;
        case 5: // MAX_LOSS
          return MAX_LOSS;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __USERID_ISSET_ID = 0;
  private static final int __MAXBETNORMAL_ISSET_ID = 1;
  private static final int __MAXBETPOSITIONAL_ISSET_ID = 2;
  private static final int __MAXBETPERMATCH_ISSET_ID = 3;
  private static final int __MAXLOSS_ISSET_ID = 4;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.USER_ID, new org.apache.thrift.meta_data.FieldMetaData("userId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.MAX_BET_NORMAL, new org.apache.thrift.meta_data.FieldMetaData("maxBetNormal", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.MAX_BET_POSITIONAL, new org.apache.thrift.meta_data.FieldMetaData("maxBetPositional", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.MAX_BET_PER_MATCH, new org.apache.thrift.meta_data.FieldMetaData("maxBetPerMatch", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.MAX_LOSS, new org.apache.thrift.meta_data.FieldMetaData("maxLoss", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ThriftTradeLimit.class, metaDataMap);
  }

  public ThriftTradeLimit() {
  }

  public ThriftTradeLimit(
    long userId,
    double maxBetNormal,
    double maxBetPositional,
    double maxBetPerMatch,
    double maxLoss)
  {
    this();
    this.userId = userId;
    setUserIdIsSet(true);
    this.maxBetNormal = maxBetNormal;
    setMaxBetNormalIsSet(true);
    this.maxBetPositional = maxBetPositional;
    setMaxBetPositionalIsSet(true);
    this.maxBetPerMatch = maxBetPerMatch;
    setMaxBetPerMatchIsSet(true);
    this.maxLoss = maxLoss;
    setMaxLossIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ThriftTradeLimit(ThriftTradeLimit other) {
    __isset_bitfield = other.__isset_bitfield;
    this.userId = other.userId;
    this.maxBetNormal = other.maxBetNormal;
    this.maxBetPositional = other.maxBetPositional;
    this.maxBetPerMatch = other.maxBetPerMatch;
    this.maxLoss = other.maxLoss;
  }

  public ThriftTradeLimit deepCopy() {
    return new ThriftTradeLimit(this);
  }

  @Override
  public void clear() {
    setUserIdIsSet(false);
    this.userId = 0;
    setMaxBetNormalIsSet(false);
    this.maxBetNormal = 0.0;
    setMaxBetPositionalIsSet(false);
    this.maxBetPositional = 0.0;
    setMaxBetPerMatchIsSet(false);
    this.maxBetPerMatch = 0.0;
    setMaxLossIsSet(false);
    this.maxLoss = 0.0;
  }

  public long getUserId() {
    return this.userId;
  }

  public ThriftTradeLimit setUserId(long userId) {
    this.userId = userId;
    setUserIdIsSet(true);
    return this;
  }

  public void unsetUserId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __USERID_ISSET_ID);
  }

  /** Returns true if field userId is set (has been assigned a value) and false otherwise */
  public boolean isSetUserId() {
    return EncodingUtils.testBit(__isset_bitfield, __USERID_ISSET_ID);
  }

  public void setUserIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __USERID_ISSET_ID, value);
  }

  public double getMaxBetNormal() {
    return this.maxBetNormal;
  }

  public ThriftTradeLimit setMaxBetNormal(double maxBetNormal) {
    this.maxBetNormal = maxBetNormal;
    setMaxBetNormalIsSet(true);
    return this;
  }

  public void unsetMaxBetNormal() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __MAXBETNORMAL_ISSET_ID);
  }

  /** Returns true if field maxBetNormal is set (has been assigned a value) and false otherwise */
  public boolean isSetMaxBetNormal() {
    return EncodingUtils.testBit(__isset_bitfield, __MAXBETNORMAL_ISSET_ID);
  }

  public void setMaxBetNormalIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __MAXBETNORMAL_ISSET_ID, value);
  }

  public double getMaxBetPositional() {
    return this.maxBetPositional;
  }

  public ThriftTradeLimit setMaxBetPositional(double maxBetPositional) {
    this.maxBetPositional = maxBetPositional;
    setMaxBetPositionalIsSet(true);
    return this;
  }

  public void unsetMaxBetPositional() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __MAXBETPOSITIONAL_ISSET_ID);
  }

  /** Returns true if field maxBetPositional is set (has been assigned a value) and false otherwise */
  public boolean isSetMaxBetPositional() {
    return EncodingUtils.testBit(__isset_bitfield, __MAXBETPOSITIONAL_ISSET_ID);
  }

  public void setMaxBetPositionalIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __MAXBETPOSITIONAL_ISSET_ID, value);
  }

  public double getMaxBetPerMatch() {
    return this.maxBetPerMatch;
  }

  public ThriftTradeLimit setMaxBetPerMatch(double maxBetPerMatch) {
    this.maxBetPerMatch = maxBetPerMatch;
    setMaxBetPerMatchIsSet(true);
    return this;
  }

  public void unsetMaxBetPerMatch() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __MAXBETPERMATCH_ISSET_ID);
  }

  /** Returns true if field maxBetPerMatch is set (has been assigned a value) and false otherwise */
  public boolean isSetMaxBetPerMatch() {
    return EncodingUtils.testBit(__isset_bitfield, __MAXBETPERMATCH_ISSET_ID);
  }

  public void setMaxBetPerMatchIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __MAXBETPERMATCH_ISSET_ID, value);
  }

  public double getMaxLoss() {
    return this.maxLoss;
  }

  public ThriftTradeLimit setMaxLoss(double maxLoss) {
    this.maxLoss = maxLoss;
    setMaxLossIsSet(true);
    return this;
  }

  public void unsetMaxLoss() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __MAXLOSS_ISSET_ID);
  }

  /** Returns true if field maxLoss is set (has been assigned a value) and false otherwise */
  public boolean isSetMaxLoss() {
    return EncodingUtils.testBit(__isset_bitfield, __MAXLOSS_ISSET_ID);
  }

  public void setMaxLossIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __MAXLOSS_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case USER_ID:
      if (value == null) {
        unsetUserId();
      } else {
        setUserId((Long)value);
      }
      break;

    case MAX_BET_NORMAL:
      if (value == null) {
        unsetMaxBetNormal();
      } else {
        setMaxBetNormal((Double)value);
      }
      break;

    case MAX_BET_POSITIONAL:
      if (value == null) {
        unsetMaxBetPositional();
      } else {
        setMaxBetPositional((Double)value);
      }
      break;

    case MAX_BET_PER_MATCH:
      if (value == null) {
        unsetMaxBetPerMatch();
      } else {
        setMaxBetPerMatch((Double)value);
      }
      break;

    case MAX_LOSS:
      if (value == null) {
        unsetMaxLoss();
      } else {
        setMaxLoss((Double)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case USER_ID:
      return Long.valueOf(getUserId());

    case MAX_BET_NORMAL:
      return Double.valueOf(getMaxBetNormal());

    case MAX_BET_POSITIONAL:
      return Double.valueOf(getMaxBetPositional());

    case MAX_BET_PER_MATCH:
      return Double.valueOf(getMaxBetPerMatch());

    case MAX_LOSS:
      return Double.valueOf(getMaxLoss());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case USER_ID:
      return isSetUserId();
    case MAX_BET_NORMAL:
      return isSetMaxBetNormal();
    case MAX_BET_POSITIONAL:
      return isSetMaxBetPositional();
    case MAX_BET_PER_MATCH:
      return isSetMaxBetPerMatch();
    case MAX_LOSS:
      return isSetMaxLoss();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ThriftTradeLimit)
      return this.equals((ThriftTradeLimit)that);
    return false;
  }

  public boolean equals(ThriftTradeLimit that) {
    if (that == null)
      return false;

    boolean this_present_userId = true;
    boolean that_present_userId = true;
    if (this_present_userId || that_present_userId) {
      if (!(this_present_userId && that_present_userId))
        return false;
      if (this.userId != that.userId)
        return false;
    }

    boolean this_present_maxBetNormal = true;
    boolean that_present_maxBetNormal = true;
    if (this_present_maxBetNormal || that_present_maxBetNormal) {
      if (!(this_present_maxBetNormal && that_present_maxBetNormal))
        return false;
      if (this.maxBetNormal != that.maxBetNormal)
        return false;
    }

    boolean this_present_maxBetPositional = true;
    boolean that_present_maxBetPositional = true;
    if (this_present_maxBetPositional || that_present_maxBetPositional) {
      if (!(this_present_maxBetPositional && that_present_maxBetPositional))
        return false;
      if (this.maxBetPositional != that.maxBetPositional)
        return false;
    }

    boolean this_present_maxBetPerMatch = true;
    boolean that_present_maxBetPerMatch = true;
    if (this_present_maxBetPerMatch || that_present_maxBetPerMatch) {
      if (!(this_present_maxBetPerMatch && that_present_maxBetPerMatch))
        return false;
      if (this.maxBetPerMatch != that.maxBetPerMatch)
        return false;
    }

    boolean this_present_maxLoss = true;
    boolean that_present_maxLoss = true;
    if (this_present_maxLoss || that_present_maxLoss) {
      if (!(this_present_maxLoss && that_present_maxLoss))
        return false;
      if (this.maxLoss != that.maxLoss)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_userId = true;
    list.add(present_userId);
    if (present_userId)
      list.add(userId);

    boolean present_maxBetNormal = true;
    list.add(present_maxBetNormal);
    if (present_maxBetNormal)
      list.add(maxBetNormal);

    boolean present_maxBetPositional = true;
    list.add(present_maxBetPositional);
    if (present_maxBetPositional)
      list.add(maxBetPositional);

    boolean present_maxBetPerMatch = true;
    list.add(present_maxBetPerMatch);
    if (present_maxBetPerMatch)
      list.add(maxBetPerMatch);

    boolean present_maxLoss = true;
    list.add(present_maxLoss);
    if (present_maxLoss)
      list.add(maxLoss);

    return list.hashCode();
  }

  @Override
  public int compareTo(ThriftTradeLimit other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetUserId()).compareTo(other.isSetUserId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUserId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.userId, other.userId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetMaxBetNormal()).compareTo(other.isSetMaxBetNormal());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMaxBetNormal()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.maxBetNormal, other.maxBetNormal);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetMaxBetPositional()).compareTo(other.isSetMaxBetPositional());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMaxBetPositional()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.maxBetPositional, other.maxBetPositional);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetMaxBetPerMatch()).compareTo(other.isSetMaxBetPerMatch());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMaxBetPerMatch()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.maxBetPerMatch, other.maxBetPerMatch);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetMaxLoss()).compareTo(other.isSetMaxLoss());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMaxLoss()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.maxLoss, other.maxLoss);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ThriftTradeLimit(");
    boolean first = true;

    sb.append("userId:");
    sb.append(this.userId);
    first = false;
    if (!first) sb.append(", ");
    sb.append("maxBetNormal:");
    sb.append(this.maxBetNormal);
    first = false;
    if (!first) sb.append(", ");
    sb.append("maxBetPositional:");
    sb.append(this.maxBetPositional);
    first = false;
    if (!first) sb.append(", ");
    sb.append("maxBetPerMatch:");
    sb.append(this.maxBetPerMatch);
    first = false;
    if (!first) sb.append(", ");
    sb.append("maxLoss:");
    sb.append(this.maxLoss);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // alas, we cannot check 'userId' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ThriftTradeLimitStandardSchemeFactory implements SchemeFactory {
    public ThriftTradeLimitStandardScheme getScheme() {
      return new ThriftTradeLimitStandardScheme();
    }
  }

  private static class ThriftTradeLimitStandardScheme extends StandardScheme<ThriftTradeLimit> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ThriftTradeLimit struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // USER_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.userId = iprot.readI64();
              struct.setUserIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // MAX_BET_NORMAL
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.maxBetNormal = iprot.readDouble();
              struct.setMaxBetNormalIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // MAX_BET_POSITIONAL
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.maxBetPositional = iprot.readDouble();
              struct.setMaxBetPositionalIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // MAX_BET_PER_MATCH
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.maxBetPerMatch = iprot.readDouble();
              struct.setMaxBetPerMatchIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // MAX_LOSS
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.maxLoss = iprot.readDouble();
              struct.setMaxLossIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetUserId()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'userId' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ThriftTradeLimit struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(USER_ID_FIELD_DESC);
      oprot.writeI64(struct.userId);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(MAX_BET_NORMAL_FIELD_DESC);
      oprot.writeDouble(struct.maxBetNormal);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(MAX_BET_POSITIONAL_FIELD_DESC);
      oprot.writeDouble(struct.maxBetPositional);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(MAX_BET_PER_MATCH_FIELD_DESC);
      oprot.writeDouble(struct.maxBetPerMatch);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(MAX_LOSS_FIELD_DESC);
      oprot.writeDouble(struct.maxLoss);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ThriftTradeLimitTupleSchemeFactory implements SchemeFactory {
    public ThriftTradeLimitTupleScheme getScheme() {
      return new ThriftTradeLimitTupleScheme();
    }
  }

  private static class ThriftTradeLimitTupleScheme extends TupleScheme<ThriftTradeLimit> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ThriftTradeLimit struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeI64(struct.userId);
      BitSet optionals = new BitSet();
      if (struct.isSetMaxBetNormal()) {
        optionals.set(0);
      }
      if (struct.isSetMaxBetPositional()) {
        optionals.set(1);
      }
      if (struct.isSetMaxBetPerMatch()) {
        optionals.set(2);
      }
      if (struct.isSetMaxLoss()) {
        optionals.set(3);
      }
      oprot.writeBitSet(optionals, 4);
      if (struct.isSetMaxBetNormal()) {
        oprot.writeDouble(struct.maxBetNormal);
      }
      if (struct.isSetMaxBetPositional()) {
        oprot.writeDouble(struct.maxBetPositional);
      }
      if (struct.isSetMaxBetPerMatch()) {
        oprot.writeDouble(struct.maxBetPerMatch);
      }
      if (struct.isSetMaxLoss()) {
        oprot.writeDouble(struct.maxLoss);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ThriftTradeLimit struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.userId = iprot.readI64();
      struct.setUserIdIsSet(true);
      BitSet incoming = iprot.readBitSet(4);
      if (incoming.get(0)) {
        struct.maxBetNormal = iprot.readDouble();
        struct.setMaxBetNormalIsSet(true);
      }
      if (incoming.get(1)) {
        struct.maxBetPositional = iprot.readDouble();
        struct.setMaxBetPositionalIsSet(true);
      }
      if (incoming.get(2)) {
        struct.maxBetPerMatch = iprot.readDouble();
        struct.setMaxBetPerMatchIsSet(true);
      }
      if (incoming.get(3)) {
        struct.maxLoss = iprot.readDouble();
        struct.setMaxLossIsSet(true);
      }
    }
  }

}

