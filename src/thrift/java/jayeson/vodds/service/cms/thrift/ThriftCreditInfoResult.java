/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package jayeson.vodds.service.cms.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (1.0.0-dev)", date = "2022-4-26")
public class ThriftCreditInfoResult implements org.apache.thrift.TBase<ThriftCreditInfoResult, ThriftCreditInfoResult._Fields>, java.io.Serializable, Cloneable, Comparable<ThriftCreditInfoResult> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ThriftCreditInfoResult");

  private static final org.apache.thrift.protocol.TField REQ_INFO_FIELD_DESC = new org.apache.thrift.protocol.TField("reqInfo", org.apache.thrift.protocol.TType.STRUCT, (short)1);
  private static final org.apache.thrift.protocol.TField RESULT_CODE_FIELD_DESC = new org.apache.thrift.protocol.TField("resultCode", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField CURRENCY_FIELD_DESC = new org.apache.thrift.protocol.TField("currency", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField CREDIT_FIELD_DESC = new org.apache.thrift.protocol.TField("credit", org.apache.thrift.protocol.TType.DOUBLE, (short)4);
  private static final org.apache.thrift.protocol.TField RESPONSE_JSON_FIELD_DESC = new org.apache.thrift.protocol.TField("responseJson", org.apache.thrift.protocol.TType.STRING, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ThriftCreditInfoResultStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ThriftCreditInfoResultTupleSchemeFactory());
  }

  public ThriftCreditInfo reqInfo; // required
  /**
   * 
   * @see ThriftResultCode
   */
  public ThriftResultCode resultCode; // required
  public String currency; // required
  public double credit; // required
  public String responseJson; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    REQ_INFO((short)1, "reqInfo"),
    /**
     * 
     * @see ThriftResultCode
     */
    RESULT_CODE((short)2, "resultCode"),
    CURRENCY((short)3, "currency"),
    CREDIT((short)4, "credit"),
    RESPONSE_JSON((short)5, "responseJson");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // REQ_INFO
          return REQ_INFO;
        case 2: // RESULT_CODE
          return RESULT_CODE;
        case 3: // CURRENCY
          return CURRENCY;
        case 4: // CREDIT
          return CREDIT;
        case 5: // RESPONSE_JSON
          return RESPONSE_JSON;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __CREDIT_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  private static final _Fields optionals[] = {_Fields.RESPONSE_JSON};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.REQ_INFO, new org.apache.thrift.meta_data.FieldMetaData("reqInfo", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, ThriftCreditInfo.class)));
    tmpMap.put(_Fields.RESULT_CODE, new org.apache.thrift.meta_data.FieldMetaData("resultCode", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.EnumMetaData(org.apache.thrift.protocol.TType.ENUM, ThriftResultCode.class)));
    tmpMap.put(_Fields.CURRENCY, new org.apache.thrift.meta_data.FieldMetaData("currency", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.CREDIT, new org.apache.thrift.meta_data.FieldMetaData("credit", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.RESPONSE_JSON, new org.apache.thrift.meta_data.FieldMetaData("responseJson", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ThriftCreditInfoResult.class, metaDataMap);
  }

  public ThriftCreditInfoResult() {
  }

  public ThriftCreditInfoResult(
    ThriftCreditInfo reqInfo,
    ThriftResultCode resultCode,
    String currency,
    double credit)
  {
    this();
    this.reqInfo = reqInfo;
    this.resultCode = resultCode;
    this.currency = currency;
    this.credit = credit;
    setCreditIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ThriftCreditInfoResult(ThriftCreditInfoResult other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetReqInfo()) {
      this.reqInfo = new ThriftCreditInfo(other.reqInfo);
    }
    if (other.isSetResultCode()) {
      this.resultCode = other.resultCode;
    }
    if (other.isSetCurrency()) {
      this.currency = other.currency;
    }
    this.credit = other.credit;
    if (other.isSetResponseJson()) {
      this.responseJson = other.responseJson;
    }
  }

  public ThriftCreditInfoResult deepCopy() {
    return new ThriftCreditInfoResult(this);
  }

  @Override
  public void clear() {
    this.reqInfo = null;
    this.resultCode = null;
    this.currency = null;
    setCreditIsSet(false);
    this.credit = 0.0;
    this.responseJson = null;
  }

  public ThriftCreditInfo getReqInfo() {
    return this.reqInfo;
  }

  public ThriftCreditInfoResult setReqInfo(ThriftCreditInfo reqInfo) {
    this.reqInfo = reqInfo;
    return this;
  }

  public void unsetReqInfo() {
    this.reqInfo = null;
  }

  /** Returns true if field reqInfo is set (has been assigned a value) and false otherwise */
  public boolean isSetReqInfo() {
    return this.reqInfo != null;
  }

  public void setReqInfoIsSet(boolean value) {
    if (!value) {
      this.reqInfo = null;
    }
  }

  /**
   * 
   * @see ThriftResultCode
   */
  public ThriftResultCode getResultCode() {
    return this.resultCode;
  }

  /**
   * 
   * @see ThriftResultCode
   */
  public ThriftCreditInfoResult setResultCode(ThriftResultCode resultCode) {
    this.resultCode = resultCode;
    return this;
  }

  public void unsetResultCode() {
    this.resultCode = null;
  }

  /** Returns true if field resultCode is set (has been assigned a value) and false otherwise */
  public boolean isSetResultCode() {
    return this.resultCode != null;
  }

  public void setResultCodeIsSet(boolean value) {
    if (!value) {
      this.resultCode = null;
    }
  }

  public String getCurrency() {
    return this.currency;
  }

  public ThriftCreditInfoResult setCurrency(String currency) {
    this.currency = currency;
    return this;
  }

  public void unsetCurrency() {
    this.currency = null;
  }

  /** Returns true if field currency is set (has been assigned a value) and false otherwise */
  public boolean isSetCurrency() {
    return this.currency != null;
  }

  public void setCurrencyIsSet(boolean value) {
    if (!value) {
      this.currency = null;
    }
  }

  public double getCredit() {
    return this.credit;
  }

  public ThriftCreditInfoResult setCredit(double credit) {
    this.credit = credit;
    setCreditIsSet(true);
    return this;
  }

  public void unsetCredit() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __CREDIT_ISSET_ID);
  }

  /** Returns true if field credit is set (has been assigned a value) and false otherwise */
  public boolean isSetCredit() {
    return EncodingUtils.testBit(__isset_bitfield, __CREDIT_ISSET_ID);
  }

  public void setCreditIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __CREDIT_ISSET_ID, value);
  }

  public String getResponseJson() {
    return this.responseJson;
  }

  public ThriftCreditInfoResult setResponseJson(String responseJson) {
    this.responseJson = responseJson;
    return this;
  }

  public void unsetResponseJson() {
    this.responseJson = null;
  }

  /** Returns true if field responseJson is set (has been assigned a value) and false otherwise */
  public boolean isSetResponseJson() {
    return this.responseJson != null;
  }

  public void setResponseJsonIsSet(boolean value) {
    if (!value) {
      this.responseJson = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case REQ_INFO:
      if (value == null) {
        unsetReqInfo();
      } else {
        setReqInfo((ThriftCreditInfo)value);
      }
      break;

    case RESULT_CODE:
      if (value == null) {
        unsetResultCode();
      } else {
        setResultCode((ThriftResultCode)value);
      }
      break;

    case CURRENCY:
      if (value == null) {
        unsetCurrency();
      } else {
        setCurrency((String)value);
      }
      break;

    case CREDIT:
      if (value == null) {
        unsetCredit();
      } else {
        setCredit((Double)value);
      }
      break;

    case RESPONSE_JSON:
      if (value == null) {
        unsetResponseJson();
      } else {
        setResponseJson((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case REQ_INFO:
      return getReqInfo();

    case RESULT_CODE:
      return getResultCode();

    case CURRENCY:
      return getCurrency();

    case CREDIT:
      return Double.valueOf(getCredit());

    case RESPONSE_JSON:
      return getResponseJson();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case REQ_INFO:
      return isSetReqInfo();
    case RESULT_CODE:
      return isSetResultCode();
    case CURRENCY:
      return isSetCurrency();
    case CREDIT:
      return isSetCredit();
    case RESPONSE_JSON:
      return isSetResponseJson();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ThriftCreditInfoResult)
      return this.equals((ThriftCreditInfoResult)that);
    return false;
  }

  public boolean equals(ThriftCreditInfoResult that) {
    if (that == null)
      return false;

    boolean this_present_reqInfo = true && this.isSetReqInfo();
    boolean that_present_reqInfo = true && that.isSetReqInfo();
    if (this_present_reqInfo || that_present_reqInfo) {
      if (!(this_present_reqInfo && that_present_reqInfo))
        return false;
      if (!this.reqInfo.equals(that.reqInfo))
        return false;
    }

    boolean this_present_resultCode = true && this.isSetResultCode();
    boolean that_present_resultCode = true && that.isSetResultCode();
    if (this_present_resultCode || that_present_resultCode) {
      if (!(this_present_resultCode && that_present_resultCode))
        return false;
      if (!this.resultCode.equals(that.resultCode))
        return false;
    }

    boolean this_present_currency = true && this.isSetCurrency();
    boolean that_present_currency = true && that.isSetCurrency();
    if (this_present_currency || that_present_currency) {
      if (!(this_present_currency && that_present_currency))
        return false;
      if (!this.currency.equals(that.currency))
        return false;
    }

    boolean this_present_credit = true;
    boolean that_present_credit = true;
    if (this_present_credit || that_present_credit) {
      if (!(this_present_credit && that_present_credit))
        return false;
      if (this.credit != that.credit)
        return false;
    }

    boolean this_present_responseJson = true && this.isSetResponseJson();
    boolean that_present_responseJson = true && that.isSetResponseJson();
    if (this_present_responseJson || that_present_responseJson) {
      if (!(this_present_responseJson && that_present_responseJson))
        return false;
      if (!this.responseJson.equals(that.responseJson))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_reqInfo = true && (isSetReqInfo());
    list.add(present_reqInfo);
    if (present_reqInfo)
      list.add(reqInfo);

    boolean present_resultCode = true && (isSetResultCode());
    list.add(present_resultCode);
    if (present_resultCode)
      list.add(resultCode.getValue());

    boolean present_currency = true && (isSetCurrency());
    list.add(present_currency);
    if (present_currency)
      list.add(currency);

    boolean present_credit = true;
    list.add(present_credit);
    if (present_credit)
      list.add(credit);

    boolean present_responseJson = true && (isSetResponseJson());
    list.add(present_responseJson);
    if (present_responseJson)
      list.add(responseJson);

    return list.hashCode();
  }

  @Override
  public int compareTo(ThriftCreditInfoResult other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetReqInfo()).compareTo(other.isSetReqInfo());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetReqInfo()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.reqInfo, other.reqInfo);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetResultCode()).compareTo(other.isSetResultCode());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetResultCode()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.resultCode, other.resultCode);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCurrency()).compareTo(other.isSetCurrency());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCurrency()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.currency, other.currency);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCredit()).compareTo(other.isSetCredit());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCredit()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.credit, other.credit);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetResponseJson()).compareTo(other.isSetResponseJson());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetResponseJson()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.responseJson, other.responseJson);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ThriftCreditInfoResult(");
    boolean first = true;

    sb.append("reqInfo:");
    if (this.reqInfo == null) {
      sb.append("null");
    } else {
      sb.append(this.reqInfo);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("resultCode:");
    if (this.resultCode == null) {
      sb.append("null");
    } else {
      sb.append(this.resultCode);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("currency:");
    if (this.currency == null) {
      sb.append("null");
    } else {
      sb.append(this.currency);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("credit:");
    sb.append(this.credit);
    first = false;
    if (isSetResponseJson()) {
      if (!first) sb.append(", ");
      sb.append("responseJson:");
      if (this.responseJson == null) {
        sb.append("null");
      } else {
        sb.append(this.responseJson);
      }
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (reqInfo == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'reqInfo' was not present! Struct: " + toString());
    }
    if (resultCode == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'resultCode' was not present! Struct: " + toString());
    }
    if (currency == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'currency' was not present! Struct: " + toString());
    }
    // alas, we cannot check 'credit' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
    if (reqInfo != null) {
      reqInfo.validate();
    }
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ThriftCreditInfoResultStandardSchemeFactory implements SchemeFactory {
    public ThriftCreditInfoResultStandardScheme getScheme() {
      return new ThriftCreditInfoResultStandardScheme();
    }
  }

  private static class ThriftCreditInfoResultStandardScheme extends StandardScheme<ThriftCreditInfoResult> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ThriftCreditInfoResult struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // REQ_INFO
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.reqInfo = new ThriftCreditInfo();
              struct.reqInfo.read(iprot);
              struct.setReqInfoIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // RESULT_CODE
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.resultCode = jayeson.vodds.service.cms.thrift.ThriftResultCode.findByValue(iprot.readI32());
              struct.setResultCodeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // CURRENCY
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.currency = iprot.readString();
              struct.setCurrencyIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // CREDIT
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.credit = iprot.readDouble();
              struct.setCreditIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // RESPONSE_JSON
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.responseJson = iprot.readString();
              struct.setResponseJsonIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetCredit()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'credit' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ThriftCreditInfoResult struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.reqInfo != null) {
        oprot.writeFieldBegin(REQ_INFO_FIELD_DESC);
        struct.reqInfo.write(oprot);
        oprot.writeFieldEnd();
      }
      if (struct.resultCode != null) {
        oprot.writeFieldBegin(RESULT_CODE_FIELD_DESC);
        oprot.writeI32(struct.resultCode.getValue());
        oprot.writeFieldEnd();
      }
      if (struct.currency != null) {
        oprot.writeFieldBegin(CURRENCY_FIELD_DESC);
        oprot.writeString(struct.currency);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(CREDIT_FIELD_DESC);
      oprot.writeDouble(struct.credit);
      oprot.writeFieldEnd();
      if (struct.responseJson != null) {
        if (struct.isSetResponseJson()) {
          oprot.writeFieldBegin(RESPONSE_JSON_FIELD_DESC);
          oprot.writeString(struct.responseJson);
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ThriftCreditInfoResultTupleSchemeFactory implements SchemeFactory {
    public ThriftCreditInfoResultTupleScheme getScheme() {
      return new ThriftCreditInfoResultTupleScheme();
    }
  }

  private static class ThriftCreditInfoResultTupleScheme extends TupleScheme<ThriftCreditInfoResult> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ThriftCreditInfoResult struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      struct.reqInfo.write(oprot);
      oprot.writeI32(struct.resultCode.getValue());
      oprot.writeString(struct.currency);
      oprot.writeDouble(struct.credit);
      BitSet optionals = new BitSet();
      if (struct.isSetResponseJson()) {
        optionals.set(0);
      }
      oprot.writeBitSet(optionals, 1);
      if (struct.isSetResponseJson()) {
        oprot.writeString(struct.responseJson);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ThriftCreditInfoResult struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.reqInfo = new ThriftCreditInfo();
      struct.reqInfo.read(iprot);
      struct.setReqInfoIsSet(true);
      struct.resultCode = jayeson.vodds.service.cms.thrift.ThriftResultCode.findByValue(iprot.readI32());
      struct.setResultCodeIsSet(true);
      struct.currency = iprot.readString();
      struct.setCurrencyIsSet(true);
      struct.credit = iprot.readDouble();
      struct.setCreditIsSet(true);
      BitSet incoming = iprot.readBitSet(1);
      if (incoming.get(0)) {
        struct.responseJson = iprot.readString();
        struct.setResponseJsonIsSet(true);
      }
    }
  }

}

