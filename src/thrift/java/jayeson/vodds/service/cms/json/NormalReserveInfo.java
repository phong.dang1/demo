package jayeson.vodds.service.cms.json;

/**
 * JSON mapping class for the information passed in as string parameter in the normal reserve credit request
 * @author Praveen
 *
 */
public class NormalReserveInfo {
	
	// desired credit that should be reserved
	public double creditAmount;
	
	// the model id for which the request is made
	public long modelId;
	
	// should the cms reserve all available credit of the user if the required credit is 
	// greater than the credit of user
	public boolean useAllCredit=false;
	
	// remark to be recorded in the transactions table after succesfully processing this request 
	public String remark="reserve request";
	

}
