package jayeson.vodds.service.cms.json;

/**
 * JSON mapping class for the information passed in as string parameter in the Positional open order reserve credit request
 * PO Order require same information as Normal Order and additional leverage ratio value
 * @author Praveen
 *
 */

public class LeverageRefundInfo extends NormalRefundInfo {

	// leverage ratio of the match which is used in calculating the margin of positional order 
	public double leverageRatio;
	
}
