package jayeson.vodds.service.cms.json;

public class LeverageRefundInfoCloseOrder {
	
	//confirmed stake of open order
	public double confirmedStakeOpenOrder;
	
	// desired credit that should be refunded
    public double refundAmount;

    public double mpl;
    
    // the model id for which the request is made
    public long modelId;

    // remark to be recorded in the transactions table after succesfully processing this request 
    public String remark="refund request";
}
