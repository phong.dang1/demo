package jayeson.vodds.service.cms.json;

/**
 * Information that need to be included in the JSON portion of thrift reservation result
 * 
 * @author Praveen
 *
 */
public class NormalReserveResult {
	
	// amount of credit that is succesfully reserved 
	public double sCreditAmount;
	// amount of commission deducted
	public double sCommission;
	
	public String message;
	
	public NormalReserveResult(){
		super();
	}

	public NormalReserveResult(double credit, double commission,String message) {
		super();
		this.sCreditAmount = credit;
		this.message = message;
		this.sCommission= commission;
	}
	
	

}
