package jayeson.vodds.service.cms.json;

/**
 * JSON mapping class for the information passed in as string parameter in the Positional open order reserve credit request.
 * PO Order require same information as Normal Order and additional leverage ratio value. The margin topup requests are identified
 * by a boolean flag. Margin top-up requests do not require to handle commissions.
 *  
 * @author Praveen
 *
 */
public class LeverageReserveInfo extends NormalReserveInfo{
	
	// leverage ratio of the match which is used in calculating the margin of positional order 
	public double leverageRatio;
	
	// to identify whether or not the request is for margin topup
	public boolean isMarginTopup;
	
	public double mpl;
	
	public double confirmedStakeOpenOrder;
	
	
}
