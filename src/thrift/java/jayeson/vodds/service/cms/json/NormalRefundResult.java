package jayeson.vodds.service.cms.json;

/***
 * mapping for the result of normal refund action
 * @author Praveen
 *
 */

public class NormalRefundResult {
	

	public double refundedCredit;
	public double refundedCommission;
	public String message;
	
	public NormalRefundResult(){
		super();
	}

	public NormalRefundResult(double refundedCredit, double commission, String message) {
		super();
		this.refundedCredit = refundedCredit;
		this.message = message;
		this.refundedCommission = commission;
	}
	
 

}
