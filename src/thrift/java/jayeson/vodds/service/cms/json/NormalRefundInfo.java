package jayeson.vodds.service.cms.json;

/***
 *  JSON mapping class for the information passed in as string parameter in the normal refund credit request
 *  
 * @author Praveen
 *
 */
public class NormalRefundInfo {
	
	// desired credit that should be refunded
	public double refundAmount;
	
	// the model id for which the request is made
	public long modelId;
	
	// stake for rejected bet
	public double rejectedStake; 

	// remark to be recorded in the transactions table after succesfully processing this request 
	public String remark="refund request";
}
