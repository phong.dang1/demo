namespace java jayeson.vodds.service.cms.thrift

// this file will list out all the thrift datastructures and services offered by cms

enum ThriftOrderType {

	NORMAL =0,
	POSITIONAL_OPEN =1,
	POSITIONAL_CLOSE =2
 
}

enum ThriftTransactionType {

	RESERVE = 1,
	REFUND = 2,
	ADJUST = 3,
	RETURN = 4

}

enum ThriftResultCode {
	SUCCESS =1,
	SUCCESS_PARTIAL=2,
	INSUFFICIENT_FUNDS=3,
	FAILED =4,
	FAILED_TRADE_LIMIT = 5,
	OUTSTANDING_AMOUNT_NEGATIVE = 6
}

struct ThriftCreditRequestInfo {

	1: required ThriftOrderType oType;
	2: required ThriftTransactionType tType;
	3: required string serviceId;
	4: required i64 userId;
	5: required string matchId;
	6: required string sportType;
	7: required string orderStatus;
	8: string executionStage;
	9: required i64 startTime;
	10: double originalStake
	//the details of the request are embedded inside a json, mapped later to java struct by application
	11: required string requestJson
}

struct ThriftGameCreditRequestInfo {
	1: required ThriftTransactionType tType;
	2: required i64 userId;
	3: required double amount;
	4: required i64 gameDetailId;
}

struct ThriftCreditInfo {
	1: required i64 userId;
}

struct ThriftActualizedInfo{
	1: required string serviceId;
	2: required i64 userIds;
	3: required i64 openOrderId;
	4: required i64 normalOrderId;
	5: required double refundMargin;
	6: required double reserveStake;
	7: required string matchId;
	8: required string sportType;
	9: required string orderStatus;
	10: required i64 startTime;
	11: string executionStage
}

struct ThriftCreditRequestResult {

	// request corresponding to the result 
	1: required ThriftCreditRequestInfo reqInfo;
	2: required ThriftResultCode resultCode;
	//optional json string to provide the details such as reserved amount, failure reasons, commissions deducted 
	3: optional string responseJson;

} 

struct ThriftGameCreditRequestResult {
	// request corresponding to the result
	1: required ThriftGameCreditRequestInfo reqInfo;
	2: required ThriftResultCode resultCode;
	3: required string currency;
	4: required double credit;
	5: required string transactionId;
	//optional json string to provide the details such as failure reasons
	6: optional string responseJson;
}

struct ThriftCreditInfoResult {
	// request corresponding to the result
	1: required ThriftCreditInfo reqInfo;
	2: required ThriftResultCode resultCode;
	3: required string currency;
	4: required double credit;
	//optional json string to provide the details such as failure reasons
	5: optional string responseJson;
}

struct ThriftUserUpdate {

	1: optional list<ThriftCreditUpdate> cUpdates;
	2: optional list<ThriftMplUpdate> mUpdates;

}

struct ThriftCreditUpdate {
	1: required string userName;
	2: optional double credit;
	3: optional double profitLoss;
	4: optional double outStanding;
}

struct ThriftMplUpdate {
	1: required i64 closeId;
	2: required double mplValue;
	3: required string userName;
	4: required i64 pId;
}

struct ThriftExecuteTransactionResult{
	1: optional i64 status;
	2: optional string message;
}

struct ThriftOutstandingPlCache{
	1: optional i64 userId;
	2: optional double profitLoss;
	3: optional double outStanding;
}
struct ThriftTradeLimit{
	1: required i64 userId;
	2: double maxBetNormal;
	3: double maxBetPositional;
	4: double maxBetPerMatch;
	5: double maxLoss;
}

struct ThriftUpdateTradeLimit{
       	1: required i64 userId;
       	2: required i64 orderId;
       	3: string matchId;
       	4: double amount;
}

struct ThriftUpdateCreditForUser {
    1:string serviceId;
    2: i64 userIds;
    3: i64 initiating_user;
    4: double credit;
    5: string remark;
    6: string ipAddress;
    7: i64 aliasId;
    8: bool isAliasActivity;
    9: double totalTransaction;
    10: double totalMargin;
}

struct ThriftTransferPtPlForUser {
    1:string serviceId;
    2: i64 userIds;
    3: double credit;
    4: string remark;
    5: string ipAddress;
    6: i64 aliasId;
    7: bool isAliasActivity;
}

service CMS {

	// a single thrift function is sufficient and the logic on how to implement different transaction and order types will be dealt at the application logic
	ThriftCreditRequestResult processCreditRequest(1: ThriftCreditRequestInfo reqInfo)
	
	// to set the required user id list for each order executor service
	void setUserIdforUpdate(1: string serviceId, 2: list<i64> userIds)
	
	// persistent thrift method to update the oe service for any of the user updates
	ThriftUserUpdate observeUserUpdate(1:string serviceId, 2: list<i64> userIds)
	
	//Update credit of user 
	ThriftExecuteTransactionResult updateCreditForUser(1: ThriftUpdateCreditForUser updateInfo)
	
	//Update manual credit for user
	ThriftExecuteTransactionResult executeTransaction(1:string type, 2: i64 userId, 3: i64 modelId, 4: double credit, 5: string remark)
	
	//Transfer Pt P/L of user
	ThriftExecuteTransactionResult transferPtPLForUser(1: ThriftTransferPtPlForUser transferInfo)
	
	//actualized Order 
	ThriftExecuteTransactionResult actualizedOrder(1:ThriftActualizedInfo actualizedInfo)
	
	ThriftOutstandingPlCache getOutstandingPlCache(1: i64 userId)
	
	//clear trade limit cache of user
	ThriftExecuteTransactionResult clearTradeLimitCache(1: string userName)
	
	//clear trade limit cache by matchId
	ThriftExecuteTransactionResult clearTradeLimitCacheByMatchId(1: list<string> matchId)

    // update trade limit cache of user
    ThriftExecuteTransactionResult updateTradeLimitCache(1: ThriftUpdateTradeLimit updateInfo)

    // process game credit request
    ThriftGameCreditRequestResult processGameCreditRequest(1: ThriftGameCreditRequestInfo reqInfo)
    ThriftCreditInfoResult getCreditInfo(1: ThriftCreditInfo reqInfo)
}



