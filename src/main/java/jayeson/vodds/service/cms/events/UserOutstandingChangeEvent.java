package jayeson.vodds.service.cms.events;

public class UserOutstandingChangeEvent implements CMSEvent{
	String userName;
	double outstandingValue;
	long userId;
	
	public UserOutstandingChangeEvent(String userName,double outstandingValue,long userId){
		this.userName = userName;
		this.outstandingValue = outstandingValue;
		this.userId = userId;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public double getOutstandingValue() {
		return outstandingValue;
	}

	public void setOutstandingValue(double outstandingValue) {
		this.outstandingValue = outstandingValue;
	}

	@Override
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
}
