package jayeson.vodds.service.cms.events;

import com.google.common.eventbus.Subscribe;

/***
 * Event class to notify that the positional order is settled and the MPL value of the user
 * to be returned back
 * 
 * @author Praveen
 *
 */
public class POSettledEvent implements CMSEvent{
	
	String userName;
	long closeId;
	double mplValue;
	long userId;
	long pId;
	
	public POSettledEvent(String userName, long pId, long closeId, double mplValue, long userId) {
		super();
		this.userName = userName;
		this.closeId = closeId;
		this.mplValue = mplValue;
		this.userId = userId;
		this.pId = pId;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	public long getCloseId() {
		return closeId;
	}

	public double getMplValue() {
		return mplValue;
	}

	@Override
	public long getUserId() {
		return userId;
	}

	public long getpId() {
		return pId;
	}

	public void setpId(long pId) {
		this.pId = pId;
	}
	
	
	
	

}
