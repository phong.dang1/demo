package jayeson.vodds.service.cms.events;

public class UserPlChangeEvent implements CMSEvent{
	String userName;
	double plValue;
	long userId;
	
	public UserPlChangeEvent(String user,double pl,long id){
		userName = user;
		plValue = pl;
		userId = id;
	}
	
	@Override
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public double getPl() {
		return plValue;
	}
	public void setPl(double pl) {
		this.plValue = pl;
	}
	@Override
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
}
