package jayeson.vodds.service.cms.events;

/***
 * Event class to notify the change in user credit
 * @author Praveen
 *
 */
public class UserCreditChangeEvent implements CMSEvent{
	
	String userName;
	double creditValue;
	long userId;
	
	public UserCreditChangeEvent(String user, double credit, long id){
		
		userName = user;
		creditValue = credit;
		userId = id;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	public double getCreditValue() {
		return creditValue;
	}

	@Override
	public long getUserId() {
		return userId;
	}
	
	

}
