package jayeson.vodds.service.cms.events;

public interface CMSEvent {
	public long getUserId() ;
	public String getUserName(); 
}
