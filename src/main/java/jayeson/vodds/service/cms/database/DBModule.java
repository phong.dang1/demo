package jayeson.vodds.service.cms.database;

import jayeson.database.DatabaseManager;
import jayeson.vodds.service.cms.order.settlement.PLNotificationTask;

import com.google.inject.AbstractModule;

public class DBModule extends AbstractModule {

	@Override
	protected void configure() {
		
		bind(IDBService.class).to(DBService.class);
		bind(DatabaseManager.class).toInstance(DatabaseManager.instance());	
	}
	

}
