package jayeson.vodds.service.cms.database;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import jayeson.database.*;
import jayeson.database.newvodds.*;
import jayeson.lib.betting.datastructure.BetStatus;
import jayeson.vodds.orders.normal.NormalOrderStatus;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.data.*;
import jayeson.vodds.service.cms.data.game.GameTransactionWrapper;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.order.settlement.MPLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.OutstandingNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PLNotificationTask;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.transactions.TransactionQueue;

import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/***
 * Provides utility methods  to interact with the underlying database such as
 * 
 * find bet user node
 * find list of bets related to an order
 * insert order transactions
 * etc..
 * 
 * @author Praveen
 *
 */

@Singleton
public class DBService implements IDBService {
	static Logger logger = LoggerFactory.getLogger("persist.data.IDBService");

	DatabaseManager dbMgr;
	ConfigBean bean;
	GeneralUtility genUtil;
	
	@Inject
	public DBService(DatabaseManager dbm, ConfigBean cBean, GeneralUtility genUtil){
		dbMgr = dbm;
		bean = cBean;
		this.genUtil = genUtil;
	}
	
	@Override
	public VoddsBetUser findVoddsBetUser(long id){
		EntityManager em = null; 
		try {
			// May throw connection exception
			em = dbMgr.getEM();
			
			VoddsBetUser betuser = VoddsBetUser.findByID(em, id);
			return betuser;
		} catch(Exception ex) {
			logger.error("failed to find user {} exception caught {}",id,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	}
	
	@Override
	public BetUser findBetUser(long id) {
		EntityManager em = null; 
		try {
			// May throw connection exception
			em = dbMgr.getEM();
			
			BetUser betuser = BetUser.findByID(em, id);
			return betuser;
		} catch(Exception ex) {
			logger.error("failed to find user {} exception caught {}",id,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<AgentUser> findAllAgentOfBetUser(long betUserId){
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			UserNestedSet node = UserNestedSet.findByID(em, betUserId);
			TypedQuery<AgentUser> query = (TypedQuery<AgentUser>) em.createNativeQuery("SELECT agent_users.* FROM agent_users "
					+ "JOIN ( "
					+ "SELECT users_nested_set.id AS id FROM users_nested_set "
					+ "WHERE (users_nested_set.left_id < :leftId) "
					+ "AND (users_nested_set.right_id > :rightId) "
					+ "AND users_nested_set.user_type = :type "
					+ ") AS temp1 "
					+ "ON agent_users.id = temp1.id "
					+ "ORDER BY agent_users.id DESC ", AgentUser.class);
			query.setParameter("leftId", node.getLeftId());
			query.setParameter("rightId", node.getRightId());
			query.setParameter("type", (byte)2);
			return query.getResultList();
		} catch(Exception ex){
			logger.error("failed to find agent user for user {} exception caught {}",betUserId,ex);
			return Collections.<AgentUser> emptyList();
		} finally {
			if(em != null) em.close();
		}
	}
	
	@Override
	public AgentUser findAgentUser(long id){
		EntityManager em = null; 
		try {
			// May throw connection exception
			em = dbMgr.getEM();
			
			AgentUser aUser =  AgentUser.findByID(em, id);
			return aUser;
		} catch(Exception ex) {
			logger.error("failed to find agent user {} exception caught {}",id,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	}

	@Override
	public void persistResults(List transactions) {
		EntityManager em = null;
		try{
			
			em = dbMgr.getEM();
			if(transactions.size() >0){				
				for(Object transaction:transactions){
					try{
						EntityTransaction tx = em.getTransaction();
						tx.begin();
						em.merge(transaction);			
						tx.commit();
						
					}
					catch(Exception ex){
						logger.error("error while persisting transaction {}",ex);

					}

				}
				
			}
			
		}
		catch(Exception ex){
			logger.error("error while processing transactions {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
		
	}
	
	@Override
	public void persistResults(Set<?> transactions) {
		EntityManager em = null;
		try{
			
			em = dbMgr.getEM();
			if(transactions.size() >0){
				Iterator <?> it = transactions.iterator();
				EntityTransaction tx = em.getTransaction();
				
				try {
					tx.begin();
					
					while (it.hasNext()) {
						Object trans = it.next();
						it.remove();
						em.merge(trans);			
					}
					
					tx.commit();
					
				} catch(Exception ex){
					logger.error("error while persisting transaction {}",ex);
				} finally {
					if (tx != null && tx.isActive()) tx.rollback();
				}
			}
			
		}
		catch(Exception ex){
			logger.error("error while processing transactions",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}
	
	@Override
	public void persistBMResults(Set<BettingModel> transactions,OrderWrapperManager owManger,PLNotificationTask plNotificationTask
			, MPLNotificationTask mplNotificationTask) {
		EntityManager em = null;
		EntityTransaction tx = null;
		try{
			
			em = dbMgr.getEM();
			if(transactions.size() >0){
				Iterator <BettingModel> it = transactions.iterator();
				    BettingModel trans = null;
				    tx = em.getTransaction();
					while (it.hasNext()) {
						try{
							tx.begin();
							trans = it.next();
							
							String sqlUpdate = " UPDATE betting_models SET model_status = ?1 , is_modified = ?2 WHERE id = ?3";	
							Query query = em.createNativeQuery(sqlUpdate);
			    		    query.setParameter(1, trans.getModelStatus());
			    		    query.setParameter(2, trans.IsModified());
			    		    query.setParameter(3, trans.getId());
			    		    query.executeUpdate();
							
							tx.commit();
							logger.info("{}_SNO, status = {} , modified = {} complete",
									trans.getId(),trans.getModelStatus(),trans.IsModified());
							//put user has order change to Set
							plNotificationTask.saveUserHavePlChange(trans.getOwner());
							//owManger.removeOrderFromCache(trans.getId());
						}catch(Exception ex){
							//owManger.removeOrderFromCache(trans.getId());
							logger.error("update orderId = {} ,status = {} , modifiled = {} failded with exception: {}",
								 	trans.getId(),trans.getModelStatus(),trans.IsModified(),ex);
							continue;
						}
						 finally {
							if (tx != null && tx.isActive()) tx.rollback();
						}
					}
			}
			
		}
		catch(Exception ex){
			logger.error("error while persisting betting model {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public Bet findBetById(String id) {
		EntityManager em = null; 
		try {
			// May throw connection exception
			em = dbMgr.getEM();
			
			Bet dBet =  Bet.findByID(em, id);
			return dBet;
		} catch(Exception ex) {
			logger.error("error while finding bet by id {} exception caught {}",id,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	}
	@Override
	public BettingModel findOpeningOrder(long orderId){
		
		EntityManager em = null;
		try{
			em = dbMgr.getEM();

			Query oQuery = em.createQuery("Select m From PositionalModel m where m.closeModelId.id = ?1", PositionalModel.class);
			oQuery.setParameter(1, orderId);
			PositionalModel oModel = (PositionalModel) oQuery.getSingleResult();
			return oModel.getOpenModelId();
		} catch(Exception ex) {
			logger.error("error while finding opening order {} exception caught {}",orderId,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}		
		
	}

	@Override
	public BettingModel findClosingOrder(long orderId){
		
		EntityManager em = null;
		try{
			em = dbMgr.getEM();

			Query oQuery = em.createQuery("Select m From PositionalModel m where m.openModelId.id = ?1", PositionalModel.class);
			oQuery.setParameter(1, orderId);
			PositionalModel oModel = (PositionalModel) oQuery.getSingleResult();
			return oModel.getCloseModelId();
			
		} catch(Exception ex) {
			logger.error("error while finding closing order {} exception caught {}",orderId,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}		
		
	}
	
	@Override
	public PositionalModel findPositionalModelByCloseId(long closeId){
		EntityManager em = null;
		try{
			
			em = dbMgr.getEM();
			Query oQuery = em.createQuery("Select m From PositionalModel m where m.closeModelId.id = ?1", PositionalModel.class);
			oQuery.setParameter(1, closeId);
			PositionalModel oModel = (PositionalModel) oQuery.getSingleResult();
			return oModel;
			
		}
		catch(Exception ex){
			logger.error("error while finding positional model for close id {} exception caught {}",closeId,ex);
			return null;
		}
		finally{
			if(em != null) em.close();
		}

	}
	
	@Override
	public PositionalModel findPositionalModel(long bettingModelId) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query oQuery = em.createQuery("Select m From PositionalModel m where m.closeModelId.id = ?1 or m.openModelId.id = ?2", PositionalModel.class);
			oQuery.setParameter(1, bettingModelId);
			oQuery.setParameter(2, bettingModelId);
			PositionalModel oModel = (PositionalModel) oQuery.getSingleResult();
			return oModel;
		}
		catch(Exception ex){
			logger.error("error while finding positional model for model id {} exception caught {}", bettingModelId,ex);
			return null;
		}
		finally{
			if(em != null) em.close();
		}
	}
	
	@Override
	public List<ModelTransaction> findMarginTransactions(List<Long> modelIds, long userId){

		EntityManager em = null;
		List<ModelTransaction> tList = new ArrayList<ModelTransaction>();

		// check modelId list is empty, return empty transaction
		if(modelIds == null || modelIds.isEmpty()) {
			return tList;
		}

		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId IN (?1) and (t.type=?2 or t.type=?3) and t.user.id=?4 ", ModelTransaction.class);
			query.setParameter(1, modelIds);
			query.setParameter(2, ModelTransactionType.RESERVE);
			query.setParameter(3, ModelTransactionType.REFUND);
			query.setParameter(4, userId);

			tList.addAll(query.getResultList());
			return tList;
			
		} catch(Exception ex) {
			logger.error("error while finding margin transaction by {} exception caught {}", modelIds, ex);
			return tList;
		} finally {
			if(em != null) em.close();
		}		
				
	}
	
	

	@Override
	public List<Bet> findBetsByOrderType(long orderId, String oType) {

		EntityManager em = null; 
		List<Bet> betList = new ArrayList<Bet>();

		try {
			// May throw connection exception
			em = dbMgr.getEM();
			
			Query query = em.createQuery("Select b From Bet b where b.model.id = ?1", Bet.class);
			query.setParameter(1, orderId);
			betList.addAll(query.getResultList());
			// populate the bets related to opening order
			if(oType.equals("VOClose")){
				logger.debug("finding opening order of close order {}",orderId);
				BettingModel openModel = findOpeningOrder(orderId);
				long oModelId = openModel.getId();				
				
				query = em.createQuery("Select b From Bet b where b.model.id = ?1", Bet.class);
				query.setParameter(1, oModelId);
				betList.addAll(query.getResultList());				
			}
			
			return betList;
			
		} catch(Exception ex) {
			logger.error("error while finding bets by order type by {} exception caught {}",orderId,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
		
	}
	
	@Override
	public List<Bet> findBetsByOrder(long orderId) {

		EntityManager em = null; 
		List<Bet> betList = new ArrayList<Bet>();

		try {
			// May throw connection exception
			em = dbMgr.getEM();
			
			Query query = em.createQuery("Select b From Bet b where b.model.id = ?1", Bet.class);
			query.setParameter(1, orderId);
			betList.addAll(query.getResultList());
			return betList;
			
		} catch(Exception ex) {
			logger.error("error while finding bets by orders {} exception caught",orderId,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
		
	}
	
	
	@Override
	public BettingModel findStoppedOrderById(long id) {
		EntityManager em = null; 
		try {
			// May throw connection exception
			em = dbMgr.getEM();
			Query query = em.createNativeQuery("select * from betting_models where id = :id"
					+ " and model_status in (:status)",BettingModel.class);
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name(),NormalOrderStatus.MODIFIED.name(),NormalOrderStatus.STOPPED.name());
			query.setParameter("status", lstStatus);
			query.setParameter("id", id);
			BettingModel model = (BettingModel) query.getSingleResult();
			logger.info("find order by id {} with status {}",model.getId(),model.getModelStatus());
			//BettingModel model =  BettingModel.findByID(em, id);
			return model;
		} catch(Exception ex) {
			logger.error("error while finding stopped order by {} exception caught {}",id,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	
	}
	
	public int findUnsettledOrdersCount(){
		EntityManager em = null;
		try{
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currDateStr = dateFormat.format(cal.getTime());
			
			em = dbMgr.getEM();
			//Query query = em.createQuery("Select m From BettingModel m where m.modelStatus = ?1", BettingModel.class);
			 Query query = em.createNativeQuery("select count(*) from betting_models where (model_status in (:status) or is_modified = 1)"
					+ " and id in (select id from betting_models_extra_vodds where statement_date <= :curr_date )");
			
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name(),NormalOrderStatus.MODIFIED.name(),NormalOrderStatus.STOPPED.name());
			
			query.setParameter("status", lstStatus);
			//query.setParameter("status2", "MODIFIED");
			
//			List<String> lstType = Arrays.asList(VOddsModelType.POSITIONAL_OPEN.name(),VOddsModelType.FUTURE_POSITIONAL_OPEN.name());
			
//			query.setParameter("type", lstType);
			query.setParameter("curr_date", currDateStr);
			
			BigInteger resultCount = (BigInteger) query.getSingleResult();
	

			return resultCount.intValue();
		}
		catch(Exception ex){
			logger.error("error while finding unsettled order count exception caught {}",ex);
			return 0;
		}
		finally{
			if(em!=null) em.close();
		}	
	}
	
	
	/**
	 * 
	 * count unsettled order with is_modified = 1 
	 */
	public int findUnsettledWithAcceptedBetCount(){
		EntityManager em = null;
		try{
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currDateStr = dateFormat.format(cal.getTime());
			
			em = dbMgr.getEM();
			 Query query = em.createNativeQuery("select count(*) from betting_models where (model_status in (:status) and is_modified = 1)"
					+ " and id in (select id from betting_models_extra_vodds where statement_date <= :curr_date )");
			
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.CONFIRMED.name(),NormalOrderStatus.PARTIAL.name());
			
			query.setParameter("status", lstStatus);
			query.setParameter("curr_date", currDateStr);
			
			BigInteger resultCount = (BigInteger) query.getSingleResult();
	

			return resultCount.intValue();
		}
		catch(Exception ex){
			logger.error("error while finding unsettled order with accepted bet count exception caught {}",ex);
			return 0;
		}
		finally{
			if(em!=null) em.close();
		}	
		
	}

	/**
	 *
	 * check exist last revert transactions unsettled order
	 */
	public boolean checkExistLastRevertTransactionsUnsettledOrder(long modelId){
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createNativeQuery("select if(type = 'rev', 1, 0) from model_transactions where model_id = :model_id order by id desc");

			query.setParameter("model_id", modelId);

			BigInteger resultCount = (BigInteger) query.getSingleResult();

			return resultCount.intValue() > 0;
		}
		catch(Exception ex){
			logger.error("error while check exist last revert transactions unsettled order exception caught {}",ex);
			return false;
		}
		finally{
			if(em!=null) em.close();
		}
	}
	
	/**
	 * retrieve the list of stopped orders which are not of type positional open
	 */
	@Override
	public List<BettingModel> findUnsettledOrders(int limitStart, int limitEnd){
		List<BettingModel> orderList = new ArrayList<BettingModel>();
		EntityManager em = null;
		try{
			
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currDateStr = dateFormat.format(cal.getTime());
			
			em = dbMgr.getEM();
			//Query query = em.createQuery("Select m From BettingModel m where m.modelStatus = ?1", BettingModel.class);
			TypedQuery<BettingModel> query = (TypedQuery<BettingModel>) em.createNativeQuery("select * from betting_models where (model_status in (:status) or is_modified = 1)"
					+ " and id in (select id from betting_models_extra_vodds where statement_date <= :curr_date ) order by end_date asc limit :order_limit_start,:order_limit_end",
					BettingModel.class);
			
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name(),NormalOrderStatus.MODIFIED.name(),NormalOrderStatus.STOPPED.name());
			
			query.setParameter("status", lstStatus);
				
//			List<String> lstType = Arrays.asList(VOddsModelType.POSITIONAL_OPEN.name(),VOddsModelType.FUTURE_POSITIONAL_OPEN.name());
			
//			query.setParameter("type", lstType);
			query.setParameter("order_limit_start", limitStart);
			query.setParameter("order_limit_end", limitEnd);
			query.setParameter("curr_date", currDateStr);
			
			List<BettingModel> resultList = query.getResultList();
			
			orderList.addAll(resultList);

			return orderList;
			
		}
		catch(Exception ex){
			logger.error("error while finding unsettled order exception caught {}",ex);
			return orderList;
		}
		finally{
			if(em!=null) em.close();
		}	
		
	}

	public int findUnsettledOrdersWithMatchNotStartedCount(){
		EntityManager em = null;
		try{
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currDateStr = dateFormat.format(cal.getTime());

			em = dbMgr.getEM();
			Query query = em.createNativeQuery(
					"select count(bm.id) " +
							"from betting_models bm " +
							"join ( " +
							" select bme.id " +
							" from betting_models_extra_vodds bme " +
							" join bets b on b.model_id = bme.id " +
							" where bme.statement_date > :curr_date " +
							" group by bme.id " +
							" having count(b.id) = count(if(b.status in (:betStatus), 1, null)) " +
							") as bmet on bmet.id = bm.id " +
							"where (bm.model_status in (:status) or is_modified = 1)");

			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name());

			List<String> lstBetStatus = Arrays.asList(BetStatus.REJECTED.name(), BetStatus.CANCELLED.name());

			query.setParameter("status", lstStatus);
			query.setParameter("curr_date", currDateStr);
			query.setParameter("betStatus", lstBetStatus);

			BigInteger resultCount = (BigInteger) query.getSingleResult();

			return resultCount.intValue();
		}
		catch(Exception ex){
			logger.error("error while finding unsettled order with the match not started count exception caught {}",ex);
			return 0;
		}
		finally{
			if(em!=null) em.close();
		}
	}

	public List<BettingModel> findUnsettledOrdersWithMatchNotStarted(int limitStart, int limitEnd){
		List<BettingModel> orderList = new ArrayList<BettingModel>();
		EntityManager em = null;
		try{

			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currDateStr = dateFormat.format(cal.getTime());

			em = dbMgr.getEM();
			TypedQuery<BettingModel> query = (TypedQuery<BettingModel>) em.createNativeQuery(
					"select bm.* " +
							"from betting_models bm " +
							"join ( " +
							" select bme.id " +
							" from betting_models_extra_vodds bme " +
							" join bets b on b.model_id = bme.id " +
							" where bme.statement_date > :curr_date " +
							" group by bme.id " +
							" having count(b.id) = count(if(b.status in (:betStatus), 1, null)) " +
							") as bmet on bmet.id = bm.id " +
							"where (bm.model_status in (:status) or is_modified = 1)" +
							"order by end_date asc " +
							"limit :order_limit_start,:order_limit_end", BettingModel.class);

			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name());

			List<String> lstBetStatus = Arrays.asList(BetStatus.REJECTED.name(), BetStatus.CANCELLED.name(), BetStatus.DRAW.name());

			query.setParameter("status", lstStatus);
			query.setParameter("betStatus", lstBetStatus);
			query.setParameter("order_limit_start", limitStart);
			query.setParameter("order_limit_end", limitEnd);
			query.setParameter("curr_date", currDateStr);

			List<BettingModel> resultList = query.getResultList();

			orderList.addAll(resultList);

			return orderList;

		}
		catch(Exception ex){
			logger.error("error while finding unsettled order with the match not started exception caught {}",ex);
			return orderList;
		}
		finally{
			if(em!=null) em.close();
		}
	}
	
	public List<BettingModel> findUnsettledWithAcceptedBet(){
		List<BettingModel> orderList = new ArrayList<BettingModel>();
		EntityManager em = null;
		try{
			
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currDateStr = dateFormat.format(cal.getTime());
			
			em = dbMgr.getEM();
			//Query query = em.createQuery("Select m From BettingModel m where m.modelStatus = ?1", BettingModel.class);
			TypedQuery<BettingModel> query = (TypedQuery<BettingModel>) em.createNativeQuery("select * from betting_models where (model_status in (:status) and is_modified = 1)"
					+ " and id in (select id from betting_models_extra_vodds where statement_date <= :curr_date ) order by end_date asc",
					BettingModel.class);
			
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.CONFIRMED.name(),NormalOrderStatus.PARTIAL.name());
			
			query.setParameter("status", lstStatus);

			query.setParameter("curr_date", currDateStr);
			
			List<BettingModel> resultList = query.getResultList();
			
			orderList.addAll(resultList);

			return orderList;
		}
		catch(Exception ex){
			logger.error("error while finding unsettled order with accepted bet exception caught {}",ex);
			return orderList;
		}
		finally{
			if(em!=null) em.close();
		}	
	}
	
	
	
	@Override
	public ModelTransaction findLatestReturnTransaction(long orderId,long userId){
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId = ?1 and t.type= ?2 and t.user.id = ?3 order by t.time desc", ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2,  ModelTransactionType.RETURN);
			query.setParameter(3, userId);
			query.setMaxResults(1);
			if(!query.getResultList().isEmpty()) {
				return (ModelTransaction) query.getResultList().get(0);
			}
		}
		catch(Exception ex){
			logger.error("error while finding lastest return transaction with order {} , id {} exception caught {}",orderId,userId,ex);
		}
		finally{
			if(em!=null) em.close();
		}

		logger.info("order id {} user id {} no latest return transaction", orderId, userId);
		return null;
	}
	
	@Override
	public ModelTransaction findLatestSettlementDarkpool(long orderId,long userId){
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId = ?1 and t.type= ?2 and t.user.id = ?3 order by t.time desc", ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2,  ModelTransactionType.DPOOLRERTURN);
			query.setParameter(3, userId);
			query.setMaxResults(1);
			ModelTransaction returnTrans = (ModelTransaction) query.getResultList().get(0);
			return returnTrans;
			
		}
		catch(Exception ex){
			logger.error("error while finding lastest return transaction with order {} , id {} exception caught {}",orderId,userId,ex);
		}
		finally{
			if(em!=null) em.close();
		}
		
		return null;
		
	}
	
	@Override
	public ModelTransaction findLatestReturnPTPLTransaction(long orderId,long userId){
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId = ?1 and t.type= ?2 and t.user.id = ?3 order by t.time desc", ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2,  ModelTransactionType.PT_PL);
			query.setParameter(3, userId);
			query.setMaxResults(1);
			if(!query.getResultList().isEmpty()) {
				return (ModelTransaction) query.getResultList().get(0);
			}
		}
		catch(Exception ex){
			logger.error("error while finding lastest return transaction with order {} , id {} exception caught",orderId,userId,ex);
		}
		finally{
			if(em!=null) em.close();
		}

		logger.info("order id {} user id {} no latest return ptpl transaction", orderId, userId);
		return null;
	}
	
	public List<Bet> findBetListById(List<String> idList){
		
		EntityManager em = null;
		List<Bet> betList = new ArrayList<Bet>();
		if(idList == null || idList.isEmpty()){
			logger.error("Cannot find bet list for empty parameter");
			return betList;
		}
		try{
			em = DatabaseManager.instance().getEM();
			StringBuffer sb = new StringBuffer();
			idList.stream().forEach(id -> sb.append("'"+id+"',"));
			sb.deleteCharAt(sb.lastIndexOf(","));
			logger.info("list bet id {}",sb.toString());
			Query query = em.createNativeQuery("SELECT * from bets WHERE id in ("+sb.toString()+")",Bet.class);
			betList = query.getResultList();
			logger.info("{} bets retrieved from database for the queried list of size {}",betList.size(),idList.size());
			return betList;
			
		}
		catch(Exception ex){
			logger.error("Error while retrieving bet list by account info exception caught {}",ex);
		}
		finally{
			if(em!=null) em.close();
		}
		
		
		return betList;
		
	}
	
	/***
	 * convert the commission value from user currency to agent currency 
	 * @param aNode
	 * @param bNode
	 * @param commValue
	 * @return
	 */
	@Override
	public BigDecimal getAgentCommission(AgentUserNode aNode, BetUserNode bNode, BigDecimal commValue ){
	
		Currency aCurrency = aNode.getUser().getCurrency();
		Currency bCurrency = bNode.getUser().getCurrency();
		if(aCurrency.getCode().equals(bCurrency.getCode())){
			logger.debug("user and agent has same currency value {}",bCurrency.getCode());
			return commValue;
		}
			
		else{
			EntityManager em = null;
			try{
				em = dbMgr.getEM();
				CurrencyConversion conversion = CurrencyConversion.findById(em, bCurrency.getCode(), aCurrency.getCode());
				logger.debug("commission value converted from {} with conversion rate {} for user currency {} and agent currency {}",
						commValue,conversion.getRate(),bCurrency.getCode(),aCurrency.getCode());
				return conversion.getRate().multiply(commValue);
			}
			catch(Exception e){
				logger.error("Exception while converting from user curency {} to agent currency {} ",aCurrency.getCode(),bCurrency.getCode(),e);
			}
			finally{
				if(em !=null) em.close();
			}
			
			
		}
		return null;
		
	}

	@Override
	public BettingModelExtraVodds getModelExtraEntity(long orderId) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();

			Query oQuery = em.createQuery("Select m From BettingModelExtraVodds m where m.modelId.id = ?1", BettingModelExtraVodds.class);
			oQuery.setParameter(1, orderId);
			BettingModelExtraVodds oModel = (BettingModelExtraVodds) oQuery.getSingleResult();
			return oModel;
		} catch(Exception ex) {
			logger.error("error while getting modelExtra {}",orderId,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}	
	}

	@Override
	public Service getServiceById(String id) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Service service = Service.findById(em, id);
			return service;
		} catch(Exception ex) {
			logger.error("database exception caught",ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	}

	@Override
	public BigDecimal getConversionRate(String currencyChildUser, String currencyUserParrent) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			
			if (currencyChildUser.equals(currencyUserParrent)) return new BigDecimal(1);
			
			CurrencyConversion objCurrencyConversion = CurrencyConversion.findById(em, currencyChildUser, currencyUserParrent);
			
			BigDecimal conversionRate = objCurrencyConversion == null ? new BigDecimal(1) : objCurrencyConversion.getRate();
			return conversionRate;
			
		} catch(Exception ex) {
			
			logger.error("Database exception while getting conversion rate.",ex);
			return null;
			
		} finally {
			if(em != null) em.close();
		}
		
	}

	@Override
	public void persistResults(Object obj) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			try{
				EntityTransaction tx = em.getTransaction();
				tx.begin();
				em.merge(obj);			
				tx.commit();
				
			}
			catch(Exception ex){
				logger.error("error while persisting transaction",ex);
			}
		}
		catch(Exception ex){
			logger.error("error while processing transactions",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public List<DarkPoolBetInfo> findUnsettledDPoolBets() {

		List<DarkPoolBetInfo> dpoolList = new ArrayList<DarkPoolBetInfo>();
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			TypedQuery<DarkPoolBetInfo> dQuery = (TypedQuery<DarkPoolBetInfo>)em.
					createNativeQuery("select * from darkpool_betinfo where status IN (:dstatus) limit :max_limit", DarkPoolBetInfo.class);
			
			dQuery.setParameter("dstatus", Arrays.asList(DPoolBetStatus.RESOLVED.toString(), DPoolBetStatus.MODIFIED.toString()));
			dQuery.setParameter("max_limit", bean.getPopulateOrdersPerInterval());
			
			dpoolList.addAll(dQuery.getResultList());

		}
		catch(Exception ex){
			logger.error("database exception caught while fetching dpool bet info",ex);

		}
		finally{
			if(em != null) em.close();
		}
		return dpoolList;
	}

	@Override
	public List<DPoolLiquidationMapping> fetchLiquidationMappings(String dBetId) {
		EntityManager em = null;
		List<DPoolLiquidationMapping> liqMapping = new ArrayList<DPoolLiquidationMapping>();
		try{
			em = dbMgr.getEM();
			TypedQuery<DPoolLiquidationMapping> dQuery = (TypedQuery<DPoolLiquidationMapping>) em.
					createNativeQuery("select * from darkpool_liquidation_mapping where dpool_id=:dpid",DPoolLiquidationMapping.class);
			dQuery.setParameter("dpid", dBetId);
			
			liqMapping.addAll(dQuery.getResultList());

		}
		catch(Exception ex){
			logger.error("database exception caught while fetching liquidation mappings",ex);

		}
		finally{
			if(em != null) em.close();
		}
		return liqMapping;
	}

	@Override
	public List<Bet> fetchLiquidatingBets(String dBetId) {
		EntityManager em = null;
		List<Bet> liquidatingBets = new ArrayList<Bet>();
		try{
			em = dbMgr.getEM();
			TypedQuery<Bet> dQuery = (TypedQuery<Bet>) em.createNativeQuery("select * from bets where id in ( select bet_id from darkpool_liquidation_mapping where dpool_id=:dp_id)",Bet.class);
			dQuery.setParameter("dp_id", dBetId);
			
			liquidatingBets.addAll(dQuery.getResultList());

		}
		catch(Exception ex){
			logger.error("database exception caught while fetching liquidating bets",ex);

		}
		finally{
			if(em != null) em.close();
		}
		
		return liquidatingBets;
	}

	@Override
	public void saveUserCredit(VoddsBetUser betUser) {
		EntityManager em = null;
		try {
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			try {
				tx.begin();
				String sqlUpdate = " UPDATE bet_users SET credit = ?1,pl = ?2,outstanding_amount =?3 WHERE id = ?4";
				Query query = em.createNativeQuery(sqlUpdate);
				query.setParameter(1, betUser.getCredit());
				query.setParameter(2, betUser.getpL());
				query.setParameter(3, betUser.getOutstandingAmount());
				query.setParameter(4, betUser.getId());
				query.executeUpdate();
				tx.commit();
				logger.info("{} persist user credit {} successful", betUser.getId(), betUser.getCredit());

				genUtil.logUserStats(betUser);
			} catch (Exception ex) {
				logger.error("{} update user credit = {} failed with exception", betUser.getId(), betUser.getCredit(), ex);
			} finally {
				if (tx != null && tx.isActive()) tx.rollback();
			}
		} catch (Exception ex) {
			logger.error("{} error while updating credit", betUser.getId(), ex);
		} finally {
			if (em != null) em.close();
		}

	}

	@Override
	public void saveAgentUserCredit(Long id, BigDecimal credit) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			try{
				tx.begin();
				String sqlUpdate = " UPDATE agent_users SET credit = ?1 WHERE id = ?2";	
				Query query = em.createNativeQuery(sqlUpdate);
    		    query.setParameter(1, credit);
    		    query.setParameter(2, id);
    		    query.executeUpdate();
				tx.commit();
				logger.info("persist agent user with user = {} , credit = {} into db successful",
						id,credit);
			}
			catch(Exception ex){
				logger.error("update user = {} , credit = {} failed with exception : {}",
						id,credit,ex);
			}finally {
				if (tx != null && tx.isActive()) tx.rollback();
			}
		}
		catch(Exception ex){
			logger.error("error while updating credit of agent user {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public BigDecimal getPlOfBetUser(Long id) {
		EntityManager em = null;
		try{
			logger.info("idUser having orders change {}",id);
			em = dbMgr.getEM();
			String sql = " SELECT plt.normal_pl +  plt.positional_pl as pl FROM"
					   + " ( SELECT b.id,"
					   + " 	 SUM(IF(bme.profit_loss IS NULL OR bm.model_type not like '%NORMAL', 0, bme.profit_loss)) as normal_pl,"
					   + "   SUM(IF(pm.effective_pl IS NULL, 0, pm.effective_pl)) as positional_pl"
					   + "	 FROM bet_users b "
					   + "	 RIGHT JOIN betting_models bm ON (owner_id = b.id) "
					   + "   LEFT JOIN betting_models_extra_vodds bme ON (bme.id = bm.id) "
					   + "   LEFT JOIN positional_model pm ON (bm.id = pm.open_models_id)"
					   + "   WHERE b.id = ?1"
					   + "   GROUP BY b.id"
					   + " ) plt";
			Query query = em.createNativeQuery(sql);
		    query.setParameter(1, id);
		    return (BigDecimal)query.getSingleResult();
		}catch(Exception ex){
			logger.error("error while get pl of user have orders changes {} exception caught",id,ex);
			return null;
		}finally{
			if(em != null) em.close();
		}
	}

	@Override
	public void persistBUResults(Set<VoddsBetUser> setBetUser) {
		EntityManager em = null;
		try {
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			for (VoddsBetUser betUser : setBetUser) {
				try {
					tx.begin();
					String sqlUpdate = " UPDATE bet_users SET credit = ?1,pl = ?2,outstanding_amount =?3,positional_potential_loss = ?4 WHERE id = ?5";
					Query query = em.createNativeQuery(sqlUpdate);
					query.setParameter(1, betUser.getCredit());
					query.setParameter(2, betUser.getpL());
					query.setParameter(3, betUser.getOutstandingAmount());
					query.setParameter(4, betUser.getPositionalPotentialLoss());
					query.setParameter(5, betUser.getId());
					query.executeUpdate();
					tx.commit();
					logger.info("{} Update BU credit {}, pl {}, outstanding {} complete",
							betUser.getId(), betUser.getCredit(), betUser.getpL(), betUser.getOutstandingAmount());

					genUtil.logUserStats(betUser);
				} catch (Exception ex) {
					logger.error("{} Update BU credit {}, pl {}, outstanding {} failed with exception",
							betUser.getId(), betUser.getCredit(), betUser.getpL(), betUser.getOutstandingAmount(), ex);
				} finally {
					if (tx != null && tx.isActive()) tx.rollback();
				}
			}

		} catch (Exception ex) {
			logger.error("error while persisting BU exception caught {}", ex);
		} finally {
			if (em != null) em.close();
		}

	}

	@Override
	public void persistAUResults(Set<AgentUser> setAgentUser) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();

			for (AgentUser agentUser : setAgentUser) {
				try{
					tx.begin();
					String sqlUpdate = " UPDATE agent_users SET credit = ?1 , pt_pl =?2 WHERE id = ?3";	
					Query query = em.createNativeQuery(sqlUpdate);
	    		    query.setParameter(1, agentUser.getCredit());
	    		    query.setParameter(2, agentUser.getPtPl());
	    		    query.setParameter(3, agentUser.getId());
	    		    query.executeUpdate();
					tx.commit();
					logger.info("Update AU_{} , credit = {} , ptpl = {} complete",
							agentUser.getId(),agentUser.getCredit(),agentUser.getPtPl());
				}catch(Exception ex){
					logger.error("update AU_{} , credit = {} , ptpl = {} failed with exception : {}",
							agentUser.getId(),agentUser.getCredit(),agentUser.getPtPl(),ex);
				}finally {
					if (tx != null && tx.isActive()) tx.rollback();
				}
			}
				
		}
		catch(Exception ex){
			logger.error("error while persisting AU exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public List<ModelTransaction> findAllTransactionByOrderId(long orderId, ModelTransactionType typeTransaction) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId = ?1 and t.type= ?2", ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2, typeTransaction);
			query.setMaxResults(1);
			List<ModelTransaction> lst = query.getResultList();
			return lst;
			
		}
		catch(Exception ex){
			logger.error("error while finding all transaction by orderId {} exception caught {}",orderId,ex);
		}
		finally{
			if(em!=null) em.close();
		}
		
		return null;
	}

	@Override
	public List<ModelTransaction> findLatestReturnTransactionForOperator(long orderId, long userId) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId = ?1 and (t.type=?2 or t.type=?3) and t.user.id = ?4 order by t.time desc", ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2,  ModelTransactionType.RETURN);
			query.setParameter(3,  ModelTransactionType.REVERTED);
			query.setParameter(4,  userId);
			List<ModelTransaction> lst = query.getResultList();
			return lst;
			
		}
		catch(Exception ex){
			logger.error("error while findLatestReturnTransactionForOperator with order {} , user {} , exception caught",orderId,userId,ex);
		}
		finally{
			if(em!=null) em.close();
		}
		
		return null;
	}

	@Override
	public int findVoidingOrdersCount() {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createNativeQuery("select count(*) from betting_models where id in "
					+ "(select close_models_id from positional_model pm join betting_models bm"
					+ " on pm.open_models_id = bm.id and (model_status in (:status) or is_modified = 1) where execution_stage in (:exeStage))");
			
	
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name(),NormalOrderStatus.MODIFIED.name(),NormalOrderStatus.STOPPED.name());
			
			query.setParameter("status", lstStatus); 
			
			List<String> lstexeStage = Arrays.asList(PositionalOrderStage.VOIDING.name(),PositionalOrderStage.VOIDED.name());
			
			query.setParameter("exeStage", lstexeStage);

			
			BigInteger resultCount = (BigInteger) query.getSingleResult();
	

			return resultCount.intValue();
		}
		catch(Exception ex){
			logger.error("error while finding voiding order count exception caught {}",ex);
			return 0;
		}
		finally{
			if(em!=null) em.close();
		}	
	}

	@Override
	public List<BettingModel> findVoidingOrders(int limitStart, int limitEnd) {
		List<BettingModel> orderList = new ArrayList<BettingModel>();
		EntityManager em = null;
		try{
			
			em = dbMgr.getEM();
			//Query query = em.createQuery("Select m From BettingModel m where m.modelStatus = ?1", BettingModel.class);
			TypedQuery<BettingModel> query = (TypedQuery<BettingModel>) em.createNativeQuery("select * from betting_models where id in "
					+ "(select close_models_id from positional_model pm join betting_models bm"
					+ " on pm.open_models_id = bm.id and (model_status in (:status) or is_modified = 1) where execution_stage in (:exeStage))"
					+ " order by end_date asc limit :order_limit_start,:order_limit_end",
					BettingModel.class);
			
			List<String> lstStatus = Arrays.asList(NormalOrderStatus.REJECTED.name(),NormalOrderStatus.CONFIRMED.name(),
					NormalOrderStatus.PARTIAL.name(),NormalOrderStatus.MODIFIED.name(),NormalOrderStatus.STOPPED.name());
			
			query.setParameter("status", lstStatus);

			List<String> lstexeStage = Arrays.asList(PositionalOrderStage.VOIDING.name(),PositionalOrderStage.VOIDED.name());
			
			query.setParameter("exeStage", lstexeStage);
			
			query.setParameter("order_limit_start", limitStart);
			query.setParameter("order_limit_end", limitEnd);
			
			List<BettingModel> resultList = query.getResultList();
			
			orderList.addAll(resultList);

			return orderList;
			
		}
		catch(Exception ex){
			logger.error("error while finding voiding order exception caught {}",ex);
			return orderList;
		}
		finally{
			if(em!=null) em.close();
		}	
	}
	
	@Override
	public BettingModel findBettingModelById(long id) {
		
		EntityManager em = null; 
		try {
			// May throw connection exception
			em = dbMgr.getEM();
			Query query = em.createNativeQuery("select * from betting_models where id=:id",BettingModel.class);
			query.setParameter("id", id);
			BettingModel model = (BettingModel) query.getSingleResult();
			logger.info("find order by id {} with status {}",model.getId(),model.getModelStatus());
			return model;
		} catch(Exception ex) {
			logger.error("error while finding betting model with {} exception caught {}",id,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
		
	}

	@Override
	public void updateBMResults(Set<BettingModel> transactions,OrderWrapperManager owManger) {
		EntityManager em = null;
		EntityTransaction tx = null;
		try{
			
			em = dbMgr.getEM();
			if(transactions.size() >0){
				Iterator <BettingModel> it = transactions.iterator();
				    BettingModel trans = null;
				    tx = em.getTransaction();
					while (it.hasNext()) {
						try{
							tx.begin();
							trans = it.next();
							
							String sqlUpdate = " UPDATE betting_models SET is_modified = ?1 WHERE id = ?2";	
							Query query = em.createNativeQuery(sqlUpdate);
			    		    query.setParameter(1, (byte)1);
			    		    query.setParameter(2, trans.getId());
			    		    query.executeUpdate();
							
							tx.commit();
							logger.info("persist betting model with order = {} , is_modified = {} into db successful",
									trans.getId(),trans.IsModified());
							owManger.removeOrderFromCache(trans.getId());
						}catch(Exception ex){
							owManger.removeOrderFromCache(trans.getId());
							logger.error("update order = {} , modified = {} failed with exception : {}",
									trans.getId(),trans.IsModified(),ex);
							continue;
						}
						 finally {
							if (tx != null && tx.isActive()) tx.rollback();
						}
					}
					
			}
			
		}
		catch(Exception ex){
			logger.error("error while updating BMResults {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public void persistPMResults(Set<PositionalModel> setPositionModel,
			MPLNotificationTask mplNotificationTask,NoQPersister noQ) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = null;
			for (PositionalModel pM : setPositionModel) {
				try{
					tx = em.getTransaction();
					tx.begin();
					String oldStatus = noQ.getOldStatus(pM.getId());
					String sqlUpdate = " UPDATE positional_model SET total_margin = ?1 , execution_stage = ?2 , max_potential_loss = ?3 , effective_pl = ?4 ,"
							+ " operator_gain = ?6, operator_loss = ?7, voided_pl = ?9 WHERE id = ?5 and execution_stage = ?8";
					Query query = em.createNativeQuery(sqlUpdate);
	    		    query.setParameter(1, pM.getTotalMargin());
	    		    query.setParameter(2, pM.getExecutionStage());
	    		    query.setParameter(3, pM.getMaxPotentialLoss());
	    		    query.setParameter(4, pM.getEffectivePL());
	    		    query.setParameter(5, pM.getId());
	    		    query.setParameter(6, pM.getOperatorGain());
	    		    query.setParameter(7, pM.getOperatorLoss());
	    		    query.setParameter(8, oldStatus);
	    		    query.setParameter(9, pM.getVoidedPl());
	    		    int affectedRows = query.executeUpdate();
	    		    tx.commit();
	    		    logger.info("{}_SPO , margin = {} , status = {} , mpl = {} , voided_pl = {}, epl = {}  complete {}",
	    		    		pM.getId(),pM.getTotalMargin(),pM.getExecutionStage(),pM.getMaxPotentialLoss(), pM.getVoidedPl(), pM.getEffectivePL(), affectedRows);
	    		    mplNotificationTask.onPositionalOrderPersisted(pM.getId());
				}catch(Exception ex){
					logger.error("update poId = {} , total_margin = {} , status = {} , "
	    		    		+ "mpl = {} , voided_pl = {}, epl = {}  failed with exception : {}",
	    		    		pM.getId(),pM.getTotalMargin(),pM.getExecutionStage(),pM.getMaxPotentialLoss(), pM.getVoidedPl(), pM.getEffectivePL(),ex);
				}finally {
					if (tx != null && tx.isActive()) tx.rollback();
				}
			}
	
		}
		catch(Exception ex){
			logger.error("error while persisting PM exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public void persistBMEVResults(Set<BettingModelExtraVodds> setBMEV,OutstandingNotificationTask amtNotificationTask) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			for (BettingModelExtraVodds bMEV : setBMEV) {
				try{
					tx.begin();
					BigDecimal outstanding = bMEV.getOutstandingAmount();
					String sqlUpdate = " UPDATE betting_models_extra_vodds SET profit_loss = ?1 , confirmed_stake = ?2, outstanding_amount = ?3 WHERE id = ?4";	
					Query query = em.createNativeQuery(sqlUpdate);
	    		    query.setParameter(1, bMEV.getProfitLoss());
	    		    query.setParameter(2, bMEV.getConfirmedStake());
	    		    query.setParameter(3, 0);
	    		    query.setParameter(4, bMEV.getModelId().getId());
	      		    query.executeUpdate();
	    		    tx.commit();
	    		    logger.info("{}_BMEV , pl = {} , confirmed_stake = {} complete",
	    		    		bMEV.getModelId().getId(),bMEV.getProfitLoss(),bMEV.getConfirmedStake());
	    		    
	    			//Notification outstanding amt
					UserOutstandingChangeEvent amt = new UserOutstandingChangeEvent(bMEV.getModelId().getOwner().getUsername(), outstanding.negate().doubleValue(), bMEV.getModelId().getOwner().getId());
					logger.info("submit oustanding {} - user {} - value oustanding {} - userId {}",amt,bMEV.getModelId().getOwner().getUsername(),outstanding.negate().doubleValue(), bMEV.getModelId().getOwner().getId());
					amtNotificationTask.submitOustanding(amt);
				}catch(Exception ex){
					logger.error("update orderId = {} , pl = {} , confirmed_stake = {} failed with exception : {}",
	    		    		bMEV.getModelId().getId(),bMEV.getProfitLoss(),bMEV.getConfirmedStake(),ex);
				}finally {
					if (tx != null && tx.isActive()) tx.rollback();
				}
			}
	
		}
		catch(Exception ex){
			logger.error("error while persisting BMEV exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public void persistModelTransaction(List<ModelTransaction> transactions,TransactionQueue tQueue) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			//Thread.sleep(3*60*1000);
			if(transactions.size() >0){		
				EntityTransaction tx = em.getTransaction();
				for(ModelTransaction transaction:transactions){
					try{
						tx.begin();
						em.merge(transaction);			
						tx.commit();
						tQueue.removeTransactionRecordOfOrder(transaction.getModelId());
						logger.info("{}_O{} amt = {} , remark = {} ,"
								+ " user = {} , bcredit = {} , acredit = {} complete",
								transaction.getType().name() ,transaction.getModelId(),transaction.getCreditAmount(),transaction.getRemarks(),
		    		    		transaction.getUser().getId(),transaction.getCreditBefore(),transaction.getCreditAfter());
					}
					catch(Exception ex){
						logger.error("insert orderId = {} , type = {} , amt = {} , time = {} , remark = {} ,"
								+ " user = {} , bcredit = {} , acredit = {} failed with exception : {}",
		    		    		transaction.getModelId(),transaction.getType().name(),transaction.getCreditAmount(),transaction.getTime(),transaction.getRemarks(),
		    		    		transaction.getUser().getId(),transaction.getCreditBefore(),transaction.getCreditAfter(),ex);
					}
					finally {
						if (tx != null && tx.isActive()) tx.rollback();
					}
				}
				
			}
			
		}
		catch(Exception ex){
			logger.error("error while persisting model transaction exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public void persistTransaction(List<TransactionWrapper> transactions) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			if(transactions.size() >0){		
				EntityTransaction tx = em.getTransaction();
				for(TransactionWrapper transactionWrapper : transactions){
					Transaction transaction = transactionWrapper.getTransaction();
					try{
						tx.begin();
						em.persist(transaction);
						UserActivity activity = transactionWrapper.getActivity();
						activity.setDescription(activity.getDescription().replaceAll(":transId", transaction.getId() + ""));
						em.persist(activity);
						tx.commit();
						logger.info("{}_U{} amt = {} , bcredit = {} , acredit = {} , remark = {} ,"
								+ " initUser = {} complete",
								transaction.getType().name() ,transaction.getUser().getId(),transaction.getAmount(),
								transaction.getBefore_credit(),transaction.getAfter_credit(),transaction.getDescription(),
								transaction.getInitiating_user().getId());
					}
					catch(Exception ex){
						logger.error("insert userId = {} , type = {} , time = {} , bcredit = {} ,"
								+ " acredit = {} , amt = {} , description = {} , initial_user {} failed with exception : {}",
		    		    		transaction.getUser().getId(),transaction.getType().name(),transaction.getTime(),transaction.getBefore_credit(),transaction.getAfter_credit(),
		    		    		transaction.getAmount(),transaction.getDescription(),transaction.getInitiating_user().getId(),ex);					}
					finally {
						if (tx != null && tx.isActive()) tx.rollback();
					}
				}
				
			}
			
		}
		catch(Exception ex){
			logger.error("error while persisting transaction exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public void persistDPTransaction(List<DarkPoolBetInfo> transactions) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			if(transactions.size() >0){		
				EntityTransaction tx = em.getTransaction();
				for(DarkPoolBetInfo transaction:transactions){
					try{
						tx.begin();
						em.merge(transaction);			
						tx.commit();
						logger.info("DP_{} , event = {} , odd = {} , status = {} ,"
								+ " time = {} , amt = {} complete",
								transaction.getBetId().getId(),transaction.getEventId(),transaction.getOddId(),transaction.getStatus().name(),
		    		    		transaction.getStartTime(),transaction.getSettledAmount());
					}
					catch(Exception ex){
						logger.error("insert betId = {} , eventId = {} , odd = {} , status = {} ,"
								+ " time = {} , amt = {} failed with exception : {}",
								transaction.getBetId().getId(),transaction.getEventId(),transaction.getOddId(),transaction.getStatus().name(),
		    		    		transaction.getStartTime(),transaction.getSettledAmount(),ex);				}
					finally {
						if (tx != null && tx.isActive()) tx.rollback();
					}
				}
				
			}
			
		}
		catch(Exception ex){
			logger.error("error while persisting dp transaction exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public ModelTransaction findLastestRefundTransaction(long orderId, long userId) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			
			String sql = " select * from model_transactions where model_id = :orderId"
					+ " and user_id = :userId and type = 'ref' and id > (select id from model_transactions where model_id = :orderId"
					+ " and user_id = :userId and type = 'rev' order by time desc) order by time desc";
			
			Query query = em.createNativeQuery(sql, ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2, userId);
			query.setMaxResults(1);
			ModelTransaction returnTrans = (ModelTransaction) query.getResultList().get(0);
			return returnTrans;
			
		}
		catch(Exception ex){
			logger.error("error while finding lastest refund transaction with void order {} , id {} exception caught {}",orderId,userId,ex);
		}
		finally{
			if(em!=null) em.close();
		}
		
		return null;
	}

	@Override
	public int checkOrderIsValid(long userId, long orderId) {
		EntityManager em = null;
		try{
			
			em = dbMgr.getEM();
			
			Query query = em.createNativeQuery("select count(*) from betting_models where owner_id = ?1 and id = ?2");
						
			query.setParameter(1, userId);
			query.setParameter(2, orderId);
			BigInteger resultCount = (BigInteger) query.getSingleResult();
	
			return resultCount.intValue();
		}
		catch(Exception ex){
			logger.error("error while checking order {} with user {} valid exception caught {}",orderId,userId,ex);
			return 0;
		}
		finally{
			if(em!=null) em.close();
		}	
	}

	@Override
	public Sportbook findSportbookById(String id) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Sportbook sportbook = Sportbook.findByID(em, id);
			return sportbook;
		}
		catch(Exception ex){
			return null;
		}
		finally{
			if(em!=null) em.close();
		}	
	}

	@Override
	public void persistAgentPtPl(Map<Long, List<BettingModelAgentPtPl>> agentPtPl) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			
			for (long modelId:agentPtPl.keySet()) {
				for (BettingModelAgentPtPl ptpl : agentPtPl.get(modelId)) {
					try {
						tx.begin();
						em.merge(ptpl);
						tx.commit();
						logger.info("uId {} oId {} ptpl = {} , base ptpl = {} complete",
								ptpl.getUserId(), ptpl.getModelId(), ptpl.getPl2(), ptpl.getBasePl2());
					} catch (Exception ex) {
						logger.info("uId {} oId {} ptpl = {} , base ptpl = {} complete",
								ptpl.getUserId(), ptpl.getModelId(), ptpl.getPl2(), ptpl.getBasePl2(), ex);
					} finally {
						if (tx != null && tx.isActive()) tx.rollback();
					}
				}
			}
		}
		catch(Exception ex){
			logger.error("error while persisting agent ptpl exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
	}

	@Override
	public void persistAgentPtPl(BettingModelAgentPtPl agentPtPl) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			
			try{
				tx.begin();
				em.merge(agentPtPl);			
				tx.commit();
				logger.info("uId {} oId {} , ptpl = {} , base ptpl = {} complete",
						agentPtPl.getUserId(), agentPtPl.getModelId(), agentPtPl.getPl2(), agentPtPl.getBasePl2());
			}catch(Exception ex){
				logger.info("uId {} oId {} , ptpl = {} , base ptpl = {} complete",
						agentPtPl.getUserId(), agentPtPl.getModelId(), agentPtPl.getPl2(), agentPtPl.getBasePl2(), ex);
			}finally {
				if (tx != null && tx.isActive()) tx.rollback();
			}
		}
		catch(Exception ex){
			logger.error("error while persisting agent ptpl exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
	}

	@Override
	public void persistBetOfAgentPtPl(Map<String, List<BetAgentPtPl>> betAgentPtPl) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();

			for (String betId:betAgentPtPl.keySet()) {
				for (BetAgentPtPl bAptpl : betAgentPtPl.get(betId)) {
					try {
						tx.begin();
						em.merge(bAptpl);
						tx.commit();
						logger.info("bId: {} uId {} oId {} , base_pl_2 = {}, base_pl_3 = {}, base_pl_4 = {} complete",
								bAptpl.getId(), bAptpl.getUser().getId(), bAptpl.getModel().getId(), bAptpl.getBasePl2(), bAptpl.getBasePl3(), bAptpl.getBasePl4());
					} catch (Exception ex) {
						logger.info("bId: {} uId {} oId {} , base_pl_2 = {}, base_pl_3 = {}, base_pl_4 = {} complete",
								bAptpl.getId(), bAptpl.getUser().getId(), bAptpl.getModel().getId(), bAptpl.getBasePl2(), bAptpl.getBasePl3(), bAptpl.getBasePl4(), ex);
						if (tx != null && tx.isActive()) tx.rollback();
					}
				}
			}
		}
		catch(Exception ex){
			logger.error("error while persisting agent ptpl exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
	}

	@Override
	public void persistBetOfAgentPtPl(BetAgentPtPl betAgentPtPl) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();

			try{
				tx.begin();
				em.merge(betAgentPtPl);
				tx.commit();
				logger.info("bId: {} uId {} oId {} , base_pl_2 = {}, base_pl_3 = {}, base_pl_4 = {} complete",
						betAgentPtPl.getId(), betAgentPtPl.getUser().getId(), betAgentPtPl.getModel().getId(), betAgentPtPl.getBasePl2(), betAgentPtPl.getBasePl3(), betAgentPtPl.getBasePl4());
			}catch(Exception ex){
				logger.info("bId: {} uId {} oId {} , base_pl_2 = {}, base_pl_3 = {}, base_pl_4 = {} complete",
						betAgentPtPl.getId(), betAgentPtPl.getUser().getId(), betAgentPtPl.getModel().getId(), betAgentPtPl.getBasePl2(), betAgentPtPl.getBasePl3(), betAgentPtPl.getBasePl4(), ex);
			}finally {
				if (tx != null && tx.isActive()) tx.rollback();
			}
		}
		catch(Exception ex){
			logger.error("error while persisting agent ptpl exception caught {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
	}

	@Override
	public void saveAgentUserCreditAndPtpl(Long id, BigDecimal credit,BigDecimal ptpl) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			EntityTransaction tx = em.getTransaction();
			try{
				tx.begin();
				String sqlUpdate = " UPDATE agent_users SET credit = ?1 , pt_pl = ?2 WHERE id = ?3";	
				Query query = em.createNativeQuery(sqlUpdate);
    		    query.setParameter(1, credit);
    		    query.setParameter(2, ptpl);
    		    query.setParameter(3, id);
    		    query.executeUpdate();
				tx.commit();
				logger.info("persist agent user with user = {} , credit = {} , ptpl= {} into db successful",
						id,credit);
			}
			catch(Exception ex){
				logger.error("update user = {} , credit = {} , ptpl= {} failed with exception : {}",
						id,credit,ex);
			}finally {
				if (tx != null && tx.isActive()) tx.rollback();
			}
		}
		catch(Exception ex){
			logger.error("error while updating credit and ptpl of agent user {}",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}

	@Override
	public BigDecimal calculateTotalTransactionByActualizedOrder(long openOrderId,long userId) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
     		Query query = em.createNativeQuery("select sum(credit_amount) from model_transactions where user_id = ?1 and model_id = ?2");
			
			query.setParameter(1, userId);
			query.setParameter(2, openOrderId);
			
			BigDecimal total = (BigDecimal) query.getSingleResult();
			return total.negate();
		}
		catch(Exception ex){
			logger.error("error while calculate total transaction actulized Po {}",ex);
			return BigDecimal.ZERO;
		}
		finally{
			if(em!=null) em.close();
		}
	}

	@Override
	public ModelTransaction findLatestSettlementMissingBet(long orderId, long userId) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			Query query = em.createQuery("Select t From ModelTransaction t where t.modelId = ?1 and t.type= ?2 and t.user.id = ?3 and remarks like '%missing%bet%' order by t.time desc", ModelTransaction.class);
			query.setParameter(1, orderId);
			query.setParameter(2,  ModelTransactionType.RETURN);
			query.setParameter(3, userId);
			query.setMaxResults(1);
			ModelTransaction returnTrans = (ModelTransaction) query.getResultList().get(0);
			return returnTrans;
			
		}
		catch(Exception ex){
			logger.error("error while finding lastest settlement missing with order {} , id {} exception caught {}",orderId,userId,ex);
		}
		finally{
			if(em!=null) em.close();
		}
		
		return null;
	}

	@Override
	public Map<String, StakePerMatch> findStakePerMatchByUserId(long userId) {
		Map<String, StakePerMatch> result = new HashMap<>();
		EntityManager em = null; 
		try {
			
			// May throw connection exception
			em = dbMgr.getEM();
			List<StakePerMatch> stakePerMatchs =  StakePerMatch.find(em, userId);
			if(!stakePerMatchs.isEmpty()) {
				for (StakePerMatch stakePerMatch : stakePerMatchs) {
					result.put(stakePerMatch.getStakePerMatchPK().getMatchId(), stakePerMatch);
				}
			}
			return result;
		} catch(Exception ex) {
			logger.error("failed to find stake per Match By User Id {} exception caught {}",userId,ex);
			return result;
		} finally {
			if(em != null) em.close();
		}
	}

	@Override
	public BetUserSetting findBetUserSettingById(long userId) {
		EntityManager em = null; 
		try {
			
			// May throw connection exception
			em = dbMgr.getEM();

			BetUserSetting betUserSetting = null;
			Query query = em.createQuery("Select b From BetUserSetting b where b.betUser = ?1", BetUserSetting.class);
			query.setParameter(1, userId);
            betUserSetting = (BetUserSetting) query.getSingleResult();
			return betUserSetting;

		} catch(Exception ex) {
			logger.error("failed to find bet user setting {} exception caught {}",userId,ex);
			return null;
		} finally {
			if(em != null) em.close();
		}
	}

	@Override
	public void persistSPMResults(Set<StakePerMatch> setStakePerMatch) {
		EntityManager em = null;
		try{
			em = dbMgr.getEM();
			for (StakePerMatch stakePerMatch : setStakePerMatch) {
				try{
					stakePerMatch.save(em);
					logger.info("Success persist StakePerMatch with userId {} - matchId {} - sportType {} - startTime {} - totalStake {}",
							stakePerMatch.getStakePerMatchPK().getUserId(),stakePerMatch.getStakePerMatchPK().getMatchId(),stakePerMatch.getSportType(),stakePerMatch.getStartTime(),stakePerMatch.getTotalStake());
				}catch(Exception ex){
					logger.error("Failed persist StakePerMatch with userId {} - matchId {} - sportType {} - startTime {} - totalStake {}, with exception: ",
							stakePerMatch.getStakePerMatchPK().getUserId(),stakePerMatch.getStakePerMatchPK().getMatchId(),stakePerMatch.getSportType(),stakePerMatch.getStartTime(),stakePerMatch.getTotalStake(),ex);
				}
			}
	
		}
		catch(Exception ex){
			logger.error("error while persisting stake per match exception caught: ",ex);
		}
		finally{
			if(em != null) em.close();
		}
		
	}
	
	public String getMatchId(long orderId) {
		EntityManager em = null;
		try {
			em = dbMgr.getEM();
			BettingModelExtraVodds bme = BettingModelExtraVodds.findById(em, orderId);
			return bme.getMatchId();
		}catch (Exception e) {
			logger.error("failed to get match id: ",e);
			return null;
		}finally {
			if(em != null) em.close();
		}
	}
	

	@Override
	public UserAction findUserAction(String actionName) {
		EntityManager em = null;
		try {
			em = dbMgr.getEM();
			Query query = em.createNativeQuery("select * from user_actions where name = ?1", UserAction.class);
			query.setParameter(1, actionName);
			List<UserAction> actions = query.getResultList();
			if (!actions.isEmpty()) {
				return actions.get(0);
			}
		} catch (Exception e) {
			logger.error("failed find user action {}", actionName, e);
		} finally {
			if (em != null) em.close();
		}
		return null;
	}

	@Override
	public UserGameDetail findGameDetail(long gameDetailId) {
		EntityManager em = null;
		try {
			em = dbMgr.getEM();
			return UserGameDetail.findById(em, gameDetailId);
		} catch (Exception e) {
			logger.error("{} failed find game detail", gameDetailId, e);
		} finally {
			if(em != null) {
				em.close();
			}
		}
		return null;
	}

	@Override
	public UserGameTransaction findGameTransaction(long userGameDetailId) {
		EntityManager em = null;
		try {
			em = dbMgr.getEM();
			return UserGameTransaction.findByUserGameDetailId(em, userGameDetailId);
		} catch (Exception e) {
			logger.error("{} Failed find user game transaction. Error: {}", userGameDetailId, e.getMessage(), e);
		} finally {
			if(em != null) {
				em.close();
			}
		}
		return null;
	}

	@Override
	public void persistGameTransactions(List<GameTransactionWrapper> transactions) {
		EntityManager em = null;
		try {
			em = getEm();
			if (transactions.size() > 0) {
				EntityTransaction tx = em.getTransaction();
				tx.begin();
				for (GameTransactionWrapper transactionWrapper : transactions) {
					UserGameTransaction transaction = transactionWrapper.getTransaction();
					em.persist(transaction);

					logger.info("{} is persisted gameDetailId {} amount {}", transaction.getType(), transaction.getUserGameDetail().getId(),
							transaction.getAmount());
				}
				tx.commit();
			}

		} catch (Exception ex) {
			logger.error("error while persisting game transaction exception caught", ex);
		} finally {
			closeEm(em);
		}
	}

	@Override
	public BigDecimal getCachedPlOfBetUser(Long id) {
		EntityManager em = null;
		try{
			logger.info("getCachedPlOfBetUser idUser having orders change {}",id);
			em = dbMgr.getEM();
			String sql = "select pl from bet_users where id = :id";
			Query query = em.createNativeQuery(sql);
		    query.setParameter("id", id);
		    return (BigDecimal)query.getSingleResult();
		}catch(Exception ex){
			logger.error("getCachedPlOfBetUser error while get pl of user have orders changes {} exception caught",id,ex);
			return null;
		}finally{
			if(em != null) em.close();
		}
	}

	private EntityManager getEm() {
		return dbMgr.getEM();
	}

	private void closeEm(EntityManager em) {
		if(em != null) {
			em.close();
		}
	}
}
