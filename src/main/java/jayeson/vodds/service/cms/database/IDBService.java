package jayeson.vodds.service.cms.database;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jayeson.database.*;
import jayeson.database.newvodds.*;
import jayeson.vodds.service.cms.data.*;
import jayeson.vodds.service.cms.data.game.GameTransactionWrapper;
import jayeson.vodds.service.cms.order.settlement.MPLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.OutstandingNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PLNotificationTask;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.transactions.TransactionQueue;

/**
 * An interface for the database service provider that will facilitate helper methods to required operations
 * 
 * @author Praveen
 *
 */
public interface IDBService {
	
	/**
	 * finds the bet user entity given the id
	 * @param id
	 * @return
	 */
	public VoddsBetUser findVoddsBetUser(long id);
	
	public BetUser findBetUser(long id);
	
	/**
	 * finds the list of all parent agent users given the bet user id
	 * @param betUserId
	 * @return
	 */
	public List<AgentUser> findAllAgentOfBetUser(long betUserId);
	public AgentUser findAgentUser(long id);
	/**
	 * persist the list of database transactions
	 * @param transactions
	 */
	public void persistResults(List transactions);
	/**
	 * find the bet entity given the id
	 * @param id
	 * @return
	 */
	public Bet findBetById(String id);
	/**
	 * find the betting model entity given the model id
	 * @param id
	 * @return
	 */
	public BettingModel findStoppedOrderById(long id);
	
	/**
	 * find the list of bets given the order id. If the order is of type positional close, the function
	 * returns the list of both closing order and opening order bets
	 * 
	 * @param orderId
	 * @param oType
	 * @return
	 */
	public List<Bet> findBetsByOrderType(long orderId, String oType);
	
	/***
	 * retrieves the list of bets by the id of betting model
	 * @param orderId
	 * @return
	 */
	public List<Bet> findBetsByOrder(long orderId);
	
	/**
	 * find the list of completed orders that are unsettled
	 * @return
	 */
	public List<BettingModel> findUnsettledOrders(int limitStart, int limitEnd);
	
	/**
	 * compute the number of possible unsettled orders in database
	 * @return
	 */
	public int findUnsettledOrdersCount();

	/**
	 * compute the number of possible unsettled orders with the match not started in database
	 * @return
	 */
	public int findUnsettledOrdersWithMatchNotStartedCount();

	/**
	 * find the list of completed orders with the match not started that are unsettled
	 * @return
	 */
	public List<BettingModel> findUnsettledOrdersWithMatchNotStarted(int limitStart, int limitEnd);
	
	/***
	 * find the list of dark pool bets that are resolved and ready to be settled
	 * @return
	 */
	public List<DarkPoolBetInfo> findUnsettledDPoolBets();
	
	/***
	 * find the list of liquidation mappings associated with the dark pool bet
	 * @return
	 */
	public List<DPoolLiquidationMapping> fetchLiquidationMappings(String dBetId);
	
	/***
	 * fetches the list of liquidating real bets associated with the dark pool bet
	 * @return
	 */
	public List<Bet> fetchLiquidatingBets(String dBetId);
	
	/**
	 * fetch the list of transactions used to calculate the margin transactions of opening order
	 * @param modelIds
	 * @return
	 */
	public List<ModelTransaction> findMarginTransactions(List<Long> modelIds, long userId);
	
	/**
	 * find the corresponding opening order of a positional order given the closing order id
	 * @param orderId
	 * @return
	 */
	public BettingModel findOpeningOrder(long orderId);
	
	/**
	 * find the corresponding opening order of a positional order given the closing order id
	 * @param orderId
	 * @return
	 */
	public BettingModel findClosingOrder(long orderId);
	
	/***
	 * find the positional model entry given the closing order id
	 * @param orderId
	 * @return
	 */
	public PositionalModel findPositionalModelByCloseId(long closeId);
	
	/***
	 * find the positional model entry given the open or close model id
	 * @param orderId
	 * @return
	 */
	public PositionalModel findPositionalModel(long bettingModelId);
	
	
	/**
	 * find the most recent return amount transaction given the order id
	 */
	public ModelTransaction findLatestReturnTransaction(long orderId,long userId);
	
	/**
	 * 
	 * find the most recent return amount transaction ptpl given the order id, user id
	 */
	public ModelTransaction findLatestReturnPTPLTransaction(long orderId,long userId);
	
	/**
	 * find lastest refund transaction of voiding order
	 * @param orderId
	 * @param userId
	 * @return
	 */
	public ModelTransaction findLastestRefundTransaction(long orderId,long userId);

	/**
	 * converts the user commission value to agent currency
	 * @param aNode
	 * @param bNode
	 * @param commValue
	 * @return
	 */
	public BigDecimal getAgentCommission(AgentUserNode aNode, BetUserNode bNode,
			BigDecimal commValue);
	
	/**
	 * fetches the vodds betting model extra entity given the model id
	 * @param modelId
	 * @return
	 */
	
	public BettingModelExtraVodds getModelExtraEntity(long modelId);
	
	/**
	 * find and fetch bets given a list of id 
	 * @param idList
	 * @return
	 */
	public List<Bet> findBetListById(List<String> idList);
	
	/**
	 * persist the HashMap of database transactions
	 * @param transactions
	 */
	public void persistResults(Set<?> transactions);
	
	/**
	 * 
	 * @param transactions
	 */
	public void persistBMResults(Set<BettingModel> transactions,OrderWrapperManager owManger,PLNotificationTask plNotificationTask
			, MPLNotificationTask mplNotificationTask);
	
	public Service getServiceById(String id);
	
	public BigDecimal getConversionRate(String currencyChildUser,String currencyUserParrent);
	
	/**
	 * persist obj
	 * @param obj
	 */
	public void persistResults(Object obj);
	
	public void saveUserCredit(VoddsBetUser betUser);
	
	public void saveAgentUserCredit(Long id,BigDecimal credit);
	
	public void saveAgentUserCreditAndPtpl(Long id,BigDecimal credit,BigDecimal ptpl);
	
	/**
	 * get user, pl having status of orders is changed
	 * @param id
	 * @return
	 */
	public BigDecimal getPlOfBetUser(Long id);
	
	public BigDecimal getCachedPlOfBetUser(Long id);
	
	public void persistBUResults(Set<VoddsBetUser> setBetUser);
	
	public void persistAUResults(Set<AgentUser> setAgentUser);
	
	/**
	 * 
	 * @param setPositionModel
	 */
	public void persistPMResults(Set<PositionalModel> setPositionModel, 
			MPLNotificationTask mplNotificationTask,NoQPersister noQ);
	
	/**
	 * 
	 * @param setBMEV
	 */
	public void persistBMEVResults(Set<BettingModelExtraVodds> setBMEV,OutstandingNotificationTask amtNotificationTask);
	
	/**
	 * 
	 * @param transactions
	 */
	public void persistModelTransaction(List<ModelTransaction> transactions,TransactionQueue tQueue);
	
	/**
	 * 
	 * @param transactions
	 */
	public void persistTransaction(List<TransactionWrapper> transactions);
	
	/**
	 * 
	 * @param transactions
	 */
	public void persistDPTransaction(List<DarkPoolBetInfo> transactions);
	
	/**
	 * find all transaction given the order id
	 */
	public List<ModelTransaction> findLatestReturnTransactionForOperator(long orderId,long userId);
	
	/**
	 * compute the number of possible voiding orders in database
	 * @return
	 */
	public int findVoidingOrdersCount();
	
	/**
	 * find the list of voiding orders
	 * @return
	 */
	public List<BettingModel> findVoidingOrders(int limitStart, int limitEnd);
	
	
	public List<ModelTransaction> findAllTransactionByOrderId(long orderId, ModelTransactionType typeTransaction);
	
	public BettingModel findBettingModelById(long id);
	
	/**
	 * 
	 * @param transactions
	 */
	public void updateBMResults(Set<BettingModel> transactions,OrderWrapperManager owManger);
	
	/**
	 * 
	 * @param userId
	 * @param orderId
	 * @return
	 */
	public int checkOrderIsValid(long userId,long orderId);
	
	public Sportbook findSportbookById(String id);
	
	public void persistAgentPtPl(Map<Long, List<BettingModelAgentPtPl>> agentPtPl);
	
	public void persistAgentPtPl(BettingModelAgentPtPl agentPtPl);

	public void persistBetOfAgentPtPl(Map<String, List<BetAgentPtPl>> betAgentPtPl);

	public void persistBetOfAgentPtPl(BetAgentPtPl betAgentPtPl);
	
	public BigDecimal calculateTotalTransactionByActualizedOrder(long openOrderId,long userId);
	
	public int findUnsettledWithAcceptedBetCount();

	public boolean checkExistLastRevertTransactionsUnsettledOrder(long modelId);

	public List<BettingModel> findUnsettledWithAcceptedBet();
	
	public ModelTransaction findLatestSettlementDarkpool(long orderId,long userId);
	
	public ModelTransaction findLatestSettlementMissingBet(long orderId,long userId);
	
	public Map<String, StakePerMatch> findStakePerMatchByUserId(long userId);
	
	public BetUserSetting findBetUserSettingById(long userId);
	
	public void persistSPMResults(Set<StakePerMatch> setStakePerMatch);
	
	public String getMatchId(long orderId);

	UserAction findUserAction(String actionName);

	/**
	 * Find user game detail
	 * @param gameDetailId
	 * @return
	 */
	UserGameDetail findGameDetail(long gameDetailId);
	/**
	 * Find user game transaction
	 * @param id
	 * @return
	 */
	UserGameTransaction findGameTransaction(long id);

	/**
	 * Persist game transactions
	 * @param transactions
	 */
	void persistGameTransactions(List<GameTransactionWrapper> transactions);
}
