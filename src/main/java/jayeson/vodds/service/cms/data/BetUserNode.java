package jayeson.vodds.service.cms.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.events.CMSEvent;
import jayeson.vodds.service.cms.events.UserCreditChangeEvent;
import jayeson.database.AgentUser;
import jayeson.database.BetUser;
import jayeson.database.newvodds.StakePerMatch;
import jayeson.database.newvodds.VoddsBetUser;

/**
 * Data structure to store the information related to the individual bet user, and 
 * @author Praveen
 *
 */
public class BetUserNode {
	
	static Logger logger = LoggerFactory.getLogger(BetUserNode.class.getName());

	
	// cached bet user credit
	BigDecimal credit;
	
	// cached bet user outstanding_amt
	BigDecimal outstandingAmount;
	
	// cached bet user pl
	BigDecimal pl;
	
	//cache bet user positional_potential_loss
	BigDecimal positionalPotentialLoss;
	
	// database entry corresponding to the node 
	VoddsBetUser user;
	
	/*** a list of agent users that the bet user belongs to, 
	 the hierarchy of agents are not required as the objective is only to add/deduct commissions 
	 of valid agent users*/
	List<AgentUserNode> agentUsers;
	
	Map<String,StakePerMatch> mapStakePerMatch;
	
	BigDecimal maxBetNormal;
	
	BigDecimal maxBetPositional;
	
	BigDecimal maxBetPerMatch;
	
	BigDecimal maxLoss;

	// these fields for displaying error message
	BigDecimal defaultMaxBetNormal;

	BigDecimal defaultMaxBetPositional;

	BigDecimal defaultMaxBetPerMatch;

	BigDecimal defaultMaxLoss;

	BigDecimal userMaxBetNormal;

	BigDecimal userMaxBetPositional;

	BigDecimal userMaxBetPerMatch;

	BigDecimal userMaxLoss;
	
	// user manager instance that manages the bet user node
	UserManager uManager;
	
	private final Semaphore sem = new Semaphore(1, true);

	public BetUserNode(){
		
		agentUsers = new ArrayList<AgentUserNode>();
		mapStakePerMatch = new ConcurrentHashMap<>();
	}


	/***
	 *  make the credit call thread safe
	 * @return
	 */
	public BigDecimal getCredit() {
		try{
			sem.acquire();
			return credit;	
		}
		catch(InterruptedException e){
			return null;
		}
		finally{
			sem.release();
		}
		
	}

	// make the credit call thread safe
	public void setCredit(BigDecimal credit) {
		try{
			sem.acquire();
			this.credit = credit;
		}
		catch(InterruptedException e){
		}
		finally{
			sem.release();
		}

	}
	
	
	
	public BigDecimal getOutstandingAmount() {
		try{
			sem.acquire();
			return outstandingAmount;
		}
		catch(InterruptedException e){
			return null;
		}
		finally{
			sem.release();
		}
	}


	public void setOutstandingAmount(BigDecimal outstandingAmount) {
		try{
			sem.acquire();
			this.outstandingAmount = outstandingAmount;
		}
		catch(InterruptedException e){
		}
		finally{
			sem.release();
		}
	}


	public BigDecimal getPl() {
		try{
			sem.acquire();
			return pl;
		}
		catch(InterruptedException e){
			return null;
		}
		finally{
			sem.release();
		}
	}


	public void setPl(BigDecimal pl) {
		try{
			sem.acquire();
			this.pl = pl;
		}
		catch(InterruptedException e){
		}
		finally{
			sem.release();
		}
	}
	
	public BigDecimal[] addPositionalPotentialLoss(BigDecimal positionalPotentialLoss, long orderId){
		List<BigDecimal> lstPositionalPotentialLoss = new ArrayList<>();
		try{
			sem.acquire();
			lstPositionalPotentialLoss.add(this.positionalPotentialLoss);
			this.positionalPotentialLoss = this.positionalPotentialLoss.add(positionalPotentialLoss);
			this.user.setPositionalPotentialLoss(this.user.getPositionalPotentialLoss().add(positionalPotentialLoss));
			lstPositionalPotentialLoss.add(this.positionalPotentialLoss);
			logger.info("add {} positionalPotentialLoss update for user id {} order id {} - positionalPotentialLoss before {} - positionalPotentialLoss after {}",
					positionalPotentialLoss,user.getId(), orderId, lstPositionalPotentialLoss.get(0),lstPositionalPotentialLoss.get(1));
			return (BigDecimal[]) lstPositionalPotentialLoss.toArray(new BigDecimal[lstPositionalPotentialLoss.size()]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to add new value for positionalPotentialLoss ",e);
			return null;
		}
		finally{
			sem.release();
		}
	}

	public BigDecimal[] addOutstanding(BigDecimal outstandingAmount, long orderId){
		List<BigDecimal> lstOutstanding = new ArrayList<>();
		try{
			sem.acquire();
			lstOutstanding.add(this.outstandingAmount);
			this.outstandingAmount = this.outstandingAmount.add(outstandingAmount);
			this.user.setOutstandingAmount(this.user.getOutstandingAmount().add(outstandingAmount));
			lstOutstanding.add(this.outstandingAmount);
			logger.info("add {} outstanding update for user id {} order id {} - outstanding before {} - outstanding after {}",
					outstandingAmount,user.getId(), orderId, lstOutstanding.get(0),lstOutstanding.get(1));
			return (BigDecimal[]) lstOutstanding.toArray(new BigDecimal[lstOutstanding.size()]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to add new value for outstanding ",e);
			return null;
		}
		finally{
			sem.release();
		}
	}
	
	public BigDecimal[] addPl(BigDecimal pl, long orderId){
		List<BigDecimal> lstPl = new ArrayList<>();
		try{
			sem.acquire();
			lstPl.add(this.pl);
			this.pl = this.pl.add(pl);
			this.user.setpL(this.user.getpL().add(pl));
			lstPl.add(this.pl);
			logger.info("add {} pl update for user id {} order id {} - pl before {} - pl after {}",pl,user.getId(), orderId, lstPl.get(0),lstPl.get(1));
			return (BigDecimal[]) lstPl.toArray(new BigDecimal[lstPl.size()]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to add new value for pl ",e);
			return null;
		}
		finally{
			sem.release();
		}
	}

	// a thread safe method to add credit to the bet user node
	public BigDecimal[] addCredit(BigDecimal credit, long orderId){
		List<BigDecimal> lstCredit = new ArrayList<>();
		try{
			sem.acquire();
			lstCredit.add(this.credit);
			this.credit = this.credit.add(credit);
			this.user.setCredit(this.user.getCredit().add(credit));
			lstCredit.add(this.credit);
			// post the message to event bus, to be listened by thrift server
			logger.info("credit update message posted to event bus for user id {} order id {} and value {}",user.getId(), orderId,
					this.credit.doubleValue());
			
			this.uManager.geteBus().post(new UserCreditChangeEvent(user.getUser().getUsername(),this.credit.doubleValue(),user.getId()));
			return (BigDecimal[]) lstCredit.toArray(new BigDecimal[lstCredit.size()]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to add new value for credit ",e);
			return null;
		}
		finally{
			sem.release();
			// send a communicator message here
		}

		
	}
	
	public BigDecimal[] moveCredit(BigDecimal refund,BigDecimal reserve, long orderId){
		List<BigDecimal> lstCredit = new ArrayList<>();
		try{
			sem.acquire();
			
			lstCredit.add(this.credit);
			this.credit = this.credit.add(refund);
			this.user.setCredit(this.user.getCredit().add(refund));
			lstCredit.add(this.credit);
			this.credit = this.credit.add(reserve);
			this.user.setCredit(this.user.getCredit().add(reserve));
			lstCredit.add(this.credit);
			// post the message to event bus, to be listened by thrift server
			logger.info("credit update message posted to event bus for user id {} order id {} and value {}",user.getId(), orderId, this.credit.doubleValue());
			
			this.uManager.geteBus().post(new UserCreditChangeEvent(user.getUser().getUsername(),this.credit.doubleValue(),user.getId()));
			return (BigDecimal[]) lstCredit.toArray(new BigDecimal[lstCredit.size()]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to add new value for credit ",e);
			return null;
		}
		finally{
			sem.release();
			// send a communicator message here
		}

		
	}
	
	public BigDecimal[] moveCreditCommission(BigDecimal refund,BigDecimal reserve,BigDecimal commissionVal){
		List<BigDecimal> lstCredit = new ArrayList<>();
		try{
			sem.acquire();
			
			lstCredit.add(this.credit);
			this.credit = this.credit.add(refund);
			this.user.setCredit(this.user.getCredit().add(refund));
			lstCredit.add(this.credit);
			this.credit = this.credit.add(reserve);
			this.user.setCredit(this.user.getCredit().add(reserve));
			lstCredit.add(this.credit);
			this.credit = this.credit.add(commissionVal);
			this.user.setCredit(this.user.getCredit().add(commissionVal));
			lstCredit.add(this.credit);
			// post the message to event bus, to be listened by thrift server
			logger.info("credit update message posted to event bus for user id {} and value {}",user.getId(),this.credit.doubleValue());
			
			this.uManager.geteBus().post(new UserCreditChangeEvent(user.getUser().getUsername(),this.credit.doubleValue(),user.getId()));
			return (BigDecimal[]) lstCredit.toArray(new BigDecimal[lstCredit.size()]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to add new value for credit ",e);
			return null;
		}
		finally{
			sem.release();
			// send a communicator message here
		}

		
	}

	public VoddsBetUser getUser() {
		return user;
	}

	public void setUser(VoddsBetUser user) {
		this.user = user;
	}


	public List<AgentUserNode> getAgentUsers() {
		return agentUsers;
	}

	public void setAgentUsers(List<AgentUserNode> agentUsers) {
		this.agentUsers = agentUsers;
	}
	
	public synchronized void addAgentUser(AgentUserNode aNode){
		this.agentUsers.add(aNode);
	}
	
	public Map<String, StakePerMatch> getMapStakePerMatch() {
		return mapStakePerMatch;
	}


	public void setMapStakePerMatch(Map<String, StakePerMatch> mapStakePerMatch) {
		this.mapStakePerMatch = mapStakePerMatch;
	}
	
	public void addMapStakePerMatchCahe(Map<String, StakePerMatch> mapStakePerMatch) {
		this.mapStakePerMatch.putAll(mapStakePerMatch);
	}


	public UserManager getuManager() {
		return uManager;
	}


	public void setuManager(UserManager uManager) {
		this.uManager = uManager;
	}


	public BigDecimal getMaxBetNormal() {
		try{
			sem.acquire();
			return maxBetNormal;
		}catch(InterruptedException e){
			return null;
		}
		finally{
			sem.release();
		}
	}


	public void setMaxBetNormal(BigDecimal maxBetNormal) {
		try{
			sem.acquire();
			this.maxBetNormal = maxBetNormal;
		}catch(InterruptedException e){
		}
		finally{
			sem.release();
		}
	}


	public BigDecimal getMaxBetPositional() {
		try{
			sem.acquire();
			return maxBetPositional;
		}catch(InterruptedException e){
			return null;
		}
		finally{
			sem.release();
		}
	}


	public void setMaxBetPositional(BigDecimal maxBetPositional) {
		try{
			sem.acquire();
			this.maxBetPositional = maxBetPositional;
		}catch(InterruptedException e){
			
		}
		finally{
			sem.release();
		}
	}


	public BigDecimal getMaxBetPerMatch() {
		try{
			sem.acquire();
			return maxBetPerMatch;
		}catch(InterruptedException e){
			return null;	
		}
		finally{
			sem.release();
		}
	}


	public void setMaxBetPerMatch(BigDecimal maxBetPerMatch) {
		try{
			sem.acquire();
			this.maxBetPerMatch = maxBetPerMatch;
		}catch(InterruptedException e){
		}
		finally{
			sem.release();
		}
	}


	public BigDecimal getMaxLoss() {
		try{
			sem.acquire();
			return maxLoss;
		}catch(InterruptedException e){
			return null;	
		}
		finally{
			sem.release();
		}
	}


	public void setMaxLoss(BigDecimal maxLoss) {
		try{
			sem.acquire();
			this.maxLoss = maxLoss;
		}catch(InterruptedException e){
		}
		finally{
			sem.release();
		}
	}


	public BigDecimal getPositionalPotentialLoss() {
		try{
			sem.acquire();
			return positionalPotentialLoss;
		}catch(InterruptedException e){
			return null;
		}
		finally{
			sem.release();
		}
	}


	public void setPositionalPotentialLoss(BigDecimal positionalPotentialLoss) {
		try{
			sem.acquire();
			this.positionalPotentialLoss = positionalPotentialLoss;
		}catch(InterruptedException e){
		}
		finally{
			sem.release();
		}
	}
	
	public void clearStakePerMatchCache() {
		mapStakePerMatch.clear();
	}

	public BigDecimal getDefaultMaxBetNormal() {
		try {
			sem.acquire();
			return defaultMaxBetNormal;
		}catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setDefaultMaxBetNormal(BigDecimal defaultMaxBetNormal) {
		try {
			sem.acquire();
			this.defaultMaxBetNormal = defaultMaxBetNormal;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}

	public BigDecimal getDefaultMaxBetPositional() {
		try {
			sem.acquire();
			return defaultMaxBetPositional;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setDefaultMaxBetPositional(BigDecimal defaultMaxBetPositional) {
		try {
			sem.acquire();
			this.defaultMaxBetPositional = defaultMaxBetPositional;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}

	public BigDecimal getDefaultMaxBetPerMatch() {
		try {
			sem.acquire();
			return defaultMaxBetPerMatch;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setDefaultMaxBetPerMatch(BigDecimal defaultMaxBetPerMatch) {
		try {
			sem.acquire();
			this.defaultMaxBetPerMatch = defaultMaxBetPerMatch;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}

	public BigDecimal getDefaultMaxLoss() {
		try {
			sem.acquire();
			return defaultMaxLoss;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setDefaultMaxLoss(BigDecimal defaultMaxLoss) {
		try {
			sem.acquire();
			this.defaultMaxLoss = defaultMaxLoss;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}

	public BigDecimal getUserMaxBetNormal() {
		try {
			sem.acquire();
			return userMaxBetNormal;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}

	}

	public void setUserMaxBetNormal(BigDecimal userMaxBetNormal) {
		try {
			sem.acquire();
			this.userMaxBetNormal = userMaxBetNormal;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}

	}

	public BigDecimal getUserMaxBetPositional() {
		try {
			sem.acquire();
			return userMaxBetPositional;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setUserMaxBetPositional(BigDecimal userMaxBetPositional) {
		try {
			sem.acquire();
			this.userMaxBetPositional = userMaxBetPositional;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}

	public BigDecimal getUserMaxBetPerMatch() {
		try {
			sem.acquire();
			return userMaxBetPerMatch;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setUserMaxBetPerMatch(BigDecimal userMaxBetPerMatch) {
		try {
			sem.acquire();
			this.userMaxBetPerMatch = userMaxBetPerMatch;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}

	public BigDecimal getUserMaxLoss() {
		try {
			sem.acquire();
			return userMaxLoss;
		} catch (InterruptedException e) {
			return null;
		} finally {
			sem.release();
		}
	}

	public void setUserMaxLoss(BigDecimal userMaxLoss) {
		try {
			sem.acquire();
			this.userMaxLoss = userMaxLoss;
		} catch (InterruptedException e) {
		} finally {
			sem.release();
		}
	}
	
	
	public boolean isCheckTradeLimit() {
		if (this.uManager.excludeTradeLimitList.contains(user.getId())) {
			return false;
		}
		for (AgentUserNode agentNode : agentUsers) {
			if (this.uManager.excludeTradeLimitList.contains(agentNode.getUser().getId())) {
				return false;
			}
		}
		return true;
	}
}
