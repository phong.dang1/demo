package jayeson.vodds.service.cms.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.database.Bet;
import jayeson.database.BettingModel;
import jayeson.database.newvodds.PositionalModel;
import jayeson.vodds.orders.common.VOddsModelType;
import jayeson.vodds.orders.normal.NormalOrderStatus;
import jayeson.vodds.orders.positional.PositionalCloseStatus;
import jayeson.vodds.orders.positional.PositionalOpenStatus;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.database.IDBService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/*** Storage of all orders and constituent bets that are processed by CMS
 * 
 * Normal Order - collection of model id and all the corresponding bets
 * Positional Order - collection of closing model id and all corresponding bets in
 * opening and closing order
 * 
 * @author Praveen
 *
 */

@Singleton
public class OrderWrapperManager {
	
	static Logger logger = LoggerFactory.getLogger(OrderWrapperManager.class.getName());


	static Set<String> unsettledStatuses = new HashSet<String>(Arrays.asList(NormalOrderStatus.REJECTED.name()
			,NormalOrderStatus.CONFIRMED.name(),NormalOrderStatus.PARTIAL.name(),NormalOrderStatus.STOPPED.name()));

	static Set<String> rejectedStatuses = new HashSet<String>(Arrays.asList(BetStatus.CANCELLED.name(),BetStatus.EXCHANGE_DELETED.name(),BetStatus.REJECTED.name(),BetStatus.REJECTED_ABNORMAL.name()));
	
	static Set<String> unsettledStatusesWithAcceptedBet = new HashSet<String>(Arrays.asList(NormalOrderStatus.CONFIRMED.name(),NormalOrderStatus.PARTIAL.name()));

	
	// cache of all orders handled by cms
	Map<Long,OrderWrapper> orderCache;

	// collection of bets grouped by the order id
	//HashMultimap<Long,BetWrapper> orderBets;
	
	// mapping of opening and closing order id of a positional order
	// creates a bimap to look for bidirectional lookup
	BiMap<Long,Long> positionalIdMap;
	
	
		
	IDBService dbHelper;
	ConfigBean cBean;
	
	@Inject
	public OrderWrapperManager(IDBService helper,ConfigBean bean){
		
		dbHelper = helper;
		cBean = bean;
		//orderBets = HashMultimap.create();
		positionalIdMap = HashBiMap.create();
		//orderCache = CacheBuilder.newBuilder().expireAfterWrite(cBean.getOrderExpiryInterval(), TimeUnit.SECONDS).build();
		orderCache = new ConcurrentHashMap<Long, OrderWrapper>();
	}
	
	public OrderWrapper createOrderWrapperForUnsettledOrder(BettingModel bm){
		OrderWrapper wrapper = orderCache.get(bm.getId());
		
		if(wrapper != null){
			return wrapper;

		}else{
			try{
				BettingModel dModel = dbHelper.findBettingModelById(bm.getId());
				String orderType = dModel.getModelType();
				List<Bet> dBets = new ArrayList<Bet>();
				List<Bet> openBets = new ArrayList<Bet>();
				List<Bet> closeBets = new ArrayList<Bet>();
				if(unsettledStatusesWithAcceptedBet.contains(dModel.getModelStatus()) && dModel.IsModified() == 1){
					logger.info("order {} - create order wrapper with status {} - is_modified {}",dModel.getId(),dModel.getModelStatus(),dModel.IsModified());
					if(orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name()) || orderType.equals("VOFutureNormal") || orderType.equals(VOddsModelType.FUTURE_NORMAL.name())){
						dBets.addAll(dbHelper.findBetsByOrder(dModel.getId()));
						OrderWrapper oWrapper = new OrderWrapper(dModel, dModel.getModelStatus());
						for (Bet dBet : dBets) {
							BetWrapper bWrapper = new BetWrapper();
							bWrapper.setBet(dBet);
							oWrapper.getBetWrappers().add(bWrapper);
						}
						logger.info("order {} - push bet into order wrapper with size of bet cache {}",dModel.getId(),oWrapper.getBetWrappers().size());
						orderCache.put(bm.getId(), oWrapper);
						return oWrapper;
					} else if(orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name()) || orderType.equals("VOFutureClose") || orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())){
						PositionalModel pModel = dbHelper.findPositionalModelByCloseId(bm.getId());
						BettingModel openModel = pModel.getOpenModelId();
						PositionalOrderWrapper oWrapper = new PositionalOrderWrapper(dModel,dModel.getModelStatus());
						openBets = dbHelper.findBetsByOrder(openModel.getId());
						closeBets = dbHelper.findBetsByOrder(dModel.getId());
						return addBetForOrderWrapper(oWrapper,pModel,bm.getId(),openModel,dBets,openBets,closeBets);
					}else if(orderType.equals("VOOpen") || orderType.equals(VOddsModelType.POSITIONAL_OPEN.name()) || orderType.equals("VOFutureOpen") || orderType.equals(VOddsModelType.FUTURE_POSITIONAL_OPEN.name())){
						BettingModel closeModel = dbHelper.findClosingOrder(bm.getId());
						PositionalModel pModel = dbHelper.findPositionalModelByCloseId(closeModel.getId());
						PositionalOrderWrapper oWrapper = new PositionalOrderWrapper(closeModel,dModel.getModelStatus());
						openBets = dbHelper.findBetsByOrder(bm.getId());
						closeBets = dbHelper.findBetsByOrder(closeModel.getId());
						return addBetForOrderWrapper(oWrapper,pModel,closeModel.getId(),dModel,dBets,openBets,closeBets);
						
					}else{
						return null;
					}
				}else{
					logger.warn("unidentified model type {}",dModel.getModelType());
					return null;
				}
				
			}catch(Exception ex){
				logger.error("Exception while crteating order wraper for order {}",bm.getId());
				return null;
			}
		}
	}
	
	/***
	 * finds and loads the betting model corresponding to the model id, into cms cache
	 * @param id
	 * @return
	 */
	
	public synchronized OrderWrapper findStoppedOrderById(long id){
		
		OrderWrapper wrapper = orderCache.get(id);
		
		if(wrapper != null){
			return wrapper;

		}else{
	
			try{
				BettingModel dModel = dbHelper.findBettingModelById(id);
				String orderType = dModel.getModelType();
				List<Bet> dBets = new ArrayList<Bet>();
				List<Bet> openBets = new ArrayList<Bet>();
				List<Bet> closeBets = new ArrayList<Bet>();
				if(orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name()) || orderType.equals("VOFutureNormal") || orderType.equals(VOddsModelType.FUTURE_NORMAL.name())){
					
					dBets.addAll(dbHelper.findBetsByOrder(id));
					
					if((dModel.getModelStatus().equals(NormalOrderStatus.MODIFIED.name()) || dModel.IsModified() == 1) && isAllBetsResolved(dBets)){
						
						OrderWrapper oWrapper = new OrderWrapper(dModel,NormalOrderStatus.MODIFIED.name());
						logger.debug("adding normal order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
						
						for(Bet dBet:dBets){
							
							BetWrapper bWrapper = new BetWrapper();
							bWrapper.setBet(dBet);
							oWrapper.getBetWrappers().add(bWrapper);
						}
						orderCache.put(id, oWrapper);
						
						return oWrapper;
					}
					
					else if(unsettledStatuses.contains(dModel.getModelStatus()) && isAllBetsResolved(dBets)){
						OrderWrapper oWrapper = new OrderWrapper(dModel,dModel.getModelStatus());
						logger.debug("adding normal order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
						
						for(Bet dBet:dBets){
							
							BetWrapper bWrapper = new BetWrapper();
							bWrapper.setBet(dBet);
							oWrapper.getBetWrappers().add(bWrapper);
						}
						orderCache.put(id, oWrapper);
						
						return oWrapper;
				
					}else{
						return null;
					}
				}
				else if(orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name()) || orderType.equals("VOFutureClose") || orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())){
					
					// set the positional order object
					PositionalModel pModel = dbHelper.findPositionalModelByCloseId(id);
					BettingModel openModel = pModel.getOpenModelId();
					PositionalOrderWrapper oWrapper = null;
					logger.info("close order {} with execution stage {}",dModel.getId(),pModel.getExecutionStage());
					if(!pModel.getExecutionStage().equals(PositionalOrderStage.CLOSE_FAILED.name())){
					
						openBets = dbHelper.findBetsByOrder(openModel.getId());
						closeBets = dbHelper.findBetsByOrder(dModel.getId());
						
						if((dModel.getModelStatus().equals(NormalOrderStatus.MODIFIED.name()) || dModel.IsModified() == 1) && isAllBetsResolved(closeBets) && isAllBetsResolved(openBets)){
							oWrapper = new PositionalOrderWrapper(dModel,PositionalCloseStatus.MODIFIED.name());
							logger.debug("adding positional order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
							return addBetForOrderWrapper(oWrapper,pModel,id,openModel,dBets,openBets,closeBets);
						}
//						else if(dModel.getModelStatus().equals(NormalOrderStatus.UNFULFILLED.name()) && isAllBetsResolved(openBets) && !isAllBetsRejected(openBets) && unsettledStatuses.contains(openModel.getModelStatus())){
//							oWrapper = new PositionalOrderWrapper(dModel,dModel.getModelStatus());
//							logger.debug("adding positional order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
//							return addBetForOrderWrapper(oWrapper,pModel,id,openModel,dBets,openBets,closeBets);
//						}
//						else if (dModel.getModelStatus().equals(PositionalCloseStatus.UNFULFILLED.name()) && isAllBetsOnOpenResolved(openBets) && pModel.getExecutionStage().equals(PositionalOrderStage.VOIDING.name())){
//							oWrapper = new PositionalOrderWrapper(dModel,dModel.getModelStatus());
//							logger.debug("adding positional order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
//							return addBetForOrderWrapper(oWrapper,pModel,id,openModel,dBets,openBets,closeBets);
//						}
						else if(unsettledStatuses.contains(dModel.getModelStatus()) && isAllBetsResolved(closeBets) && isAllBetsResolved(openBets)){
							oWrapper = new PositionalOrderWrapper(dModel,dModel.getModelStatus());
							logger.debug("adding positional order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
							return addBetForOrderWrapper(oWrapper,pModel,id,openModel,dBets,openBets,closeBets);
						}else{
							return null;
						}
					}else{
						return null;
					}
				}
				else if(orderType.equals("VOOpen") || orderType.equals(VOddsModelType.POSITIONAL_OPEN.name()) || orderType.equals("VOFutureOpen") || orderType.equals(VOddsModelType.FUTURE_POSITIONAL_OPEN.name())){
					
					// set the positional order object
					BettingModel closeModel = dbHelper.findClosingOrder(id);
					PositionalModel pModel = dbHelper.findPositionalModelByCloseId(closeModel.getId());

					PositionalOrderWrapper oWrapper = null;
					logger.info("open order {} with execution stage {}",dModel.getId(),pModel.getExecutionStage());
					
					if(!pModel.getExecutionStage().equals(PositionalOrderStage.CLOSE_FAILED.name())){
						
						openBets = dbHelper.findBetsByOrder(id);
						closeBets = dbHelper.findBetsByOrder(closeModel.getId());
						
						if((dModel.getModelStatus().equals(PositionalOpenStatus.MODIFIED.name()) || dModel.IsModified() == 1) && isAllBetsResolved(openBets)){
							oWrapper = new PositionalOrderWrapper(closeModel,PositionalOpenStatus.MODIFIED.name());
							return addBetForOrderWrapper(oWrapper,pModel,closeModel.getId(),dModel,dBets,openBets,closeBets);
						}// #Case close order is unfulfilled and open order has unsettle status , all bet for close and open order are resolve (status of bets on open order should not be rejected ) 
						else if(closeModel.getModelStatus().equals(NormalOrderStatus.UNFULFILLED.name()) && isAllBetsResolved(openBets) && !isAllBetsRejected(openBets) && unsettledStatuses.contains(dModel.getModelStatus())){
							oWrapper = new PositionalOrderWrapper(closeModel,closeModel.getModelStatus());
							logger.debug("adding positional order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
							return addBetForOrderWrapper(oWrapper,pModel,closeModel.getId(),dModel,dBets,openBets,closeBets);
						}// #Case positional order is voiding 
						else if (closeModel.getModelStatus().equals(PositionalCloseStatus.UNFULFILLED.name()) && isAllBetsOnOpenResolved(openBets) && pModel.getExecutionStage().equals(PositionalOrderStage.VOIDING.name())){
							oWrapper = new PositionalOrderWrapper(closeModel,closeModel.getModelStatus());
							logger.debug("adding positional order bet list of size {} to order {} with status {}",dBets.size(),id,oWrapper.getOrderSettlement());
							return addBetForOrderWrapper(oWrapper,pModel,closeModel.getId(),dModel,dBets,openBets,closeBets);
						}else{
							return null;
						}
					}else{
						return null;
					}
					
				}
				else{
					logger.warn("unidentified model type {}",dModel.getModelType());
					return null;
				}
				
			}
			catch(Exception ex){
				logger.error("error while retrieving order wrapper for id {}",id,ex);
				return null;
			}
			
		}
	}
	
	public void removeOrderFromCache(long id){
		if(this.orderCache.containsKey(id)){
			logger.info("remove order id {} from order cache",id);
			orderCache.remove(id);
		}
	}
	
	public boolean existOrderCache(long orderId){
		if(this.orderCache.containsKey(orderId)){
			logger.info("order id {} is still order cache",orderId);
			return true;
		}
		return false;
	}
	
	public OrderWrapper addBetForOrderWrapper(PositionalOrderWrapper oWrapper,PositionalModel pModel,long id,BettingModel openModel,List<Bet> dBets,List<Bet> openBets,List<Bet> closeBets){
		logger.debug("finding opening order of close order {}",id);
		long oModelId = openModel.getId();
		//List<Bet> oBets = dbHelper.findBetsByOrder(oModelId);
		
/*		if(pModel.getId() == 4377){
			try {
	            logger.info("Waiting for order {}",pModel.getId());
	            Thread.sleep(60000);
	        } catch (InterruptedException e) {
	            System.out.println("got interrupted!");
	        }
		}*/
		
		oWrapper.setpModel(pModel);
		dBets.addAll(closeBets);
		logger.debug("adding close order bet list of size {} to close order {}",dBets.size(),id);

		// set the open model objects 
		oWrapper.setoModel(openModel);
		
		OrderWrapper openWrapper = new OrderWrapper(openModel,openModel.getModelStatus());

		if(!positionalIdMap.containsValue(id)){
			positionalIdMap.put(oModelId, id);
			logger.debug("added positional order mapping {},{} to cache",oModelId,id);
		}

		for (Bet bet : openBets) {
			BetWrapper bWrapper = new BetWrapper();
			bWrapper.setBet(bet);
			openWrapper.getBetWrappers().add(bWrapper);
		}
		orderCache.put(oModelId, openWrapper);
		
		logger.debug("adding open order bet list of size {} to close order {}",openBets.size(),id);
		dBets.addAll(openBets);
		
		for(Bet dBet:dBets){
			
			BetWrapper bWrapper = new BetWrapper();
			bWrapper.setBet(dBet);
			oWrapper.getBetWrappers().add(bWrapper);
		}
		
		orderCache.put(id, oWrapper);
		return oWrapper;
		
	}
	/***
	 * find and returns the list of betwrappers corresponding to an order id
	 * @param orderId
	 * @return
	 */
	
	/*	public Set<BetWrapper> findOrderBets(long id){
		
		OrderWrapper wrapper = findOrder(id);
		return wrapper.getBetWrappers();
		//return orderBets.get(wrapper.getOrder().getId());
		
	}*/
	
	public boolean isAllBetsResolved(List<Bet> betWrappers){
		boolean resolved=true;
		if(betWrappers.isEmpty()){
			resolved = false;
		}else{
			for(Bet bWrapper:betWrappers){
				
				if(bWrapper.getStatus() == BetStatus.PENDING || bWrapper.getStatus() == BetStatus.ACCEPTED || 
						bWrapper.getStatus() == BetStatus.ACCEPTED_UNKNOWN_ID){
					logger.info("bet id {} has status {} on OrderWrapper",bWrapper.getId(),bWrapper.getStatus());						
					resolved = false;
					break;
				}
				
			}
		}
		
		return resolved;
	}
	
	public boolean isAllBetsOnOpenResolved(List<Bet> betWrappers){
		boolean resolved=true;
		for(Bet bWrapper:betWrappers){
			
			if(bWrapper.getStatus() == BetStatus.PENDING || bWrapper.getStatus() == BetStatus.ACCEPTED || 
					bWrapper.getStatus() == BetStatus.ACCEPTED_UNKNOWN_ID){
				logger.info("bet id {} has status {} on OrderWrapper",bWrapper.getId(),bWrapper.getStatus());						
				resolved = false;
				break;
			}
			
		}
		return resolved;
	}
	
	public boolean isAllBetsRejected(List<Bet> betWrappers){
		boolean isRejected=true;
		if(betWrappers.isEmpty()){
			isRejected = false;
		}else{
			for(Bet bWrapper:betWrappers){
				if(!rejectedStatuses.contains(bWrapper.getStatus().name().trim())){
					logger.info("bet id {} has status {} on OrderWrapper",bWrapper.getId(),bWrapper.getStatus());						
					isRejected = false;
					break;
				}
				
			}
		}
		
		return isRejected;
	}
	
	/***
	 * updates the status of bet wrapper corresponding to the order
	 * @param id
	 * @param bWrapper
	 */
	public void updateOrderBets(long id, BetWrapper bWrapper){
		
		OrderWrapper oWrapper = null;
		oWrapper = findStoppedOrderById(id);
		if(oWrapper != null){
			// bets of open order are assigned to closing order id, as the list of betwrappers are stored
			// by their closing order id for positional order
			String orderType = oWrapper.getOrder().getModelType();
			
			if(orderType.equals("VOOpen") || orderType.equals(VOddsModelType.POSITIONAL_OPEN.name()) || orderType.equals("VOFutureOpen") || orderType.equals(VOddsModelType.FUTURE_POSITIONAL_OPEN.name())){
				try{
						if(oWrapper.getBetWrappers().contains(bWrapper)){
							logger.debug("updating bet wrapper {} status for the order id {} to {}",bWrapper.getBet().getId(),id,bWrapper.getBet().getStatus());
							// remove and insert the wrapper as the hashmultimap discards the insert otherwise
							boolean rResult = oWrapper.getBetWrappers().remove(bWrapper);
							boolean sResult = oWrapper.getBetWrappers().add(bWrapper);
							logger.debug("updating bet wrapper {} status for the order id {} to {}, with remove status {} and update status {}",
									bWrapper.getBet().getId(),id,bWrapper.getBet().getStatus(),rResult,sResult);
				
						}
						id = positionalIdMap.get(id);
						oWrapper = findStoppedOrderById(id);
				}
				catch(Exception e){
					// should not update the orderBets cache of open order
					logger.error("error while fetching closing order of open order id {}",id);
					return;
				}
				
			}
			
			if(oWrapper != null){
			
				// search for the bet wrapper inside the list of order bet wrappers
				if(oWrapper.getBetWrappers().contains(bWrapper)){
					logger.debug("updating bet wrapper {} status for the order id {} to {}",bWrapper.getBet().getId(),id,bWrapper.getBet().getStatus());
					// remove and insert the wrapper as the hashmultimap discards the insert otherwise
					boolean rResult = oWrapper.getBetWrappers().remove(bWrapper);
					boolean sResult = oWrapper.getBetWrappers().add(bWrapper);
					logger.debug("updating bet wrapper {} status for the order id {} to {}, with remove status {} and update status {}",
							bWrapper.getBet().getId(),id,bWrapper.getBet().getStatus(),rResult,sResult);
		
					
				}
				else{
					logger.debug("inserting new bet wrapper {} for the order id {} with status {}",bWrapper.getBet().getId(),id,bWrapper.getBet().getStatus());
					oWrapper.getBetWrappers().add(bWrapper);
					
				}
			}
		}
		
	}
	

	

}
