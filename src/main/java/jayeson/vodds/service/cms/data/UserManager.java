package jayeson.vodds.service.cms.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.AgentUser;
import jayeson.database.newvodds.BetUserSetting;
import jayeson.database.newvodds.ModelTransaction;
import jayeson.database.newvodds.StakePerMatch;
import jayeson.database.newvodds.VoddsBetUser;
import jayeson.vodds.common.model.notification.UserTradeLimitUpdateMessage;
import jayeson.vodds.service.cms.database.IDBService;

/***
 * Organizes, loads and maintains the bet user and agent user nodes in the CMS 
 * @author Praveen
 *
 */

@Singleton
public class UserManager {

	Map<Long,BetUserNode> betUserCache;
	Map<Long,AgentUserNode> agentCache;
	Map<String, Long> mapBetUserName;
	IDBService dbHelper;
	
	static Logger logger = LoggerFactory.getLogger(UserManager.class.getName());
	
	EventBus eBus;
	Map<Long, BetUserSetting> betUserSettingCache;
	List<Long> excludeTradeLimitList;
	

	@Inject
	public UserManager(IDBService helper,EventBus bus){
		dbHelper = helper;
		betUserCache = new ConcurrentHashMap<Long,BetUserNode>(); 
		agentCache = new ConcurrentHashMap<Long,AgentUserNode>();
		mapBetUserName = new ConcurrentHashMap<String, Long>();
		betUserSettingCache = new ConcurrentHashMap<Long, BetUserSetting>();
		excludeTradeLimitList = new ArrayList<Long>();
		eBus = bus;
		logger.debug("event bus created..");

	}
	
	/**
	 * provides the bet user node given the user id
	 * Builds the bet user node if not found in the cache
	 * @param id
	 * @return
	 */
	public synchronized BetUserNode findBetUserNode(long id){
		try {
			if(betUserCache.containsKey(id))
				return betUserCache.get(id);
			else{
				VoddsBetUser user =  dbHelper.findVoddsBetUser(id);
				if(user == null){
					
					logger.debug("user id {} not found in the database",id);
					return null;
				}

				
				BetUserNode userNode = new BetUserNode();
				userNode.setUser(user);
				userNode.setCredit(user.getCredit());
				userNode.setPl(user.getpL());
				userNode.setOutstandingAmount(user.getOutstandingAmount());
				userNode.setPositionalPotentialLoss(user.getPositionalPotentialLoss());
				userNode.setuManager(this);
				
				Map<String, StakePerMatch> mapStakePerMatch = dbHelper.findStakePerMatchByUserId(id);
				userNode.addMapStakePerMatchCahe(mapStakePerMatch);
				logger.debug("stake per match size {} added to user node",mapStakePerMatch.size());

				// find bet user setting in cache
				BetUserSetting betUserSetting = betUserSettingCache.get(id);
				if(betUserSetting == null) {
					// find bet user setting in db
					betUserSetting = dbHelper.findBetUserSettingById(id);
				}
				userNode.setMaxBetNormal(betUserSetting.getMaxBetNormal()== null ? betUserSetting.getDefaultMaxBetNormal() : betUserSetting.getMaxBetNormal().min(betUserSetting.getDefaultMaxBetNormal()));
				userNode.setMaxBetPositional(betUserSetting.getMaxBetPositional() == null ? betUserSetting.getDefaultMaxBetPositional() : betUserSetting.getMaxBetPositional().min(betUserSetting.getDefaultMaxBetPositional()));
				userNode.setMaxBetPerMatch(betUserSetting.getMaxBetPerMatch() == null ? betUserSetting.getDefaultMaxBetPerMatch() : betUserSetting.getMaxBetPerMatch().min(betUserSetting.getDefaultMaxBetPerMatch()));
				userNode.setMaxLoss(betUserSetting.getMaxLoss() == null ? betUserSetting.getDefaultMaxLoss() : betUserSetting.getMaxLoss().min(betUserSetting.getDefaultMaxLoss()));
				// set default limit for user
				setTradeLimitForUser(userNode, betUserSetting.getDefaultMaxBetNormal(), betUserSetting.getDefaultMaxBetPositional(), betUserSetting.getDefaultMaxBetPerMatch(), betUserSetting.getDefaultMaxLoss(),
						betUserSetting.getMaxBetNormal(), betUserSetting.getMaxBetPositional(), betUserSetting.getMaxBetPerMatch(), betUserSetting.getMaxLoss());

				List<AgentUser> aList = dbHelper.findAllAgentOfBetUser(id);
				
				logger.debug("agent list of size {} added to user node",aList.size());
				
				aList.stream().forEach(agent -> userNode.addAgentUser(findAgentUserNode(agent.getId())));
				
				logger.debug("user id {}  with credit {} - pl {} - outstanding {} - positionalPotentialLoss {} - maxBetNormal {} - maxBetPositional {} - maxBetPerMatch {} - maxLoss{} added into cache",
						id, userNode.getCredit(),userNode.getPl(),userNode.getOutstandingAmount(),userNode.getPositionalPotentialLoss(),userNode.getMaxBetNormal(),userNode.getMaxBetPositional(),userNode.getMaxBetPerMatch(),userNode.getMaxLoss());
				betUserCache.put(id, userNode);
				mapBetUserName.put(user.getUsername(), user.getId());
				return userNode;
			}
		}catch (Exception e) {
			logger.error("failed to find bet user node, exception ",e);
			return null;
		}
	}

	private void setTradeLimitForUser(BetUserNode userNode, BigDecimal defaultMaxBetNormal, BigDecimal defaultMaxBetPositional, BigDecimal defaultMaxBetPerMatch, BigDecimal defaultMaxLoss,
									  BigDecimal userMaxBetNormal, BigDecimal userMaxBetPositional, BigDecimal userMaxBetPerMatch, BigDecimal userMaxLoss) {
		userNode.setDefaultMaxBetNormal(defaultMaxBetNormal);
		userNode.setDefaultMaxBetPositional(defaultMaxBetPositional);
		userNode.setDefaultMaxBetPerMatch(defaultMaxBetPerMatch);
		userNode.setDefaultMaxLoss(defaultMaxLoss);
		userNode.setUserMaxBetNormal(userMaxBetNormal);
		userNode.setUserMaxBetPositional(userMaxBetPositional);
		userNode.setUserMaxBetPerMatch(userMaxBetPerMatch);
		userNode.setUserMaxLoss(userMaxLoss);
	}

	/***
	 * Provides the agent user node given the user id
	 * Builds the agent user nod if not found in the cache
	 * @param id
	 * @return
	 */
	public synchronized AgentUserNode findAgentUserNode(long id){
		
		if(agentCache.containsKey(id))
			return agentCache.get(id);
		else{
			AgentUser user = dbHelper.findAgentUser(id);
			if(user == null){
				logger.debug("agent id {} not found in db",id);
				return null;
			}
			
			AgentUserNode aNode = new AgentUserNode();
			aNode.setUser(user);
			aNode.setCredit(user.getCredit());
			aNode.setCreditPtpl(user.getPtPl());
			agentCache.put(id, aNode);
			logger.debug("agent id {} added into cache",id);
			
			return aNode;
		}
		
	}
	
	public void updateTradeLimit(UserTradeLimitUpdateMessage message) {
		try {
			BetUserNode uNode = findBetUserNode(message.getUserId());
			uNode.setMaxBetNormal(message.getMaxBetNormal() == null ? message.getDefaultMaxBetNormal() : message.getMaxBetNormal().min(message.getDefaultMaxBetNormal()));
			uNode.setMaxBetPositional(message.getMaxBetPositional() == null ? message.getDefaultMaxBetPositional() : message.getMaxBetPositional().min(message.getDefaultMaxBetPositional()));
			uNode.setMaxBetPerMatch(message.getMaxBetPerMatch() == null ? message.getDefaultMaxBetPerMatch() : message.getMaxBetPerMatch().min(message.getDefaultMaxBetPerMatch()));
			uNode.setMaxLoss(message.getMaxLoss() == null ? message.getDefaultMaxLoss() : message.getMaxLoss().min(message.getDefaultMaxLoss()));
			// set default limit for user
			setTradeLimitForUser(uNode, message.getDefaultMaxBetNormal(), message.getDefaultMaxBetPositional(), message.getDefaultMaxBetPerMatch(), message.getDefaultMaxLoss(),
					message.getMaxBetNormal(), message.getMaxBetPositional(), message.getMaxBetPerMatch(), message.getMaxLoss());

			logger.info("Update max value for userId {} - maxBetNormal {} - maxBetPositional {} - maxBetPerMatch {} - maxLoss {} , when recive UserTradeLimitUpdateMessage "
					+ "maxBetNormal {} - defaultmaxBetNormal {} , maxBetPositional {} - defaultmaxBetPositional {} , maxBetPerMatch {} - defaultmaxBetPerMatch {} , maxLoss {} - defaultmaxLoss {}",
					message.getUserId(),uNode.getMaxBetNormal(),uNode.getMaxBetPositional(),uNode.getMaxBetPerMatch(),uNode.getMaxLoss(),
					message.getMaxBetNormal(),message.getDefaultMaxBetNormal(),message.getMaxBetPositional(),message.getDefaultMaxBetPositional(),message.getMaxBetPerMatch(),message.getDefaultMaxBetPerMatch(),message.getMaxLoss(),message.getDefaultMaxLoss());
		}catch (Exception e) {
			logger.error("failed to update trade limit, exception ",e);
		}
		
	}

	public Map<Long, BetUserNode> getBetUserCache() {
		return betUserCache;
	}

	public void setBetUserCache(Map<Long, BetUserNode> betUserCache) {
		this.betUserCache = betUserCache;
	}

	public Map<Long, AgentUserNode> getAgentCache() {
		return agentCache;
	}

	public void setAgentCache(Map<Long, AgentUserNode> agentCache) {
		this.agentCache = agentCache;
	}

	public EventBus geteBus() {
		return eBus;
	}
	
	public Long getUserIdFromBetUserCache(String userName) {
		return mapBetUserName.get(userName);
	}

	public Map<Long, BetUserSetting> getBetUserSettingCache() {
		return betUserSettingCache;
	}

	public void setBetUserSettingCache(Map<Long, BetUserSetting> betUserSettingCache) {
		this.betUserSettingCache = betUserSettingCache;
	}
	
	public List<Long> getExcludeTradeLimitList() {
		return excludeTradeLimitList;
	}

	public void setExcludeTradeLimitList(String excludeConst) {
		logger.info("Exclude const {}", excludeConst);
		List<Long> newExcludeLst = new ArrayList<>();
		if (excludeConst != null && !excludeConst.isEmpty()) {
			String[] excludeArrays = excludeConst.split(",");
			for (String item : excludeArrays) {
				try {
					newExcludeLst.add(Long.valueOf(item));
				} catch (NumberFormatException ex) {
					logger.error("Skipped item {} due to NumberFormatException", item, ex);
				}
			}
		} 
		this.excludeTradeLimitList = newExcludeLst;
		logger.info("Update excludeTradeLimitList, number of item {}", this.excludeTradeLimitList.size());
	}
	
}
