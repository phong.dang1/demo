package jayeson.vodds.service.cms.data;

import jayeson.database.Transaction;
import jayeson.database.UserActivity;

public class TransactionWrapper {
    private Transaction transaction;
    private UserActivity activity;

    public TransactionWrapper() {
    }

    public TransactionWrapper(Transaction transaction, UserActivity activity) {
        this.transaction = transaction;
        this.activity = activity;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public UserActivity getActivity() {
        return activity;
    }

    public void setActivity(UserActivity activity) {
        this.activity = activity;
    }
}
