package jayeson.vodds.service.cms.data.game;

import jayeson.database.newvodds.UserGameTransaction;

public class GameTransactionWrapper {
    private UserGameTransaction transaction;

    public GameTransactionWrapper() {
    }

    public GameTransactionWrapper(UserGameTransaction transaction) {
        this.transaction = transaction;
    }

    public UserGameTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(UserGameTransaction transaction) {
        this.transaction = transaction;
    }
}
