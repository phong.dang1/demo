package jayeson.vodds.service.cms.data;

import jayeson.vodds.service.cms.events.CMSEvent;

public class ServiceEmission {
	CMSEvent cmsEvent;
	String serviceId;

	public ServiceEmission() {
		super();
	}
	public ServiceEmission(CMSEvent cmsEvent, String serviceId) {
		super();
		this.cmsEvent = cmsEvent;
		this.serviceId = serviceId;
	}
	public CMSEvent getCmsEvent() {
		return cmsEvent;
	}
	public void setCmsEvent(CMSEvent cmsEvent) {
		this.cmsEvent = cmsEvent;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	
}
