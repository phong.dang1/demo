package jayeson.vodds.service.cms.data;

import jayeson.database.Bet;

/***
 * Wrapper around the Bet object in database
 * @author Praveen
 *
 */
public class BetWrapper {

	Bet bet;

	public Bet getBet() {
		return bet;
	}

	public void setBet(Bet bet) {
		this.bet = bet;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bet == null) ? 0 : bet.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetWrapper other = (BetWrapper) obj;
		if (bet == null) {
			if (other.bet != null)
				return false;
		} else if (!bet.getId().equals(other.bet.getId()))
			return false;
		return true;
	}

	public BetWrapper(Bet bet) {
		super();
		this.bet = bet;
	}

	public BetWrapper() {
		super();
	
	}
	
	
	
	
	
}
