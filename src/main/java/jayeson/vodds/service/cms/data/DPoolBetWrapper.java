package jayeson.vodds.service.cms.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jayeson.database.newvodds.DPoolBetStatus;
import jayeson.database.newvodds.DPoolLiquidationMapping;
import jayeson.database.newvodds.DarkPoolBetInfo;
/***
 * wrapper for the dark pool bet database object
 * @author Praveen
 *
 */
public class DPoolBetWrapper {
	

	DarkPoolBetInfo dpBet;
	// collection of bet wrappers that are part of its liquidation
	Map<String,BetWrapper> betWrappers;
	// set of liquidation mapping for the dark pool bet that contains the information about fulfilled stake
	List<DPoolLiquidationMapping> liqMapping;
	
	private long id;
	
	public DarkPoolBetInfo getDpBet() {
		return dpBet;
	}
	public void setDpBet(DarkPoolBetInfo dpBet) {
		this.dpBet = dpBet;
	}
	
	public long getId() {
		return id;
	}
	
	
	public Map<String,BetWrapper> getBetWrappers() {
		return betWrappers;
	}
	public void setBetWrappers(Map<String,BetWrapper> betWrappers) {
		this.betWrappers = betWrappers;
	}
	public List<DPoolLiquidationMapping> getLiqMapping() {
		return liqMapping;
	}
	public void setLiqMapping(List<DPoolLiquidationMapping> liqMapping) {
		this.liqMapping = liqMapping;
	}
	public synchronized DPoolBetStatus getStatus(){
		return this.dpBet.getStatus();
	}
	
	public synchronized void setStatus(DPoolBetStatus status){
		this.dpBet.setStatus(status);
	}
	
	public synchronized void setSettledAmount(BigDecimal amt){
		this.dpBet.setSettledAmount(amt);
	}
	
	public DPoolBetWrapper(DarkPoolBetInfo dpBet) {
		super();
		this.dpBet = dpBet;
		this.id = dpBet.getId();
		this.liqMapping = new ArrayList<DPoolLiquidationMapping>();
		this.betWrappers = new HashMap<String,BetWrapper>();
		
	}
	
	public DPoolBetWrapper() {
		super();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DPoolBetWrapper other = (DPoolBetWrapper) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
