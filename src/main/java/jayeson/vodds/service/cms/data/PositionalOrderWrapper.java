package jayeson.vodds.service.cms.data;

import jayeson.database.BettingModel;
import jayeson.database.newvodds.PositionalModel;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/*** 
 * Wrapper for the positional betting model object that contains additional information 
 * than normal betting model such positional model object
 * 
 * @author Praveen
 *
 */
public class PositionalOrderWrapper extends OrderWrapper {
	
	public PositionalOrderWrapper(BettingModel bModel, String settlement) {
		super(bModel, settlement);
	}
	
	public PositionalOrderWrapper(BettingModel bModel, String settlement,PositionalModel model) {
		super(bModel, settlement);
		pModel = model;

	}


	
	// positional model object stored in the database
	PositionalModel pModel;
	// store the open model object
	BettingModel oModel;

	public PositionalModel getpModel() {
		return pModel;
	}

	public void setpModel(PositionalModel pModel) {
		this.pModel = pModel;
	}

	public BettingModel getoModel() {
		return oModel;
	}

	public void setoModel(BettingModel oModel) {
		this.oModel = oModel;
	}
	
	

}
