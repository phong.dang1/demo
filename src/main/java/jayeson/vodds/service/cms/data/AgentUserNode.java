package jayeson.vodds.service.cms.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jayeson.database.AgentUser;
import jayeson.database.BetUser;
import jayeson.vodds.service.cms.order.settlement.UnsettledQueue;


/***
 * Data structure to store the information related to an agent user and its child and parent users
 * 
 * @author Praveen
 *
 */
public class AgentUserNode {
	
	AgentUser user;
	
	// cached bet user credit
	BigDecimal credit;
	
	BigDecimal creditPtpl;
	
	
	private final Semaphore sem = new Semaphore(1, true);

	public AgentUserNode(){
		
	}
		
	public AgentUser getUser() {
		return user;
	}
	public void setUser(AgentUser user) {
		this.user = user;
	}
	
	// a thread safe method to add credit to the bet user node
	public BigDecimal[]  addCredit(BigDecimal credit) throws InterruptedException{
		List<BigDecimal> lstCredit = new ArrayList<>();
		try{
			sem.acquire();
			lstCredit.add(this.credit);
			this.credit = this.credit.add(credit);
			this.user.setCredit(this.user.getCredit().add(credit));
			lstCredit.add(this.credit);
			return (BigDecimal[]) lstCredit.toArray(new BigDecimal[lstCredit.size()]);
		}
		finally{
			sem.release();
		}

		
	}
	
	public BigDecimal[] addCreditPtpl(BigDecimal creditPtpl)throws InterruptedException{
		List<BigDecimal> lstCreditPtpl = new ArrayList<>();
		try{
			sem.acquire();
			lstCreditPtpl.add(this.creditPtpl);
			this.creditPtpl = this.creditPtpl.add(creditPtpl);
			this.user.setPtPl(this.user.getPtPl().add(creditPtpl));
			lstCreditPtpl.add(this.creditPtpl);
			return (BigDecimal[]) lstCreditPtpl.toArray(new BigDecimal[lstCreditPtpl.size()]);
		}
		finally{
			sem.release();
		}

		
	}


	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public BigDecimal getCreditPtpl() {
		return creditPtpl;
	}

	public void setCreditPtpl(BigDecimal creditPtpl) {
		this.creditPtpl = creditPtpl;
	}
	
	


}
