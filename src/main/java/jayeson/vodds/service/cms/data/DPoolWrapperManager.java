package jayeson.vodds.service.cms.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import jayeson.database.Bet;
import jayeson.database.newvodds.DarkPoolBetInfo;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.database.IDBService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/***
 * Manager to store and maintain the collection of dark pool bet wrappers
 * @author Praveen
 *
 */

@Singleton
public class DPoolWrapperManager {
	private static  Logger logger = LoggerFactory.getLogger(DPoolWrapperManager.class);
	Cache<Long,DPoolBetWrapper> dpCache;
	IDBService dbHelper;
	ConfigBean cBean;
	
	@Inject
	public DPoolWrapperManager(IDBService service,ConfigBean bean){
		this.dbHelper = service;
		this.cBean= bean; 
		dpCache = CacheBuilder.newBuilder().expireAfterWrite(cBean.getOrderExpiryInterval(), TimeUnit.SECONDS).build();

	}
	
	public synchronized DPoolBetWrapper findDPoolWrapper(DarkPoolBetInfo dpBet){
		
		DPoolBetWrapper dWrapper = dpCache.getIfPresent(dpBet.getId());
		if(dWrapper != null)
			return dWrapper;
		else{
			 dWrapper = new DPoolBetWrapper(dpBet);
			 // set the liquidation mappings and the corresponding liquidating bets
			 dWrapper.setLiqMapping(dbHelper.fetchLiquidationMappings(dpBet.getBetId().getId()));
			 List<Bet> liqBets = dbHelper.fetchLiquidatingBets(dpBet.getBetId().getId());
			 Map<String,BetWrapper> liqWrappers= new HashMap<String,BetWrapper>();
			 liqBets.stream().forEach(lBet -> liqWrappers.put(lBet.getId(),new BetWrapper(lBet)));
			 dWrapper.setBetWrappers(liqWrappers);
			 logger.info("liquidation mapping of size {} and bet wrappers of size {} set for dark pool bet info {}",dWrapper.getLiqMapping().size(),
					 dWrapper.getBetWrappers().size(),dWrapper.getId());
			 
			 dpCache.put(dpBet.getId(), dWrapper);
			 
			 return dWrapper;
		}
	}


}
