package jayeson.vodds.service.cms.data;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import jayeson.database.Bet;
import jayeson.database.BettingModel;

/*** Wrapper for the betting model object 
 * 
 * @author Praveen
 *
 */
public class OrderWrapper {
	
	BettingModel order;
	BigDecimal returnAmount;
	BigDecimal profitLoss;
	String orderSettlement; 
	Set<BetWrapper> betWrappers;
	AtomicBoolean isSettling;

	public boolean getIsSettling() {
		return isSettling.get();
	}

	public void setIsSettling(boolean isSettling) {
		this.isSettling.set(isSettling);
	}

	public BigDecimal getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}

	public BettingModel getOrder() {
		return order;
	}

	public void setOrder(BettingModel order) {
		this.order = order;
	}

	public String getOrderSettlement() {
		return orderSettlement;
	}

	public void setOrderSettlement(String orderSettlement) {
		this.orderSettlement = orderSettlement;
	}
	
	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}

	public OrderWrapper(BettingModel bModel, String settlement){
		order = bModel;
		orderSettlement = settlement;
		betWrappers = new HashSet<BetWrapper>();
		isSettling = new AtomicBoolean(false);
	}

	public Set<BetWrapper> getBetWrappers() {
		return betWrappers;
	}

	public void setBetWrappers(Set<BetWrapper> betWrappers) {
		this.betWrappers = betWrappers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order == null) ? 0 : Long.valueOf(order.getId()).hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderWrapper other = (OrderWrapper) obj;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!Long.valueOf(order.getId()).equals(Long.valueOf(other.order.getId())))
			return false;
		return true;
	}
	

	
	
	

}
