package jayeson.vodds.service.cms.service;

import java.lang.management.ManagementFactory;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import jayeson.lib.servicemanager.serviceexplorer.ServiceExplorer;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.database.DBModule;
import jayeson.vodds.service.cms.order.settlement.MPLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PopulateQueueTask;
import jayeson.vodds.service.cms.order.settlement.SettleTask;
import jayeson.vodds.service.cms.order.worker.OutstandingWorker;
import jayeson.vodds.service.cms.thrift.CMS;
import jayeson.vodds.service.cms.transactions.TransactionModule;
import jayeson.vodds.service.cms.transactions.TransactionProcessor;
import jayeson.vodds.service.cms.transactions.TransactionQueue;

import jayeson.vodds.service.cms.transactions.game.GameTransactionQueue;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

/***
 * Implementation class of cms server, initialized the thrift server and registers it to the service explorer
 * schedules the tasks for
 * * processing transaction queue periodically
 * * populate the unsettled orders queue
 * * settle the orders in the unsettled queue
 * @author Praveen
 *
 */
public class ServerImpl {

	private static Logger logger = LoggerFactory.getLogger(ServerImpl.class);
	private static final String seConfigPath = "conf/SEBean.json";


	// setup and start the thrift server with necessary configurations
	// registers the server with service explorer
	private class StartThriftServer implements Runnable {

		private CMSThriftServer tServer;
		private ConfigBean cBean;

		public StartThriftServer(CMSThriftServer server, ConfigBean bean) {
			tServer = server;
			cBean = bean;
		}

		@Override
		public void run() {
			ServiceExplorer.instance().addListener(tServer);
			try {
				logger.info("Thrift service is ready with id as " + ManagementFactory.getRuntimeMXBean().getName());
				while (true) {
					try {
						TNonblockingServerTransport serverTransport = new TNonblockingServerSocket(cBean.getPort());
						//CMS.Processor<CMSThriftServer> processor = new CMS.Processor<CMSThriftServer>(tServer);
						CMS.AsyncProcessor<CMSThriftServer> processor = new CMS.AsyncProcessor<CMSThriftServer>(tServer);

						TThreadedSelectorServer.Args args1 = new TThreadedSelectorServer.Args(serverTransport);
						args1.processor(processor);
						args1.transportFactory(new TFramedTransport.Factory());
						args1.protocolFactory(new TBinaryProtocol.Factory());
						args1.selectorThreads(cBean.getSelectorThreads());
						args1.workerThreads(cBean.getWorkerThreads());


						TServer server = new TThreadedSelectorServer(args1);

						server.serve();
						logger.debug("Thrift Server Stopped..");
						Thread.sleep(5000);
					} catch (Exception ex) {
						logger.error("Thrift server stopped due to error {}", ex);
					}
				}

			} catch (Exception ex) {
				logger.error("Fail to start the thrift service", ex);
				System.exit(-1);
			}


		}

	}

	CMSThriftServer tServer;
	ConfigBean cBean;
	Provider<TransactionProcessor> trProvider;
	Provider<PopulateQueueTask> poQProvider;
	Provider<SettleTask> soProvider;
	ScheduledExecutorService scService;
	EventBus eBus;
	TransactionQueue tQueue;
	GameTransactionQueue gameTransQueue;
	OutstandingWorker outStandingWorker;
	private MPLNotificationTask mplNotiTask;

	@Inject
	public ServerImpl(CMSThriftServer server, ConfigBean bean, Provider<TransactionProcessor> processor, Provider<PopulateQueueTask> qTask,
					  Provider<SettleTask> sTask, EventBus bus, TransactionQueue queue, MPLNotificationTask mplNotiTask, OutstandingWorker outstandingWorker,
					  ScheduledExecutorService scService, GameTransactionQueue gameTransQueue) {
		this.tServer = server;
		this.cBean = bean;
		this.trProvider = processor;
		this.poQProvider = qTask;
		this.soProvider = sTask;
		this.eBus = bus;
		this.tQueue = queue;
		this.gameTransQueue = gameTransQueue;
		this.mplNotiTask = mplNotiTask;
		this.outStandingWorker = outstandingWorker;
		this.scService = scService;
		// register the thrift server as the listening component for user credit and mpl update events
		this.eBus.register(this.tServer);
	}

	// start the thrift server, register the service explorer and schedule the necessary tasks
	public void startServer() {
		Thread serverThread = new Thread(new StartThriftServer(tServer, cBean));
		serverThread.start();

		try {
			ServiceExplorer.instance().startExplorer(seConfigPath, "NA", null);
			logger.info("service explorer started..");
		} catch (Exception e) {
			logger.error("failed to start service explorer:", e);
			System.exit(-1);
		}

		scService.scheduleWithFixedDelay(trProvider.get(), 1, cBean.getTransactionProcessInterval(), TimeUnit.SECONDS);
		scService.scheduleWithFixedDelay(poQProvider.get(), 1, cBean.getUnsettledOrderQInterval(), TimeUnit.SECONDS);
		scService.scheduleWithFixedDelay(soProvider.get(), 1, cBean.getSettleOrderQInterval(), TimeUnit.SECONDS);
		scService.scheduleWithFixedDelay(this.mplNotiTask, 1, 3, TimeUnit.SECONDS);

	}

	// shutdown the scheduled executor service
	public void stopServer() {
		// Stop service explorer
		ServiceExplorer.instance().stopExplorer();
		logger.info("Service Explorer Stopped...");
		while (!tQueue.isChkEmpty().get() || tQueue.isHandledByTransactionProcessor() || !gameTransQueue.isChkEmpty().get()) {
		}
		logger.info("stopping scheduling services..");
		scService.shutdown();
	}

	// thread to execute necessary actions before stopping the server
	private static class StopThread extends Thread {
		ServerImpl cServer;

		public StopThread(ServerImpl server) {
			cServer = server;
		}

		@Override
		public void run() {
			cServer.stopServer();

		}
	}

	public static void main(String args[]) {

		Injector injector = Guice.createInjector(new DBModule(), new ServiceModule(), new TransactionModule());
		ServerImpl cmsServer = injector.getInstance(ServerImpl.class);

		cmsServer.startServer();

		Runtime.getRuntime().addShutdownHook(new StopThread(cmsServer));

	}

}
