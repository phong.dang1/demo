package jayeson.vodds.service.cms.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import jayeson.database.newvodds.ModelTransactionType;
import jayeson.database.newvodds.PositionalModel;
import jayeson.database.newvodds.UserGameTransaction;
import jayeson.utility.concurrent.worker.batch.SharedExecutorBatchFutureWorker;
import jayeson.utility.concurrent.worker.batch.SharedExecutorBatchFutureWorkerGroup;
import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.data.*;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.events.CMSEvent;
import jayeson.vodds.service.cms.game.action.GameCreditAction;
import jayeson.vodds.service.cms.game.result.GameCreditResult;
import jayeson.vodds.service.cms.json.*;
import jayeson.vodds.service.cms.order.action.*;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.order.worker.CMSThriftHandler;
import jayeson.vodds.service.cms.thrift.*;
import jayeson.vodds.service.cms.tradelimit.TradeLimitManager;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;
import jayeson.vodds.service.cms.transactions.game.GameTransactionBuilder;
import jayeson.vodds.service.cms.util.ConfigUtil;
import jayeson.vodds.service.cms.util.GeneralUtility;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.observable.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/***
 * implementation class of the thrift server interface, the class will handle the thrift requests from other services and
 * process them according to the content of request 
 * 
 * @author Praveen
 *
 */

public class CMSThriftServer implements CMS.Iface, CMS.AsyncIface {
	// jackson object mapper
	ObjectMapper oMapper;
	// a listening executor service that supports adding callback to the future objects
	ListeningExecutorService service;
	
	// user manager instance to control the agent and user node cache inside cms
	UserManager uManager;
	// transaction builder to build all necessary transactions
	TransactionBuilder tBuilder;
	// game transaction builder to build all game transactions
	GameTransactionBuilder gameTransBuilder;
	
	IDBService dbHelper;
	OrderWrapperManager oManager;
	ConfigBean bean;
	TradeLimitManager tradeLimitManager;

	Set<String> serviceIds;
	
	// store the mapping of user and serviceId they are loaded on
	ConcurrentHashMap<Long,Set<String>> userServiceMap;
	
	// store the mapping of service and emitter to enable updating the user list in emitterUserMap
	ConcurrentHashMap<String,Emitter<ThriftUserUpdate>> serviceEmitterMap;
	
	private SharedExecutorBatchFutureWorkerGroup<ServiceEmission, Boolean> seBW;
	
	NoQPersister noQPersister;
	
	VoddsCommonUtility voddsCommonUtility;

	public static Logger logger = LoggerFactory.getLogger(CMSThriftServer.class.getName());

	private ConfigUtil configUtil;

	private GeneralUtility genUtil;

	@Inject 
	public CMSThriftServer(ObjectMapper mapper, ExecutorService eService, UserManager manager, TransactionBuilder builder,
			IDBService dbService, OrderWrapperManager wrapManager,NoQPersister qPersister,VoddsCommonUtility voddsCommonUtility,
			@Named("CommonSES") ScheduledExecutorService ses,ConfigBean bean,TradeLimitManager tradeLimit, ConfigUtil configUtil,
			GeneralUtility generalUtility, GameTransactionBuilder gameTransBuilder){
		oMapper = mapper;
		service = MoreExecutors.listeningDecorator(eService);
		uManager = manager;
		tBuilder = builder;
		this.gameTransBuilder = gameTransBuilder;
		dbHelper = dbService;
		oManager = wrapManager;
		noQPersister = qPersister;
		serviceIds = new HashSet<>();
		userServiceMap = new ConcurrentHashMap<Long,Set<String>>();
		serviceEmitterMap = new ConcurrentHashMap<String,Emitter<ThriftUserUpdate>>();
		tradeLimitManager = tradeLimit;
		this.voddsCommonUtility = voddsCommonUtility;
		this.bean = bean;
		this.seBW = new SharedExecutorBatchFutureWorkerGroup<>(ses, new CMSThriftHandler(this), this.bean.getBatchSizeOfThriftUserUpdate(), 
				new SharedExecutorBatchFutureWorker.BatchWorkerCallback<ServiceEmission, Boolean>(){

					@Override
					public void batchProcessed(SharedExecutorBatchFutureWorker<ServiceEmission, Boolean> worker,
							List<ServiceEmission> data, Boolean result, Throwable ex) {
						if (result){
							logger.trace("Calling batch processed after emit {} thrift user update", data.size());
						}else{
							logger.error("Failed to emit thrift user update ", ex);
						}
					}
			
		}, true);
		this.configUtil = configUtil;
		this.genUtil = generalUtility;
		String excludeTradeLimitConst = configUtil.loadConstants().get(ConfigUtil.EXCLUDE_TRADE_LIMIT_CHECK);
		this.uManager.setExcludeTradeLimitList(excludeTradeLimitConst);
	}

	/***
	 * Process thre credit request calls to the service and take appropriate action based on input thrift parameters
	 */
	@Override
	public ThriftCreditRequestResult processCreditRequest(ThriftCreditRequestInfo reqInfo) throws TException {
		
		ThriftCreditRequestResult cResult = new ThriftCreditRequestResult();
		cResult.setReqInfo(reqInfo);
		cResult.setResultCode(ThriftResultCode.FAILED);
		BigDecimal originalStake = BigDecimal.valueOf(reqInfo.getOriginalStake());
		
		logger.info("received request of order type {} and request type {} to the async server - startTime {}",reqInfo.oType,reqInfo.tType,reqInfo.getStartTime());

		
		if(reqInfo.getTType() == ThriftTransactionType.RESERVE){
			
			if(reqInfo.getOType() == ThriftOrderType.NORMAL){
				
				try {
					NormalReserveInfo info = oMapper.readValue(reqInfo.requestJson, NormalReserveInfo.class);
					String resultValite = tradeLimitManager.validateTradeLimitWithMaxValue(reqInfo,originalStake, BigDecimal.valueOf(info.creditAmount), BigDecimal.ZERO,info.modelId);
					if(!resultValite.isEmpty()) {
						cResult.setResultCode(ThriftResultCode.FAILED_TRADE_LIMIT);
						cResult.setResponseJson(resultValite);
					}else {
						logger.info("reserving credit amount {} for normal order for the user {}",info.creditAmount,reqInfo.userId);
						
						ListenableFuture<ReserveCreditResult> reserveResult = service.submit(
								new ReserveNormalCreditAction(uManager, tBuilder, info, reqInfo, this.voddsCommonUtility, this.genUtil));
					
						ReserveCreditResult result = reserveResult.get();
						
						if(Double.isNaN(result.getReservedCredit())) 
							cResult.setResultCode(ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE);	
						else if(result.getReservedCredit() == -1)
							cResult.setResultCode(ThriftResultCode.FAILED);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) < 0.001) 
							cResult.setResultCode(ThriftResultCode.SUCCESS);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) > 0.001)
							cResult.setResultCode(ThriftResultCode.SUCCESS_PARTIAL);
						
						logger.info("processed normal order with credit amount {} and commission {} for the user {} with the result {}",
								result.getReservedCredit(),result.getCommissionAmount(),reqInfo.userId,cResult.getResultCode());
						
						if(cResult.getResultCode() == ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE) {
							cResult.setResponseJson(result.getMessage());
						}else {
							cResult.setResponseJson(oMapper.writeValueAsString(new NormalReserveResult(result.getReservedCredit(),result.getCommissionAmount(),result.getMessage())));
						}
						
					}	

				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
			}
		
				
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_OPEN){
				
				try {
					LeverageReserveInfo info = oMapper.readValue(reqInfo.requestJson, LeverageReserveInfo.class);
					String resultValite = "";
					if(!info.isMarginTopup) {
						resultValite = tradeLimitManager.validateTradeLimitWithMaxValue(reqInfo, originalStake, BigDecimal.valueOf(info.creditAmount), BigDecimal.ZERO,info.modelId);
					}
					if(!resultValite.isEmpty()) {
						cResult.setResultCode(ThriftResultCode.FAILED_TRADE_LIMIT);
						cResult.setResponseJson(resultValite);
					}else {
						logger.info("reserving credit amount {} for positional open order for the user {}",info.creditAmount,reqInfo.userId);
						ListenableFuture<ReserveCreditResult> reserveResult = service.submit(new ReserveOpenCreditAction(uManager, tBuilder, info, reqInfo, voddsCommonUtility, genUtil));
					
						ReserveCreditResult result = reserveResult.get();
						
						if(Double.isNaN(result.getReservedCredit())) 
							cResult.setResultCode(ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE);	
						else if(result.getReservedCredit() == -1)
							cResult.setResultCode(ThriftResultCode.FAILED);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) < 0.001) 
							cResult.setResultCode(ThriftResultCode.SUCCESS);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) > 0.001)
							cResult.setResultCode(ThriftResultCode.SUCCESS_PARTIAL);
						
						logger.info("processed positional open order with credit amount {} and commission {} for the user {} with the result {}",
								result.getReservedCredit(),result.getCommissionAmount(),reqInfo.userId,cResult.getResultCode());
						
						if(cResult.getResultCode() == ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE) {
							cResult.setResponseJson(result.getMessage());
						}else {
							cResult.setResponseJson(oMapper.writeValueAsString(new NormalReserveResult(result.getReservedCredit(),result.getCommissionAmount(),result.getMessage())));
						}
					}			
			
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
				
			}
			
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_CLOSE){
				try {
					LeverageReserveInfo info = oMapper.readValue(reqInfo.requestJson, LeverageReserveInfo.class);
					logger.info("reserving credit amount {} for positional close order for the user {}",info.creditAmount,reqInfo.userId);
					ListenableFuture<ReserveCreditResult> reserveResult = service.submit(new ReserveCloseCreditAction(uManager,tBuilder,info,reqInfo,voddsCommonUtility));
				
					ReserveCreditResult result = reserveResult.get();
					
					if(result.getReservedCredit() == -1)
						cResult.setResultCode(ThriftResultCode.FAILED);
					else if(Math.abs(result.getReservedCredit() - info.creditAmount) < 0.001) 
						cResult.setResultCode(ThriftResultCode.SUCCESS);
					else if(Math.abs(result.getReservedCredit() - info.creditAmount) > 0.001)
						cResult.setResultCode(ThriftResultCode.SUCCESS_PARTIAL);
					
					logger.info("processed positional close order with credit amount {} and commission {} for the user {} with the result {}",
							result.getReservedCredit(),result.getCommissionAmount(),reqInfo.userId,cResult.getResultCode());
					
					cResult.setResponseJson(oMapper.writeValueAsString(new NormalReserveResult(result.getReservedCredit(),result.getCommissionAmount(),result.getMessage())));
				
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
				
			}
		}
		else if(reqInfo.getTType() == ThriftTransactionType.REFUND){
			
			if(reqInfo.getOType() == ThriftOrderType.NORMAL){
				
				try {
					NormalRefundInfo info = oMapper.readValue(reqInfo.requestJson, NormalRefundInfo.class);
					logger.info("refunding credit amount {} for normal order for the user {}",info.refundAmount,reqInfo.userId);

					ListenableFuture<RefundCreditResult> reserveResult = service.submit(new RefundNormalCreditAction(uManager,tBuilder,info,reqInfo,voddsCommonUtility));
				

					RefundCreditResult result = reserveResult.get();
					if(Math.abs(result.getRefundedCredit() - info.refundAmount) < 0.001){
						cResult.setResultCode(ThriftResultCode.SUCCESS);
						cResult.setResponseJson(oMapper.writeValueAsString(new NormalRefundResult(result.getRefundedCredit(),result.getRefundedCommission(),result.getMessage())));

					}
					logger.info("processed normal order refund request with credit amount {} and commission {} for the user {} with the result {}",
							result.getRefundedCredit(),result.getRefundedCommission(),reqInfo.userId,cResult.getResultCode());

				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}

			}
				
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_OPEN){
				
				try {
					LeverageRefundInfo info = oMapper.readValue(reqInfo.requestJson, LeverageRefundInfo.class);

					logger.info("refunding credit amount {} for positional open order for the user {}",info.refundAmount,reqInfo.userId);

					
					ListenableFuture<RefundCreditResult> reserveResult = service.submit(new RefundOpenCreditAction(uManager,tBuilder,info,reqInfo,voddsCommonUtility));
				

					RefundCreditResult result = reserveResult.get();
					if(Math.abs(result.getRefundedCredit() - info.refundAmount) < 0.001){
						cResult.setResultCode(ThriftResultCode.SUCCESS);
						cResult.setResponseJson(oMapper.writeValueAsString(new NormalRefundResult(result.getRefundedCredit(),result.getRefundedCommission(),result.getMessage())));

					}
					logger.info("processed positional open order refund request with credit amount {} and commission {} for the user {} with the result {}",
							result.getRefundedCredit(),result.getRefundedCommission(),reqInfo.userId,cResult.getResultCode());
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
			
			
			}
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_CLOSE){
				
				try {
					LeverageRefundInfoCloseOrder info = oMapper.readValue(reqInfo.requestJson, LeverageRefundInfoCloseOrder.class);

					logger.info("refunding credit amount {} for positional close order for the user {}",info.refundAmount,reqInfo.userId);
					
					
					ListenableFuture<RefundCreditResult> reserveResult = service.submit(new RefundCloseCreditAction(uManager,tBuilder,info,reqInfo));
					

					RefundCreditResult result = reserveResult.get();
					if(Math.abs(result.getRefundedCredit() - info.refundAmount) < 0.001){
						cResult.setResultCode(ThriftResultCode.SUCCESS);
						cResult.setResponseJson(oMapper.writeValueAsString(new NormalRefundResult(result.getRefundedCredit(),result.getRefundedCommission(),result.getMessage())));

					}
					logger.info("processed positional close order refund request with credit amount {} and commission {} for the user {} with the result {}",
							result.getRefundedCredit(),result.getRefundedCommission(),reqInfo.userId,cResult.getResultCode());
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}

			}
			
		}
		
		return cResult;
	}

	@Override
	public void setUserIdforUpdate(String serviceId, List<Long> userIds)
			throws TException {
		
	}
	
	/**
	 * Update the mapping from service id to users. Needs to remove the serviceIds from the
	 * set of ids for a particular user
	 * @param serviceId
	 * @param userIds
	 */
	private void updateUserIdsForService(Map<Long, Set<String>> userServiceMap, String serviceId, List<Long> userIds) {
		
		Set<Long> newSet = new HashSet<>();
		newSet.addAll(userIds);
		
		// find out all userid that is set by this service id before but not in the new list
		Set<Long> usersToRemove = userServiceMap.entrySet().stream().filter(entry -> { 
			return entry.getValue().contains(serviceId)	&& 
					!newSet.contains(entry.getKey());
		}).map(entry -> entry.getKey()).collect(Collectors.toSet());
		
		// remove serviceId from the set of serviceIds for users who is not in userIds set.
		usersToRemove.forEach(uid -> {
			Set<String> sids = userServiceMap.computeIfPresent(uid, (key, value) -> {
				Set<String> s = new HashSet<>(value);
				s.remove(serviceId);
				return s;
			});
			
			if (sids == null || sids.isEmpty()) userServiceMap.remove(uid);
			else userServiceMap.put(uid, sids);
		});
		
		userIds.forEach(uid -> {
			// avoid concurrent modification
			Set<String> sids = new HashSet<>(
					userServiceMap.compute(uid, (key, value) -> { 
						return value != null? value:new HashSet<>();
					})
			);
			
			sids.add(serviceId);
			userServiceMap.put(uid, sids);
		});
		
	}
	
	private ThriftUserUpdate generateUpdatesForUserSet(String serviceId, List<Long> userIds) {

		ThriftUserUpdate update = new ThriftUserUpdate();
		
		List<ThriftCreditUpdate> cUpdates = new ArrayList<ThriftCreditUpdate>();
		update.setCUpdates(cUpdates);
		
		logger.trace("Preparing updates of userIds {} to service {}", userIds.toString(), serviceId);

		uManager.setBetUserSettingCache(configUtil.loadTradeLimit(userIds));

		for(long userId:userIds){
			// send the credit updates immediately after the update request, even if there is no credit update event
			BetUserNode uNode = uManager.findBetUserNode(userId);
			if(uNode != null){
				
				ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();
				cUpdate.setUserName(uNode.getUser().getUsername());
				cUpdate.setCredit(uNode.getCredit().doubleValue());
				cUpdates.add(cUpdate);
				

				logger.trace("Added update for user {} and value {}",
						uNode.getUser().getUsername(),uNode.getCredit().doubleValue());
			}
		}
		
		return update;
		
	}
	
	/**
	 * Emitting update to a particular service, using an emitter
	 * 
	 * @param emitter
	 * @param update
	 * @param serviceId
	 */
	private void emitThriftUserUpdate(Emitter<ThriftUserUpdate> emitter,
			ThriftUserUpdate update, String serviceId) {
		
		CompletableFuture<Boolean> emitFuture = emitter.emit(update,false);
		
		emitFuture.thenAccept(result -> {
			
			logger.trace("Emission is done - result {}", result);
			
		}).exceptionally(e -> {
			
			logger.error("Error durring emission to service {}", serviceId, e);
			return null;
			
		});	
		
	}

	@Override
	public void observeUserUpdate(String serviceId, List<Long> userIds,
			Emitter<ThriftUserUpdate> emitter) throws TException {

		serviceIds.add(serviceId);
		logger.info("Received an observing request from service id {} and user list of {} with emitter {} ",serviceId,userIds.size(),emitter);

		// remove the existing emitter- user mapping on new observe update request
		if(serviceEmitterMap.containsKey(serviceId)){
			Emitter<ThriftUserUpdate> prevEmitter = serviceEmitterMap.get(serviceId);
			logger.debug("Removed old emitter for service ID {}",prevEmitter,serviceId);
			
			try {
				prevEmitter.emit(null, true);
			} catch (Exception e) {
				logger.trace("Close the expired emitter");
			}
			
		}
		
		serviceEmitterMap.put(serviceId, emitter);
		
		updateUserIdsForService(userServiceMap, serviceId, userIds);
		
		ThriftUserUpdate update = generateUpdatesForUserSet(serviceId, userIds);
		
		
		if (update.cUpdates.isEmpty()) return;
		
		logger.trace("Emitting credit updates!");
		emitThriftUserUpdate(emitter, update, serviceId);
	}


	@Override
	public void processCreditRequest(ThriftCreditRequestInfo reqInfo, AsyncMethodCallback resultHandler) throws TException {
		
		ThriftCreditRequestResult cResult = new ThriftCreditRequestResult();
		cResult.setReqInfo(reqInfo);
		cResult.setResultCode(ThriftResultCode.FAILED);
		BigDecimal originalStake = BigDecimal.valueOf(reqInfo.getOriginalStake());
		
		logger.info("received request of order type {} and request type {} to the async server - startTime {}",reqInfo.oType,reqInfo.tType,reqInfo.getStartTime());
		
		if(reqInfo.getTType() == ThriftTransactionType.RESERVE){
			
			if(reqInfo.getOType() == ThriftOrderType.NORMAL){
				
				try {
					NormalReserveInfo info = oMapper.readValue(reqInfo.requestJson, NormalReserveInfo.class);
					String resultValite = tradeLimitManager.validateTradeLimitWithMaxValue(reqInfo,originalStake,BigDecimal.valueOf(info.creditAmount), BigDecimal.ZERO,info.modelId);
					if(!resultValite.isEmpty()) {
						cResult.setResultCode(ThriftResultCode.FAILED_TRADE_LIMIT);
						cResult.setResponseJson(resultValite);
					}else {
						logger.info("reserving credit amount {} for normal order for the user {}",info.creditAmount,reqInfo.userId);
						
						ListenableFuture<ReserveCreditResult> reserveResult = service.submit(
								new ReserveNormalCreditAction(uManager, tBuilder, info, reqInfo, this.voddsCommonUtility, this.genUtil));
					
						ReserveCreditResult result = reserveResult.get();
						
						if(Double.isNaN(result.getReservedCredit())) 
							cResult.setResultCode(ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE);						
						else if(result.getReservedCredit() == -1)
							cResult.setResultCode(ThriftResultCode.FAILED);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) < 0.001) 
							cResult.setResultCode(ThriftResultCode.SUCCESS);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) > 0.001)
							cResult.setResultCode(ThriftResultCode.SUCCESS_PARTIAL);
						
						logger.info("processed normal order with credit amount {} and commission {} for the user {} with the result {}",
								result.getReservedCredit(),result.getCommissionAmount(),reqInfo.userId,cResult.getResultCode());
						
						if(cResult.getResultCode() == ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE) {
							cResult.setResponseJson(result.getMessage());
						}else {
							cResult.setResponseJson(oMapper.writeValueAsString(new NormalReserveResult(result.getReservedCredit(),result.getCommissionAmount(),result.getMessage())));
						}
							
	
					} 
				}	
				catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
				
			}
		
				
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_OPEN){
				
				try {
					LeverageReserveInfo info = oMapper.readValue(reqInfo.requestJson, LeverageReserveInfo.class);
					String resultValite = "";
					if(!info.isMarginTopup) {
						resultValite = tradeLimitManager.validateTradeLimitWithMaxValue(reqInfo, originalStake, BigDecimal.valueOf(info.creditAmount), BigDecimal.ZERO,info.modelId);
					}
					if(!resultValite.isEmpty()) {
						cResult.setResultCode(ThriftResultCode.FAILED_TRADE_LIMIT);
						cResult.setResponseJson(resultValite);
					}else {
						logger.info("reserving credit amount {} for positional open order for the user {}",info.creditAmount,reqInfo.userId);
						ListenableFuture<ReserveCreditResult> reserveResult = service.submit(new ReserveOpenCreditAction(uManager, tBuilder, info, reqInfo, voddsCommonUtility, genUtil));
					
						ReserveCreditResult result = reserveResult.get();
						
						if(Double.isNaN(result.getReservedCredit())) 
							cResult.setResultCode(ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE);	
						else if(result.getReservedCredit() == -1)
							cResult.setResultCode(ThriftResultCode.FAILED);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) < 0.001) 
							cResult.setResultCode(ThriftResultCode.SUCCESS);
						else if(Math.abs(result.getReservedCredit() - info.creditAmount) > 0.001)
							cResult.setResultCode(ThriftResultCode.SUCCESS_PARTIAL);
						
						logger.info("processed positional open order with credit amount {} and commission {} for the user {} with the result {}",
								result.getReservedCredit(),result.getCommissionAmount(),reqInfo.userId,cResult.getResultCode());
						
						if(cResult.getResultCode() == ThriftResultCode.OUTSTANDING_AMOUNT_NEGATIVE) {
							cResult.setResponseJson(result.getMessage());
						}else {
							cResult.setResponseJson(oMapper.writeValueAsString(new NormalReserveResult(result.getReservedCredit(),result.getCommissionAmount(),result.getMessage())));
						}
						
						
					}
			
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
				
			}
			
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_CLOSE){
				try {
					LeverageReserveInfo info = oMapper.readValue(reqInfo.requestJson, LeverageReserveInfo.class);


					logger.info("reserving credit amount {} for positional close order for the user {}",info.creditAmount,reqInfo.userId);
					ListenableFuture<ReserveCreditResult> reserveResult = service.submit(new ReserveCloseCreditAction(uManager,tBuilder,info,reqInfo,voddsCommonUtility));
				
					ReserveCreditResult result = reserveResult.get();
					
					if(result.getReservedCredit() == -1)
						cResult.setResultCode(ThriftResultCode.FAILED);
					else if(Math.abs(result.getReservedCredit() - info.creditAmount) < 0.001) 
						cResult.setResultCode(ThriftResultCode.SUCCESS);
					else if(Math.abs(result.getReservedCredit() - info.creditAmount) > 0.001)
						cResult.setResultCode(ThriftResultCode.SUCCESS_PARTIAL);
					
					logger.info("processed positional close order with credit amount {} and commission {} for the user {} with the result {}",
							result.getReservedCredit(),result.getCommissionAmount(),reqInfo.userId,cResult.getResultCode());
					
					cResult.setResponseJson(oMapper.writeValueAsString(new NormalReserveResult(result.getReservedCredit(),result.getCommissionAmount(),result.getMessage())));
					
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
				
			}

		}
		else if(reqInfo.getTType() == ThriftTransactionType.REFUND){
			
			if(reqInfo.getOType() == ThriftOrderType.NORMAL){
				
				try {
					NormalRefundInfo info = oMapper.readValue(reqInfo.requestJson, NormalRefundInfo.class);
	
					logger.info("refunding credit amount {} for normal order for the user {}",info.refundAmount,reqInfo.userId);

					ListenableFuture<RefundCreditResult> reserveResult = service.submit(new RefundNormalCreditAction(uManager,tBuilder,info,reqInfo,voddsCommonUtility));
				

					RefundCreditResult result = reserveResult.get();
					if(Math.abs(result.getRefundedCredit() - info.refundAmount) < 0.001){
						cResult.setResultCode(ThriftResultCode.SUCCESS);
						cResult.setResponseJson(oMapper.writeValueAsString(new NormalRefundResult(result.getRefundedCredit(),result.getRefundedCommission(),result.getMessage())));

					}
					logger.info("processed normal order refund request with credit amount {} and commission {} for the user {} with the result {}",
							result.getRefundedCredit(),result.getRefundedCommission(),reqInfo.userId,cResult.getResultCode());

					
					
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}

			}
				
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_OPEN){
				
				try {
					LeverageRefundInfo info = oMapper.readValue(reqInfo.requestJson, LeverageRefundInfo.class);
											logger.info("refunding credit amount {} for positional open order for the user {}",info.refundAmount,reqInfo.userId);
	
						
					ListenableFuture<RefundCreditResult> reserveResult = service.submit(new RefundOpenCreditAction(uManager,tBuilder,info,reqInfo,voddsCommonUtility));
				

					RefundCreditResult result = reserveResult.get();
					if(Math.abs(result.getRefundedCredit() - info.refundAmount) < 0.001){
						cResult.setResultCode(ThriftResultCode.SUCCESS);
						cResult.setResponseJson(oMapper.writeValueAsString(new NormalRefundResult(result.getRefundedCredit(),result.getRefundedCommission(),result.getMessage())));

					}
					logger.info("processed positional open order refund request with credit amount {} and commission {} for the user {} with the result {}",
							result.getRefundedCredit(),result.getRefundedCommission(),reqInfo.userId,cResult.getResultCode());
					
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}
			
			
			}
			else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_CLOSE){
				
				try {
					LeverageRefundInfoCloseOrder info = oMapper.readValue(reqInfo.requestJson, LeverageRefundInfoCloseOrder.class);

					logger.info("refunding credit amount {} for positional close order for the user {}",info.refundAmount,reqInfo.userId);

					
					ListenableFuture<RefundCreditResult> reserveResult = service.submit(new RefundCloseCreditAction(uManager,tBuilder,info,reqInfo));
					

					RefundCreditResult result = reserveResult.get();
					if(Math.abs(result.getRefundedCredit() - info.refundAmount) < 0.001){
						cResult.setResultCode(ThriftResultCode.SUCCESS);
						cResult.setResponseJson(oMapper.writeValueAsString(new NormalRefundResult(result.getRefundedCredit(),result.getRefundedCommission(),result.getMessage())));

					}
					logger.info("processed positional close order refund request with credit amount {} and commission {} for the user {} with the result {}",
							result.getRefundedCredit(),result.getRefundedCommission(),reqInfo.userId,cResult.getResultCode());
					
				} catch ( IOException ex3 ) {
					logger.error("Exception while parsing json {}",reqInfo.requestJson,ex3);
				} 
				catch( InterruptedException |  ExecutionException ex){
					logger.error("thread interrupted while reserving credit",ex);
				}

			}
		}
		logger.info("completed request {} with result  {} and json response {}",cResult.reqInfo.requestJson,cResult.resultCode,cResult.responseJson);
		resultHandler.onComplete(cResult);		
	}

	@Override
	public void setUserIdforUpdate(String serviceId, List<Long> userIds,
			AsyncMethodCallback resultHandler) throws TException {
		
		serviceIds.add(serviceId);
		
		logger.info("Received an user list update request from service id {} - list {} ", serviceId, userIds.toString());

		Emitter<ThriftUserUpdate> emitter = serviceEmitterMap.get(serviceId);
		
		if (emitter == null) {
			logger.error("Unable to emit credit update for new set of userIds {}. "
					+ "The emitter for service {} is not set!!!", userIds.toString(), serviceId);
			return;
		}
		
		updateUserIdsForService(userServiceMap, serviceId, userIds);

		ThriftUserUpdate update = generateUpdatesForUserSet(serviceId, userIds);
		if (update.cUpdates.isEmpty()) {
			logger.info("User list is recorded, emitter is available, but the update generated is empty for user list {}!", userIds.toString());
			return;
		}
		
		emitThriftUserUpdate(emitter, update, serviceId);
		
		resultHandler.onComplete(null);
				
	}
	
	/**
	 * Given an update for an user, emit it to all concerning services.
	 * @param userId
	 * @param update
	 */
	private void emitUpdatesForOneUser(long userId, ThriftUserUpdate update) {
		
		Optional.ofNullable(userServiceMap.get(userId)).ifPresent(serviceIds -> {
	
			for (String serviceId : serviceIds) {
				Emitter<ThriftUserUpdate> emitter = serviceEmitterMap.get(serviceId);
				try{
					if(emitter == null) {
						logger.warn("Emitter object is null for service {}", serviceId);
						
						continue;
					}
					
					CompletableFuture<Boolean> emitFuture = emitter.emit(update,false);
									
					emitFuture.exceptionally(e -> {
						logger.error("future exceptionally stopped while trying to emit the update for user {}",userId ,e);
						return null;
					}).thenAccept(result -> {
						logger.debug("Emitted the update for user {} to service {} - result {}", userId, serviceId, result);
					});
	
				}
				catch(Exception ex){
					logger.error("ERROR while trying to emit the update for user {} and value {}", userId, ex);
				}
			}
		});
	}
	
	public CompletableFuture<Boolean> emitThriftUserUpdate(ThriftUserUpdate update,String serviceId){
		Emitter<ThriftUserUpdate> emitter = this.serviceEmitterMap.get(serviceId);
		CompletableFuture<Boolean> emitFuture = new CompletableFuture<Boolean>();
		try{
			if(emitter == null) {
				logger.warn("Emitter object is null for service {}", serviceId);
			}else{
				emitFuture = emitter.emit(update,false);
				emitFuture.exceptionally(e -> {
					logger.error("future exceptionally stopped while trying to emit the update for user ",e);
					return null;
				}).thenAccept(r -> {
					logger.debug("Emitted the update to service {} - result {}", serviceId, r);
				});
			}
		}catch(Exception ex){
			logger.error("ERROR while trying to emit the update value {}", ex);
		}
		return emitFuture;
	}

	

	@Override
	public void updateCreditForUser(ThriftUpdateCreditForUser updateInfo, AsyncMethodCallback resultHandler) throws TException {
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		
		executeTransactionResult.setStatus(-1);
		executeTransactionResult.setMessage("Update credit failed");
		
		ThriftUserUpdate update = new ThriftUserUpdate();
		update.cUpdates = new ArrayList<>();
		
		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();
		
		BigDecimal bCredit = new BigDecimal(updateInfo.credit);
		BetUserNode betUserNode = uManager.findBetUserNode(updateInfo.userIds);
		
		AgentUserNode agentUserNode = null;
		AgentUserNode agentInitUserNode = uManager.findAgentUserNode(updateInfo.initiating_user);
		
		if(betUserNode == null) {
			agentUserNode = uManager.findAgentUserNode(updateInfo.userIds);

			executeTransactionResult = tBuilder.updateCreditForAgent(agentUserNode, bCredit, agentInitUserNode, updateInfo);

			cUpdate.setUserName(agentUserNode.getUser().getUsername());
			cUpdate.setCredit(agentUserNode.getCredit().doubleValue());
			update.cUpdates.add(cUpdate);

			logger.info("{} Update credit {} with message {}", agentUserNode.getUser().getUsername(), bCredit, executeTransactionResult.getMessage());

		} else {
			executeTransactionResult = tBuilder.updateCreditForMember(betUserNode, bCredit, agentInitUserNode, updateInfo);

			cUpdate.setUserName(betUserNode.getUser().getUsername());
			cUpdate.setCredit(betUserNode.getCredit().doubleValue());
			update.cUpdates.add(cUpdate);

			logger.info("{} Update credit {} with message {}", betUserNode.getUser().getUsername(), bCredit, executeTransactionResult.getMessage());
		}
		
		emitUpdatesForOneUser(updateInfo.userIds, update);
		
		resultHandler.onComplete(executeTransactionResult);
		
	}
	

	@Override
	public ThriftExecuteTransactionResult updateCreditForUser(ThriftUpdateCreditForUser updateInfo)
			throws TException {
		return null;
	}

	@Override
	public void executeTransaction(String type, long userId, long modelId, double credit, String remark,
			AsyncMethodCallback resultHandler) throws TException {
		ThriftUserUpdate update = new ThriftUserUpdate();
		update.cUpdates = new ArrayList<>();
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		
		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();
		logger.info("Executing transaction !");
		int count = dbHelper.checkOrderIsValid(userId, modelId);
		if(count == 0){
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("Order is not exist !");
			logger.error("Execute transaction is not successful , order {} is not exist",modelId);
			resultHandler.onComplete(executeTransactionResult);
		}else{
			try{
				ModelTransactionType mtt = ModelTransactionType.valueOf(type);
				BigDecimal bCredit = new BigDecimal(credit);
				BetUserNode betUserNode = uManager.findBetUserNode(userId);
				
				
				tBuilder.executeTransaction(betUserNode, bCredit, modelId, remark, mtt);
				
				cUpdate.setUserName(betUserNode.getUser().getUsername());
				cUpdate.setCredit(betUserNode.getCredit().doubleValue());
				update.cUpdates.add(cUpdate);
				
	
				emitUpdatesForOneUser(userId, update);
				executeTransactionResult.setStatus(0);
				executeTransactionResult.setMessage("Execute transaction sucessfull !");
				logger.info("Execute transaction is successful with user {} , order {} , credit {}, type {}",userId,modelId,credit,type);
				resultHandler.onComplete(executeTransactionResult);
			}
			catch(IllegalArgumentException e){
				executeTransactionResult.setStatus(1);
				executeTransactionResult.setMessage(e.getMessage());
				resultHandler.onComplete(executeTransactionResult);
				logger.error("Error while execute transaction exception caught {}",e);
			}
			catch(Exception e){
				resultHandler.onError(e);
				logger.error("Error while execute transaction exception caught {}",e);
			}
		}
		
	}

	@Override
	public ThriftExecuteTransactionResult executeTransaction(String type, long userId, long modelId, double credit,
			String remark) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void transferPtPLForUser(ThriftTransferPtPlForUser transferInfo,	AsyncMethodCallback resultHandler) throws TException {

		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		ThriftUserUpdate update = new ThriftUserUpdate();
		update.cUpdates = new ArrayList<>();

		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();

		BigDecimal bCredit = new BigDecimal(transferInfo.credit);

		AgentUserNode agentUserNode = uManager.findAgentUserNode(transferInfo.userIds);

		if (agentUserNode != null) {
			executeTransactionResult = tBuilder.transferPtplForAgent(agentUserNode, bCredit, transferInfo);
			cUpdate.setUserName(agentUserNode.getUser().getUsername());
			cUpdate.setCredit(agentUserNode.getCredit().doubleValue());
			update.cUpdates.add(cUpdate);

		} else {
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("agent user is not exist !");
		}

		emitUpdatesForOneUser(transferInfo.userIds, update);
		resultHandler.onComplete(executeTransactionResult);

	}

	@Override
	public ThriftExecuteTransactionResult transferPtPLForUser(ThriftTransferPtPlForUser transferInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void actualizedOrder(ThriftActualizedInfo actualizedInfo, AsyncMethodCallback resultHandler) throws TException {

		logger.info("actualized PO open order {} to {},refundMargin {} , reserveStake {}, user {}",
				actualizedInfo.openOrderId,actualizedInfo.normalOrderId,actualizedInfo.refundMargin,actualizedInfo.reserveStake,actualizedInfo.userIds);
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		ThriftUserUpdate update = new ThriftUserUpdate();
		update.cUpdates = new ArrayList<>();
		
		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();

		BetUserNode betUserNode = uManager.findBetUserNode(actualizedInfo.userIds);
		if(betUserNode != null)
		{
			executeTransactionResult = tBuilder.moveCreditFromPoToActualizingOrder(actualizedInfo, betUserNode);
			cUpdate.setUserName(betUserNode.getUser().getUsername());
			cUpdate.setCredit(betUserNode.getCredit().doubleValue());
			update.cUpdates.add(cUpdate);
		}else{
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("bet user is not exist !");
			logger.info("can not actualized order {} of user {}",actualizedInfo.openOrderId,actualizedInfo.userIds);
		}
		
		emitUpdatesForOneUser(actualizedInfo.userIds, update);
		resultHandler.onComplete(executeTransactionResult);
	}

	@Override
	public ThriftExecuteTransactionResult actualizedOrder(ThriftActualizedInfo actualizedInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}


	@Subscribe
	public void submitCMSEvent(CMSEvent event) {
		
		Optional.ofNullable(userServiceMap.get(event.getUserId())).ifPresent(serviceIds -> {
			
			for (String serviceId : serviceIds) {
				Emitter<ThriftUserUpdate> emitter = serviceEmitterMap.get(serviceId);
				if(emitter != null){
					ServiceEmission se = new ServiceEmission(event,serviceId);
					seBW.submit(serviceId, se);
				}
			}
		
		});
	}
	@Override
	public void getOutstandingPlCache(long userId, AsyncMethodCallback resultHandler) throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ThriftOutstandingPlCache getOutstandingPlCache(long userId) throws TException {
		BetUserNode betUserNode = uManager.findBetUserNode(userId);
		if(betUserNode != null) {
			ThriftOutstandingPlCache cache = new ThriftOutstandingPlCache();
			cache.setUserId(userId);
			cache.setOutStanding(betUserNode.getOutstandingAmount().doubleValue());
			cache.setProfitLoss(betUserNode.getPl().doubleValue());
			return cache;
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clearTradeLimitCache(String userName, AsyncMethodCallback resultHandler) throws TException {
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		Long userId = uManager.getUserIdFromBetUserCache(userName);
		if(userId != 0) {
			BetUserNode uNode = uManager.findBetUserNode(userId);
			uNode.clearStakePerMatchCache();
			logger.info("Clear trade limit cache for userId {} - size of stake per match cache {}",userId,tradeLimitManager.getStakePerMatchCahe(uNode).size());
			executeTransactionResult.setStatus(0);
			executeTransactionResult.setMessage("clear trade limit cache successfully");
			
		}else {
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("username is not exist stake per match cahe !");
			logger.info("username {} is not exist stake per match cahe",userName);
		}
		resultHandler.onComplete(executeTransactionResult);
		
	}

	@Override
	public ThriftExecuteTransactionResult clearTradeLimitCache(String userName) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateTradeLimitCache(ThriftUpdateTradeLimit updateInfo, AsyncMethodCallback resultHandler) throws TException {
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		BetUserNode userNode = uManager.findBetUserNode(updateInfo.userId);
		if (userNode != null) {
			if(tradeLimitManager.getStakePerMatchCahe(userNode).get(updateInfo.matchId) != null) {
				BigDecimal amount = BigDecimal.valueOf(updateInfo.amount);
				// update stake_per_match = current_stake_per_match + amount
				BigDecimal currentStakePerMatch = tradeLimitManager.getStakePerMatchCahe(userNode).get(updateInfo.matchId).getTotalStake();
				tradeLimitManager.getStakePerMatchCahe(userNode).get(updateInfo.matchId).setTotalStake(currentStakePerMatch.add(amount));

				noQPersister.saveStakePerMatch(updateInfo.matchId, tradeLimitManager.getStakePerMatchCahe(userNode).get(updateInfo.matchId));

				// if order id is open or close model id, update related limit
				PositionalModel pm = dbHelper.findPositionalModel(updateInfo.orderId);
				if(pm != null) {
					// update positional_potential_loss = current_positional_potential_loss + amount
					userNode.addPositionalPotentialLoss(amount, pm.getId());
					noQPersister.saveBetUser(userNode.getUser());

					// if execution staging is voiding, update order_max_potential_loss = current_order_max_potential_loss + amount
//					if(pm.getExecutionStage().equalsIgnoreCase(PositionalOrderStage.VOIDING.name())) {
//						pm.setOrderMaxPotentialLoss(genUtil.ifnull(pm.getOrderMaxPotentialLoss()).add(amount));
//						dbHelper.persistOrderMpl(pm);
//					}
				}

			} else {
				executeTransactionResult.setStatus(1);
				executeTransactionResult.setMessage("match is not exist!");
				logger.info("match {} is not exist", updateInfo.matchId);
			}

		} else {
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("user is not exist!");
			logger.info("user {} is not exist", updateInfo.userId);
		}
		resultHandler.onComplete(executeTransactionResult);
	}

	/**
	 * Skip implementation for sync method
	 * @param thriftGameCreditRequestInfo
	 * @return
	 * @throws TException
	 */
	@Override
	public ThriftGameCreditRequestResult processGameCreditRequest(ThriftGameCreditRequestInfo thriftGameCreditRequestInfo) throws TException {
		return processGameCreditRequestPrivate(thriftGameCreditRequestInfo);
	}

	/**
	 * The thrift API is used to process game credit request
	 * @param thriftReqInfo
	 * @param asyncCallback
	 * @throws TException
	 */
	@Override
	public void processGameCreditRequest(ThriftGameCreditRequestInfo thriftReqInfo, AsyncMethodCallback asyncCallback) throws TException {
		asyncCallback.onComplete(processGameCreditRequestPrivate(thriftReqInfo));
	}

	private ThriftGameCreditRequestResult processGameCreditRequestPrivate(ThriftGameCreditRequestInfo thriftReqInfo) {
		logger.info("{} received game credit request {}", thriftReqInfo.userId, thriftReqInfo);
		ThriftGameCreditRequestResult result = new ThriftGameCreditRequestResult();
		result.setReqInfo(thriftReqInfo);
		if (ThriftTransactionType.RESERVE.equals(thriftReqInfo.tType)) {
			UserGameTransaction userGameTransaction = gameTransBuilder.getUserGameTransaction(thriftReqInfo.gameDetailId);
			if (Objects.nonNull(userGameTransaction)) {
				BetUserNode userNode = uManager.findBetUserNode(thriftReqInfo.userId);
				if(userNode == null) {
					logger.error("{} Bet user not exist", thriftReqInfo.userId);
					result.setResultCode(ThriftResultCode.FAILED);
					result.responseJson = "Bet user doesn't exist";
					return result;
				}

				result.setCredit(userNode.getCredit().doubleValue());
				result.setCurrency(userNode.getUser().getCurrency().getCode());
				result.setTransactionId(userGameTransaction.getId());
				result.setResultCode(ThriftResultCode.SUCCESS);
				result.responseJson = "Success";
				
				logger.info("{} Transaction is exits. So skip to save new Transaction and returned current game credit result {}", thriftReqInfo.userId, result);

				return result;
			}
		}
		GameCreditAction creditAction = new GameCreditAction(uManager, gameTransBuilder, thriftReqInfo);
		ListenableFuture<GameCreditResult> actionResultFuture = service.submit(creditAction);
		try {
			GameCreditResult actionResult = actionResultFuture.get();
			result.setResultCode(actionResult.convertToThriftCode());
			result.setCredit(actionResult.getCredit());
			result.setCurrency(actionResult.getCurrency());
			result.setTransactionId(actionResult.getTransactionId());
			result.responseJson = actionResult.getMessage();
			logger.info("{} returned game credit result {}", thriftReqInfo.userId, result);
		} catch (Exception ex) {
			result.setResultCode(ThriftResultCode.FAILED);
			result.setResponseJson(ex.getMessage());
			logger.error("{} Error while processing game credit action", thriftReqInfo.getUserId(), ex);
		}
		return result;
	}

	/**
	 * Skip implementation for sync method
	 * @param thriftCreditInfo
	 * @return
	 * @throws TException
	 */
	@Override
	public ThriftCreditInfoResult getCreditInfo(ThriftCreditInfo reqInfo) throws TException {
		logger.info("{} Received get credit info request", reqInfo.getUserId());
		ThriftCreditInfoResult result = new ThriftCreditInfoResult();

		BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
		if(userNode == null) {
			logger.error("{} Bet user not exist", reqInfo.getUserId());
			result.setResultCode(ThriftResultCode.FAILED);
			result.responseJson = "Bet user doesn't exist";
			return result;
		}

		result.setReqInfo(reqInfo);
		result.setCredit(userNode.getCredit().doubleValue());
		result.setCurrency(userNode.getUser().getCurrency().getCode());
		result.setResultCode(ThriftResultCode.SUCCESS);
		result.responseJson = "Success";
		return result;
	}

	/**
	 * The thrift API is used to process game credit request
	 * @param thriftCreditInfo
	 * @param asyncCallback
	 * @throws TException
	 */
	@Override
	public void getCreditInfo(ThriftCreditInfo thriftCreditInfo, AsyncMethodCallback asyncCallback) throws TException {
		asyncCallback.onComplete(getCreditInfo(thriftCreditInfo));
	}

	@Override
	public ThriftExecuteTransactionResult updateTradeLimitCache(ThriftUpdateTradeLimit updateInfo) throws TException {
		return null;
	}

	@Override
	public void clearTradeLimitCacheByMatchId(List<String> matchId, AsyncMethodCallback resultHandler)
			throws TException {
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		if(matchId.size() != 0) {
			try {
				uManager.getBetUserCache().forEach((id,betUserNode)->{
					for (String matchIdItem : matchId) {
						logger.debug("Remove matchId {} from stake per match cahe with size {} of userId {}",matchIdItem,betUserNode.getMapStakePerMatch().size(),betUserNode.getUser().getId());
						betUserNode.getMapStakePerMatch().keySet().removeIf(e->(matchIdItem.trim().equalsIgnoreCase(e)));
						logger.debug("Stake per match cahe with size {} of userId {} after remove",matchIdItem,betUserNode.getMapStakePerMatch().size(),betUserNode.getUser().getId());
					}
				});
				executeTransactionResult.setStatus(0);
				executeTransactionResult.setMessage("clear trade limit cache successfully");
			}catch (Exception e) {
				logger.error("failed to clear trade limit cache,exception: ",e);
				executeTransactionResult.setStatus(1);
				executeTransactionResult.setMessage(e.getMessage());
			}
		}
		resultHandler.onComplete(executeTransactionResult);
		
	}

	@Override
	public ThriftExecuteTransactionResult clearTradeLimitCacheByMatchId(List<String> matchId) throws TException {
		// TODO Auto-generated method stub
		return null;
	}


}
