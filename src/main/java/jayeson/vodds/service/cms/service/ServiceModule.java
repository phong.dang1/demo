package jayeson.vodds.service.cms.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jayeson.utility.JacksonConfig;
import jayeson.utility.JacksonConfigFormat;
import jayeson.vodds.common.handler.ITranslationLoader;
import jayeson.vodds.common.handler.TranslationLoader;
import jayeson.vodds.common.module.VoddsCommonModule;
import jayeson.vodds.common.utility.VoddsCommonUtility;

import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.config.KafkaConfig;
import jayeson.vodds.service.cms.kafka.CacheMaintainer;
import jayeson.vodds.service.cms.kafka.CmsKafkaConsumer;
import jayeson.vodds.service.cms.kafka.ICacheMaintainer;
import jayeson.vodds.service.cms.kafka.IParser;
import jayeson.vodds.service.cms.kafka.IProcessor;
import jayeson.vodds.service.cms.kafka.MessageParser;
import jayeson.vodds.service.cms.kafka.MessageProcessor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public class ServiceModule  extends AbstractModule {
	
	private static Logger logger = LoggerFactory.getLogger(ServerImpl.class);

	@Override
	protected void configure() {

		bind(ObjectMapper.class).toInstance(new ObjectMapper());
		bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(50));
		bind(ScheduledExecutorService.class).toInstance(Executors.newScheduledThreadPool(100));
		bind(ConfigBean.class).toInstance(JacksonConfig.readConfig("conf/config.json", "NA", ConfigBean.class, JacksonConfigFormat.JSON));
		
		// inject a single instance of event bus
		bind(EventBus.class).asEagerSingleton();
		
		  // kafka subscribe
        bind(ICacheMaintainer.class).to(CacheMaintainer.class);
        bind(IParser.class).to(MessageParser.class);
        bind(IProcessor.class).to(MessageProcessor.class);
        bind(CmsKafkaConsumer.class).asEagerSingleton();
		
		install(new VoddsCommonModule());
		
	}

}
