package jayeson.vodds.service.cms.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import jayeson.database.newvodds.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jayeson.database.AgentUser;
import jayeson.database.BetUser;
import jayeson.database.BettingModel;
import jayeson.vodds.service.cms.database.IDBService;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Certain things doesn't need to queue but instead accumulated. E.g. update for the same bet user.
 * 
 * @author ryan
 *
 */
@Singleton
public class NoQPersister {

	private Map<Long, VoddsBetUser> betUserToSave;
	
	private Map<Long, AgentUser> agentUserToSave;
	
	private Map<Long, BettingModel> orderToSave;
	
	private Map<Long, String> oldStatus;
	
	private Map<Long, PositionalModel> positionalModelToSave;
	
	private Map<Long, BettingModelExtraVodds> vExtraModelToSave;
	
	private Map<String, StakePerMatch> stakePerMatchToSave;
	

	private IDBService dbService;
	
	static Logger logger = LoggerFactory.getLogger(NoQPersister.class.getName());
	
	private Lock lock = new ReentrantLock();
	
	@Inject
	public NoQPersister(IDBService service) {
		betUserToSave = new HashMap<>();
		agentUserToSave = new HashMap<>();
		orderToSave = new HashMap<>();
		positionalModelToSave = new HashMap<>();
		vExtraModelToSave = new HashMap<>();
		oldStatus = new HashMap<>();
		dbService = service;
		stakePerMatchToSave = new ConcurrentHashMap<>();
	}
	
	
	public Set<VoddsBetUser> getAllBetUserToSave() {
		
		Set<VoddsBetUser> set = new HashSet<>();
		
		try{
			lock.lock();
			set.addAll(betUserToSave.values());
			betUserToSave.clear();
		} finally{
			lock.unlock();
		}
		
		return set;
	}
	
	public Set<StakePerMatch> getAllStakePerMatchToSave(){
		Set<StakePerMatch> set = new HashSet<>();
		try{
			lock.lock();
			set.addAll(stakePerMatchToSave.values());
			stakePerMatchToSave.clear();
		} finally{
			lock.unlock();
		}
		
		return set;
	}



	public void setBetUserToSave(Map<Long, VoddsBetUser> betUserToSave) {
		this.betUserToSave = betUserToSave;
	}


	public Set<AgentUser> getAllAgentUserToSave() {
		Set<AgentUser> set = new HashSet<>();
		try{
			lock.lock();
			set.addAll(agentUserToSave.values());
			agentUserToSave.clear();
		} finally{
			lock.unlock();
		}
		return set;
	}



	public void setAgentUserToSave(Map<Long, AgentUser> agentUserToSave) {
		this.agentUserToSave = agentUserToSave;
	}



	public Set<BettingModel> getAllOrderToSave() {
		Set<BettingModel> set = new HashSet<>();
		try{
			lock.lock();
			set.addAll(orderToSave.values());
			orderToSave.clear();
		} finally{
			lock.unlock();
		}
		return set;
	}



	public void setOrderToSave(Map<Long, BettingModel> orderToSave) {
		this.orderToSave = orderToSave;
	}



	public Set<PositionalModel> getAllPositionalModelToSave() {
		Set<PositionalModel> set = new HashSet<>();
		try{
			lock.lock();
			set.addAll(positionalModelToSave.values());
			positionalModelToSave.clear();
		} finally{
			lock.unlock();
		}
		return set;
	}



	public void setPositionalModelToSave(Map<Long, PositionalModel> positionalModelToSave) {
		this.positionalModelToSave = positionalModelToSave;
	}



	public Set<BettingModelExtraVodds> getAllvExtraModelToSave() {
		Set<BettingModelExtraVodds> set = new HashSet<>();
		try{
			lock.lock();
			set.addAll(vExtraModelToSave.values());
			vExtraModelToSave.clear();
		} finally{
			lock.unlock();
		}
		return set;
	}



	public void setvExtraModelToSave(Map<Long, BettingModelExtraVodds> vExtraModelToSave) {
		this.vExtraModelToSave = vExtraModelToSave;
	}



	public void saveBetUser(VoddsBetUser user){
		try{
			lock.lock();
			betUserToSave.put(user.getId(), user);
		} finally{
			lock.unlock();
		}
	}
	
	public void saveStakePerMatch(String matchId,StakePerMatch stakePerMatch) {
		stakePerMatchToSave.put(matchId, stakePerMatch);
	}
	
	public void saveAndRemoveObjBetUserFromMap(VoddsBetUser user){
		try {
			lock.lock();
			VoddsBetUser obj = betUserToSave.get(user.getUser().getId());
			if(null != obj){
				dbService.saveUserCredit(obj);
				betUserToSave.remove(user.getUser().getId(), obj);
			}
		} finally{
			lock.unlock();
		}
	}
	
	public void saveAgentUser(AgentUser user){
		try{
			lock.lock();
			logger.info("save agent user {}",user.getUser().getId());
			agentUserToSave.put(user.getId(), user);
		} finally{
			lock.unlock();
		}
	}
	
	public void saveAndRemoveObjAgentUserFromMap(AgentUser user){
		try{
			lock.lock();
			AgentUser obj = agentUserToSave.get(user.getUser().getId());
			if(null != obj){
				//dbService.persistResults(obj);
				dbService.saveAgentUserCredit(obj.getId(), obj.getCredit());
				agentUserToSave.remove(user.getUser().getId(), obj);
			}
		} finally{
			lock.unlock();
		}
	}
	
	public void saveAndRemoveObjAgentUserFromMapWithPtpl(AgentUser user){
		try{
			lock.lock();
			AgentUser obj = agentUserToSave.get(user.getUser().getId());
			if(null != obj){
				//dbService.persistResults(obj);
				dbService.saveAgentUserCreditAndPtpl(obj.getId(), obj.getCredit(),obj.getPtPl());
				agentUserToSave.remove(user.getUser().getId(), obj);
			}
		} finally{
			lock.unlock();
		}
	}
	
	public void saveOrder(BettingModel bm){
		try{
			lock.lock();
			orderToSave.put(bm.getId(), bm);
		} finally{
			lock.unlock();
		}
	}
	
	public void saveOldStatus(long orderId,String status){
		try{
			lock.lock();
			oldStatus.put(orderId, status);
		} finally{
			lock.unlock();
		}
	}
	
	public String getOldStatus(long orderId){
		try{
			lock.lock();
			String status = oldStatus.get(orderId);
			oldStatus.remove(orderId);
			return status;
			
		} finally{
			lock.unlock();
		}
	}
	
	public void saveAndRemoveObjOrderFromMap(BettingModel bm){
		try{
			lock.lock();
			BettingModel obj = orderToSave.get(bm.getId());
			if(null != obj){
				dbService.persistResults(obj);
				orderToSave.remove(bm.getId(), obj);
			}
		} finally{
			lock.unlock();
		}
	}
	
	public void savePositionalModel(PositionalModel pm){
		try{
			lock.lock();
			positionalModelToSave.put(pm.getId(), pm);
		} finally{
			lock.unlock();
		}
	}
	
	public void saveAndRemoveObjPositionalModelFromMap(PositionalModel pm){
		try{
			lock.lock();
			PositionalModel obj = positionalModelToSave.get(pm.getId());
			if(null != obj){
				dbService.persistResults(obj);
				positionalModelToSave.remove(pm.getId(), obj);
			}
		} finally{
			lock.unlock();
		}
	}
	
	public void saveBettingModelExtraVodds(BettingModelExtraVodds bme){
		try{
			lock.lock();
			vExtraModelToSave.put(bme.getModelId().getId(), bme);
		} finally{
			lock.unlock();
		}
	}
	
	public void saveAndRemoveObjBettingModelExtraVoddsFromMap(BettingModelExtraVodds bme){
		try{
			lock.lock();
			BettingModelExtraVodds obj = vExtraModelToSave.get(bme.getModelId().getId());
			if(null != obj){
				dbService.persistResults(obj);
				vExtraModelToSave.remove(bme.getModelId().getId(), obj);
			}
		} finally{
			lock.unlock();
		}
	}
	
	public void persistBettingModelAgentPtPl(BettingModelAgentPtPl bmaPtpl){
		try{
			lock.lock();
			dbService.persistAgentPtPl(bmaPtpl);
		} finally{
			lock.unlock();
		}
	}

	public void persistBetOfAgentPtPl(BetAgentPtPl betAgentPtPl){
		try{
			lock.lock();
			dbService.persistBetOfAgentPtPl(betAgentPtPl);
		} finally{
			lock.unlock();
		}
	}

	public void persistBettingModelExtraVoddsForStatusChange(BettingModelExtraVodds extraVModel){
		dbService.persistResults(extraVModel);
	}
}
