package jayeson.vodds.service.cms.game.action;

import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.game.result.GameCreditResult;
import jayeson.vodds.service.cms.game.result.GameCreditResultCode;
import jayeson.vodds.service.cms.thrift.ThriftGameCreditRequestInfo;
import jayeson.vodds.service.cms.transactions.game.GameTransactionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

public class GameCreditAction implements Callable<GameCreditResult> {
    private Logger logger = LoggerFactory.getLogger(getClass());

    // user manager instance to manager the bet user node
    UserManager uManager;
    // game transaction builder to populate game transactions
    GameTransactionBuilder gameTransBuilder;
    // game credit request info
    ThriftGameCreditRequestInfo reqInfo;

    public GameCreditAction(UserManager um, GameTransactionBuilder gtb, ThriftGameCreditRequestInfo requestInfo) {
        this.uManager = um;
        this.gameTransBuilder = gtb;
        this.reqInfo = requestInfo;
    }

    @Override
    public GameCreditResult call() throws Exception {
        return processCreditAction();
    }

    public GameCreditResult processCreditAction() {
        GameCreditResult result = new GameCreditResult();
        BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
        if(userNode == null) {
            result.setCode(GameCreditResultCode.BET_USER_NOT_EXIST);
            logger.error("{} bet user not exist", reqInfo.getUserId());
            return result;
        }

        // when the user has sufficient credit that is requested
        BigDecimal reqAmt = BigDecimal.valueOf(reqInfo.amount);
        // if reserve transaction, verify user credit with requested amount
        if(reqAmt.compareTo(BigDecimal.ZERO) > 0 && userNode.getCredit().compareTo(reqAmt) < 0){
            result.setCode(GameCreditResultCode.INSUFFICIENT_CREDIT);
            logger.error("{} not enough credit {} amount {}", reqInfo.getUserId(), userNode.getCredit(), reqAmt);
            return result;
        }

        // populate game transaction
        gameTransBuilder.populateGameCreditTransactions(reqInfo, userNode, result);
        logger.info("{} finish game action {}", reqInfo.userId, reqInfo);
        return result;
    }
}
