package jayeson.vodds.service.cms.game.result;

import jayeson.vodds.service.cms.thrift.ThriftResultCode;

import java.util.Objects;

public class GameCreditResult {
    private GameCreditResultCode code;
    private String message;
    private double credit;
    private String currency;
    private String transactionId;

    public GameCreditResult() {
    }

    public GameCreditResultCode getCode() {
        return code;
    }

    public void setCode(GameCreditResultCode code) {
        this.code = code;
    }

    public String getMessage() {
        if(Objects.isNull(message)) {
            return Objects.nonNull(code) ? code.getMessage() : null;
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ThriftResultCode convertToThriftCode() {
        return code.getThriftResultCode();
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
