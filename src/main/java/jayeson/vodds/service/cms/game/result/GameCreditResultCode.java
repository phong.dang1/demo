package jayeson.vodds.service.cms.game.result;

import jayeson.vodds.service.cms.thrift.ThriftResultCode;

public enum GameCreditResultCode {
    BET_USER_NOT_EXIST("Bet user doesn't exist", ThriftResultCode.FAILED),
    INSUFFICIENT_CREDIT("User doesn't have enough credit", ThriftResultCode.INSUFFICIENT_FUNDS),
    INVALID_GAME_DETAIL("User game detail doesn't exist", ThriftResultCode.FAILED),
    SUCCESS("Success", ThriftResultCode.SUCCESS);
    private String message;
    private ThriftResultCode thriftResultCode;

    GameCreditResultCode(String message, ThriftResultCode thriftResultCode) {
        this.message = message;
        this.thriftResultCode = thriftResultCode;
    }

    public String getMessage() {
        return message;
    }

    public ThriftResultCode getThriftResultCode() {
        return thriftResultCode;
    }
}
