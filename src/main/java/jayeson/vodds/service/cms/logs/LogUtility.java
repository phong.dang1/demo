package jayeson.vodds.service.cms.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.vodds.orders.logs.BaseMessageLogs;

@Singleton
public class LogUtility {
	private final Logger log = LoggerFactory.getLogger("OrderSettlementLogCSV");

	@Inject
	public LogUtility() {
	}
	
	public void log(BaseMessageLogs logs) {
		log.debug(logs.serialize());
	}
	
}