package jayeson.vodds.service.cms.transactions;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import jayeson.database.AgentUser;
import jayeson.database.BettingModel;
import jayeson.database.Transaction;
import jayeson.database.BetUser;
import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.database.newvodds.DarkPoolBetInfo;
import jayeson.database.newvodds.ModelTransaction;
import jayeson.database.newvodds.PositionalModel;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import jayeson.vodds.service.cms.data.TransactionWrapper;

public class TransactionModule extends AbstractModule {

	@Override
	protected void configure() {
		
	}

	@Provides
	Queue<ModelTransaction> provideTQueue() {
		
		return new LinkedBlockingQueue <ModelTransaction>();
	}
	
	@Provides
	Queue<AgentUser> provideAQueue() {
		
		return new LinkedBlockingQueue <AgentUser>();
	}

	@Provides
	Queue<BetUser> provideBQueue() {
		
		return new LinkedBlockingQueue <BetUser>();
	}
	
	@Provides
	Queue<BettingModel> provideOQueue() {
		
		return new LinkedBlockingQueue <BettingModel>();
	}
	
	@Provides
	Queue<PositionalModel> providePMQueue() {
		
		return new LinkedBlockingQueue <PositionalModel>();
	}
	
	@Provides
	Queue<BettingModelExtraVodds> provideEVMQueue(){
		return new LinkedBlockingQueue <BettingModelExtraVodds>();
	}

	
	@Provides
	Queue<TransactionWrapper> provideUserTransaction(){
		return new LinkedBlockingQueue <TransactionWrapper>();
	}
	
	@Provides
	Queue<DarkPoolBetInfo> provideDPoolQueue(){
		return new LinkedBlockingQueue <DarkPoolBetInfo>();
	}
	

}
