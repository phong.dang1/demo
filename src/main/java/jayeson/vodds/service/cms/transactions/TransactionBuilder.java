package jayeson.vodds.service.cms.transactions;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import jayeson.database.*;
import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.database.newvodds.ModelTransaction;
import jayeson.database.newvodds.ModelTransactionType;
import jayeson.database.newvodds.PositionalModel;
import jayeson.lib.betting.datastructure.BetStatus;
import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.orders.common.VOddsModelType;
import jayeson.vodds.orders.logs.OrderSettlementMessageLogs;
import jayeson.vodds.orders.logs.PoSettlement;
import jayeson.vodds.orders.logs.PositionalOrderStateMessageLog;
import jayeson.vodds.orders.logs.SettlementType;
import jayeson.vodds.orders.logs.Type;
import jayeson.vodds.orders.normal.NormalOrderStatus;
import jayeson.vodds.orders.positional.PositionalCloseStatus;
import jayeson.vodds.orders.positional.PositionalOpenStatus;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.data.*;

import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.logs.LogUtility;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.order.settlement.MPLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.OutstandingNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PTPLSettlementInfo;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.thrift.*;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.tradelimit.TradeLimitManager;

import jayeson.vodds.service.cms.util.Activity;
import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/***
 * Builds the order transaction object and populates the queue which will be processed to persist the transactions to database 
 * @author Praveen
 *
 */
@Singleton
public class TransactionBuilder {
	
	IDBService helper;
	TransactionQueue tQueue;
	NoQPersister persistenceModerator;
	OrderWrapperManager oManager;
	MPLNotificationTask mplNotificationTask;
	OutstandingNotificationTask amtNotificationTask;
	LogUtility logUtility;
	TradeLimitManager tradeLimitManger;
	
	static Logger logger = LoggerFactory.getLogger(TransactionBuilder.class.getName());
	
	static Logger loggerJob = LoggerFactory.getLogger("persist.data.IDBService");
	
	static Set<String> rejectedStatuses = new HashSet<String>(Arrays.asList(BetStatus.CANCELLED.name(),BetStatus.EXCHANGE_DELETED.name(),BetStatus.REJECTED.name(),BetStatus.REJECTED_ABNORMAL.name()));
	
	private Lock lock = new ReentrantLock();
	
	VoddsCommonUtility voddsCommonUtility;
	
	private static String OT_MAX_BET_NORMAL_LIMIT = "ot_max_bet_normal_limit";
	
	private static String TX_RESERVE_COMMISSION_OPEN_ORDER = "tx_reserve_commission_open_order";
	
	private static String TX_RESERVE_COMMISSION_CLOSE_ORDER = "tx_reserve_commission_close_order";
	
	private static String TX_RESERVE_COMMISSION_NORMAL_ORDER = "tx_reserve_commission_normal_order";
	
	private static String TX_REFUND_COMMISSION_NORMAL_ORDER = "tx_refund_commission_normal_order";
	
	private static String TX_REFUND_COMMISSION_OPEN_ORDER = "tx_refund_commission_open_order";
	
	private static String TX_COMMISSION_REFUND_STAKE = "tx_refund_commission_refund_stake";
	
	private static String TX_COMMISSION_EARN = "tx_commission_earn";
	
	private static String TX_DARKPOOL_RETURN = "tx_darkpool_return";
	
	private static String TX_DARKPOOL_REVERT = "tx_darkpool_revert";
	
	private static String TX_MISSING_BET = "tx_missing_bet";
	
	//private static String TX_REVERTED_MISSING_BET = "tx_reverted_missing_bet";// reverted transaction missing bet as the status of bet changed
	
	private static String TX_SETTLED_ORDER = "tx_settled_order";
	
	private static String TX_REVERTED_ORDER = "tx_reverted_order";
	
	private static String TX_REVERTED_ORDER_BY_STATUS_CHANGE = "tx_reverted_order_by_status_change"; // order previous return amount reverted as the status of bet changed
	
	private static String TX_REVERTED_DP_BET_BY_STATUS_CHANGE = "tx_reverted_dp_bet_by_status_change"; // reverted transaction darkpool bet as the status of bet changed
	
	private static String TX_SETTLED_PTPL = "tx_settled_ptpl";
	
	private static String TX_REVERTED_PTPL = "tx_reverted_ptpl";
	
	private static String TX_REVERTED_ORDER_OPERATOR = "tx_reverted_order_operator";
	
	private static String TX_REFUND_VOIDING_ORDER = "tx_refund_voiding_order";

	private static String TX_ADJUSTED_CREDIT_VOIDING_ORDER = "tx_adjusted_credit_voiding_order";
	
	private static String TX_ADJUSTED_CREDIT_GURANTEE_STOP = "tx_adjusted_credit_gurantee_stop";
	
	private static String TX_TRANSLATE_PT_PL = "tx_translate_pt_pl";
	
	private static String TX_TOPUP = "tx_topup";
	
	private static String TX_WITHDRAW = "tx_withdraw";
	
	private static String TX_REFUND_CREDIT_FOR_STOP_LOSS = "tx_refund_credit_for_stop_loss";
	
	private static String TX_RESERVE_STAKE = "tx_reserve_stake";
	
	private static String TX_REFUND_MARGIN_ACTUALIZED = "tx_refund_margin_actualized";
	
	private static String TX_REFUND_MARGIN_COMMISSION_ACTUALIZED = "tx_refund_margin_commission_actualized";
	
	private static String TX_REFUND_COMMISSION_ACTUALIZED = "tx_refund_commission_actualized";

	private static String TX_OUTSTANDING_AMT_NEGATIVE = "tx_outstanding_amount_negative";

	private ConfigBean config;

	private GeneralUtility genUtil;
	
	@Inject
	public TransactionBuilder(IDBService dbHelper, TransactionQueue trQueue,NoQPersister dtPersitneceMorderator,OrderWrapperManager orManager,
			MPLNotificationTask task,OutstandingNotificationTask amtTask,LogUtility logUtility,VoddsCommonUtility voddsCommonUtility,
			TradeLimitManager tradeLimitManger, ConfigBean config, GeneralUtility genUtil){
		helper = dbHelper;
		tQueue = trQueue;
		persistenceModerator = dtPersitneceMorderator;
		oManager = orManager;
		this.mplNotificationTask = task;
		this.amtNotificationTask = amtTask;
		this.logUtility = logUtility;
		this.voddsCommonUtility = voddsCommonUtility;
		this.tradeLimitManger = tradeLimitManger;
		this.config = config;
		this.genUtil = genUtil;
	}
	
	/***
	 * populate all the relevant database transactions to store after settling a dark pool bet
	 */
	public void populateSettleDPoolTransactions(AgentUserNode agent, BigDecimal credit, BigDecimal prCredit, BigDecimal poCredit,DPoolBetWrapper dWrapper){
		
		if(credit.compareTo(BigDecimal.ZERO) != 0){
			persistenceModerator.saveAgentUser(agent.getUser());
		}
		
		// Update darkpool_betinfo
		tQueue.addDarkPoolBetInfo(dWrapper.getDpBet());
		
		ModelTransaction dTransaction = new ModelTransaction();
		dTransaction.setUser(agent.getUser().getUser());
		dTransaction.setCreditAmount(credit);
		dTransaction.setCreditBefore(prCredit);
		dTransaction.setCreditAfter(poCredit);
		dTransaction.setiService("CMS");
		dTransaction.setModelId(dWrapper.getDpBet().getBetId().getModel().getId());
		String remark = this.voddsCommonUtility.stringsToDescription(TX_DARKPOOL_RETURN,dWrapper.getDpBet().getBetId().getId());
		//dTransaction.setRemarks(String.format("adjusted credit for company agent after settling the dark pool bet %s",dWrapper.getDpBet().getBetId().getId()));
		dTransaction.setRemarks(remark);
		dTransaction.setType(ModelTransactionType.DPOOLRERTURN);
		dTransaction.setTime(new Timestamp(System.currentTimeMillis()));
		
		tQueue.addTransaction(dTransaction);
	}
	
	/**
	 * populate transactions related to settling a modified dpool bet
	 * @param order
	 * @param user
	 * @param revAmount
	 */
	public void populateSettleModifiedDPoolTransactions(AgentUserNode agent, BigDecimal revAmount, BigDecimal preCredit, BigDecimal postCredit, DPoolBetWrapper dWrapper) {
		
		if(revAmount.compareTo(BigDecimal.ZERO) != 0){
			persistenceModerator.saveAgentUser(agent.getUser());
		}
		
		//record the revert amount transaction for the darkpool bet 
		ModelTransaction dTransaction = new ModelTransaction();
			
		dTransaction.setUser(agent.getUser().getUser());
		dTransaction.setCreditAmount(revAmount.negate());
		dTransaction.setCreditBefore(preCredit);
		dTransaction.setCreditAfter(postCredit);
		dTransaction.setiService("CMS");
		dTransaction.setModelId(dWrapper.getDpBet().getBetId().getModel().getId());
		String remark = this.voddsCommonUtility.stringsToDescription(TX_DARKPOOL_REVERT,dWrapper.getDpBet().getBetId().getId());
		//dTransaction.setRemarks(String.format("dpool previous return amount reverted as the dpool bet %s changed", dWrapper.getDpBet().getBetId().getId()));
		dTransaction.setRemarks(remark);
		dTransaction.setType(ModelTransactionType.DPOOLREVERTED);
		dTransaction.setTime(new Timestamp(System.currentTimeMillis()));

		tQueue.addTransaction(dTransaction);
	
	}
	
	/**
	 * revert dp transaction by status change
	 * @param order
	 * @param agent
	 * @param revAmount
	 * @param preCredit
	 * @param postCredit
	 */
	public void revertDPoolTransactionsByStatusChange(OrderWrapper order,AgentUserNode agent, BigDecimal revAmount, BigDecimal preCredit, BigDecimal postCredit) {
		
		//record the revert amount transaction for the darkpool bet 
		ModelTransaction dTransaction = new ModelTransaction();
			
		dTransaction.setUser(agent.getUser().getUser());
		dTransaction.setCreditAmount(revAmount.negate());
		dTransaction.setCreditBefore(preCredit);
		dTransaction.setCreditAfter(postCredit);
		dTransaction.setiService("CMS");
		dTransaction.setModelId(order.getOrder().getId());
		dTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REVERTED_DP_BET_BY_STATUS_CHANGE));
		dTransaction.setType(ModelTransactionType.DPOOLREVERTED);
		dTransaction.setTime(new Timestamp(System.currentTimeMillis()));

		tQueue.addTransaction(dTransaction);
	
	}
	
	public void revertMissingBetTransactionsByStatusChange(OrderWrapper order,AgentUserNode agent, BigDecimal revAmount, BigDecimal preCredit, BigDecimal postCredit) {
		
		//record the revert amount transaction for the missing bet 
		ModelTransaction dTransaction = new ModelTransaction();
			
		dTransaction.setUser(agent.getUser().getUser());
		dTransaction.setCreditAmount(revAmount.negate());
		dTransaction.setCreditBefore(preCredit);
		dTransaction.setCreditAfter(postCredit);
		dTransaction.setiService("CMS");
		dTransaction.setModelId(order.getOrder().getId());
		dTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REVERTED_ORDER));
		dTransaction.setType(ModelTransactionType.REVERTED);
		dTransaction.setTime(new Timestamp(System.currentTimeMillis()));

		tQueue.addTransaction(dTransaction);
	
	}
	
	/**
	 * populate transactions related to settling a missing bet
	 * @param order
	 * @param agent
	 * @param revAmount
	 * @param preCredit
	 * @param postCredit
	 */
	public void populateSettleMissingBetTransactions(OrderWrapper order, AgentUserNode agent, BigDecimal revAmount, BigDecimal preCredit, BigDecimal postCredit,String orderType){
		
		persistenceModerator.saveAgentUser(agent.getUser());
		
		ModelTransaction sTransaction = new ModelTransaction();
		
		sTransaction.setModelId(order.getOrder().getId());
		String remark = this.voddsCommonUtility.stringsToDescription(TX_MISSING_BET);
		//sTransaction.setRemarks("adjusted credit for company agent after settling the positional order with missing bet");
		sTransaction.setRemarks(remark);
		sTransaction.setUser(agent.getUser().getUser());
		sTransaction.setCreditAmount(revAmount);
		sTransaction.setType(ModelTransactionType.RETURN);
		sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
		
		sTransaction.setCreditBefore(preCredit);
		sTransaction.setCreditAfter(postCredit);
		sTransaction.setiService("CMS");


		tQueue.addTransaction(sTransaction);
		
		if(orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name()) 
				|| orderType.equals("VOFutureNormal")
				|| orderType.equals(VOddsModelType.FUTURE_NORMAL.name())){
			order.getOrder().setIsModified((byte)0);
		}else{
			order.getOrder().setIsModified((byte)0);
			
			PositionalOrderWrapper poWrapper = (PositionalOrderWrapper) order;
			
			poWrapper.getoModel().setIsModified((byte)0);
		}
		
		
		//persistenceModerator.saveOrder(order.getOrder());
		
		//persistenceModerator.saveOrder(poWrapper.getoModel());
	}
	
	/**
	 * populate transactions related to settling a missing bet
	 * @param order
	 * @param agent
	 * @param revAmount
	 * @param preCredit
	 * @param postCredit
	 * @throws InterruptedException 
	 */
/*	public void populateSettleVoidingOrderTransactions(OrderWrapper order, AgentUserNode agent, BigDecimal revAmount, BigDecimal preCredit, BigDecimal postCredit){
		
		persistenceModerator.saveAgentUser(agent.getUser());
		
		ModelTransaction sTransaction = new ModelTransaction();
		
		sTransaction.setModelId(order.getOrder().getId());
		sTransaction.setRemarks("adjusted credit for company agent after settling the positional order with voiding order");
		sTransaction.setUser(agent.getUser().getUser());
		sTransaction.setCreditAmount(revAmount);
		sTransaction.setType(ModelTransactionType.RETURN);
		sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
		
		sTransaction.setCreditBefore(preCredit);
		sTransaction.setCreditAfter(postCredit);
		sTransaction.setiService("CMS");


		tQueue.addTransaction(sTransaction);
	}*/
	
	public void populateMarginTopUpTransactions(ReserveCreditResult result, BetUserNode user) throws InterruptedException{
		BigDecimal marginTopUp = new BigDecimal(result.getReservedCredit());
		ModelTransaction rTransaction = new ModelTransaction();
		rTransaction.setModelId(result.getModelId());
		rTransaction.setUser(user.getUser().getUser());
		rTransaction.setCreditAmount(marginTopUp.negate());
		rTransaction.setRemarks(result.getMessage());
		rTransaction.setType(ModelTransactionType.RESERVE);
		rTransaction.setTime(new Timestamp(System.currentTimeMillis()));
		
		
		BigDecimal[] credits = user.addCredit(marginTopUp.negate(), result.getModelId());
		
		rTransaction.setCreditBefore(credits[0]);
		rTransaction.setCreditAfter(credits[1]);
		
		rTransaction.setiService("CMS");
		
		//InfoUserWrapper 
		//InfoUserWrapper info = new InfoUserWrapper(String.valueOf(result.getModelId()),result.getModelId(),user.getUser().getUser().getId(),marginTopUp.negate());
		
		persistenceModerator.saveBetUser(user.getUser());
		
		tQueue.addTransaction(rTransaction);	
	}
	
	/***
	 *  populates relevant database transaction entries related to normal reserve credit 
	 * 
	 * @param result - reservation result object
	 * @param user - bet user node 
	 * @throws InterruptedException 
	 */
	public void populateReserveCreditTransactions(ReserveCreditResult result, BetUserNode user,String type,TradeLimitData tradeLimitData){

		List<ModelTransaction> transactionList = new ArrayList<ModelTransaction> ();
		try{
			
			double reservedCreditValue=0;
			double commissionCreditValue=0;
			double absCommissionCreditValue=0;
			String remarks = "";
			// compute the reserve credit value from the result object, only for non-close reserve credit actions
			if(!result.isUseOnlyCommission())
				reservedCreditValue = (result.getReservedCredit()/result.getLeverageRatio());
			
			commissionCreditValue =  result.getCommissionAmount();
			
			// build the reserve model transaction for user
			if(reservedCreditValue > 0){
				
				ModelTransaction rTransaction = new ModelTransaction();
				rTransaction.setModelId(result.getModelId());
				rTransaction.setUser(user.getUser().getUser());
				rTransaction.setCreditAmount(new BigDecimal(reservedCreditValue).negate());
				rTransaction.setRemarks(result.getMessage());
				rTransaction.setType(ModelTransactionType.RESERVE);
				rTransaction.setTime(new Timestamp(System.currentTimeMillis()));
				
				BigDecimal[] reserveCredit = user.addCredit(new BigDecimal(reservedCreditValue).negate(), result.getModelId());
				rTransaction.setCreditBefore(reserveCredit[0]);
				rTransaction.setCreditAfter(reserveCredit[1]);
				
				rTransaction.setiService("CMS");
				
				transactionList.add(rTransaction);
				
				if(type.equalsIgnoreCase(ThriftOrderType.NORMAL.name())){
					user.addOutstanding(new BigDecimal(reservedCreditValue), result.getModelId());
					UserOutstandingChangeEvent amt = new UserOutstandingChangeEvent(user.getUser().getUser().getUsername(), reservedCreditValue, user.getUser().getId());
					amtNotificationTask.submitOustanding(amt);
				}
				
				if(tradeLimitData != null) {
					logger.info("update stake per match cache {} - {}",user.getUser().getId(),tradeLimitData);
					tradeLimitManger.updateStakePerMatchCache(user, tradeLimitData,result.getModelId());
				}

			}
	
	
			//check order have marginTopUp
			if(!result.isMarginTopup()){
				// build the commission model transaction for user
				if(!Double.isNaN(commissionCreditValue) && Math.abs(commissionCreditValue) > 0.000001){
					absCommissionCreditValue = Math.abs(commissionCreditValue);
					ModelTransaction cTransaction = new ModelTransaction();
					cTransaction.setModelId(result.getModelId());
					cTransaction.setUser(user.getUser().getUser());
					cTransaction.setCreditAmount(new BigDecimal(absCommissionCreditValue).negate());
					if(type.equalsIgnoreCase(ThriftOrderType.POSITIONAL_OPEN.name()))
					{
						
//						remarks = String.format("Charge commission of %s for open order with stake = %s , commission rate = %s%%", 
//								format(new BigDecimal(absCommissionCreditValue)),format(new BigDecimal(result.getReservedCredit())),
//								format(user.getUser().getCommission().multiply(new BigDecimal(100))));
						remarks = this.voddsCommonUtility.stringsToDescription(TX_RESERVE_COMMISSION_OPEN_ORDER,format(new BigDecimal(absCommissionCreditValue)),format(new BigDecimal(result.getReservedCredit())),
								format(user.getUser().getCommission().multiply(new BigDecimal(100))));
					}
					else if(type.equalsIgnoreCase(ThriftOrderType.POSITIONAL_CLOSE.name())){
//						remarks = String.format("Charge commission of %s for close order with stake = %s , commission rate = %s%%", 
//								format(new BigDecimal(absCommissionCreditValue)),format(new BigDecimal(result.getReservedCredit())),
//								format(user.getUser().getCommission().multiply(new BigDecimal(100))));
						remarks = this.voddsCommonUtility.stringsToDescription(TX_RESERVE_COMMISSION_CLOSE_ORDER,format(new BigDecimal(absCommissionCreditValue)),format(new BigDecimal(result.getReservedCredit())),
								format(user.getUser().getCommission().multiply(new BigDecimal(100))));
					}
					else{
//						remarks = String.format("Charge commission of %s for normal order with stake = %s , commission rate = %s%%", 
//								format(new BigDecimal(absCommissionCreditValue)),format(new BigDecimal(result.getReservedCredit())),
//								format(user.getUser().getCommission().multiply(new BigDecimal(100))));
						remarks = this.voddsCommonUtility.stringsToDescription(TX_RESERVE_COMMISSION_NORMAL_ORDER,format(new BigDecimal(absCommissionCreditValue)),format(new BigDecimal(result.getReservedCredit())),
								format(user.getUser().getCommission().multiply(new BigDecimal(100))));
					}
					cTransaction.setRemarks(remarks);
					cTransaction.setType(ModelTransactionType.COMMISSION);
					cTransaction.setTime(new Timestamp(System.currentTimeMillis()));
		
					BigDecimal[] commissionCredit = user.addCredit(new BigDecimal(absCommissionCreditValue).negate(), result.getModelId());
					
					cTransaction.setCreditBefore(commissionCredit[0]);
					cTransaction.setCreditAfter(commissionCredit[1]);
					
					cTransaction.setiService("CMS");
					
					transactionList.add(cTransaction);
				}
			}
	
			
			// add the modified user object to the transaction queue
			persistenceModerator.saveBetUser(user.getUser());
	
			//tQueue.addTransaction(user.getUser());
			
			// populate the commission transactions only for non-margin reserve transactions for the agents
			if(!result.isMarginTopup()){
				// populate list of commission transactions of the user
				//if(user.getUser().getCommission().doubleValue() >0 ){
					
					for(AgentUserNode agent: user.getAgentUsers()){
						// create an entry only if the agent has a valid commission setting
						if(agent.getUser().getCommission().doubleValue() > 0){
							try {
								ModelTransaction aTransaction = new ModelTransaction();
								aTransaction.setModelId(result.getModelId());
								aTransaction.setUser(agent.getUser().getUser());
								
								BigDecimal commValue = new BigDecimal(result.getReservedCredit()).multiply(agent.getUser().getCommission());
								// convert the commission amount to agent currency before modifying agent credit
								BigDecimal aCommValue = helper.getAgentCommission(agent, user, commValue);
								
								if(aCommValue.compareTo(BigDecimal.ZERO) != 0)
								{
									//add the commission amount to the user credit
									BigDecimal[] credits = agent.addCredit(aCommValue);
									aTransaction.setCreditAmount(aCommValue);
									aTransaction.setCreditBefore(credits[0]);
									aTransaction.setCreditAfter(credits[1]);
									//remarks = String.format("commission of %s percent earned for using stake %s",format(agent.getUser().getCommission()),format(new BigDecimal(result.getReservedCredit())));
									remarks = voddsCommonUtility.stringsToDescription(TX_COMMISSION_EARN,format(agent.getUser().getCommission()),format(new BigDecimal(result.getReservedCredit())));
									aTransaction.setRemarks(remarks);
									aTransaction.setType(ModelTransactionType.COMMISSION);
									aTransaction.setTime(new Timestamp(System.currentTimeMillis()));
									aTransaction.setiService("CMS");
			
									transactionList.add(aTransaction);
								}
		
								// add the modified user object to the transaction queue
								//tQueue.addTransaction(agent.getUser());
								persistenceModerator.saveAgentUser(agent.getUser());
							} catch (Exception ex) {
								logger.error("Error while saving agent user", ex);
							}
						}
					
					}
				//}
				
			}
	
			tQueue.addOrderTransactions(transactionList);		
		}catch(Exception ex){
			logger.error("Error while reserve credit for order {} , exception caught: {}",result.getModelId(),ex);
		}
		
	}
	/***
	 * populates transactions related to normal refund credit action
	 * @param user
	 * @param result
	 * @throws InterruptedException 
	 */
	public void populateRefundCreditTransactions(RefundCreditResult result,BetUserNode user,String type,TradeLimitData tradeLimitData) {

		List<ModelTransaction> transactionList = new ArrayList<ModelTransaction>();
		try {
			double refundedCreditValue = 0;
			double commissionCreditValue = 0;
			double absCommissionCreditValue = 0;
			String remarks = "";

			// compute the reserve credit value from the result object, only for non-close reserve credit actions
			if (!result.isUseOnlyCommission()) {
				refundedCreditValue = (result.getRefundedCredit() / result.getLeverageRatio());
			}

			commissionCreditValue = result.getRefundedCommission();

			ModelTransaction rTransaction = new ModelTransaction();
			rTransaction.setModelId(result.getModelId());
			rTransaction.setUser(user.getUser().getUser());
			rTransaction.setCreditAmount(new BigDecimal(refundedCreditValue));
			rTransaction.setRemarks(result.getMessage());
			rTransaction.setType(ModelTransactionType.REFUND);
			rTransaction.setTime(new Timestamp(System.currentTimeMillis()));

			BigDecimal[] refundCredits = user.addCredit(new BigDecimal(refundedCreditValue), result.getModelId());
			rTransaction.setCreditBefore(refundCredits[0]);
			rTransaction.setCreditAfter(refundCredits[1]);

			rTransaction.setiService("CMS");

			if (type.equalsIgnoreCase(ThriftOrderType.NORMAL.name())) {
				user.addOutstanding(BigDecimal.valueOf(refundedCreditValue).negate(), result.getModelId());
				UserOutstandingChangeEvent amt = new UserOutstandingChangeEvent(user.getUser().getUser().getUsername(), BigDecimal.valueOf(refundedCreditValue).negate().doubleValue(), user.getUser().getId());
				amtNotificationTask.submitOustanding(amt);
			}

			tradeLimitManger.updateStakePerMatchCache(user, tradeLimitData, result.getModelId());

			transactionList.add(rTransaction);

			if (!type.equalsIgnoreCase(ThriftOrderType.POSITIONAL_CLOSE.name())) {
				if (!Double.isNaN(commissionCreditValue) && Math.abs(commissionCreditValue) > 0.000001) {
					absCommissionCreditValue = Math.abs(commissionCreditValue);
					ModelTransaction cTransaction = new ModelTransaction();
					cTransaction.setModelId(result.getModelId());
					cTransaction.setUser(user.getUser().getUser());
					cTransaction.setCreditAmount(new BigDecimal(absCommissionCreditValue));
					if (type.equalsIgnoreCase(ThriftOrderType.NORMAL.name())) {
						remarks = this.voddsCommonUtility.stringsToDescription(TX_REFUND_COMMISSION_NORMAL_ORDER, format(new BigDecimal(absCommissionCreditValue)), format(new BigDecimal(result.getRefundedCredit())), format(user.getUser().getCommission().multiply(new BigDecimal(100))));
					} else if (type.equalsIgnoreCase(ThriftOrderType.POSITIONAL_OPEN.name())) {
						remarks = this.voddsCommonUtility.stringsToDescription(TX_REFUND_COMMISSION_OPEN_ORDER, format(new BigDecimal(absCommissionCreditValue)), format(new BigDecimal(result.getRefundedCredit())), format(user.getUser().getCommission().multiply(new BigDecimal(100))));
					}

					cTransaction.setRemarks(remarks);
					cTransaction.setType(ModelTransactionType.COMMISSION);
					cTransaction.setTime(new Timestamp(System.currentTimeMillis()));

					BigDecimal[] commissionCredit = user.addCredit(new BigDecimal(absCommissionCreditValue), result.getModelId());
					cTransaction.setCreditBefore(commissionCredit[0]);
					cTransaction.setCreditAfter(commissionCredit[1]);

					cTransaction.setiService("CMS");

					transactionList.add(cTransaction);
				}
			}

			// add the modified user object to the transaction queue
			if(!genUtil.isZero(refundedCreditValue)) {
				persistenceModerator.saveBetUser(user.getUser());
			}

			// populate the commission transactions only for non-margin refund transactions
			if (!result.isMarginRefund()) {

				// populate list of commission transactions of the user
				for (AgentUserNode agent : user.getAgentUsers()) {
					// create an entry only if the agent has a valid commission setting
					if (agent.getUser().getCommission().doubleValue() > 0) {
						try {
							ModelTransaction aTransaction = new ModelTransaction();
							aTransaction.setModelId(result.getModelId());
							aTransaction.setUser(agent.getUser().getUser());
							BigDecimal commValue = new BigDecimal(result.getRefundedCredit()).multiply(agent.getUser().getCommission()).negate();
							// convert the commission amount to agent currency before modifying agent credit
							BigDecimal aCommValue = helper.getAgentCommission(agent, user, commValue);

							if (aCommValue.compareTo(BigDecimal.ZERO) != 0) {

								//subtract the commission amount from the agent credit
								BigDecimal[] credits = agent.addCredit(aCommValue);
								aTransaction.setCreditBefore(credits[0]);
								aTransaction.setCreditAmount(aCommValue);
								aTransaction.setCreditAfter(credits[1]);
								remarks = this.voddsCommonUtility.stringsToDescription(TX_COMMISSION_REFUND_STAKE, format(agent.getUser().getCommission()), format(new BigDecimal(result.getRefundedCredit())));

								aTransaction.setRemarks(remarks);
								aTransaction.setType(ModelTransactionType.COMMISSION);
								aTransaction.setTime(new Timestamp(System.currentTimeMillis()));
								aTransaction.setiService("CMS");

								transactionList.add(aTransaction);

							}

							// add the modified user object to the transaction queue
							persistenceModerator.saveAgentUser(agent.getUser());
						} catch (Exception ex) {
							logger.error("{} Error while saving agent user!", agent.getUser().getId(), ex);
						}
					}
				}
			}

			tQueue.addOrderTransactions(transactionList);

		} catch (Exception ex) {
			logger.error("{} Error while refund credit for order {}", user.getUser().getId(), result.getModelId(), ex);
		}
	}
	
	public boolean isAllBetsRejected(Set<BetWrapper> betWrappers){
		boolean isRejected=true;
		for(BetWrapper bWrapper:betWrappers){
			if(!rejectedStatuses.contains(bWrapper.getBet().getStatus().name().trim())){
				logger.info("bet id {} has status {} ",bWrapper.getBet().getId(),bWrapper.getBet().getStatus());						
				isRejected = false;
				break;
			}
			
		}
		
		return isRejected;
	}
	
	public boolean isAllBetsRejectedForCloseOrder(Set<BetWrapper> betWrappers,OrderWrapper order){
		boolean isRejected=true;
		for(BetWrapper bWrapper:betWrappers){
			if(bWrapper.getBet().getModel().getId() == order.getOrder().getId()){
				if(!rejectedStatuses.contains(bWrapper.getBet().getStatus().name().trim())){
					logger.info("bet id {} has status {} ",bWrapper.getBet().getId(),bWrapper.getBet().getStatus());						
					isRejected = false;
					break;
				}
			}
		}
		
		return isRejected;
	}
	
	/***
	 * populate required database transactions after settling a normal order 
	 * @param order
	 * @param user
	 * @param priorCredit
	 * @param postCredit
	 * @throws InterruptedException 
	 */
	
	public void populateSettleOrderTransactions(OrderWrapper order, BetUserNode user, BigDecimal priorCredit, BigDecimal postCredit, BigDecimal orderPL,BigDecimal confirmedStake,String message){
		
		try{
			// add the modified user object to the transaction queue
			//tQueue.addTransaction(user.getUser());
			
			
			//record the return amount transaction for the order 
			ModelTransaction sTransaction = new ModelTransaction();
			
			sTransaction.setModelId(order.getOrder().getId());
			String remark = this.voddsCommonUtility.stringsToDescription(TX_SETTLED_ORDER);
//			sTransaction.setRemarks("Order settled, pl is added to user credit");
			sTransaction.setRemarks(remark);
			sTransaction.setUser(user.getUser().getUser());
			sTransaction.setCreditAmount(order.getReturnAmount());
			sTransaction.setType(ModelTransactionType.RETURN);
			sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			sTransaction.setCreditBefore(priorCredit);
			sTransaction.setCreditAfter(postCredit);
			sTransaction.setiService("CMS");
	
	
			tQueue.addTransaction(sTransaction);
			
			// set the status of order to settled
			order.getOrder().setModelStatus(order.getOrderSettlement());	
			
			order.getOrder().setIsModified((byte)0);
			
			//tQueue.addOrder(order.getOrder());
			
			//only save order after finish processing settle
			//persistenceModerator.saveOrder(order.getOrder());
			
			if(order.getOrder().getModelStatus().equals(NormalOrderStatus.REFUNDED.name())) {
				logUtility.log(new OrderSettlementMessageLogs(this.getClass().getSimpleName(), Type.SETTLED, order.getOrder().getId(), null, null, null, message, order.getOrderSettlement(), orderPL.doubleValue(), order.getReturnAmount().doubleValue(),SettlementType.SETTLEMENT));
			} else {
				logUtility.log(new OrderSettlementMessageLogs(this.getClass().getSimpleName(), Type.SETTLED, order.getOrder().getId(), null, null, null, message, order.getOrderSettlement(), orderPL.doubleValue(), null,SettlementType.SETTLEMENT));
			}
			
			// set the profit loss value of the order in vodds model extra table
			BettingModelExtraVodds extraVModel = helper.getModelExtraEntity(order.getOrder().getId());
			BigDecimal refundOustanding = BigDecimal.ZERO;
			if(extraVModel != null){
				//Log data to push message to userstatscore service -> Update Bitrix contact
				this.genUtil.logOrderStats(extraVModel, orderPL, confirmedStake);

				//BigDecimal confirmedStakeBefore = extraVModel.getConfirmedStake();
				
				if(order.getOrder().getModelStatus().equals(NormalOrderStatus.REFUNDED.name())) {

					logger.info("orderId {} - refundOustanding {} - outstandingExtraVModel {} - confirmedStakeExtraVModel {}",
							order.getOrder().getId(),refundOustanding,extraVModel.getOutstandingAmount(),extraVModel.getConfirmedStake());
				}
				
				refundOustanding = extraVModel.getOutstandingAmount().negate();
				
				extraVModel.setProfitLoss(orderPL);
				extraVModel.setConfirmedStake(confirmedStake);
				//save order PL for Normal/Future Normal into BetUserNode
				user.addPl(orderPL, order.getOrder().getId());
				user.addOutstanding(refundOustanding, order.getOrder().getId());
				
				//update profitloss for orderWrapper
				order.setProfitLoss(orderPL);
				
				//tQueue.addExtraVoddsModel(extraVModel);
				logger.info("OrderId: {} update pl {} confirmed {}",order.getOrder().getId(),orderPL,confirmedStake);
				persistenceModerator.saveBettingModelExtraVodds(extraVModel);
				
				tradeLimitManger.updateStakePerMatchCacheAndPositionalPotentialLossAfterSettlement(user, extraVModel.getMatchId(), BigDecimal.ZERO,refundOustanding,order.getOrder().getId());
			}
			else {
				logger.error("failed to find the extra vodds model entry for model {}",order.getOrder().getId());
			}
			persistenceModerator.saveBetUser(user.getUser());
		
		}catch(Exception ex){
			logger.error("failed to settle order {} ",order.getOrder().getId(),ex);
		}
		
		return;
	}
	
	public void populateSettleVoidingOrderTransactions(OrderWrapper order,BetUserNode betUser,BigDecimal refundMargin,BigDecimal priorCredit, BigDecimal postCredit,
				AgentUserNode agentUser,BigDecimal settledCredit,BigDecimal priorCreditAgent,BigDecimal postCreditAgent,BigDecimal openPL,BigDecimal closePL,String status,Boolean isModified,BigDecimal mpl,Boolean isVoidingByOE){
		try{
			
		
			
			if(status.equals(PositionalOrderStage.VOIDING.name())){
				persistenceModerator.saveBetUser(betUser.getUser());
				
				//transaction refund margin for user
				ModelTransaction refundTransaction = new ModelTransaction();
				refundTransaction.setModelId(order.getOrder().getId());
				String remark = this.voddsCommonUtility.stringsToDescription(TX_REFUND_VOIDING_ORDER,format(refundMargin.negate()));
//				refundTransaction.setRemarks(String.format("Refund margin of %s for user with voiding order",format(refundMargin.negate())));
				refundTransaction.setRemarks(remark);
				refundTransaction.setUser(betUser.getUser().getUser());
				refundTransaction.setCreditAmount(refundMargin.negate());
				refundTransaction.setType(ModelTransactionType.REFUND);
				refundTransaction.setTime(new Timestamp(System.currentTimeMillis()));
				refundTransaction.setCreditBefore(priorCredit);
				refundTransaction.setCreditAfter(postCredit);
				refundTransaction.setiService("CMS");
				
				tQueue.addTransaction(refundTransaction);
			}
			
			persistenceModerator.saveAgentUser(agentUser.getUser());
			
			// set the execution stage of position order to settled
			PositionalOrderWrapper poWrapper = (PositionalOrderWrapper) order;
			
			if(isModified) {
				mpl = BigDecimal.ZERO;
			}else {
				if(!isVoidingByOE) {
					mpl = poWrapper.getpModel().getMaxPotentialLoss().negate();
				}
			}
			
			logger.debug("Update stake per match and positional potential loss value {} for isVoidingByOE {} after settled orderId {}",mpl,isVoidingByOE,order.getOrder().getId());
			
					
			persistenceModerator.saveOldStatus(poWrapper.getpModel().getId(),poWrapper.getpModel().getExecutionStage());
			
			poWrapper.getpModel().setVoidedPl(settledCredit);
			poWrapper.getpModel().setExecutionStage(PositionalOrderStage.VOIDED.name());
			poWrapper.getpModel().setMaxPotentialLoss(new BigDecimal(0));
			poWrapper.getpModel().setTotalMargin(new BigDecimal(0));
			poWrapper.getpModel().setEffectivePL(new BigDecimal(0));
			persistenceModerator.savePositionalModel(poWrapper.getpModel());
			
			logger.info("execution stage {}",poWrapper.getpModel().getExecutionStage());
			logger.info("effective pl {}",new BigDecimal(0).doubleValue());
			logger.info("refund margin {}",refundMargin.negate().doubleValue());
			
			PoSettlement poSettlement = new PoSettlement(PositionalOrderStage.valueOf(poWrapper.getpModel().getExecutionStage()),new BigDecimal(0).doubleValue(), refundMargin.negate().doubleValue(),SettlementType.SETTLEMENT);
			PositionalOrderStateMessageLog logMesage =  new PositionalOrderStateMessageLog(this.getClass().getSimpleName(), Type.SETTLED, poWrapper.getpModel().getId(), null, null, PositionalOrderStage.valueOf(poWrapper.getpModel().getExecutionStage()), "Refund margin for voiding order", betUser.getUser().getCurrency().getCode(), poWrapper.getpModel().getId(), poWrapper.getpModel().getOpenModelId().getId(), poWrapper.getpModel().getCloseModelId().getId(), null, null, null, null, null, null,  poSettlement, betUser.getUser().getId(), null,null,null,null);
			logUtility.log(logMesage);
			
			// set the settlement status of opening order to settled
			if(isAllBetsRejected(order.getBetWrappers())){
				poWrapper.getoModel().setModelStatus(PositionalCloseStatus.REFUNDED.name());
			}
			else{
				if(openPL.compareTo(BigDecimal.ZERO) > 0){
					poWrapper.getoModel().setModelStatus(PositionalCloseStatus.WIN.name());
				}else if(openPL.compareTo(BigDecimal.ZERO) == 0){
					poWrapper.getoModel().setModelStatus(PositionalCloseStatus.DRAW.name());
				}else{
					poWrapper.getoModel().setModelStatus(PositionalCloseStatus.LOSS.name());
				}
			}
			
			poWrapper.getoModel().setIsModified((byte)0);
			
			//persistenceModerator.saveOrder(poWrapper.getoModel());
			
			if(order.getOrderSettlement().equals(PositionalCloseStatus.UNFULFILLED.name())){
				order.getOrder().setModelStatus(PositionalCloseStatus.UNFULFILLED.name());
			}else{
				if(isAllBetsRejectedForCloseOrder(order.getBetWrappers(),order)){
					order.getOrder().setModelStatus(PositionalCloseStatus.REFUNDED.name());
				}
				else{
					if(closePL.compareTo(BigDecimal.ZERO) > 0){
						order.getOrder().setModelStatus(PositionalCloseStatus.WIN.name());
					}else if(closePL.compareTo(BigDecimal.ZERO) == 0){
						order.getOrder().setModelStatus(PositionalCloseStatus.DRAW.name());
					}else{
						order.getOrder().setModelStatus(PositionalCloseStatus.LOSS.name());
					}
				}
			}
			
			order.getOrder().setIsModified((byte)0);
			
			//persistenceModerator.saveOrder(order.getOrder());
			
			
			ModelTransaction settleTransaction = new ModelTransaction();
			
			settleTransaction.setModelId(order.getOrder().getId());
			//settleTransaction.setRemarks("adjusted credit for company agent after settling the positional order with voiding order");
			String remark = this.voddsCommonUtility.stringsToDescription(TX_ADJUSTED_CREDIT_VOIDING_ORDER);
			settleTransaction.setRemarks(remark);
			settleTransaction.setUser(agentUser.getUser().getUser());
			settleTransaction.setCreditAmount(settledCredit);
			settleTransaction.setType(ModelTransactionType.RETURN);
			settleTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			settleTransaction.setCreditBefore(priorCreditAgent);
			settleTransaction.setCreditAfter(postCreditAgent);
			settleTransaction.setiService("CMS");
			
			BettingModelExtraVodds openVModel = helper.getModelExtraEntity(poWrapper.getpModel().getOpenModelId().getId());
			
			
			tradeLimitManger.updateStakePerMatchCacheAndPositionalPotentialLossAfterSettlement(betUser, openVModel.getMatchId(), mpl.negate(),BigDecimal.ZERO,order.getOrder().getId());
			tQueue.addTransaction(settleTransaction);
		}catch (Exception e) {
			logger.error("Error settling voiding order {} with exception {}",order.getOrder().getId(),e);
		}
	}
	
	/****
	 * populate required database transactions after settling a positional order  
	 * @param order
	 * @param user
	 * @param priorCredit
	 * @param postCredit
	 * @param refundAmount
	 * @param stopLoss
	 * @throws InterruptedException 
	 */
	public void populateSettleOrderTransactions(OrderWrapper order, BetUserNode user, BigDecimal priorCredit, BigDecimal postCredit, BigDecimal priorRefCredit, BigDecimal postRefCredit, 
			BigDecimal refundAmount, BigDecimal stopLoss,BigDecimal openPL,BigDecimal closePL, BigDecimal orderPL, boolean isFirstSettlement,BigDecimal openStake,BigDecimal closeStake,String message){
		
		// perform the actions that are common with normal order
		//populateSettleOrderTransactions(order,user,priorCredit,postCredit);
		
		// add the modified user object to the transaction queue
		//tQueue.addTransaction(user.getUser());
		
		
		//if(order.getReturnAmount().compareTo(BigDecimal.ZERO) != 0 || order.getOrderSettlement().equals(NormalOrderStatus.LOSS.name()) || order.getOrderSettlement().equals(NormalOrderStatus.REFUNDED.name())){
		
			//record the return amount transaction for the order 
			ModelTransaction sTransaction = new ModelTransaction();
			
			sTransaction.setModelId(order.getOrder().getId());
			//sTransaction.setRemarks("Order settled, pl is added to user credit");
			sTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_SETTLED_ORDER));
			sTransaction.setUser(user.getUser().getUser());
			sTransaction.setCreditAmount(order.getReturnAmount());
			sTransaction.setType(ModelTransactionType.RETURN);
			sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			sTransaction.setCreditBefore(priorCredit);
			sTransaction.setCreditAfter(postCredit);
			sTransaction.setiService("CMS");
	
	
			tQueue.addTransaction(sTransaction);
		//}
		
		// set the status of order to settled
		if(order.getOrderSettlement().equals(PositionalCloseStatus.UNFULFILLED.name())){
			order.getOrder().setModelStatus(PositionalCloseStatus.UNFULFILLED.name());
		}else{
			if(isAllBetsRejectedForCloseOrder(order.getBetWrappers(),order)){
				order.getOrder().setModelStatus(PositionalCloseStatus.REFUNDED.name());
			}
			else{
				if(closePL.compareTo(BigDecimal.ZERO) > 0){
					order.getOrder().setModelStatus(PositionalCloseStatus.WIN.name());
				}else if(closePL.compareTo(BigDecimal.ZERO) == 0){
					order.getOrder().setModelStatus(PositionalCloseStatus.DRAW.name());
				}else{
					order.getOrder().setModelStatus(PositionalCloseStatus.LOSS.name());
				}
			}
		}
		
		order.getOrder().setIsModified((byte)0);
		// set the status of order to settled
		//order.getOrder().setModelStatus(order.getOrderSettlement());		
		//tQueue.addOrder(order.getOrder());
		
		//persistenceModerator.saveOrder(order.getOrder());
		
		
		// when there is a valid refund amount for the order
		if(refundAmount.compareTo(BigDecimal.ZERO) > 0){
			ModelTransaction refTransaction = new ModelTransaction();
			
			refTransaction.setModelId(order.getOrder().getId());
			refTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REFUND_CREDIT_FOR_STOP_LOSS,format(stopLoss)));
			refTransaction.setUser(user.getUser().getUser());
			refTransaction.setCreditAmount(refundAmount);
			refTransaction.setType(ModelTransactionType.REFUND);
			refTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			refTransaction.setCreditBefore(priorRefCredit);
			refTransaction.setCreditAfter(postRefCredit);
			refTransaction.setiService("CMS");


			tQueue.addTransaction(refTransaction);
		}

		String orderType = order.getOrder().getModelType(); 
		// set the status of positional model execution stage for positional order
		if(orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name()) || orderType.equals("VOFutureClose") || orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())){
			
			PositionalOrderWrapper poWrapper = (PositionalOrderWrapper) order;
			
			if (poWrapper.getpModel().getMaxPotentialLoss() != null && isFirstSettlement){
				mplNotificationTask.addOrderHaveMPlChange(poWrapper.getpModel().getId(), poWrapper.getpModel().getCloseModelId().getId(),
						poWrapper.getpModel().getMaxPotentialLoss().doubleValue(), poWrapper.getpModel().getOpenModelId().getOwner());
			}
			
			BigDecimal mpl = poWrapper.getpModel().getMaxPotentialLoss();
			
			poWrapper.getpModel().setMaxPotentialLoss(new BigDecimal(0));
			poWrapper.getpModel().setTotalMargin(new BigDecimal(0));
			
			// refund amount is positive when the stop loss is greater than the order effective pl
			if(refundAmount.compareTo(new BigDecimal(0)) > 0){
				poWrapper.getpModel().setEffectivePL(stopLoss);
				//save effective pl for PO/Future PO into BetUserNode
				user.addPl(stopLoss, poWrapper.getpModel().getId());
				
			}
			// set the effective pl to the return amount of sum of open and close order in all other cases (even when it is not guranteed stop)
			else{
				poWrapper.getpModel().setEffectivePL(orderPL);
				//save effective pl for PO/Future PO into BetUserNode
				user.addPl(orderPL, poWrapper.getpModel().getId());
				
				poWrapper.setProfitLoss(orderPL);
			}
			
			persistenceModerator.saveOldStatus(poWrapper.getpModel().getId(),poWrapper.getpModel().getExecutionStage());
			// set the status of order to settled
			if(isAllBetsRejected(order.getBetWrappers())){
				poWrapper.getpModel().setExecutionStage(PositionalOrderStage.REFUNDED.name());
			}
			else{
				if(poWrapper.getpModel().getEffectivePL().compareTo(BigDecimal.ZERO) > 0){
					poWrapper.getpModel().setExecutionStage(PositionalOrderStage.WIN.name());
				}else if(poWrapper.getpModel().getEffectivePL().compareTo(BigDecimal.ZERO) == 0){
					poWrapper.getpModel().setExecutionStage(PositionalOrderStage.DRAW.name());
				}else{
					poWrapper.getpModel().setExecutionStage(PositionalOrderStage.LOSS.name());
				}
			}
			
			//tQueue.addPositionalModel(poWrapper.getpModel());
			persistenceModerator.savePositionalModel(poWrapper.getpModel());
			
			String remark = "";
			if (isFirstSettlement) {
				remark = "Positional Order Settled with profit/loss value : " + format(poWrapper.getpModel().getEffectivePL());
			} else {
				remark = "Positional Order Re-Settled with profit/loss value : " + format(poWrapper.getpModel().getEffectivePL());
			}
			PoSettlement poSettlement = new PoSettlement(PositionalOrderStage.valueOf(poWrapper.getpModel().getExecutionStage()), poWrapper.getpModel().getEffectivePL().doubleValue(),null,SettlementType.SETTLEMENT);
			PositionalOrderStateMessageLog logMesage =  new PositionalOrderStateMessageLog(this.getClass().getSimpleName(), Type.SETTLED, poWrapper.getpModel().getId(), null, null, PositionalOrderStage.valueOf(poWrapper.getpModel().getExecutionStage()), remark, user.getUser().getCurrency().getCode(), poWrapper.getpModel().getId(), poWrapper.getpModel().getOpenModelId().getId(), poWrapper.getpModel().getCloseModelId().getId(), null, null, null, null, null, null,  poSettlement, user.getUser().getId(), null,null,null,null);
			logUtility.log(logMesage);	
			
			// set the settlement status of opening order to settled
			//poWrapper.getoModel().setModelStatus(order.getOrderSettlement());
			OrderWrapper oWrap = oManager.findStoppedOrderById(poWrapper.getoModel().getId());
			if(isAllBetsRejected(oWrap.getBetWrappers())){
				poWrapper.getoModel().setModelStatus(PositionalOpenStatus.REFUNDED.name());
			}
			else{
				if(openPL.compareTo(BigDecimal.ZERO) > 0){
					poWrapper.getoModel().setModelStatus(PositionalOpenStatus.WIN.name());
				}else if(openPL.compareTo(BigDecimal.ZERO) == 0){
					poWrapper.getoModel().setModelStatus(PositionalOpenStatus.DRAW.name());
				}else{
					poWrapper.getoModel().setModelStatus(PositionalOpenStatus.LOSS.name());
				}
			}
			
			poWrapper.getoModel().setIsModified((byte)0);
			//tQueue.addOrder(poWrapper.getoModel());
			//persistenceModerator.saveOrder(poWrapper.getoModel());
			
			try{
				BettingModelExtraVodds openVModel = helper.getModelExtraEntity(poWrapper.getpModel().getOpenModelId().getId());
				if(openVModel != null){
					this.genUtil.logOrderStats(openVModel, openPL, openStake);
					openVModel.setProfitLoss(openPL);
					openVModel.setConfirmedStake(openStake);
					//tQueue.addExtraVoddsModel(openVModel);
					logger.info("Open orderId: {} update pl {} confirmed {}",openVModel.getModelId().getId(),openPL,openStake);
					persistenceModerator.saveBettingModelExtraVodds(openVModel);

				}

				BettingModelExtraVodds closeVModel = helper.getModelExtraEntity(poWrapper.getpModel().getCloseModelId().getId());
				if(closeVModel != null){
					this.genUtil.logOrderStats(closeVModel, closePL, closeStake);
					closeVModel.setProfitLoss(closePL);
					closeVModel.setConfirmedStake(closeStake);
					//tQueue.addExtraVoddsModel(closeVModel);
					logger.info("Close orderId: {} update pl {} confirmed {}",closeVModel.getModelId().getId(),closePL,closeStake);
					persistenceModerator.saveBettingModelExtraVodds(closeVModel);

				}
				
				tradeLimitManger.updateStakePerMatchCacheAndPositionalPotentialLossAfterSettlement(user, openVModel.getMatchId(), mpl,BigDecimal.ZERO,order.getOrder().getId());

			}
			catch(Exception ex){
				logger.error("error while updating the order pl for positional orders",ex);
			}
			
			
			
		}
		persistenceModerator.saveBetUser(user.getUser());
		return;
	}
	
	/**
	 * populate transactions related to settling a modified order
	 * @param order
	 * @param user
	 * @param revAmount
	 */
	public void populateSettleModifiedOrderTransactions(OrderWrapper order, BetUserNode user,BigDecimal revAmount, BigDecimal priorCredit, BigDecimal postCredit){
			
		String orderType = order.getOrder().getModelType();
		//case normal order
		if(orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name()) 
			|| orderType.equals("VOFutureNormal")
			|| orderType.equals(VOddsModelType.FUTURE_NORMAL.name())){
			BettingModelExtraVodds extraVModel = helper.getModelExtraEntity(order.getOrder().getId());
			if(extraVModel != null){
				BigDecimal revertedProfitLoss = extraVModel.getProfitLoss();
				if(revertedProfitLoss != null){
					logger.info("update revert profit loss {} -  user {} - orderId {}  from db",revertedProfitLoss.negate(),user.getUser().getId(),order.getOrder().getId());
					user.addPl(revertedProfitLoss.negate(), order.getOrder().getId());
				}else{
					logger.info("update revert profit loss {} -  user {} - orderId {} from cache order wrapper",order.getProfitLoss().negate(),user.getUser().getId(),order.getOrder().getId());
					user.addPl(order.getProfitLoss().negate(), order.getOrder().getId());
				}
				
			}else{
				logger.error("failed to find the extra vodds model entry for model {}",order.getOrder().getId());
			}
		}//case po
		else if (orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name())
				|| orderType.equals("VOFutureClose")
				|| orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())) {
			PositionalOrderWrapper poWrapper = (PositionalOrderWrapper) order;
			BigDecimal revertedEffectivePl = poWrapper.getpModel().getEffectivePL();
			if(revertedEffectivePl != null){
				logger.info("update revert effective Pl {} -  user {} - orderId {} from db",revertedEffectivePl.negate(),user.getUser().getId(),order.getOrder().getId());
				user.addPl(revertedEffectivePl.negate(), order.getOrder().getId());
			}else{
				logger.info("update revert effective Pl {} -  user {} - orderId {} from cache order wrapper",poWrapper.getProfitLoss().negate(),user.getUser().getId(),order.getOrder().getId());
				user.addPl(poWrapper.getProfitLoss().negate(), order.getOrder().getId());
			}
		}
		//record the return amount transaction for the order 
		ModelTransaction sTransaction = new ModelTransaction();
		
		sTransaction.setModelId(order.getOrder().getId());
		//sTransaction.setRemarks("order previous return amount reverted as the PL changed");
		sTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REVERTED_ORDER));
		sTransaction.setUser(user.getUser().getUser());
		sTransaction.setCreditAmount(revAmount.negate());
		sTransaction.setType(ModelTransactionType.REVERTED);
		sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
		
		sTransaction.setCreditBefore(priorCredit);
		sTransaction.setCreditAfter(postCredit);
		sTransaction.setiService("CMS");

		tQueue.addTransaction(sTransaction);
		persistenceModerator.saveBetUser(user.getUser());
		
	
	}
	
	public void revertSettlementForStatusBetChange(OrderWrapper order, BetUserNode user,BigDecimal revAmount, BigDecimal priorCredit, BigDecimal postCredit,BigDecimal amount,String matchId){
		try{
			//1.Update credit for user
			
			
			//2.Revert settlement of order
			//check is exist last revert transaction to skip add more revert transaction
			if(!this.helper.checkExistLastRevertTransactionsUnsettledOrder(order.getOrder().getId())) {
				ModelTransaction sTransaction = new ModelTransaction();
				sTransaction.setModelId(order.getOrder().getId());
				sTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REVERTED_ORDER_BY_STATUS_CHANGE));
				sTransaction.setUser(user.getUser().getUser());
				sTransaction.setCreditAmount(revAmount.negate());
				sTransaction.setType(ModelTransactionType.REVERTED);
				sTransaction.setTime(new Timestamp(System.currentTimeMillis()));

				sTransaction.setCreditBefore(priorCredit);
				sTransaction.setCreditAfter(postCredit);
				sTransaction.setiService("CMS");

				tQueue.addTransaction(sTransaction);

				String orderType = order.getOrder().getModelType();

				//case normal order
				if(orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name())
						|| orderType.equals("VOFutureNormal")
						|| orderType.equals(VOddsModelType.FUTURE_NORMAL.name())){


					BettingModelExtraVodds extraVModel = helper.getModelExtraEntity(order.getOrder().getId());
					if(extraVModel != null){
						BigDecimal revertedProfitLoss = extraVModel.getProfitLoss();
						extraVModel.setOutstandingAmount(amount.negate());
						extraVModel.setProfitLoss(BigDecimal.ZERO);
						user.addPl(revertedProfitLoss.negate(), order.getOrder().getId());
						logger.info("OrderId: {} update pl {} outstanding {}",order.getOrder().getId(),BigDecimal.ZERO,amount);
						//3.Update outstanding and pl for normal order
						persistenceModerator.persistBettingModelExtraVoddsForStatusChange(extraVModel);
						//4.Notification outstanding
						user.addOutstanding(amount.negate(), order.getOrder().getId());
						UserOutstandingChangeEvent amt = new UserOutstandingChangeEvent(user.getUser().getUser().getUsername(), amount.negate().doubleValue(), user.getUser().getId());
						amtNotificationTask.submitOustanding(amt);
						//5.Update stake per match cache
						this.tradeLimitManger.updateStakePerMatchForResettlement(user, matchId, amount.negate(),order.getOrder().getId());
					}
					else{
						logger.error("failed to find the extra vodds model entry for model {}",order.getOrder().getId());
					}
					logUtility.log(new OrderSettlementMessageLogs(this.getClass().getSimpleName(), Type.SETTLED, order.getOrder().getId(), null, null, NormalOrderStatus.valueOf(order.getOrderSettlement()), TX_REVERTED_ORDER_BY_STATUS_CHANGE, null, new BigDecimal(0).doubleValue(), revAmount.negate().doubleValue(),SettlementType.REVERT_SETTLEMENT_BY_STATUS_CHANGE));

				}else if (orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name())
						|| orderType.equals("VOFutureClose")
						|| orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())) {
					PositionalOrderWrapper poWrapper = (PositionalOrderWrapper) order;
					BigDecimal revertedEffectivePl = poWrapper.getpModel().getEffectivePL();
					poWrapper.getpModel().setEffectivePL(BigDecimal.ZERO);
					user.addPl(revertedEffectivePl.negate(), poWrapper.getpModel().getId());
					poWrapper.getpModel().setTotalMargin(amount.negate());
					poWrapper.getpModel().setMaxPotentialLoss(amount);
					persistenceModerator.saveOldStatus(poWrapper.getpModel().getId(),poWrapper.getpModel().getExecutionStage());
					persistenceModerator.savePositionalModel(poWrapper.getpModel());
					mplNotificationTask.addOrderHaveMPlChange(poWrapper.getpModel().getId(), poWrapper.getpModel().getCloseModelId().getId(),
							poWrapper.getpModel().getMaxPotentialLoss().doubleValue(), poWrapper.getpModel().getOpenModelId().getOwner());
					try{
						BettingModelExtraVodds openVModel = helper.getModelExtraEntity(poWrapper.getpModel().getOpenModelId().getId());
						if(openVModel != null){
							this.genUtil.logOrderStats(openVModel, BigDecimal.ZERO, openVModel.getConfirmedStake());
							openVModel.setProfitLoss(BigDecimal.ZERO);
							//tQueue.addExtraVoddsModel(openVModel);
							logger.info("Open orderId: {} update pl {} by status change",openVModel.getModelId().getId(),openVModel.getProfitLoss());
							persistenceModerator.saveBettingModelExtraVodds(openVModel);

						}

						BettingModelExtraVodds closeVModel = helper.getModelExtraEntity(poWrapper.getpModel().getCloseModelId().getId());
						if(closeVModel != null){
							this.genUtil.logOrderStats(openVModel, BigDecimal.ZERO, closeVModel.getConfirmedStake());
							closeVModel.setProfitLoss(BigDecimal.ZERO);
							//tQueue.addExtraVoddsModel(closeVModel);
							logger.info("Close orderId: {} update pl {} by status change",closeVModel.getModelId().getId(),closeVModel.getProfitLoss());
							persistenceModerator.saveBettingModelExtraVodds(closeVModel);

						}

					}
					catch(Exception ex){
						logger.error("error while updating the order pl for positional orders",ex);
					}

					PoSettlement poSettlement = new PoSettlement(PositionalOrderStage.valueOf(poWrapper.getpModel().getExecutionStage()),new BigDecimal(0).doubleValue(), revAmount.negate().doubleValue(),SettlementType.REVERT_SETTLEMENT_BY_STATUS_CHANGE);
					PositionalOrderStateMessageLog logMesage =  new PositionalOrderStateMessageLog(this.getClass().getSimpleName(), Type.SETTLED, poWrapper.getpModel().getId(), null, null,PositionalOpenStatus.valueOf(poWrapper.getOrderSettlement()), TX_REVERTED_ORDER_BY_STATUS_CHANGE, user.getUser().getCurrency().getCode(), poWrapper.getpModel().getId(), poWrapper.getpModel().getOpenModelId().getId(), poWrapper.getpModel().getCloseModelId().getId(), null, null, null, null, null, null,  poSettlement, user.getUser().getId(), null,null,null,null);
					logUtility.log(logMesage);

					//5.Update stake per match cache
					this.tradeLimitManger.updateStakePerMatchForResettlement(user, matchId, amount.negate(),order.getOrder().getId());
				}
				persistenceModerator.saveBetUser(user.getUser());
			}
		
		}catch(Exception ex){
			logger.error("Exception while reverting settlement for order {} , exception: ",order.getOrder().getId(),ex);
		}
	}
	
	/**
	 * populate transactions related to settling a modified order
	 * @param order
	 * @param user
	 * @param revAmount
	 */
	public void populateSettleModifiedOrderTransactionsOperator(OrderWrapper order, AgentUserNode user,BigDecimal revAmount, BigDecimal priorCredit, BigDecimal postCredit){
			persistenceModerator.saveAgentUser(user.getUser());
			ModelTransaction sTransaction = new ModelTransaction();
			
			sTransaction.setModelId(order.getOrder().getId());
			//sTransaction.setRemarks("order previous return amount reverted as the PL changed");
			sTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REVERTED_ORDER));
			sTransaction.setUser(user.getUser().getUser());
			sTransaction.setCreditAmount(revAmount.negate());
			sTransaction.setType(ModelTransactionType.REVERTED);
			sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			sTransaction.setCreditBefore(priorCredit);
			sTransaction.setCreditAfter(postCredit);
			sTransaction.setiService("CMS");
	
			tQueue.addTransaction(sTransaction);
		
	
	}
	
	public void populateSettleModifiedOrderTransactionsForOperator(OrderWrapper order, AgentUserNode user,BigDecimal revAmount, BigDecimal priorCredit, BigDecimal postCredit){
			persistenceModerator.saveAgentUser(user.getUser());
			ModelTransaction sTransaction = new ModelTransaction();
			
			sTransaction.setModelId(order.getOrder().getId());
			//sTransaction.setRemarks("reverted return amount of company agent");
			sTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REVERTED_ORDER_OPERATOR));
			sTransaction.setUser(user.getUser().getUser());
			sTransaction.setCreditAmount(revAmount.negate());
			sTransaction.setType(ModelTransactionType.REVERTED);
			sTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			sTransaction.setCreditBefore(priorCredit);
			sTransaction.setCreditAfter(postCredit);
			sTransaction.setiService("CMS");
	
			tQueue.addTransaction(sTransaction);
		
	
	}

	@SuppressWarnings("finally")
	public ThriftExecuteTransactionResult transferPtplForAgent(AgentUserNode agentUser,BigDecimal ptplTransfer, ThriftTransferPtPlForUser transferInfo){
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		try {
			Transaction transaction = new Transaction();
			
			if(ptplTransfer.abs().compareTo(agentUser.getCreditPtpl().abs()) <= 0)
			{
				transaction.setType(jayeson.database.Transaction.TransactionType.TRANSFER_PTPL);
			
				agentUser.addCreditPtpl(ptplTransfer.negate());
				
				BigDecimal[] creditAgent = agentUser.addCredit(ptplTransfer);
				BigDecimal preCredit = creditAgent[0];
				BigDecimal postCredit = creditAgent[1];
				transaction.setBefore_credit(preCredit);
				transaction.setUser(agentUser.getUser().getUser());
				transaction.setTime(new Timestamp(System.currentTimeMillis()));
				transaction.setAmount(ptplTransfer);
				transaction.setAfter_credit(postCredit);
				String remarks = this.voddsCommonUtility.stringsToDescription(TX_TRANSLATE_PT_PL,format(ptplTransfer),agentUser.getUser().getCurrency().getCode(),
						agentUser.getUser().getUsername(), transferInfo.remark);

				transaction.setDescription(remarks);
				Service service = helper.getServiceById(transferInfo.serviceId);
				transaction.setInitiatingService(service);
				transaction.setInitiating_user(agentUser.getUser().getUser());
				persistenceModerator.saveAgentUser(agentUser.getUser());
				persistenceModerator.saveAndRemoveObjAgentUserFromMapWithPtpl(agentUser.getUser());

				// create user activity
				UserActivity activity = new UserActivity(agentUser.getUser().getUser(), helper.findUserAction(Activity.TRANSFER.getName()),
						transaction.getTime(), Activity.TRANSFER.getDescription(format(ptplTransfer), agentUser.getUser().getCurrency().getCode()), transferInfo.ipAddress);
				if(transferInfo.isAliasActivity) {
					User alias = new User();
					alias.setId(transferInfo.aliasId);
					activity.setUserID(alias);
				}

				TransactionWrapper transactionWrapper = new TransactionWrapper(transaction, activity);

				tQueue.addTransaction(transactionWrapper);
				logger.info("Transfer Ptpl {} for user {} with credit before {} , credit after {}",ptplTransfer,agentUser.getUser().getUsername(),preCredit,postCredit);
				
				executeTransactionResult.setStatus(0);
				executeTransactionResult.setMessage("Transfer ptpl successfully");
			}else{
				executeTransactionResult.setStatus(1);
				executeTransactionResult.setMessage("Can not transfer ptpl because the current ptpl"+agentUser.getCreditPtpl().negate()+"is lower than value transfer"+ptplTransfer.negate());
			}

		} catch (Exception ex) {
			logger.error("Error while transfer ptpl for agent!", ex);
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("Can not transfer ptpl");
		}finally{
			return executeTransactionResult;
		}

	}
	
	public ThriftExecuteTransactionResult updateCreditForAgent(AgentUserNode agentUser, BigDecimal credit, AgentUserNode initiatingUser,
															   ThriftUpdateCreditForUser updateInfo){
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		try {
			Transaction transaction = new Transaction();
			UserActivity activity = new UserActivity(initiatingUser.getUser().getUser(), null, null, null,
					updateInfo.ipAddress);
			if(updateInfo.isAliasActivity) {
				User alias = new User();
				alias.setId(updateInfo.aliasId);
				activity.setUserID(alias);
			}

			String remarks = "";
			if(credit.compareTo(new BigDecimal(0)) < 0)
			{
				transaction.setType(jayeson.database.Transaction.TransactionType.WITHDRAW);
				//systemRemark = "WITHDRAW ";
				String currencyParrentUser = initiatingUser.getUser().getCurrency().getCode();
				String currencyChildUser = agentUser.getUser().getCurrency().getCode();
				BigDecimal conversionRate = helper.getConversionRate(currencyChildUser, currencyParrentUser);
				BigDecimal creditTransfer = credit.multiply(conversionRate);
				if(agentUser.getCredit().compareTo(credit.negate()) >= 0){
					BigDecimal creditTransferOfParent = initiatingUser.getUser().getCredit().subtract(creditTransfer);
					initiatingUser.getUser().setCredit(creditTransferOfParent);
					initiatingUser.setCredit(creditTransferOfParent);
					transaction.setBefore_credit(agentUser.getCredit());
					BigDecimal creditTransferOfChild = agentUser.getUser().getCredit().add(credit);
					agentUser.getUser().setCredit(creditTransferOfChild);
					agentUser.setCredit(creditTransferOfChild);

					remarks = this.voddsCommonUtility.stringsToDescription(TX_WITHDRAW,format(credit),currencyChildUser,initiatingUser.getUser().getUsername(),
							agentUser.getUser().getUsername(),format(creditTransfer),currencyParrentUser, updateInfo.remark);
					activity.setDescription(Activity.WITHDRAW.getDescription(format(credit), currencyChildUser, agentUser.getUser().getUsername()));
					activity.setActionID(helper.findUserAction(Activity.WITHDRAW.getName()));
					executeTransactionResult.setStatus(0);
					executeTransactionResult.setMessage("Withdraw successfully");
				}else{
					executeTransactionResult.setStatus(1);
					executeTransactionResult.setMessage("Can't withdraw because agent don't have enough credit");
				}
			}else{
				transaction.setType(jayeson.database.Transaction.TransactionType.TOPUP);
				//systemRemark = "Topup credit";
				String currencyChildUser = initiatingUser.getUser().getCurrency().getCode();
				String currencyParrentUser = agentUser.getUser().getCurrency().getCode();
				BigDecimal conversionRate = helper.getConversionRate(currencyParrentUser, currencyChildUser);
				BigDecimal creditTopUp = credit.multiply(conversionRate);
				if(initiatingUser.getCredit().compareTo(creditTopUp) >= 0){
					BigDecimal creditTopUpOfParent = initiatingUser.getUser().getCredit().subtract(creditTopUp);
					initiatingUser.getUser().setCredit(creditTopUpOfParent);
					initiatingUser.setCredit(creditTopUpOfParent);
					transaction.setBefore_credit(agentUser.getCredit());
					BigDecimal creditTopUpOfChild = agentUser.getUser().getCredit().add(credit);
					agentUser.getUser().setCredit(creditTopUpOfChild);
					agentUser.setCredit(creditTopUpOfChild);
//
					remarks = this.voddsCommonUtility.stringsToDescription(TX_TOPUP,format(credit),currencyParrentUser,initiatingUser.getUser().getUsername(),
							agentUser.getUser().getUsername(),format(creditTopUp),currencyChildUser,updateInfo.remark);
					activity.setDescription(Activity.TOPUP.getDescription(format(credit), agentUser.getUser().getCurrency().getCode(), agentUser.getUser().getUsername()));
					activity.setActionID(helper.findUserAction(Activity.TOPUP.getName()));
					executeTransactionResult.setStatus(0);
					executeTransactionResult.setMessage("Topup successfully");
				}else{
					executeTransactionResult.setStatus(1);
					executeTransactionResult.setMessage("Can't topup because agent don't have enough credit");
				}
			}
	
			persistenceModerator.saveAgentUser(initiatingUser.getUser());
			persistenceModerator.saveAgentUser(agentUser.getUser());
			transaction.setUser(agentUser.getUser().getUser());
			transaction.setTime(new Timestamp(System.currentTimeMillis()));
			transaction.setAfter_credit(agentUser.getCredit());
			transaction.setAmount(credit);
			transaction.setDescription(remarks);
			Service service = helper.getServiceById(updateInfo.serviceId);
			transaction.setInitiatingService(service);
			transaction.setInitiating_user(initiatingUser.getUser().getUser());
			activity.setTime(transaction.getTime());
			persistenceModerator.saveAndRemoveObjAgentUserFromMap(initiatingUser.getUser());
			persistenceModerator.saveAndRemoveObjAgentUserFromMap(agentUser.getUser());
			TransactionWrapper transactionWrapper = new TransactionWrapper(transaction, activity);
			tQueue.addTransaction(transactionWrapper);
		} catch (Exception ex) {
			logger.error("Error while update user credit!", ex);
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("Failed to topup/withdraw");
		}
		return executeTransactionResult;
	}
	
	public ThriftExecuteTransactionResult updateCreditForMember(BetUserNode member,BigDecimal credit, AgentUserNode initiatingUser,
																ThriftUpdateCreditForUser updateInfo){
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();
		try{
			Transaction transaction = new Transaction();
			UserActivity activity = new UserActivity(initiatingUser.getUser().getUser(), null, null, null,
					updateInfo.ipAddress);
			if(updateInfo.isAliasActivity) {
				User alias = new User();
				alias.setId(updateInfo.aliasId);
				activity.setUserID(alias);
			}
			String remarks = "";
			if(credit.compareTo(BigDecimal.ZERO) < 0) {
				if(member.getCredit().compareTo(credit.negate()) >= 0){
					// verified user's credit tally
					boolean verified = true;
					BigDecimal cCredit = BigDecimal.valueOf(updateInfo.totalTransaction - updateInfo.totalMargin - member.getOutstandingAmount().doubleValue() +
							member.getPl().doubleValue());
					if(member.getCredit().subtract(cCredit).abs().compareTo(BigDecimal.valueOf(config.getCreditTallyThreshold())) > 0) {
						logger.info("{} user credit isn't tallied credit {} trans {} margin {} outstanding {} pl {}", member.getUser().getUsername(),
								member.getCredit(), updateInfo.totalTransaction, updateInfo.totalMargin, member.getOutstandingAmount(), member.getPl());
						verified = false;
						executeTransactionResult.setMessage("User credit isn't tallied");
						executeTransactionResult.setStatus(1);
					}

					if(verified) {
						transaction.setType(jayeson.database.Transaction.TransactionType.WITHDRAW);
						//type = "Withdraw credit";
						String currencyParrentUser = initiatingUser.getUser().getCurrency().getCode().toString();
						String currencyChildUser = member.getUser().getCurrency().getCode().toString();
						BigDecimal conversionRate = helper.getConversionRate(currencyChildUser, currencyParrentUser);
						BigDecimal creditTransfer = credit.multiply(conversionRate);
						initiatingUser.addCredit(creditTransfer.negate());
						BigDecimal[] credits = member.addCredit(credit, 0);
						transaction.setBefore_credit(credits[0]);
						transaction.setAfter_credit(credits[1]);
						transaction.setAmount(credit);
						remarks = this.voddsCommonUtility.stringsToDescription(TX_WITHDRAW, format(credit), currencyChildUser, initiatingUser.getUser().getUsername(),
								member.getUser().getUsername(), format(creditTransfer), currencyParrentUser, updateInfo.remark);

						activity.setDescription(Activity.WITHDRAW.getDescription(format(creditTransfer), currencyParrentUser, member.getUser().getUsername()));
						activity.setActionID(helper.findUserAction(Activity.WITHDRAW.getName()));

						executeTransactionResult.setStatus(0);
						executeTransactionResult.setMessage("Withdraw successfully");
					}
				}else{
					executeTransactionResult.setStatus(1);
					executeTransactionResult.setMessage("Can't withdraw because member don't have enough credit");
				}
		
			}else { // topup
				if (initiatingUser.getCredit().compareTo(credit) >= 0) {
					transaction.setType(jayeson.database.Transaction.TransactionType.TOPUP);
					String currencyChildUser = initiatingUser.getUser().getCurrency().getCode().toString();
					String currencyParrentUser = member.getUser().getCurrency().getCode().toString();
					BigDecimal conversionRate = helper.getConversionRate(currencyParrentUser, currencyChildUser);
					BigDecimal creditTopUp = credit.multiply(conversionRate);
					initiatingUser.addCredit(creditTopUp.negate());
					BigDecimal[] credits = member.addCredit(credit, 0);
					transaction.setBefore_credit(credits[0]);
					transaction.setAfter_credit(credits[1]);
					transaction.setAmount(credit);
					remarks = this.voddsCommonUtility.stringsToDescription(TX_TOPUP, format(credit), currencyParrentUser, initiatingUser.getUser().getUsername(),
							member.getUser().getUsername(), format(creditTopUp), currencyChildUser, updateInfo.remark);

					activity.setDescription(Activity.TOPUP.getDescription(format(creditTopUp), currencyParrentUser, member.getUser().getUsername()));
					activity.setActionID(helper.findUserAction(Activity.TOPUP.getName()));

					executeTransactionResult.setStatus(0);
					executeTransactionResult.setMessage("Topup successfully");
				} else {
					executeTransactionResult.setStatus(1);
					executeTransactionResult.setMessage("Can't topup because agent don't have enough credit");
				}
			}
			
			persistenceModerator.saveAgentUser(initiatingUser.getUser());
			persistenceModerator.saveBetUser(member.getUser());
			transaction.setUser(member.getUser().getUser());
			transaction.setTime(new Timestamp(System.currentTimeMillis()));
			transaction.setDescription(remarks);
			activity.setTime(transaction.getTime());

			Service service = helper.getServiceById(updateInfo.serviceId);
			transaction.setInitiatingService(service);
			transaction.setInitiating_user(initiatingUser.getUser().getUser());
			persistenceModerator.saveAndRemoveObjAgentUserFromMap(initiatingUser.getUser());
			persistenceModerator.saveAndRemoveObjBetUserFromMap(member.getUser());

			TransactionWrapper transactionWrapper = new TransactionWrapper(transaction, activity);
			tQueue.addTransaction(transactionWrapper);
		}catch (Exception ex) {
			logger.error("{} Error while update user credit!", member.getUser().getUsername(), ex);
			executeTransactionResult.setStatus(1);
			executeTransactionResult.setMessage("Failed to topup/withdraw");
		}
		return executeTransactionResult;
			
	}
	
	public void executeTransaction(BetUserNode betUser,BigDecimal credit,long modelId,String remark,ModelTransactionType type){
		try{
			
			ModelTransaction rTransaction = new ModelTransaction();
			rTransaction.setModelId(modelId);
			rTransaction.setUser(betUser.getUser().getUser());
			rTransaction.setCreditAmount(credit);
			rTransaction.setRemarks(remark);
			rTransaction.setType(type);
			rTransaction.setTime(new Timestamp(System.currentTimeMillis()));
			rTransaction.setCreditBefore(betUser.getCredit());
			BigDecimal creditAdfter = betUser.getUser().getCredit().add(credit);
			betUser.getUser().setCredit(creditAdfter);
			betUser.setCredit(creditAdfter);
			rTransaction.setCreditAfter(betUser.getCredit());
			rTransaction.setiService("CMS");
			
			BettingModel bm = helper.findBettingModelById(modelId);
			BettingModelExtraVodds bme = helper.getModelExtraEntity(modelId);
			if (VOddsModelType.FUTURE_NORMAL.name().equals(bm.getModelType()) || VOddsModelType.NORMAL.name().equals(bm.getModelType())) { 
				TradeLimitData data = null;
				switch (type) {
					case RESERVE:
						if (tradeLimitManger.getStakePerMatchCahe(betUser).containsKey(bme.getMatchId())) {
							data = new TradeLimitData(bme.getMatchId(), bme.getSportType().name(), bm.getModelStatus(), null, bme.getMatchStartTime(), new BigDecimal(0.0), credit.abs(), null, type.name());
							data.setTradeLimitCheck(betUser.isCheckTradeLimit());
							tradeLimitManger.updateStakePerMatchCache(betUser, data, modelId);
						}
						betUser.addOutstanding(credit.negate(), modelId);
						break;
					case REFUND:
						if (tradeLimitManger.getStakePerMatchCahe(betUser).containsKey(bme.getMatchId())) {
							data = new TradeLimitData(bme.getMatchId(), bme.getSportType().name(), bm.getModelStatus(), null, bme.getMatchStartTime(), new BigDecimal(0.0), credit, null, type.name());
							data.setTradeLimitCheck(betUser.isCheckTradeLimit());
							tradeLimitManger.updateStakePerMatchCache(betUser, data, modelId);
						}
						betUser.addOutstanding(credit.negate(), modelId);
						break;
					default:
						break;
				}
			} else {
				PositionalModel pm = helper.findPositionalModel(bm.getId());
				BettingModel openModel = pm.getOpenModelId();
				BettingModelExtraVodds openBme = helper.getModelExtraEntity(openModel.getId());
				TradeLimitData data = null;
				switch (pm.getExecutionStage()) {
					case "CREATED":
					case "OPENED":
					case "OPENING":
					case "CLOSE_FAILED":
					case "WAITING":
						ObjectMapper mapper = new ObjectMapper();
						JsonNode modelData = mapper.readTree(openModel.getModelData());
						BigDecimal leverageRatio = new BigDecimal(modelData.get("leverageRatio").asDouble());
						BigDecimal openStake = null;
						switch (type) {
							case RESERVE:
								openStake = leverageRatio.multiply(credit.abs());
								data = new TradeLimitData(openBme.getMatchId(), openBme.getSportType().name(), bm.getModelStatus(), pm.getExecutionStage(), openBme.getMatchStartTime(), new BigDecimal(0.0), openStake, new BigDecimal(0.0), type.name());
								data.setTradeLimitCheck(betUser.isCheckTradeLimit());
								tradeLimitManger.updateStakePerMatchCache(betUser, data, modelId);
								break;
							case REFUND:
								openStake = leverageRatio.multiply(credit);
								data = new TradeLimitData(openBme.getMatchId(), openBme.getSportType().name(), bm.getModelStatus(), pm.getExecutionStage(), openBme.getMatchStartTime(), new BigDecimal(0.0), openStake, new BigDecimal(0.0), type.name());
								data.setTradeLimitCheck(betUser.isCheckTradeLimit());
								tradeLimitManger.updateStakePerMatchCache(betUser, data, modelId);
								break;
							default:
								break;
						}
						break;
					case "CLOSED":
						switch (type) {
							case RESERVE:
								data = new TradeLimitData(openBme.getMatchId(), openBme.getSportType().name(), bm.getModelStatus(), pm.getExecutionStage(), openBme.getMatchStartTime(), new BigDecimal(0.0), new BigDecimal(0.0), credit, type.name());
								data.setTradeLimitCheck(betUser.isCheckTradeLimit());
								tradeLimitManger.updateStakePerMatchCache(betUser, data, modelId);
								break;
							case REFUND:
								data = new TradeLimitData(openBme.getMatchId(), openBme.getSportType().name(), bm.getModelStatus(), pm.getExecutionStage(), openBme.getMatchStartTime(), new BigDecimal(0.0), new BigDecimal(0.0), credit, type.name());
								data.setTradeLimitCheck(betUser.isCheckTradeLimit());
								tradeLimitManger.updateStakePerMatchCache(betUser, data, modelId);
								break;
							default:
								break;
						}
					default:
						break;
				}
				
				
			}
			
			tQueue.addTransaction(rTransaction);
			persistenceModerator.saveBetUser(betUser.getUser());
			
			logger.info("Execute transaction for user {} , model {} , credit {}",betUser.getUser().getId(),modelId,credit);
		}catch(Exception e){
			logger.error("Error while execute transaction!", e);
		}
		
	}
	
	/***
	 * populate all the relevant database transactions to store after settling a positional order with guarantee stop
	 */
	public void populateGsTransactions(AgentUserNode agent, BigDecimal credit, BigDecimal prCredit, BigDecimal poCredit, ModelTransactionType tranType, PositionalOrderWrapper poWrapper){
		
		persistenceModerator.saveAgentUser(agent.getUser());
		
		if(credit.compareTo(BigDecimal.ZERO) != 0){
		
			ModelTransaction transaction = new ModelTransaction();
			transaction.setUser(agent.getUser().getUser());
			transaction.setCreditAmount(credit);
			transaction.setCreditBefore(prCredit);
			transaction.setCreditAfter(poCredit);
			transaction.setiService("CMS");
			transaction.setModelId(poWrapper.getpModel().getCloseModelId().getId());
//			transaction.setRemarks("adjusted credit for company agent after settling the positional order with guarantee stop");
			transaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_ADJUSTED_CREDIT_GURANTEE_STOP));
			transaction.setType(ModelTransactionType.RETURN);

			transaction.setTime(new Timestamp(System.currentTimeMillis()));
			
			tQueue.addTransaction(transaction);
		}
	}
	
	public String format(BigDecimal bd) {
		if(bd == null || bd.compareTo(new BigDecimal(0)) == 0) { return "0"; };
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		df.setGroupingUsed(true);

		String numberFormated = df.format(bd);
		if(new BigDecimal(numberFormated.replaceAll(",", "")).compareTo(new BigDecimal(0)) == 0){
			return "0";
		}
		
		return df.format(bd);
	}

	public void populateSettlePTPLTransaction(Map<Long, PTPLSettlementInfo> ptplSettlements) {
		for (PTPLSettlementInfo settlementInfo : ptplSettlements.values()) {
			persistenceModerator.saveAgentUser(settlementInfo.getAgentUser());
			
			// Only create transaction when the change amount is not 0
			//if(settlementInfo.getCreditPTPL().compareTo(BigDecimal.ZERO) != 0){
			
				ModelTransaction transaction = new ModelTransaction();
				transaction.setUser(settlementInfo.getAgentUser().getUser());
				transaction.setCreditAmount(settlementInfo.getCreditPTPL());
				transaction.setCreditBefore(settlementInfo.getCreditPTPLBefore());
				transaction.setCreditAfter(settlementInfo.getCreditPTPLAfter());
				transaction.setiService("CMS");
				transaction.setModelId(settlementInfo.getOrderId());
				String remark = this.voddsCommonUtility.stringsToDescription(TX_SETTLED_PTPL);
				//transaction.setRemarks("Order settled, PT PL added to agent credit");
				transaction.setRemarks(remark);
				transaction.setType(ModelTransactionType.PT_PL);

				transaction.setTime(new Timestamp(System.currentTimeMillis()));
				
				tQueue.addTransaction(transaction);
			//}
		}
	}
	
	public void populateSettleModifiedPtplTransactionsForAgent(PTPLSettlementInfo settlementInfo){
			persistenceModerator.saveAgentUser(settlementInfo.getAgentUser());

			ModelTransaction transaction = new ModelTransaction();
			transaction.setUser(settlementInfo.getAgentUser().getUser());
			transaction.setCreditAmount(settlementInfo.getCreditPTPL().negate());
			transaction.setCreditBefore(settlementInfo.getCreditPTPLBefore());
			transaction.setCreditAfter(settlementInfo.getCreditPTPLAfter());
			transaction.setiService("CMS");
			transaction.setModelId(settlementInfo.getOrderId());
			String remark = this.voddsCommonUtility.stringsToDescription(TX_REVERTED_PTPL);
			//transaction.setRemarks("Previous PT PL reverted as order's PL changed");
			transaction.setRemarks(remark);
			transaction.setType(ModelTransactionType.PT_PL_REVERTED);
			transaction.setTime(new Timestamp(System.currentTimeMillis()));
			tQueue.addTransaction(transaction);
	
	}
	
	public ThriftExecuteTransactionResult moveCreditFromPoToActualizingOrder(ThriftActualizedInfo actualizedInfo,BetUserNode betUser){
		ThriftExecuteTransactionResult executeTransactionResult = new ThriftExecuteTransactionResult();

		// check user's outstanding before actualizing PO
		if(!genUtil.isValidOutstanding(betUser.getOutstandingAmount())) {
			logger.info("The outstanding {} of user {} is negative !", betUser.getOutstandingAmount(), betUser.getUser().getId());
			executeTransactionResult.setStatus(3); // use status 3 for negative outstanding, OE will handle this status
			executeTransactionResult.setMessage(this.voddsCommonUtility.stringsToDescription(TX_OUTSTANDING_AMT_NEGATIVE, format(betUser.getOutstandingAmount())));
		} else {
			BigDecimal bRefundMargin = new BigDecimal(actualizedInfo.refundMargin);
			BigDecimal bReserveStake = new BigDecimal(actualizedInfo.reserveStake);
			BigDecimal totalRefund = helper.calculateTotalTransactionByActualizedOrder(actualizedInfo.openOrderId, betUser.getUser().getId());
			BigDecimal totalReserve = (totalRefund.subtract(bReserveStake)).negate();
			logger.info("Total credit to reserve {}, open order id {}", totalReserve, actualizedInfo.openOrderId);

			try {
				if (betUser.getCredit().compareTo(totalReserve) >= 0) {
					if (betUser.isCheckTradeLimit() && !tradeLimitManger.validateTradeLimitForActualizedOrder(betUser, bReserveStake, actualizedInfo.openOrderId)) {
						executeTransactionResult.setStatus(2);
						String message = this.voddsCommonUtility.stringsToDescription(OT_MAX_BET_NORMAL_LIMIT, format(betUser.getMaxBetNormal()));
						executeTransactionResult.setMessage(message);
					} else {

						List<ModelTransaction> transactionList = new ArrayList<ModelTransaction>();

						if (betUser.getUser().getCommission().compareTo(BigDecimal.ZERO) == 0) {

							//refund margin to open order
							BigDecimal[] credits = betUser.moveCredit(bRefundMargin, bReserveStake.negate(), actualizedInfo.normalOrderId);
							ModelTransaction refTransaction = new ModelTransaction();

							refTransaction.setUser(betUser.getUser().getUser());
							refTransaction.setCreditAmount(bRefundMargin);
							refTransaction.setCreditBefore(credits[0]);
							refTransaction.setCreditAfter(credits[1]);
							refTransaction.setiService("CMS");
							refTransaction.setModelId(actualizedInfo.openOrderId);
							logger.info("refund margin {} for actualized order {} with user {} having user's credit before {} , user's credit after {}",
									bRefundMargin, actualizedInfo.openOrderId, betUser.getUser().getUser().getId(), credits[0], credits[1]);
							//refTransaction.setRemarks(String.format("refund margin (%s) for actualized order", format(refundMargin)));
							refTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REFUND_MARGIN_ACTUALIZED, format(bRefundMargin)));
							refTransaction.setType(ModelTransactionType.REFUND);
							refTransaction.setTime(new Timestamp(System.currentTimeMillis()));

							transactionList.add(refTransaction);

							//reserve stake to normal order

							ModelTransaction resTransaction = new ModelTransaction();

							resTransaction.setUser(betUser.getUser().getUser());
							resTransaction.setCreditAmount(bReserveStake.negate());
							resTransaction.setCreditBefore(credits[1]);
							resTransaction.setCreditAfter(credits[2]);
							resTransaction.setiService("CMS");
							resTransaction.setModelId(actualizedInfo.normalOrderId);
							logger.info("reserve stake {} for actualizing order {} with user {} having user's credit before {} , user's credit after {}",
									bReserveStake, actualizedInfo.normalOrderId, betUser.getUser().getUser().getId(), credits[1], credits[2]);
							//resTransaction.setRemarks(String.format("Reserve for normal order with stake = %s", format(reserveStake)));
							resTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_RESERVE_STAKE, format(bReserveStake)));
							resTransaction.setType(ModelTransactionType.RESERVE);
							resTransaction.setTime(new Timestamp(System.currentTimeMillis()));

							transactionList.add(resTransaction);

							tQueue.addOrderTransactions(transactionList);
							persistenceModerator.saveBetUser(betUser.getUser());

							executeTransactionResult.setStatus(0);
							executeTransactionResult.setMessage("Move credit from PO to actualizing normal successfully");


						} else {

							BigDecimal absCommissionValue = betUser.getUser().getCommission().multiply(bReserveStake);

							//refund margin and commission to open order
							BigDecimal[] creditComms = betUser.moveCreditCommission(totalRefund, bReserveStake.negate(), absCommissionValue);
							ModelTransaction refTransaction = new ModelTransaction();

							refTransaction.setUser(betUser.getUser().getUser());
							refTransaction.setCreditAmount(totalRefund);
							refTransaction.setCreditBefore(creditComms[0]);
							refTransaction.setCreditAfter(creditComms[1]);
							refTransaction.setiService("CMS");
							refTransaction.setModelId(actualizedInfo.openOrderId);
							logger.info("refund margin {} for actualized order {} with user {} having user's credit before {} , user's credit after {}",
									totalRefund, actualizedInfo.openOrderId, betUser.getUser().getUser().getId(), creditComms[0], creditComms[1]);
							//refTransaction.setRemarks(String.format("refund margin and commission (%s) for actualized order", format(totalRefund)));
							refTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REFUND_MARGIN_COMMISSION_ACTUALIZED, format(totalRefund)));
							refTransaction.setType(ModelTransactionType.REFUND);
							refTransaction.setTime(new Timestamp(System.currentTimeMillis()));

							transactionList.add(refTransaction);

							//refund commission of agent
							for (AgentUserNode agent : betUser.getAgentUsers()) {

								BigDecimal aCommValue = helper.calculateTotalTransactionByActualizedOrder(actualizedInfo.openOrderId, agent.getUser().getId());

								if (aCommValue.compareTo(BigDecimal.ZERO) != 0) {

									ModelTransaction aTransaction = new ModelTransaction();
									aTransaction.setModelId(actualizedInfo.openOrderId);
									aTransaction.setUser(agent.getUser().getUser());

									//add the commission amount to the user credit
									BigDecimal[] credits = agent.addCredit(aCommValue);
									aTransaction.setCreditBefore(credits[0]);
									aTransaction.setCreditAmount(aCommValue);
									aTransaction.setCreditAfter(credits[1]);
									//aTransaction.setRemarks(String.format("refund commission (%s) to agent %s for actualized order",format(aCommValue),agent.getUser().getUsername()));
									aTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_REFUND_COMMISSION_ACTUALIZED, format(aCommValue), agent.getUser().getUsername()));
									aTransaction.setType(ModelTransactionType.COMMISSION);
									aTransaction.setTime(new Timestamp(System.currentTimeMillis()));
									aTransaction.setiService("CMS");

									transactionList.add(aTransaction);
									persistenceModerator.saveAgentUser(agent.getUser());
								}

							}


							//reserve stake to new normal order
							ModelTransaction resTransaction = new ModelTransaction();

							resTransaction.setUser(betUser.getUser().getUser());
							resTransaction.setCreditAmount(bReserveStake.negate());
							resTransaction.setCreditBefore(creditComms[1]);
							resTransaction.setCreditAfter(creditComms[2]);
							resTransaction.setiService("CMS");
							resTransaction.setModelId(actualizedInfo.normalOrderId);
							logger.info("reserve stake {} for actualizing order {} with user {} having user's credit before {} , user's credit after {}",
									bReserveStake, actualizedInfo.normalOrderId, betUser.getUser().getUser().getId(), creditComms[1], creditComms[2]);
							//resTransaction.setRemarks(String.format("Reserve for normal order with stake = %s", format(reserveStake)));
							resTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_RESERVE_STAKE, format(bReserveStake)));
							resTransaction.setType(ModelTransactionType.RESERVE);
							resTransaction.setTime(new Timestamp(System.currentTimeMillis()));

							transactionList.add(resTransaction);


							//Charge commission to bet user for new normal order
							ModelTransaction cTransaction = new ModelTransaction();
							cTransaction.setModelId(actualizedInfo.normalOrderId);
							cTransaction.setUser(betUser.getUser().getUser());

							cTransaction.setCreditAmount(absCommissionValue);
//						cTransaction.setRemarks(String.format("Charge commission of %s for normal order with stake = %s , commission rate = %s%%",
//								format(absCommissionValue.negate()),format(reserveStake),
//								format(betUser.getUser().getCommission().multiply(new BigDecimal(100)))));
							cTransaction.setRemarks(this.voddsCommonUtility.stringsToDescription(TX_RESERVE_COMMISSION_NORMAL_ORDER, format(absCommissionValue.negate()), format(bReserveStake),
									format(betUser.getUser().getCommission().multiply(new BigDecimal(100)))));
							cTransaction.setType(ModelTransactionType.COMMISSION);
							cTransaction.setTime(new Timestamp(System.currentTimeMillis()));


							cTransaction.setCreditBefore(creditComms[2]);
							cTransaction.setCreditAfter(creditComms[3]);

							cTransaction.setiService("CMS");

							transactionList.add(cTransaction);

							//Charge commission for agent
							for (AgentUserNode agent : betUser.getAgentUsers()) {

								BigDecimal agentCommissionCreditValue = agent.getUser().getCommission().multiply(bReserveStake);

								if (agentCommissionCreditValue.compareTo(BigDecimal.ZERO) != 0) {

									ModelTransaction aTransaction = new ModelTransaction();
									aTransaction.setModelId(actualizedInfo.normalOrderId);
									aTransaction.setUser(agent.getUser().getUser());

									//add the commission amount to the user credit
									BigDecimal[] credits = agent.addCredit(agentCommissionCreditValue);
									aTransaction.setCreditBefore(credits[0]);
									aTransaction.setCreditAmount(agentCommissionCreditValue);
									aTransaction.setCreditAfter(credits[1]);
									//aTransaction.setRemarks(String.format("commission of %s percent earned for using stake %s",agent.getUser().getCommission(),format(reserveStake)));
									String remark = this.voddsCommonUtility.stringsToDescription(TX_COMMISSION_EARN, agent.getUser().getCommission().toString(), format(bReserveStake));
									aTransaction.setRemarks(remark);
									aTransaction.setType(ModelTransactionType.COMMISSION);
									aTransaction.setTime(new Timestamp(System.currentTimeMillis()));
									aTransaction.setiService("CMS");

									transactionList.add(aTransaction);
									persistenceModerator.saveAgentUser(agent.getUser());
								}

							}


							tQueue.addOrderTransactions(transactionList);
							persistenceModerator.saveBetUser(betUser.getUser());

							executeTransactionResult.setStatus(0);
							executeTransactionResult.setMessage("Move credit from PO to actualizing normal successfully");


						}


						//update stake per match cache for NO
						TradeLimitData tradeLimitDataNO = new TradeLimitData(actualizedInfo.getMatchId(), actualizedInfo.getSportType(), actualizedInfo.getOrderStatus(),
								null, new Timestamp(actualizedInfo.getStartTime()), BigDecimal.ZERO,
								BigDecimal.valueOf(actualizedInfo.reserveStake), BigDecimal.ZERO, ThriftTransactionType.RESERVE.name());

						tradeLimitDataNO.setTradeLimitCheck(betUser.isCheckTradeLimit());
						tradeLimitManger.updateStakePerMatchCache(betUser, tradeLimitDataNO, actualizedInfo.normalOrderId);

						//move stake from PositionalPotentialLossAmount to NormalPotentialLossAmount

						TradeLimitData tradeLimitDataPO = new TradeLimitData(actualizedInfo.getMatchId(), actualizedInfo.getSportType(), actualizedInfo.getOrderStatus(),
								actualizedInfo.getExecutionStage(), new Timestamp(actualizedInfo.getStartTime()), BigDecimal.ZERO,
								BigDecimal.valueOf(actualizedInfo.reserveStake), BigDecimal.ZERO, ThriftTransactionType.REFUND.name());

						tradeLimitDataPO.setTradeLimitCheck(betUser.isCheckTradeLimit());
						tradeLimitManger.updateStakePerMatchCache(betUser, tradeLimitDataPO, actualizedInfo.openOrderId);

						betUser.addOutstanding(bReserveStake, actualizedInfo.normalOrderId);
						//Notification outstanding amt
						UserOutstandingChangeEvent amt = new UserOutstandingChangeEvent(betUser.getUser().getUsername(), bReserveStake.doubleValue(), betUser.getUser().getId());
						amtNotificationTask.submitOustanding(amt);


					}

				} else {
					logger.info("can not reserve with stake {} > current credit {} of open order {}", totalReserve, betUser.getCredit(), actualizedInfo.openOrderId);
					executeTransactionResult.setStatus(1);
					executeTransactionResult.setMessage("Can not move credit from PO to actualizing normal");
				}

			} catch (Exception ex) {
				executeTransactionResult.setStatus(1);
				executeTransactionResult.setMessage("Move credit from PO to actualizing normal unsuccessfully");
				logger.error("Error while moving credit to open order {}, exception: ", actualizedInfo.openOrderId, ex);
			}
		}

		return executeTransactionResult;
		
	}

}
