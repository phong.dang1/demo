package jayeson.vodds.service.cms.transactions;


import java.util.*;

import jayeson.vodds.service.cms.data.TransactionWrapper;
import jayeson.vodds.service.cms.data.game.GameTransactionWrapper;
import jayeson.vodds.service.cms.transactions.game.GameTransactionQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import jayeson.database.AgentUser;
import jayeson.database.BettingModel;
import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.database.newvodds.DarkPoolBetInfo;
import jayeson.database.newvodds.ModelTransaction;
import jayeson.database.newvodds.PositionalModel;
import jayeson.database.newvodds.StakePerMatch;
import jayeson.database.newvodds.VoddsBetUser;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.order.settlement.MPLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.OutstandingNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PLNotificationTask;
import jayeson.vodds.service.cms.order.settlement.PostSysnchronizationTask;
import jayeson.vodds.service.cms.service.NoQPersister;


/**
 * Class to process the transaction queue at periodic intervals
 * @author Praveen
 *
 */
public class TransactionProcessor implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(TransactionProcessor.class.getName());

	TransactionQueue tQueue;
	IDBService dbService;
	NoQPersister nqp;
	static final int QUEUE_THRESHOLD = 10;
	UserManager userMgr;
	OrderWrapperManager owManger;
	PLNotificationTask plNotificationTask;
	PostSysnchronizationTask pSysnTask;
	MPLNotificationTask mplNotiTask;
	OutstandingNotificationTask amtNotificationTask;
	GameTransactionQueue gameTransQueue;

	@Inject
	public TransactionProcessor(TransactionQueue queue, IDBService service, NoQPersister dtPersitenceMorderator,
								UserManager userManager, OrderWrapperManager oManger, PLNotificationTask plTask, PostSysnchronizationTask postSysn,
								MPLNotificationTask mplNotiTask, OutstandingNotificationTask amtNotificationTask,
								GameTransactionQueue gameTransQueue) {

		tQueue = queue;
		dbService = service;
		nqp = dtPersitenceMorderator;
		userMgr = userManager;
		owManger = oManger;
		plNotificationTask = plTask;
		pSysnTask = postSysn;
		this.mplNotiTask = mplNotiTask;
		this.amtNotificationTask = amtNotificationTask;
		this.gameTransQueue = gameTransQueue;
	}

	@Override
	/**
	 * Method will process a configurable number of objects from the queue and use DBService to persist the results
	 */
	public void run() {
		try {
			tQueue.setHandledByTransactionProcessor(true);

			logger.debug("transaction queue processing initiated with queue size {}", QUEUE_THRESHOLD);

			ArrayList<ModelTransaction> mtQ = new ArrayList<ModelTransaction>();
			ArrayList<TransactionWrapper> tQ = new ArrayList<>();
			Set<VoddsBetUser> setBU = new HashSet<>();
			Set<AgentUser> setAU = new HashSet<>();
			Set<BettingModel> setBM = new HashSet<>();
			Set<PositionalModel> SetPM = new HashSet<>();
			Set<BettingModelExtraVodds> setBMEV = new HashSet<>();
			ArrayList<DarkPoolBetInfo> dpQ = new ArrayList<DarkPoolBetInfo>();
			Set<StakePerMatch> setStakePerMatch = new HashSet<>();
			// game transactions
			List<GameTransactionWrapper> gameTransWrappers = new ArrayList<>();

			setBU.addAll(nqp.getAllBetUserToSave());
			setAU.addAll(nqp.getAllAgentUserToSave());
			setBM.addAll(nqp.getAllOrderToSave());
			SetPM.addAll(nqp.getAllPositionalModelToSave());
			setBMEV.addAll(nqp.getAllvExtraModelToSave());
			setStakePerMatch.addAll(nqp.getAllStakePerMatchToSave());

			for (int i = 0; i < QUEUE_THRESHOLD; i++) {
				if (!tQueue.getTransQueue().isEmpty()) {
					ModelTransaction item = tQueue.getTransQueue().poll();
					mtQ.add(item);
					logger.info("info order id {}", item.getModelId());
				}

				if (!tQueue.getTransUserQueue().isEmpty()) {
					tQ.add(tQueue.getTransUserQueue().poll());
				}

				if (!tQueue.getvDpoolQueue().isEmpty()) {
					dpQ.add(tQueue.getvDpoolQueue().poll());
				}

				if(!gameTransQueue.getGameTransQueue().isEmpty()) {
					gameTransWrappers.add(gameTransQueue.getGameTransQueue().poll());
				}
			}

			logger.info("Size transaction queue {}, transaction user queue {}, dpool queue {}, game transaction queue {}",
					tQueue.getTransQueue().size(), tQueue.getTransUserQueue().size(), tQueue.getvDpoolQueue().size(),
					gameTransQueue.getGameTransQueue().size());

			logger.debug("Processing: \nagent queue of size {} , \nuser queue of size {}, \ntransaction queue of size {}, \norder queue of size {},"
							+ "\npositional queue of size {}, \ntransaction user queue of size , \nextra vodds model of size {} \ndpool queue size {}",
					setAU.size(), setBU.size(), mtQ.size(), setBM.size(), SetPM.size(), tQ.size(), setBMEV.size(), dpQ.size());

			dbService.persistBUResults(setBU);
			dbService.persistAUResults(setAU);
			dbService.persistBMResults(setBM, owManger, plNotificationTask, this.mplNotiTask);
			dbService.persistPMResults(SetPM, this.mplNotiTask, nqp);
			dbService.persistBMEVResults(setBMEV, this.amtNotificationTask);
			dbService.persistSPMResults(setStakePerMatch);

			dbService.persistModelTransaction(mtQ, tQueue);
			dbService.persistTransaction(tQ);
			dbService.persistDPTransaction(dpQ);
			dbService.persistGameTransactions(gameTransWrappers);

			pSysnTask.checkSysnDataOfOrders(setBM);

			tQueue.setHandledByTransactionProcessor(false);
			plNotificationTask.notifyPLChanges();
		} catch (Exception ex) {
			logger.error("Error while running transaction processor", ex);
		}

	}
}
