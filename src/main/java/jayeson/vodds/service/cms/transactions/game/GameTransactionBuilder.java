package jayeson.vodds.service.cms.transactions.game;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import jayeson.database.newvodds.UserGameDetail;
import jayeson.database.newvodds.UserGameTransaction;
import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.game.GameTransactionWrapper;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.game.result.GameCreditResult;
import jayeson.vodds.service.cms.game.result.GameCreditResultCode;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.thrift.ThriftGameCreditRequestInfo;
import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * The class aims to build game transactions
 * @author Huong Tang
 */
public class GameTransactionBuilder {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private static String TX_RESERVE_GAME = "tx_reserve_game";
    private static String TX_RETURN_GAME = "tx_return_game";
    private static String TX_ADJUST_GAME = "tx_adjust_game";
    private static String TX_REFUND_GAME = "tx_refund_game";

    private IDBService dbHelper;
    private NoQPersister persistenceModerator;
    private GameTransactionQueue tranQueue;
    private ObjectMapper mapper;
    private GeneralUtility genUtil;
    VoddsCommonUtility voddsCommonUtility;

    @Inject
    public GameTransactionBuilder(IDBService idbService, NoQPersister qPersister, GameTransactionQueue tQueue,
                     ObjectMapper mapper, GeneralUtility genUtil, VoddsCommonUtility voddsCommonUtility) {
        this.dbHelper = idbService;
        this.persistenceModerator = qPersister;
        this.tranQueue = tQueue;
        this.mapper = mapper;
        this.genUtil = genUtil;
        this.voddsCommonUtility = voddsCommonUtility;
    }

    /**
     * Construct GameTransaction object and return result (transactionId, current credit,...)
     * @param reqInfo
     * @param betUserNode
     * @param result
     */
    public void populateGameCreditTransactions(ThriftGameCreditRequestInfo reqInfo, BetUserNode betUserNode,
                                  GameCreditResult result) {
        logger.info("{} populate game credit transaction", reqInfo.userId, reqInfo);
        // check game detail existence
        UserGameDetail gameDetail = dbHelper.findGameDetail(reqInfo.gameDetailId);
        if(gameDetail == null) {
            logger.info("{} invalid game detail", reqInfo.gameDetailId);
            result.setCode(GameCreditResultCode.INVALID_GAME_DETAIL);
            return;
        }
        UserGameTransaction gameTransaction = new UserGameTransaction();
        BigDecimal reqAmt = BigDecimal.valueOf(reqInfo.amount);
        BigDecimal[] credits = betUserNode.addCredit(reqAmt, 0);
        gameTransaction.setId(genUtil.buildGameTransId());
        gameTransaction.setUserGameDetail(gameDetail);
        gameTransaction.setAmount(reqAmt);
        gameTransaction.setCreditBefore(credits[0]);
        gameTransaction.setCreditAfter(credits[1]);
        gameTransaction.setType(reqInfo.tType.getShortName());
        gameTransaction.setRemarks(this.getRemark(reqInfo.tType.getShortName(), reqAmt));
        // save user credit
        persistenceModerator.saveBetUser(betUserNode.getUser());
        // store game transaction
        GameTransactionWrapper transactionWrapper = new GameTransactionWrapper(gameTransaction);
        tranQueue.addTransaction(transactionWrapper);
        // return result
        result.setTransactionId(gameTransaction.getId());
        result.setCurrency(betUserNode.getUser().getCurrency().getCode());
        result.setCredit(credits[1].doubleValue());
        result.setCode(GameCreditResultCode.SUCCESS);
    }

    public UserGameTransaction getUserGameTransaction(Long gameDetailId) {
        return dbHelper.findGameTransaction(gameDetailId);
    }



    public String getRemark(String shortType, BigDecimal amount){
        switch (shortType){
            case "res":
                return this.voddsCommonUtility.stringsToDescription(TX_RESERVE_GAME, genUtil.format(amount));
            case "ref":
                return this.voddsCommonUtility.stringsToDescription(TX_REFUND_GAME, genUtil.format(amount));
            case "adj":
                return this.voddsCommonUtility.stringsToDescription(TX_ADJUST_GAME, genUtil.format(amount));
            case "ret":
                return this.voddsCommonUtility.stringsToDescription(TX_RETURN_GAME, genUtil.format(amount));
            default:
                return null;
        }
    }
}
