package jayeson.vodds.service.cms.transactions.game;

import com.google.inject.Singleton;
import jayeson.vodds.service.cms.data.game.GameTransactionWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A FIFO queue that stores the game transactions populated by credit actions, that have to be persisted into the database
 * @author Huong Tang
 *
 */

@Singleton
public class GameTransactionQueue {

	private Logger logger = LoggerFactory.getLogger(getClass());

	// queue to store the order transactions
	Queue<GameTransactionWrapper> gameTransQueue;
	AtomicBoolean isEmpty = new AtomicBoolean(true);

	public GameTransactionQueue(){
		gameTransQueue = new LinkedBlockingQueue<>();
	}

	public void addTransaction(GameTransactionWrapper trans){
		gameTransQueue.add(trans);
		logger.info("{} add to game trans queue", trans.getTransaction().getUserGameDetail().getId());
	}

	public Queue<GameTransactionWrapper> getGameTransQueue() {
		return gameTransQueue;
	}

	public AtomicBoolean isChkEmpty() {
		if(this.getGameTransQueue().isEmpty()){
			isEmpty.set(true);
		}else{
			isEmpty.set(false);
		}
		return isEmpty;
	}

}
