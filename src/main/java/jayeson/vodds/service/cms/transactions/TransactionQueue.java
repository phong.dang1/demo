package jayeson.vodds.service.cms.transactions;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import jayeson.database.newvodds.DarkPoolBetInfo;
import jayeson.database.newvodds.ModelTransaction;
import jayeson.vodds.service.cms.data.TransactionWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A FIFO queue that stores the transactions populated by credit actions, that have to be persisted into the database 
 * @author Praveen
 *
 */

@Singleton
public class TransactionQueue{

	static Logger logger = LoggerFactory.getLogger(TransactionQueue.class.getName());

	// queue to store the order transactions
	@Inject
	Queue<ModelTransaction> transQueue;
	
	@Inject
	Queue<TransactionWrapper> transUserQueue;
	
	// queue to store the changes of dark pool bet info entities
	@Inject
	Queue<DarkPoolBetInfo> vDpoolQueue;
	
	AtomicBoolean isHandledByTransactionProcessor = new AtomicBoolean(false);
	AtomicBoolean isEmpty = new AtomicBoolean(true);
	
	private Map<Long,Integer> transactionsRecordByOrder;
	
	public boolean isHandledByTransactionProcessor() {
		return isHandledByTransactionProcessor.get();
	}

	public void setHandledByTransactionProcessor(boolean isHandledByTransactionProcessor) {
		this.isHandledByTransactionProcessor.set(isHandledByTransactionProcessor);
	}

	public TransactionQueue(){
		transactionsRecordByOrder = new ConcurrentHashMap<>();
	}

	public void addTransaction(ModelTransaction trans){
		
		Integer count = transactionsRecordByOrder.computeIfAbsent(trans.getModelId(),r -> new Integer(0)); 
		count++;
		transactionsRecordByOrder.put(trans.getModelId(), count);
		logger.debug("OrderId {} - size after add transaction that is saved into database {}",trans.getModelId(),transactionsRecordByOrder.get(trans.getModelId()));
	
		transQueue.add(trans);
	}
	
	public void addTransaction(TransactionWrapper trans){
		transUserQueue.add(trans);
	}

	public void addDarkPoolBetInfo(DarkPoolBetInfo dpInfo){
		logger.debug("dark pool info {} set to {}",dpInfo.getId(),dpInfo.getStatus());
		vDpoolQueue.add(dpInfo);
	}
	
	public void addOrderTransactions(List<ModelTransaction> transactions){
		for (ModelTransaction modelTransaction : transactions) {
			Integer count = transactionsRecordByOrder.computeIfAbsent(modelTransaction.getModelId(),r -> new Integer(0)); 
			count++;
			transactionsRecordByOrder.put(modelTransaction.getModelId(), count);
			logger.debug("OrderId {} - size after add transaction that is saved into database {}",modelTransaction.getModelId(),transactionsRecordByOrder.get(modelTransaction.getModelId()));

		}
		transQueue.addAll(transactions);
	}
	
	public void addUserTransactions(List<TransactionWrapper> transactions){
		transUserQueue.addAll(transactions);
	}

	public Queue<ModelTransaction> getTransQueue() {
		return transQueue;
	}

	public Queue<TransactionWrapper> getTransUserQueue() {
		return transUserQueue;
	}

	public void setTransUserQueue(Queue<TransactionWrapper> transUserQueue) {
		this.transUserQueue = transUserQueue;
	}

	public Queue<DarkPoolBetInfo> getvDpoolQueue() {
		return vDpoolQueue;
	}
	

	public AtomicBoolean isChkEmpty() {
		if(this.getTransQueue().isEmpty() && this.getTransUserQueue().isEmpty() && this.getvDpoolQueue().isEmpty()){
			isEmpty.set(true);
		}else{
			isEmpty.set(false);
		}
		return isEmpty;
	}
	
	public Integer getNumberTransactionRecordByOrder(Long orderId) {
		if(transactionsRecordByOrder.containsKey(orderId)) {
			return transactionsRecordByOrder.get(orderId);
		}else {
			return null;
		}
	}
	
	public void removeTransactionRecordOfOrder(Long orderId) {
		if(transactionsRecordByOrder.containsKey(orderId)) {
			Integer count = transactionsRecordByOrder.get(orderId);
			count--;
			logger.debug("OrderId {} - size after remove transaction that is saved into database {}",orderId,count);
			if(count == 0) {
				transactionsRecordByOrder.remove(orderId);
			}else {
				transactionsRecordByOrder.put(orderId, count);	
			}
		}
	}

}
