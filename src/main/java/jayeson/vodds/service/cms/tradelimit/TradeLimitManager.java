package jayeson.vodds.service.cms.tradelimit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.concurrent.Semaphore;

import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.newvodds.ModelTransactionType;
import jayeson.database.newvodds.StakePerMatch;
import jayeson.database.newvodds.StakePerMatchPK;
import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.json.NormalReserveResult;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestResult;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftResultCode;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.thrift.ThriftUserUpdate;



@Singleton
public class TradeLimitManager {
	static Logger logger = LoggerFactory.getLogger("TradeLimitManager");
	NoQPersister persistenceModerator;
	UserManager uManager;
	VoddsCommonUtility voddsCommonUtility;
	private static String OT_MAX_BET_NORMAL_LIMIT = "ot_max_bet_normal_limit";
	private static String OT_MAT_BET_POSITIONAL_LIMIT = "ot_max_bet_positional_limit";
	private static String OT_MAX_BET_PER_MATCH_LIMIT = "ot_max_bet_per_match_limit";
	private static String OT_MAX_LOSS_LIMIT = "ot_max_loss_limit";
	ObjectMapper oMapper;
	GeneralUtility genUtil;
	IDBService dbService;
	
	private final Semaphore sem = new Semaphore(1, true);
	
	@Inject
	public TradeLimitManager(NoQPersister dtPersitneceMorderator,UserManager userManager,VoddsCommonUtility commonUtility,ObjectMapper mapper,
							 GeneralUtility iGenUtil,IDBService helper) {
		persistenceModerator = dtPersitneceMorderator;
		uManager = userManager;
		voddsCommonUtility = commonUtility;
		oMapper = mapper;
		genUtil = iGenUtil;
		dbService = helper;
	}
	
	private BigDecimal calculateTotalStakeForNormalOrder(BetUserNode betUser,TradeLimitData data,long orderId) {
		BigDecimal totalStake = BigDecimal.ZERO;
		if(data.getTransactionType().equalsIgnoreCase(ModelTransactionType.RESERVE.name())) {
			totalStake = data.getStake();
		}else if (data.getTransactionType().equalsIgnoreCase(ModelTransactionType.REFUND.name())){
			//totalStake = (data.getStake().add(data.getRejectedStake())).negate();
			totalStake = (data.getStake()).negate();
		}
		logger.info("Calculate Stake For Normal Order With UserId {} - orderId {} - matchId {} - transactionType {} - orderStatus {} - executeStage {} - sportType {} - Stake {} - RejectedStake {} - TotalStake {}",
				betUser.getUser().getId(),orderId,data.getMatchId(),data.getTransactionType(),data.getOrderStatus(),data.getExecutionStage(),data.getSportType(),data.getStake(),data.getRejectedStake(),totalStake);
		return totalStake;
	}
	
	private BigDecimal calculateTotalStakeForPO(BetUserNode betUser,TradeLimitData data,long orderId) {
		BigDecimal totalStake = BigDecimal.ZERO;
		if(data.getExecutionStage().equalsIgnoreCase(PositionalOrderStage.CLOSED.name())) {
			totalStake = data.getMpl().negate().subtract(data.getConfirmedStakeOpenOrder());
		}else {
			if(data.getTransactionType().equalsIgnoreCase(ModelTransactionType.RESERVE.name())) {
				totalStake = data.getStake();
			}else if (data.getTransactionType().equalsIgnoreCase(ModelTransactionType.REFUND.name())){
				// case rejected bet in open order
				if(data.getExecutionStage().equalsIgnoreCase(PositionalOrderStage.VOIDING.name())) {
					totalStake = data.getStake().negate();
				}else {
					totalStake = data.getStake().negate().subtract(data.getRejectedStake());
				}
			}
		}

		//update positionalPotentialLoss
		BigDecimal[] result = betUser.addPositionalPotentialLoss(totalStake, orderId);
		logger.info("Calculate Stake For Positional Order With UserId {} - orderId {} - matchId {} - transactionType {} - orderStatus {} - executeStage {} - sportType {} - Stake {} - Mpl {} - TotalStake {} - priorPPL {} - postPPL {} - ConfirmedStakeOpenOrder {} - RejectedStake {}",
				betUser.getUser().getId(),orderId,data.getMatchId(),data.getTransactionType(),data.getOrderStatus(),data.getExecutionStage(),data.getSportType(),data.getStake(),data.getMpl(),totalStake,result[0],result[1],data.getConfirmedStakeOpenOrder(),data.getRejectedStake());
		return totalStake;
	}
	
	private void pushIntoStakePerMatchCache(BetUserNode betUser,TradeLimitData data,BigDecimal totalStake,StakePerMatch stakePerMatch,long orderId) {
		if(stakePerMatch.getStakePerMatchPK() == null) {
			StakePerMatchPK stakePerMatchPK = new StakePerMatchPK(betUser.getUser().getId(), data.getMatchId());
			stakePerMatch.setStakePerMatchPK(stakePerMatchPK);
			stakePerMatch.setSportType(data.getSportType());
			stakePerMatch.setStartTime(data.getStartTime());
			stakePerMatch.setTotalStake(totalStake);
			getStakePerMatchCahe(betUser).put(data.getMatchId(), stakePerMatch);
			logger.info("Add new data into stake per match cache userId {} - matchId {} - totalStake {} - orderId {}",betUser.getUser().getId(),data.getMatchId(),totalStake,orderId);
		}else {
			BigDecimal previousStake = stakePerMatch.getTotalStake();
			stakePerMatch.setTotalStake(previousStake.add(totalStake));
			logger.info("Update stake per match cache userId {} - orderId {} - matchId {} amout {} - previousStake {} - afterStake {} - ordersStauts {} - executionStage {} - confirmedStakeOpenOrder {} - mpl {}",
					betUser.getUser().getId(),orderId,data.getMatchId(),totalStake,previousStake,stakePerMatch.getTotalStake(),data.getOrderStatus(),data.getExecutionStage(),data.getConfirmedStakeOpenOrder(),data.getMpl());
		}
	}
	
	public void updateStakePerMatchCache(BetUserNode betUser,TradeLimitData data,long orderId) {
		// skip stake per match cache for uncheck trade limit
		if(!data.isTradeLimitCheck()) {
			logger.info("Skip update stake per match cache userId {} - matchId {} - orderId {}", betUser.getUser().getId(), data.getMatchId(),orderId);
			return;
		}
		//update stake per match cache
		try {
			StakePerMatch stakePerMatch =  getStakePerMatchCahe(betUser).computeIfAbsent(data.getMatchId(), r -> new StakePerMatch());
			logger.debug("OrderId {} - userId {} - matchId {} - stakePerMatch {} ",orderId,betUser.getUser().getId(),data.getMatchId(),stakePerMatch);
			if(data.getExecutionStage() != null) {
				if(!data.getExecutionStage().equalsIgnoreCase(PositionalOrderStage.VOIDING.name())) {
					BigDecimal totalStake = calculateTotalStakeForPO(betUser,data,orderId);
					pushIntoStakePerMatchCache(betUser,data,totalStake,stakePerMatch,orderId);
				}else {
					//if order is voiding , only update stake per match with positional open
					if(data.getoType() == ThriftOrderType.POSITIONAL_OPEN) {
						BigDecimal totalStake = calculateTotalStakeForPO(betUser,data,orderId);
						pushIntoStakePerMatchCache(betUser,data,totalStake,stakePerMatch,orderId);
					}
				}
			}else {
				BigDecimal totalStake = calculateTotalStakeForNormalOrder(betUser,data,orderId);
				pushIntoStakePerMatchCache(betUser,data,totalStake,stakePerMatch,orderId);
			}
			persistenceModerator.saveStakePerMatch(data.getMatchId(), stakePerMatch);
		}catch (Exception e) {
			logger.error("Failed to update stake per match cahe , exception: ",e);
		}
	}
	
	public void updateStakePerMatchForResettlement(BetUserNode betUser,String matchId,BigDecimal outstanding,long orderId) {
		try {
			if(betUser.isCheckTradeLimit()) {
				StakePerMatch stakePerMatch = getStakePerMatchCahe(betUser).computeIfAbsent(matchId, r -> new StakePerMatch());
				logger.debug("OrderId {} - userId {} - matchId {} - stakePerMatch {} - oustanding {}",orderId,betUser.getUser().getId(),matchId,stakePerMatch,outstanding);
				if(stakePerMatch.getStakePerMatchPK() != null) {
					stakePerMatch.setTotalStake(stakePerMatch.getTotalStake().add(outstanding));
				}else {
					StakePerMatchPK stakePerMatchPK = new StakePerMatchPK(betUser.getUser().getId(), matchId);
					stakePerMatch.setStakePerMatchPK(stakePerMatchPK);
					stakePerMatch.setSportType(null);
					stakePerMatch.setTotalStake(outstanding);
					getStakePerMatchCahe(betUser).put(matchId, stakePerMatch);
				}
				logger.info("orderId {} - update statke per match for re-settlement userId {} - matchId {} - totalstake {}",orderId,betUser.getUser().getId(),matchId,outstanding);
				persistenceModerator.saveStakePerMatch(matchId, stakePerMatch);
			}
		}catch (Exception e) {
			logger.error("Failed to updateStakePerMatchForResettlement , exception: ",e);
		}
	}
	
	public void updateStakePerMatchCacheAndPositionalPotentialLossAfterSettlement(BetUserNode betUser,String matchId,BigDecimal mpl,BigDecimal outstanding,long orderId) {
		try {
			if(betUser.isCheckTradeLimit()) {
				StakePerMatch stakePerMatch = getStakePerMatchCahe(betUser).computeIfAbsent(matchId, r -> new StakePerMatch());
				logger.debug("OrderId {} - userId {} - matchId {} - stakePerMatch {} - mpl {} - oustanding {}",orderId,betUser.getUser().getId(),matchId,stakePerMatch,mpl,outstanding);
				stakePerMatch.setTotalStake(stakePerMatch.getTotalStake().add(mpl).add(outstanding));
				BigDecimal[] ppl = betUser.addPositionalPotentialLoss(mpl, orderId);
				logger.info("update stake per match after settlement orderId {} - userId {} - matchId {} - totalStake {} - priorPPL {} - postPPL {} - mpl {} - outstanding {}",
						orderId,betUser.getUser().getId(),matchId,stakePerMatch.getTotalStake(),ppl[0],ppl[1],mpl,outstanding);
				persistenceModerator.saveStakePerMatch(matchId, stakePerMatch);
			}
		}catch (Exception e) {
			logger.error("Failed to updateStakePerMatchCacheAndPositionalPotentialLossAfterSettlement , exception: ",e);
		}
	}
	
	public boolean validateTradeLimitForActualizedOrder(BetUserNode betUser,BigDecimal reserveStake,long orderId) {
		boolean flag = true;
		if(reserveStake.compareTo(betUser.getMaxBetNormal()) == 1) {
			logger.info("OrderId {} - Max Bet Normal orderId {} - stake for actualized order {} of userId {}",orderId,betUser.getMaxBetNormal(),reserveStake,betUser.getUser().getId());
			flag = false ;
		}
		return flag;
	}
	
	public String validateTradeLimitWithMaxValue(ThriftCreditRequestInfo reqInfo,BigDecimal originalStake,BigDecimal reqCredit,BigDecimal mpl,long modelId) {
		String message = "";

		BetUserNode betUser = uManager.findBetUserNode(reqInfo.getUserId());
		
		// verify trade limit check flag. if it's false, skip checking
		if(!betUser.isCheckTradeLimit()) {
			logger.trace("skip checking trade limit flag {} for user id {}", betUser.isCheckTradeLimit(), reqInfo.userId);
			return message;
		}
		
		StakePerMatch stakePerMatch = getStakePerMatchCahe(betUser).get(reqInfo.getMatchId());
		BigDecimal maxPerMatchNOPO = stakePerMatch == null ? BigDecimal.ZERO : stakePerMatch.getTotalStake();
		BigDecimal totalStake = originalStake.add(reqCredit);
				
		BigDecimal remainingStakeForPerMatch = betUser.getMaxBetPerMatch().subtract(maxPerMatchNOPO);
		BigDecimal remainingStakeForMaxLoss = betUser.getPl().subtract(betUser.getOutstandingAmount()).subtract(betUser.getPositionalPotentialLoss()).add(betUser.getMaxLoss());
		
		logger.info("UserId {} - matchId {} - orderId {} - reqStake {} - mpl {} - maxBetNormal {} - maxBetPo {} - maxBetPerMatch {} - maxPerMatchNOPO {} - maxLoss {} - pl {} - outstandingAmt {} - positionalPotentialLoss {} - remainingStakeForPerMatch {} - remainingStakeForMaxLoss {}",
				reqInfo.userId,reqInfo.matchId,modelId,reqCredit,mpl,betUser.getMaxBetNormal(),betUser.getMaxBetPositional(),betUser.getMaxBetPerMatch(),maxPerMatchNOPO,betUser.getMaxLoss(),betUser.getPl(),betUser.getOutstandingAmount(),betUser.getPositionalPotentialLoss(),remainingStakeForPerMatch,remainingStakeForMaxLoss);
		if(reqInfo.getTType() == ThriftTransactionType.RESERVE){
			if(reqInfo.getOType() == ThriftOrderType.NORMAL){
				if(totalStake.compareTo(betUser.getMaxBetNormal()) == 1) {
					message =  buildErrorMessage(OT_MAX_BET_NORMAL_LIMIT, null, betUser.getUserMaxBetNormal(), betUser.getDefaultMaxBetNormal());
					logger.info("Max Bet Normal Order is {} - additionalStake {} - originalStake {} - totalStake {} of userId {}",betUser.getMaxBetNormal(),reqCredit,originalStake,totalStake,reqInfo.getUserId());
					return message;
				}
				if(reqCredit.compareTo(remainingStakeForPerMatch) == 1) {
					message = buildErrorMessage(OT_MAX_BET_PER_MATCH_LIMIT, remainingStakeForPerMatch, betUser.getUserMaxBetPerMatch(), betUser.getDefaultMaxBetPerMatch());
					logger.info("Remaining stake for this match {} - {} - reqStake {} of userId {}",reqInfo.getMatchId(),remainingStakeForPerMatch,reqCredit,reqInfo.getUserId());
					return message;
				}
				if(reqCredit.compareTo(remainingStakeForMaxLoss) == 1) {
					message = buildErrorMessage(OT_MAX_LOSS_LIMIT, remainingStakeForMaxLoss, betUser.getUserMaxLoss(), betUser.getDefaultMaxLoss());
					logger.info("Remaining stake for max loss {} - reqStake {} of userId {}",remainingStakeForMaxLoss,reqCredit,reqInfo.getUserId());
					return message;
				}
			}else if(reqInfo.getOType() == ThriftOrderType.POSITIONAL_OPEN) {
				if(totalStake.compareTo(betUser.getMaxBetPositional()) == 1) {
					message = buildErrorMessage(OT_MAT_BET_POSITIONAL_LIMIT, null, betUser.getUserMaxBetPositional(), betUser.getDefaultMaxBetPositional());
					logger.info("Max Bet Normal Order is {} - additionalStake {} - originalStake {} - totalStake {} of userId {}",betUser.getMaxBetPositional(),reqCredit,originalStake,totalStake,reqInfo.getUserId());
					return message;
				}
				if(reqCredit.compareTo(remainingStakeForPerMatch) == 1) {
					message = buildErrorMessage(OT_MAX_BET_PER_MATCH_LIMIT, remainingStakeForPerMatch, betUser.getUserMaxBetPerMatch(), betUser.getDefaultMaxBetPerMatch());
					logger.info("Remaining stake for this match {} - {} - reqStake {} of userId {}",reqInfo.getMatchId(),remainingStakeForPerMatch,reqCredit,reqInfo.getUserId());
					return message;
				}
				if(reqCredit.compareTo(remainingStakeForMaxLoss) == 1) {
					message = buildErrorMessage(OT_MAX_LOSS_LIMIT, remainingStakeForMaxLoss, betUser.getUserMaxLoss(), betUser.getDefaultMaxLoss());
					logger.info("Remaining stake for max loss {} - reqStake {} of userId {}",remainingStakeForMaxLoss,reqCredit,reqInfo.getUserId());
					return message;
				}
				
			}
		}
		return message;
		
	}

	private String buildErrorMessage(String key, BigDecimal remaining, BigDecimal userLimit, BigDecimal defaultLimit) {
		String message;
		if(userLimit == null || genUtil.isLess(defaultLimit, userLimit)) {
			if(remaining == null) {
				message = voddsCommonUtility.stringsToDescription(key + 1);
			} else {
				message = voddsCommonUtility.stringsToDescription(key + 1, genUtil.format(remaining, RoundingMode.FLOOR));
			}
		} else {
			message = voddsCommonUtility.stringsToDescription(key, genUtil.format(remaining == null ? userLimit : remaining, RoundingMode.FLOOR));
		}
		
		return message;
	}
	
	public Map<String, StakePerMatch> getStakePerMatchCahe(BetUserNode betUser){
		try{
			sem.acquire();
			if(betUser.getMapStakePerMatch() == null || betUser.getMapStakePerMatch().size() == 0) {
				logger.debug("userId {} - cahe stake per match {}-{} before loading data from database",betUser.getUser().getId(),betUser.getMapStakePerMatch(),betUser.getMapStakePerMatch().size());
				Map<String, StakePerMatch> mapStakePerMatch = dbService.findStakePerMatchByUserId(betUser.getUser().getId());
				betUser.setMapStakePerMatch(mapStakePerMatch);
				logger.debug("userId {} - cahe stake per match {} after loading data from database",betUser.getUser().getId(),mapStakePerMatch.size());
	
			}
			return betUser.getMapStakePerMatch();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Failed to get stake per match cache of userId {}, ",betUser.getUser().getId(),e);
			return null;
		}
		finally{
			sem.release();
		}
	}
}
