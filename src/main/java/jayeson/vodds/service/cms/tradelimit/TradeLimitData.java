package jayeson.vodds.service.cms.tradelimit;

import java.math.BigDecimal;
import java.sql.Timestamp;

import jayeson.vodds.service.cms.thrift.ThriftOrderType;

public class TradeLimitData {
	String matchId;
	
	String sportType;
	
	String orderStatus;
	
	String executionStage;
	
	Timestamp startTime;
	
	BigDecimal rejectedStake;
	
	BigDecimal stake;
	
	BigDecimal mpl;
	
	String transactionType;
	
	BigDecimal confirmedStakeOpenOrder;
	
	ThriftOrderType oType;

	boolean tradeLimitCheck = true;
	
	public TradeLimitData() {
		super();
	}

	public TradeLimitData(String matchId, String sportType, String orderStatus, String executionStage,
			Timestamp startTime, BigDecimal rejectedStake, BigDecimal stake, BigDecimal mpl,String transactionType) {
		super();
		this.matchId = matchId;
		this.sportType = sportType;
		this.orderStatus = orderStatus;
		this.executionStage = executionStage;
		this.startTime = startTime;
		this.rejectedStake = rejectedStake;
		this.stake = stake;
		this.mpl = mpl;
		this.transactionType = transactionType;
		this.confirmedStakeOpenOrder = BigDecimal.ZERO;
	}

	public String getMatchId() {
		return matchId;
	}

	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public String getSportType() {
		return sportType;
	}

	public void setSportType(String sportType) {
		this.sportType = sportType;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getExecutionStage() {
		return executionStage;
	}

	public void setExecutionStage(String executionStage) {
		this.executionStage = executionStage;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = Timestamp.valueOf(startTime);
	}

	public BigDecimal getRejectedStake() {
		return rejectedStake;
	}

	public void setRejectedStake(BigDecimal rejectedStake) {
		this.rejectedStake = rejectedStake;
	}

	public BigDecimal getStake() {
		return stake;
	}

	public void setStake(BigDecimal stake) {
		this.stake = stake;
	}

	public BigDecimal getMpl() {
		return mpl;
	}

	public void setMpl(BigDecimal mpl) {
		this.mpl = mpl;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public BigDecimal getConfirmedStakeOpenOrder() {
		return confirmedStakeOpenOrder;
	}

	public void setConfirmedStakeOpenOrder(BigDecimal confirmedStakeOpenOrder) {
		this.confirmedStakeOpenOrder = confirmedStakeOpenOrder;
	}

	public boolean isTradeLimitCheck() {
		return tradeLimitCheck;
	}

	public void setTradeLimitCheck(boolean tradeLimitCheck) {
		this.tradeLimitCheck = tradeLimitCheck;
	}

	public ThriftOrderType getoType() {
		return oType;
	}

	public void setoType(ThriftOrderType oType) {
		this.oType = oType;
	}
	
	
	
	
}
