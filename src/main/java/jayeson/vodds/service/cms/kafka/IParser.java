package jayeson.vodds.service.cms.kafka;

public interface IParser {
    public void onMessage(String message);
}
