package jayeson.vodds.service.cms.kafka;


import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.vodds.common.model.notification.BaseMessage;
import jayeson.vodds.common.model.notification.ConfigUpdatedMessage;
import jayeson.vodds.common.model.notification.UserTradeLimitUpdateMessage;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.util.ConfigUtil;




@Singleton
public class MessageProcessor implements IProcessor {
  
	UserManager uManager;
	ConfigUtil configUtil;

    @Inject
    public MessageProcessor(UserManager manager, ConfigUtil configUtil) {
    	this.uManager = manager;
    	this.configUtil = configUtil;
    }

    @Override
    public void processMessage(BaseMessage message) {

    	if(message instanceof UserTradeLimitUpdateMessage) {
    		this.uManager.updateTradeLimit((UserTradeLimitUpdateMessage)message);
    	} else if (message instanceof ConfigUpdatedMessage) {
    		ConfigUpdatedMessage configUpdateMessge = (ConfigUpdatedMessage) message;
    		if (configUpdateMessge.isConstantUpdated()) {
    			String excludeTradeLimitConst = configUtil.loadConstants().get(ConfigUtil.EXCLUDE_TRADE_LIMIT_CHECK);
    			this.uManager.setExcludeTradeLimitList(excludeTradeLimitConst);
    		}
    	}

        
    }
}
