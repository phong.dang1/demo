package jayeson.vodds.service.cms.kafka;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import jayeson.utility.UtilityModule;
import jayeson.vodds.service.cms.config.ConfigBean;

import org.apache.kafka.common.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Singleton
public class CmsKafkaConsumer extends BaseKafkaConsumer implements Runnable {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private ConfigBean config;

    @Inject
    public CmsKafkaConsumer(ConfigBean config, @Named(UtilityModule.COMMON_SES) ScheduledExecutorService ses, IParser parser, ICacheMaintainer cacheMaintainer) {
        super(ses, parser, cacheMaintainer);
        this.config = config;
        ses.schedule(this,0, TimeUnit.MILLISECONDS);

    }

    public void run() {
        try {
            logger.info("start consuming");
            //setting
            this.buildKafkaConsumer(null, config.getKafkaConfig().getTopic(), config.getKafkaConfig().getPollDelay(),
                    config.getKafkaConfig().getStartOffset());
            this.start();
        } catch (Exception ex) {
            logger.error("Exception starting TpKafkaConsumer ", ex);
        }
    }
}
