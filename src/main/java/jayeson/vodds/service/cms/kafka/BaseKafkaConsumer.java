package jayeson.vodds.service.cms.kafka;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;

public abstract class BaseKafkaConsumer {
    protected ICacheMaintainer cacheMaintainer;
    protected IParser parser;
    protected long pollDelayMs;
    protected String topic;
    protected Long startOffset;
    protected ObjectMapper objMapper;
    protected Logger logger = LoggerFactory.getLogger("KafkaConsumer");
    protected KafkaConsumer<String, String> kafkaConsumer;//<keytype, valueType>
    protected ScheduledExecutorService ses;

    public BaseKafkaConsumer(ScheduledExecutorService ses, IParser parser, ICacheMaintainer cacheMaintainer) {
        this.ses = ses;
        this.parser = parser;
        this.cacheMaintainer = cacheMaintainer;
        this.objMapper = new ObjectMapper();
    }

    public void start() {
        logger.info("kafka consumer started");
        try {
            // Keep polling to continuing receiving the data stream
            boolean seekOffset = true;
            while (true) {
                try {
                    this.logger.trace("IS CONTINUE GETTING DATA {}", this.cacheMaintainer.isContinueGettingData());
                    if (this.cacheMaintainer.isContinueGettingData()){
	        			ConsumerRecords<String, String> records = kafkaConsumer.poll(this.pollDelayMs);
	        			
	        			if (seekOffset && this.startOffset != null){
	        				this.kafkaConsumer.seek(new TopicPartition(this.topic, 0), this.startOffset);
	        				seekOffset = false;
	        			}
	        			
	                    for (ConsumerRecord<String, String> record : records) {
	                    	this.logger.trace("offset = {}, key = {}, value = {}",
	                                record.offset(), record.key(), record.value());
	                    	String receivedMessage = record.value();
	                    	JsonNode node = this.objMapper.readTree(receivedMessage);
	                    	if (node.get("message") != null){
	                    		this.parser.onMessage(node.get("message").asText());
	                    	}else{
	                    		this.logger.debug("Invalid message {}", receivedMessage);
	                    	}
	                    }
	        		}
                } catch (Exception ex) {
                    this.logger.error("Error while waiting/processing message from kafka", ex);
                }
            }
        } catch (Exception ex) {
            logger.error("Error while starting kafka consumer", ex);
        } finally {
            this.kafkaConsumer.close();
        }
    }

    protected void buildKafkaConsumer(String configFilePath, String topic, Long pollDelayMs, Long startOffset) {
        String effectiveFilePath = configFilePath == null? "conf/kafka-config.properties": configFilePath;
        FileInputStream input = null;
        Properties props = null;
        try{
            input = new FileInputStream(effectiveFilePath);

            props = new Properties();
            props.load(input);

            this.pollDelayMs = pollDelayMs;
            this.startOffset = startOffset;
            this.topic = topic;

        }catch(Exception ex){
            this.logger.debug("Error loading kafka config from {}", effectiveFilePath, ex);
            System.exit(-1);
        }finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    logger.debug("Error closing kafka config file", e);
                }
            }
        }

        // Build consumer
        this.kafkaConsumer = new KafkaConsumer<>(props);
        // Subscribe to topic, initialized on each concrete class
        this.kafkaConsumer.subscribe(Arrays.asList(this.topic));

    }
}
