package jayeson.vodds.service.cms.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.vodds.common.model.notification.ConfigUpdatedMessage;
import jayeson.vodds.common.model.notification.MessageType;
import jayeson.vodds.common.model.notification.UserTradeLimitUpdateMessage;


@Singleton
public class MessageParser implements IParser {
    private Logger logger = LoggerFactory.getLogger(MessageParser.class);
    private ObjectMapper mapper;
    private IProcessor processor;

    

    @Inject
    public MessageParser(IProcessor processor) {
        this.processor = processor;
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public void onMessage(String message) {
		try{
			
			JsonNode baseLog = this.mapper.readTree(message);
			String messageTypeStr = baseLog.get("messageType").asText();
			MessageType messageType = MessageType.valueOf(messageTypeStr);
			this.logger.debug("Process new message {} {}", messageTypeStr, message);
			if (MessageType.TRADE_LIMIT_UPDATE == messageType){
				UserTradeLimitUpdateMessage log = this.mapper.readValue(message, UserTradeLimitUpdateMessage.class); 
				logger.debug("tradeLimitUpdatedMessage {}",log);
				this.processor.processMessage(log);
			} else if (MessageType.CONFIG_UPDATE == messageType) {
				ConfigUpdatedMessage log = this.mapper.readValue(message, ConfigUpdatedMessage.class); 
				logger.debug("configUpdatedMessage {}",log);
				this.processor.processMessage(log);
			}
	
		}catch(Exception ex){
			this.logger.error("Cannot parse message: {} ", message, ex);
		}

    }

}
