package jayeson.vodds.service.cms.kafka;

import com.google.inject.Inject;

import javax.inject.Singleton;

@Singleton
public class CacheMaintainer implements ICacheMaintainer {
    private boolean continueGettingData;

    @Inject
    public CacheMaintainer() {
        this.continueGettingData = true;
    }

    @Override
    public boolean isContinueGettingData() {
        return this.continueGettingData;
    }

    @Override
    public void setContinueGettingData(boolean isContinue) {
        this.continueGettingData = isContinue;
    }
}
