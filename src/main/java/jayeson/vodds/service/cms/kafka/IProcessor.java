package jayeson.vodds.service.cms.kafka;

import jayeson.vodds.common.model.notification.BaseMessage;

public interface IProcessor {
    void processMessage(BaseMessage message);
}
