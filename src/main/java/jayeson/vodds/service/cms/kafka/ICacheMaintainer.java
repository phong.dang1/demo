package jayeson.vodds.service.cms.kafka;

public interface ICacheMaintainer {
    public boolean isContinueGettingData();
    public void setContinueGettingData(boolean isContinue);
}
