package jayeson.vodds.service.cms.order.worker;



import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.BetUser;
import jayeson.utility.concurrent.worker.batch.SharedExecutorBatchWorker;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;

@Singleton
public class OutstandingWorker {
	
	private static Logger logger = LoggerFactory.getLogger(OutstandingWorker.class);
	
	private SharedExecutorBatchWorker<UserOutstandingChangeEvent, Void> seBW;
	
	private OutstandingCallback callback;

	@Inject
	public OutstandingWorker(ConfigBean bean,ScheduledExecutorService service,EventBus eBus){
		logger.debug("Get max batch size: {}",bean.getBatchSize());
		this.seBW = new SharedExecutorBatchWorker<>(service, new OutstandingHandler(), bean.getBatchSize(),new OutstandingCallback(eBus));

	}
	
	public void submitOutstanding(UserOutstandingChangeEvent item) {
			logger.debug("Submit user {} with out standing {}",item.getUserName(),item.getOutstandingValue());
			seBW.submit(item);
	}

}
