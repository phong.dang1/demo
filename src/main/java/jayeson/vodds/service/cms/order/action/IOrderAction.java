package jayeson.vodds.service.cms.order.action;

import jayeson.vodds.service.cms.order.result.IActionResult;

/***
 * Interface for the actions to be taken by normal and positional order tasks
 * 
 * @author Praveen
 *
 */

public interface IOrderAction {
	
public IActionResult processCreditAction();
	
}
