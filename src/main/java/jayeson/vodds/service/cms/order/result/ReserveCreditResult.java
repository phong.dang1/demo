package jayeson.vodds.service.cms.order.result;

public class ReserveCreditResult extends IActionResult {

	// amount of credit that is succesfully reserved
	double reservedCredit;
	
	// amount of commission value deducted from user
	double commissionAmount;
	
	// whether to or not use only the commission amount in deducting user credit, true for closing order reservation
	boolean useOnlyCommission=false;
	
	// whether the reserved credit is to topup margin, true for open order reservation during upl margin calls
	boolean marginTopup = false;
	
	//default leverage ratio for the result, values set to corresponding ratio in positional opening order 
	double leverageRatio=1;
	

	public ReserveCreditResult() {
		super();
	}
	public double getReservedCredit() {
		return reservedCredit;
	}
	public void setReservedCredit(double reservedCredit) {
		this.reservedCredit = reservedCredit;
	}
	public double getCommissionAmount() {
		return commissionAmount;
	}
	public void setCommissionAmount(double commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
	public boolean isUseOnlyCommission() {
		return useOnlyCommission;
	}
	public void setUseOnlyCommission(boolean useOnlyCommission) {
		this.useOnlyCommission = useOnlyCommission;
	}
	public boolean isMarginTopup() {
		return marginTopup;
	}
	public void setMarginTopup(boolean marginTopup) {
		this.marginTopup = marginTopup;
	}
	public double getLeverageRatio() {
		return leverageRatio;
	}
	public void setLeverageRatio(double leverageRatio) {
		this.leverageRatio = leverageRatio;
	}

	


}
