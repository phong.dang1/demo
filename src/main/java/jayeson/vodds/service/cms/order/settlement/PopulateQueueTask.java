package jayeson.vodds.service.cms.order.settlement;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jayeson.database.BettingModel;
import jayeson.database.newvodds.DarkPoolBetInfo;
import jayeson.lib.betting.api.datastructure.BetStatus;
import jayeson.vodds.orders.normal.NormalOrderStatus;
import jayeson.vodds.orders.positional.PositionalCloseStatus;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.data.BetWrapper;
import jayeson.vodds.service.cms.data.DPoolBetWrapper;
import jayeson.vodds.service.cms.data.DPoolWrapperManager;
import jayeson.vodds.service.cms.data.OrderWrapper;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.events.UserPlChangeEvent;
import jayeson.vodds.service.cms.transactions.TransactionQueue;

import com.google.inject.Inject;

/***
 *  populates the order queue periodically or on demand as required
 * 	
 * 	@author Praveen
 *
 */
public class PopulateQueueTask implements Runnable{

	
	@Inject
	UnsettledQueue oQueue;
	@Inject
	IDBService dbService;
	@Inject
	OrderWrapperManager oManager;
	@Inject
	DPoolWrapperManager dManager;
	@Inject
	ConfigBean cBean;
	@Inject
	UserManager uManager;
	@Inject
	TransactionQueue tQueue;
	static Logger logger = LoggerFactory.getLogger(PopulateQueueTask.class.getName());
	
	static Set<String> settledStatuses = new HashSet<String>(Arrays.asList(NormalOrderStatus.WIN.name(),NormalOrderStatus.LOSS.name(),
			NormalOrderStatus.DRAW.name(),NormalOrderStatus.REFUNDED.name()));

	public void pushOrderWithAcceptedBetIntoUnsettledQueue(List<BettingModel> lst){
		for (BettingModel bm : lst) {
			try{
				logger.info("order {} push into unsettled queue for status change , size of list {} - number of transaction isn't saved into database {}",bm.getId(),lst.size(),tQueue.getNumberTransactionRecordByOrder(bm.getId()));
				OrderWrapper oWrapper = oManager.createOrderWrapperForUnsettledOrder(bm);
				if(null != oWrapper && !oWrapper.getIsSettling() && tQueue.getNumberTransactionRecordByOrder(bm.getId()) == null){
					logger.debug("{} unsettle order with is_modified = 1 has status {}",bm.getId(),oWrapper.getOrderSettlement().toString());
					Set<BetWrapper> bWrappers = oWrapper.getBetWrappers();
					boolean isValid = isAcceptedBet(bWrappers);
					if(isValid){
						logger.info("order {} with bet list size of {}, added to the unsettled order with accepted bet queue",bm.getId(),bWrappers.size());
						oWrapper.setIsSettling(true);
						oQueue.addUnsettledOrderWithAcceptedBet(oWrapper);
					}
				}
			}catch(Exception e){
				logger.error("error while pushing order with accepted bet into queue",e);
			}
		}
	}

	public void pushOrderIntoUnsettleQueue(List<BettingModel> lst){
		for(BettingModel order:lst){
			try{
				long oId =  order.getId();
				OrderWrapper oWrapper = oManager.findStoppedOrderById(order.getId());
				//Set<BetWrapper> bWrappers = oManager.findOrderBets(oId);
				logger.debug("OrderId {} - number of transaction isn't saved into database {}",oId,tQueue.getNumberTransactionRecordByOrder(oId));
				if(null != oWrapper && !oWrapper.getIsSettling() && tQueue.getNumberTransactionRecordByOrder(oId) == null){
					logger.debug("{} order with status {} - isSettling {}",order.getId(),oWrapper.getOrderSettlement().toString(),oWrapper.getIsSettling());
					// Check status of order . If status is WIN,LOSS,DRAW,REFUND or PENDING , will remove order from order cache and don't push order into unsettled queue
					// Do that to avoid status orders change when call function oManager.findOrder() 
					if(settledStatuses.contains(oWrapper.getOrderSettlement()) || oWrapper.getOrderSettlement().equals(NormalOrderStatus.PENDING.name())){
						oManager.removeOrderFromCache(oWrapper.getOrder().getId());
					}else{
						Set<BetWrapper> bWrappers = oWrapper.getBetWrappers();
						
						boolean isResolved = isAllBetsResolved(bWrappers);
						boolean isSettled = true;
						
						if(oWrapper.getOrderSettlement().equals(NormalOrderStatus.REJECTED.name()) || oWrapper.getOrderSettlement().equals(NormalOrderStatus.CONFIRMED.name())
								|| oWrapper.getOrderSettlement().equals(NormalOrderStatus.PARTIAL.name()) || oWrapper.getOrderSettlement().equals(NormalOrderStatus.MODIFIED.name()) ||
								oWrapper.getOrderSettlement().equals(NormalOrderStatus.STOPPED.name()) || oWrapper.getOrderSettlement().equals(PositionalCloseStatus.UNFULFILLED.name()) ){
							isSettled = false;
						}
						
						logger.debug("{} bets retrieved for order id {} with resolved status {} and settled status {}",bWrappers.size(),oWrapper.getOrder().getId(),isResolved,isSettled);
						// add to the queue when all precondition are met
						if(isResolved && !isSettled){
							logger.info("order {} with bet list size of {}, added to the unsettled queue",oId,bWrappers.size());
							oWrapper.setIsSettling(true);
							oQueue.add(oWrapper);
						}
					}
				}
			}
			catch(Exception e){
				logger.error("error while populating the queue",e);
			}
		}
	}
	
	public void populateUnsettledOrderWithAcceptedBetQueue(){
		//populate the queue in batches to handle the case where is_modified = 1 for unsettled order
		int unsettleCount = dbService.findUnsettledWithAcceptedBetCount();
		if(unsettleCount != 0){
			List<BettingModel> unsettledList = dbService.findUnsettledWithAcceptedBet();
			pushOrderWithAcceptedBetIntoUnsettledQueue(unsettledList);
		}
	}
	
	public void populateOrderQueue(){
		
		int voidingCount = dbService.findVoidingOrdersCount();
		if(voidingCount != 0){
			logger.info("Total number of voiding orders in database {}",voidingCount);
			int totalCountVoiding =0;
			while(totalCountVoiding <= voidingCount && voidingCount > 0){
				logger.info("processing orders from index {} of a size {}",totalCountVoiding,cBean.getPopulateOrdersPerInterval());
				List<BettingModel> voidingOrderList = dbService.findVoidingOrders(totalCountVoiding,cBean.getPopulateOrdersPerInterval());
				pushOrderIntoUnsettleQueue(voidingOrderList);
				totalCountVoiding += cBean.getPopulateOrdersPerInterval();
			}
		}
		// populate the queue in batches to handle the case when there are huge number of unsettled orders in the database
		int orderNotStartedCount = dbService.findUnsettledOrdersWithMatchNotStartedCount();
		logger.info("Total number of unsettled orders with the match not started in database {}",orderNotStartedCount);
		int totalOrderNotStartedCount = 0;

		while(totalOrderNotStartedCount <= orderNotStartedCount && orderNotStartedCount > 0){
			logger.info("processing orders with the match not started from index {} of a size {}",totalOrderNotStartedCount,cBean.getPopulateOrdersPerInterval());

			List<BettingModel> orderList = dbService.findUnsettledOrdersWithMatchNotStarted(totalOrderNotStartedCount,cBean.getPopulateOrdersPerInterval());
			pushOrderIntoUnsettleQueue(orderList);
			totalOrderNotStartedCount += cBean.getPopulateOrdersPerInterval();
		}

		int orderCount = dbService.findUnsettledOrdersCount();
		logger.info("Total number of unsettled orders in database {}",orderCount);
		int totalCount =0;

		while(totalCount <= orderCount && orderCount > 0){
			logger.info("processing orders from index {} of a size {}",totalCount,cBean.getPopulateOrdersPerInterval());

			List<BettingModel> orderList = dbService.findUnsettledOrders(totalCount,cBean.getPopulateOrdersPerInterval());
			pushOrderIntoUnsettleQueue(orderList);
			totalCount += cBean.getPopulateOrdersPerInterval();
		}
	}
	
	public void populateDPoolQueue(){
		List<DarkPoolBetInfo> dPoolBets = dbService.findUnsettledDPoolBets();
		logger.info("fetched dark pool bet list of size {}",dPoolBets.size());
		for(DarkPoolBetInfo dpBet:dPoolBets){
			
			long dpId = dpBet.getId();
			DPoolBetWrapper dWrapper = dManager.findDPoolWrapper(dpBet);
			Collection<BetWrapper> bWrappers = dWrapper.getBetWrappers().values();
			
			boolean isResolved = isAllBetsResolved(new HashSet<BetWrapper>(bWrappers));
//			boolean isSettled = dWrapper.getStatus().toString().equals("SETTLED");
			
			if(isResolved){
				logger.info("dpool bet info {} with bet list size of {}, added to the unsettled queue",dpId,bWrappers.size());
				oQueue.add(dWrapper);
			}

			
		}
		
		
	}
	
	/*** 
	 * checks if the bets related to order are all resolved 
	 * @param betWrappers
	 * @return
	 */
	public boolean isAllBetsResolved(Set<BetWrapper> betWrappers){
		boolean resolved=true;
		for(BetWrapper bWrapper:betWrappers){
			
			if(bWrapper.getBet().getStatus() == BetStatus.PENDING || bWrapper.getBet().getStatus() == BetStatus.ACCEPTED || 
					 bWrapper.getBet().getStatus() == BetStatus.ACCEPTED_UNKNOWN_ID){
				logger.info("bet id {} has status {} ",bWrapper.getBet().getId(),bWrapper.getBet().getStatus());						
				resolved = false;
				break;
			}
			
		}
		
		return resolved;
	}
	
	public boolean isAcceptedBet(Set<BetWrapper> betWrappers){
		boolean valid = false;
		for (BetWrapper bWrapper : betWrappers) {
			if(bWrapper.getBet().getStatus() == BetStatus.ACCEPTED){
				logger.info("bet id {} in unsettled order with modified = 1 has status {} ",bWrapper.getBet().getId(),bWrapper.getBet().getStatus());	
				valid = true;
				break;
			}
		}
		return valid;
	}
	
	@Override
	public void run() {
		try{
			populateOrderQueue();
			populateDPoolQueue();
			populateUnsettledOrderWithAcceptedBetQueue();
		}
		catch(Exception ex){
			logger.error("error while populating the queue",ex);
		}
		
	}
	
	
	
}
