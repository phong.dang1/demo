package jayeson.vodds.service.cms.order.worker;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;

@Singleton
public class OutstandingHandler implements Function<List<UserOutstandingChangeEvent>, Void>{
	
	private static Logger logger = LoggerFactory.getLogger(OutstandingHandler.class);

	public OutstandingHandler(){
		
	}

	@Override
	public Void apply(List<UserOutstandingChangeEvent> data) {
		logger.debug("saving {} outstandings: {}", data.size(), data);
		return null;
	}

}
