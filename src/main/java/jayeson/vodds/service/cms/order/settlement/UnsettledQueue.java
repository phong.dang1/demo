package jayeson.vodds.service.cms.order.settlement;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import jayeson.database.newvodds.*;
import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.Bet;
import jayeson.database.BettingModel;
import jayeson.database.Sportbook;
import jayeson.lib.betting.datastructure.BetStatus;
import jayeson.vodds.orders.common.VOddsModelType;
import jayeson.vodds.orders.logs.OrderSettlementMessageLogs;
import jayeson.vodds.orders.logs.OrderStateMessageLogs;
import jayeson.vodds.orders.logs.Type;
import jayeson.vodds.orders.normal.NormalOrderStatus;
import jayeson.vodds.orders.positional.PositionalCloseStatus;
import jayeson.vodds.orders.positional.PositionalOpenStatus;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.data.AgentUserNode;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.BetWrapper;
import jayeson.vodds.service.cms.data.DPoolBetWrapper;
import jayeson.vodds.service.cms.data.OrderWrapper;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.data.PositionalOrderWrapper;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.service.NoQPersister;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;

/***
 * A FIFO queue to process the unsettled orders with all constituent bets having
 * valid p&l methods to settle and adjust user credit and generate model
 * transactions accordingly
 * 
 * and process all the unsettled dark pool bets using their liquidating real
 * bets pl
 * 
 * @author Praveen
 *
 */

@Singleton
public class UnsettledQueue {

	OrderWrapperManager oManager;
	UserManager uManager;
	TransactionBuilder tBuilder;
	ConfigBean cBean;
	IDBService dbService;
	NoQPersister persistenceModerator;
	ConcurrentLinkedQueue<OrderWrapper> unsettledOrderWithAcceptedBetQueue;
	ConcurrentLinkedQueue<OrderWrapper> orderQueue;
	ConcurrentLinkedQueue<DPoolBetWrapper> dPoolQueue;
	static String typeMissing = "MISSING_BET";
	static String typeVoiding = "VOIDING_ORDER";
	static String defaultURL = "";
	ObjectMapper objectMapper;
	GeneralUtility genUtil;

	static Logger logger = LoggerFactory.getLogger(UnsettledQueue.class.getName());
	
	static Set<String> rejectedStatuses = new HashSet<String>(Arrays.asList(BetStatus.CANCELLED.name(),BetStatus.REJECTED.name(),BetStatus.REJECTED_ABNORMAL.name()));
	

	@Inject
	public UnsettledQueue(OrderWrapperManager manager, UserManager usermanager, TransactionBuilder builder,
			ConfigBean bean, IDBService db,NoQPersister dtPersitneceMorderator,OutstandingNotificationTask amtTask,ObjectMapper oMapper,
			GeneralUtility genUtil) {
		oManager = manager;
		uManager = usermanager;
		tBuilder = builder;
		cBean = bean;
		dbService = db;
		unsettledOrderWithAcceptedBetQueue = new ConcurrentLinkedQueue<OrderWrapper>();
		dPoolQueue = new ConcurrentLinkedQueue<DPoolBetWrapper>();
		orderQueue = new ConcurrentLinkedQueue<OrderWrapper>();
		persistenceModerator = dtPersitneceMorderator;
		this.objectMapper = oMapper;
		this.genUtil = genUtil;
	}

	// add to the queue only if the order is not already added
	public boolean add(OrderWrapper order) {

		if (!this.orderQueue.contains(order)) {

			logger.info("order {} added to the queue", order.getOrder().getId());
			return orderQueue.add(order);
		}
		return true;

	}
	
	public boolean addUnsettledOrderWithAcceptedBet(OrderWrapper order){
		if(!this.unsettledOrderWithAcceptedBetQueue.contains(order)){
			logger.info("unsettled order with accepted bet {} added to the queue",order.getOrder().getId());
			return unsettledOrderWithAcceptedBetQueue.add(order);
		}
		return true;
	}

	// add to the queue only if the bet is not already added
	public boolean add(DPoolBetWrapper dPoolBet) {
		if (!this.dPoolQueue.contains(dPoolBet)) {
			logger.info("dark pool bet {} added to the queue", dPoolBet.getId());
			return dPoolQueue.add(dPoolBet);
		}
		return true;
	}

	public BigDecimal sumCreditAmountTransaction(List<ModelTransaction> lst) {
		BigDecimal sum = new BigDecimal(0);
		for (ModelTransaction modelTransaction : lst) {
			sum = sum.add(modelTransaction.getCreditAmount());
		}
		return sum;
	}

	public ConcurrentLinkedQueue<OrderWrapper> getOrderQueue() {
		return orderQueue;
	}

	public ConcurrentLinkedQueue<DPoolBetWrapper> getdPoolQueue() {
		return dPoolQueue;
	}
		
	public ConcurrentLinkedQueue<OrderWrapper> getUnsettledOrderWithAcceptedBetQueue() {
		return unsettledOrderWithAcceptedBetQueue;
	}
	
	/***
	 * polls the dark pool bet queue and calculates the effective PL based on
	 * the return amounts of dark pool bet and constituent liquidating real bets
	 * to adjust the credit of company agent accordingly
	 */
	public void settleDPoolBets() {

		while (this.getdPoolQueue().size() > 0) {
			DPoolBetWrapper dWrapper = this.getdPoolQueue().poll();
			try {
				DarkPoolBetInfo dpBetInfo = dWrapper.getDpBet();

				if (dpBetInfo.getStatus() == DPoolBetStatus.RESOLVED) {

					settleDPoolBet(dWrapper);

				} else if (dpBetInfo.getStatus() == DPoolBetStatus.MODIFIED) {

					BigDecimal revAmount = dpBetInfo.getSettledAmount();
					logger.info("reverting amount {} the dpool bet info id {}", revAmount, dWrapper.getId());

					if (revAmount != null) {
						BetUserNode bUser = uManager.findBetUserNode(dWrapper.getDpBet().getBetId().getUser().getId());
						// retrieve the company agent of the bet user
						AgentUserNode companyAgent = bUser.getAgentUsers().stream()
								.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1)
								.collect(Collectors.toList()).get(0);

						logger.info("fetched company agent user {} for bet user {}",
								companyAgent.getUser().getUsername(), bUser.getUser().getUsername());

						//BigDecimal preCredit = companyAgent.getCredit();
						
						// Revert previous settled amount to company agent
						BigDecimal[] credits = companyAgent.addCredit(revAmount.negate());
						BigDecimal preCredit = credits[0];
						BigDecimal postCredit = credits[1];
						
						logger.info("previous company agent {} credit {}", companyAgent.getUser().getUsername(),
								preCredit);
						
						//BigDecimal postCredit = companyAgent.getCredit();
						tBuilder.populateSettleModifiedDPoolTransactions(companyAgent, revAmount, preCredit, postCredit,
								dWrapper);
					}

					// Settle dpool bet again
					settleDPoolBet(dWrapper);
				}
			} catch (Exception ex) {
				logger.error("error while settling the dark pool bet:{}", dWrapper.getId(), ex);
			}
		}
	}

	private void settleDPoolBet(DPoolBetWrapper dWrapper) throws InterruptedException {

		// ret_darkpool = calculate the dark pool return amount
		// ret_real = calculate the return amount from constituent bets
		// adjust the credit of company agent as (ret_real - ret_darkpool)

		logger.info("settling the dpool bet info id {}", dWrapper.getId());

		// Get return of DPool bet, convert it to Base currency
		Bet dpBet = dWrapper.getDpBet().getBetId();
		double retDPool = (dpBet.getReturnAmount().multiply(dpBet.getMultiplication())
				.multiply(dpBet.getBaseConversionRate())).doubleValue();
		double retReal = 0; // company return
		BetUserNode bUser = uManager.findBetUserNode(dWrapper.getDpBet().getBetId().getUser().getId());
		// retrieve the company agent of the bet user
		AgentUserNode companyAgent = bUser.getAgentUsers().stream()
				.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1).collect(Collectors.toList()).get(0);

		logger.info("fetched company agent user {} for bet user {}", companyAgent.getUser().getUsername(),
				bUser.getUser().getUsername());

		for (DPoolLiquidationMapping lMapping : dWrapper.getLiqMapping()) {

			Bet targetBet = dWrapper.getBetWrappers().get(lMapping.getBetId()).getBet();
			double fulfilledStake = lMapping.getFulfilledStake().doubleValue();

			// Convert return and stake to user currency
			double tReturn = (targetBet.getReturnAmount().multiply(targetBet.getMultiplication())).doubleValue();
			double tStake = (targetBet.getBetStake().multiply(targetBet.getMultiplication())).doubleValue();

			// effective return amount from this liquidation to the company
			// agent, convert to Base currency
			double eReturn = (fulfilledStake / tStake) * tReturn * targetBet.getBaseConversionRate().doubleValue();
			logger.debug(
					"computed effective return amount of {} for liquidation mapping bet {} that has stake {} and return {}",
					eReturn, targetBet.getId(), tStake, tReturn);
			retReal += eReturn;
		}

		double creditDiff = retReal - retDPool;
		logger.info("credit difference of {} computed with real bet return amount {} and dark pool return amount {}",
				creditDiff, retReal, retDPool);

		BigDecimal uCredit = new BigDecimal(creditDiff);

		//BigDecimal priorCredit = companyAgent.getCredit();
		BigDecimal[] credits = companyAgent.addCredit(uCredit);
		BigDecimal priorCredit = credits[0];
		BigDecimal postCredit = credits[1];
		//BigDecimal postCredit = companyAgent.getCredit();

		// set the status of dark pool bet info to settled
		dWrapper.setStatus(DPoolBetStatus.SETTLED);
		// set the settled amount of dark pool bet info
		dWrapper.setSettledAmount(uCredit);

		// populate the required database transactions
		tBuilder.populateSettleDPoolTransactions(companyAgent, uCredit, priorCredit, postCredit, dWrapper);

	}
	
	public void revertSettlementForAcceptedBet(){
		while (this.getUnsettledOrderWithAcceptedBetQueue().size() > 0) {
			OrderWrapper order = null;
			BigDecimal priorCredit;
			BigDecimal postCredit;
			try{
				order = this.getUnsettledOrderWithAcceptedBetQueue().peek();
				String orderType = order.getOrder().getModelType();
				logger.info("processing revert settlement order {} with accepted bet of type {}", order.getOrder().getId(),
						orderType);
				BetUserNode bUser = uManager.findBetUserNode(order.getOrder().getOwner().getUser().getId());
				AgentUserNode companyAgent = bUser.getAgentUsers().stream()
						.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1)
						.collect(Collectors.toList()).get(0);
				List<AgentUserNode> lstCompanyAgentPtpl = bUser.getAgentUsers().stream()
						.filter(aNode -> aNode.getUser().getAgentType().getLevel() != 0)
						.collect(Collectors.toList());
				

				
				//Revert ptpl transaction for agent 
				for (AgentUserNode agentUserNode : lstCompanyAgentPtpl) {
					revertTransactionPTPL(order,agentUserNode,BigDecimal.ZERO);
					persistenceModerator.saveAgentUser(agentUserNode.getUser());
				}
				
				// get match id 
				String matchId = dbService.getMatchId(order.getOrder().getId());
				
				
				if (orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name()) 
						|| orderType.equals("VOFutureNormal")
						|| orderType.equals(VOddsModelType.FUTURE_NORMAL.name())) {
					
					//revert settlement for operator
					List<ModelTransaction> latestReturnTransToOperator = dbService
							.findLatestReturnTransactionForOperator(order.getOrder().getId(),
									companyAgent.getUser().getId());
					if (!latestReturnTransToOperator.isEmpty()) {

						BigDecimal revAmountOperator = latestReturnTransToOperator.get(0).getCreditAmount();

						//priorCredit = companyAgent.getCredit();
						BigDecimal[] credits = companyAgent.addCredit(revAmountOperator.negate());
						priorCredit = credits[0];
						postCredit = credits[1];
						// revert credit
						tBuilder.populateSettleModifiedOrderTransactionsOperator(order, companyAgent,
								revAmountOperator, priorCredit, postCredit);
					}
					
					ModelTransaction latestReturnTrans = dbService
							.findLatestReturnTransaction(order.getOrder().getId(), bUser.getUser().getId());
					if (latestReturnTrans != null) {
						
											
						BigDecimal revAmount = latestReturnTrans.getCreditAmount();
						
						BigDecimal[] revAmtCredit = bUser.addCredit(revAmount.negate(), order.getOrder().getId());
						priorCredit = revAmtCredit[0];
						postCredit = revAmtCredit[1];

						logger.info("revert order creditBefore {}, returnAmount {}, creditAfter {}",
								priorCredit, revAmount.negate(), postCredit);
						
						BigDecimal outStanding = BigDecimal.ZERO;
						List<ModelTransaction> outStandingTransactions = dbService.findMarginTransactions(Arrays.asList(order.getOrder().getId()),bUser.getUser().getId());
						for (ModelTransaction transaction : outStandingTransactions) {
							outStanding = outStanding.add(transaction.getCreditAmount());
						}

						
								
						// revert credit
						tBuilder.revertSettlementForStatusBetChange(order, bUser, revAmount, priorCredit, postCredit, outStanding,matchId);
						
						//Update is_modified and notification pl after saving betting_model
						order.getOrder().setIsModified((byte)0);
						persistenceModerator.saveOrder(order.getOrder());
						
						
						

					}
				}else if (orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name())
						|| orderType.equals("VOFutureClose")
						|| orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())) {
					

					PositionalOrderWrapper pOrder = (PositionalOrderWrapper) order;
					
					// revert all credit amount for Vodds company
					List<ModelTransaction> lst = dbService.findLatestReturnTransactionForOperator(
							order.getOrder().getId(), companyAgent.getUser().getId());
					BigDecimal revOperator = BigDecimal.ZERO;
					if (!lst.isEmpty()) {
						for (ModelTransaction modelTransaction : lst) {
							logger.info("transaction_id {} - type {} - return amount {}",modelTransaction.getId(),modelTransaction.getType().name().toString(),modelTransaction.getCreditAmount());
							revOperator = revOperator.add(modelTransaction.getCreditAmount());
						}

						
						BigDecimal[] revAmnt = companyAgent.addCredit(revOperator.negate());
						BigDecimal priorCreditOperator = revAmnt[0];
						BigDecimal postCreditOperator = revAmnt[1];
						logger.info(
								"last revert amount of operator {} , {} for the order is found , credit before {} , credit after {}",
								companyAgent.getUser().getId(), revOperator.doubleValue(), priorCreditOperator,
								postCreditOperator);
						tBuilder.populateSettleModifiedOrderTransactionsForOperator(order, companyAgent, revOperator,
								priorCreditOperator, postCreditOperator);

					}
					
					//revert transaction for dark pool bet
					ModelTransaction latestSettlementDP = dbService
							.findLatestSettlementDarkpool(order.getOrder().getId(), companyAgent.getUser().getId());
					if(latestSettlementDP != null){
						BigDecimal revDPAmount = latestSettlementDP.getCreditAmount();

						BigDecimal[] revDPAmtCredit = companyAgent.addCredit(revDPAmount.negate());
						BigDecimal priorDPCredit = revDPAmtCredit[0];
						BigDecimal postDPCredit = revDPAmtCredit[1];
						
						logger.info("revert darkpool bet creditBefore {}, returnAmount {}, creditAfter {}",
								priorDPCredit, revDPAmount.negate(), postDPCredit);
						
						tBuilder.revertDPoolTransactionsByStatusChange(order, companyAgent, revDPAmount, priorDPCredit, postDPCredit);
						
					}

					ModelTransaction latestReturnTrans = dbService
							.findLatestReturnTransaction(order.getOrder().getId(), bUser.getUser().getId());
					if(latestReturnTrans != null){
						BigDecimal revAmount = latestReturnTrans.getCreditAmount();
						
						BigDecimal[] revAmtCredit = bUser.addCredit(revAmount.negate(), order.getOrder().getId());
						priorCredit = revAmtCredit[0];
						postCredit = revAmtCredit[1];
						
						logger.info("revert order creditBefore {}, returnAmount {}, creditAfter {}",
								priorCredit, revAmount.negate(), postCredit);
						
						BigDecimal margin = BigDecimal.ZERO;
						List<Long> modelIds = new ArrayList<>(Arrays.asList(order.getOrder().getId()));
						modelIds.add(pOrder.getoModel().getId());
						List<ModelTransaction> marginTransactions = dbService.findMarginTransactions(modelIds,bUser.getUser().getId());
						for (ModelTransaction transaction : marginTransactions) {
							margin = margin.add(transaction.getCreditAmount());
						}
						logger.info("revert margin {} for model ids {}", margin, modelIds);
						// revert credit
						tBuilder.revertSettlementForStatusBetChange(order, bUser, revAmount, priorCredit, postCredit, margin,matchId);
						
						//Update is_modified for close order 
						pOrder.getOrder().setIsModified((byte)0);
						pOrder.getOrder().setModelStatus(pOrder.getOrderSettlement());
						persistenceModerator.saveOrder(order.getOrder());
						
						
						pOrder.getoModel().setIsModified((byte)0);
						pOrder.getoModel().setModelStatus(pOrder.getOrderSettlement());
						persistenceModerator.saveOrder(pOrder.getoModel());
					}
					
					
				}
			} catch (Exception e) {
				logger.error("error while revert settlement for accepted bet ", e);
				continue;
			} finally {
				// remove order from the queue
				order = this.getUnsettledOrderWithAcceptedBetQueue().poll();
			}
		}
	}

	/***
	 * polls the order queue and calculates the cummulative PL based on order
	 * type, and credits the user balance as necessary
	 */
	public void settleOrders() {

		int qSize = Math.min(this.getOrderQueue().size(), cBean.getSettleOrderPerBlock());
		logger.info("settling orders of size {} in the queue", qSize);
		BigDecimal priorCredit;
		BigDecimal postCredit;

		OrderWrapper order = null;

		for (int i = 0; i < qSize; i++) {

			try {
				order = this.getOrderQueue().peek();
				logger.info("processing order {} of type {}", order.getOrder().getId(),
						order.getOrder().getModelType());
				// process the order based on its model type
				String orderType = order.getOrder().getModelType();
				if (orderType.equals("VONormal") || orderType.equals(VOddsModelType.NORMAL.name()) 
						|| orderType.equals("VOFutureNormal")
						|| orderType.equals(VOddsModelType.FUTURE_NORMAL.name())) { // keeps
																					// VONormal
																					// for
																					// backward
																					// compatibility
					// handle the case when the order is resolved and not
					// settled

					BigDecimal orderPL = new BigDecimal(0);
					BigDecimal orderStake = new BigDecimal(0);
					BigDecimal plMissingBet = new BigDecimal(0);

					// Set<BetWrapper> oBets =
					// oManager.findOrderBets(order.getOrder().getId());
					Set<BetWrapper> oBets = order.getBetWrappers();

					boolean isRejected = tBuilder.isAllBetsRejected(oBets);

					
					logger.debug("stake {} and pl {} for order {}", orderStake.doubleValue(), orderPL.doubleValue(),
							order.getOrder().getId());
					BetUserNode bUser = uManager.findBetUserNode(order.getOrder().getOwner().getUser().getId());

					// retrieve the company agent of the bet user
					AgentUserNode companyAgent = bUser.getAgentUsers().stream()
							.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1)
							.collect(Collectors.toList()).get(0);
					
					List<AgentUserNode> lstCompanyAgentPtpl = bUser.getAgentUsers().stream()
							.filter(aNode -> aNode.getUser().getAgentType().getLevel() != 0)
							.collect(Collectors.toList());

					boolean isSkip = false; // this flag indicate that cms need to save order

					if (order.getOrderSettlement().equals(NormalOrderStatus.STOPPED.name())
							|| order.getOrderSettlement().equals(NormalOrderStatus.CONFIRMED.name())
							|| order.getOrderSettlement().equals(NormalOrderStatus.PARTIAL.name())
							|| order.getOrderSettlement().equals(NormalOrderStatus.REJECTED.name())) {

						BigDecimal totalStake = new BigDecimal(0);
						for (BetWrapper oBet : oBets) {
							Bet dBet = oBet.getBet();
                            if (dBet.getReturnAmount() != null){
                                // handle the case bet is missing
                                if (dBet.IsMissingBet() == 1) {
                                    plMissingBet = plMissingBet
                                            .add(dBet.getReturnAmount().multiply(dBet.getMultiplication())
                                                    .multiply(dBet.getBaseConversionRate()));
                                } else {
                                    if(!rejectedStatuses.contains(dBet.getStatus().name())){
                                        logger.debug("bet id {} has return amount {} and stake {}", dBet.getId(),
                                                dBet.getReturnAmount().doubleValue(), dBet.getBetStake().doubleValue());

                                        orderPL = orderPL
                                                .add(dBet.getReturnAmount().multiply(dBet.getMultiplication()));

                                        orderStake = orderStake
                                                .add(dBet.getBetStake().multiply(dBet.getMultiplication()));
                                    }

                                }
                            }
						}
						
						List<ModelTransaction> marginTransactions = dbService.findMarginTransactions(Arrays.asList(order.getOrder().getId()),bUser.getUser().getId());
						if (marginTransactions.isEmpty()) {
							isSkip = true;
						} else {
							for (ModelTransaction transaction : marginTransactions) {
								totalStake = totalStake.add(transaction.getCreditAmount());
							}

							BigDecimal refundAmtRejectedBet = totalStake.negate().subtract(orderStake);

							BigDecimal settledAmt = orderPL.add(orderStake);

							logger.info("OrderId: {},total stake base on transaction {} , total stake base on bet {},settlement amnt {}", order.getOrder().getId(), totalStake, orderStake, settledAmt);

							BigDecimal newRevAmount = settledAmt.add(refundAmtRejectedBet);


							//caculate ptpl , get ptpl from cache and cache ptpl after update plpl for agent
							settlePTPLNO(lstCompanyAgentPtpl, oBets, order, false);

							// settle for case missing bet to operator
							settleMissingBet(order, plMissingBet, companyAgent, orderType);


							// add the values to the user wrapper
							BigDecimal[] newRevs = bUser.addCredit(newRevAmount, order.getOrder().getId());
							priorCredit = newRevs[0];
							postCredit = newRevs[1];

							// set the return amount of order in the wrapper
							order.setReturnAmount(newRevAmount);

							if (isRejected) {
								order.setOrderSettlement(NormalOrderStatus.REFUNDED.name());
							} else {
								if (orderPL.compareTo(BigDecimal.ZERO) > 0) {
									order.setOrderSettlement(NormalOrderStatus.WIN.name());
								} else if (orderPL.compareTo(BigDecimal.ZERO) == 0) {
									order.setOrderSettlement(NormalOrderStatus.DRAW.name());
								} else {
									order.setOrderSettlement(NormalOrderStatus.LOSS.name());
								}
							}

//						BettingModelExtraVodds extraVModel = dbService.getModelExtraEntity(order.getOrder().getId());
//						UserOutstandingChangeEvent amt = new UserOutstandingChangeEvent(bUser.getUser().getUsername(), extraVModel.getConfirmedStake().negate().doubleValue(), bUser.getUser().getId());
//						amtNotificationTask.submitOustanding(amt);
							String message = "Order Settled";

							tBuilder.populateSettleOrderTransactions(order, bUser, priorCredit, postCredit, orderPL, orderStake, message);
						}
					}
					// handle the case when the PL of underlying bets in an
					// order are modified and not settled
					else if (order.getOrderSettlement().equals(NormalOrderStatus.MODIFIED.name())) {
						BigDecimal totalStake = new BigDecimal(0);
						for (BetWrapper oBet : oBets) {
							Bet dBet = oBet.getBet();
						if (dBet.getReturnAmount() != null){
							// handle the case bet is missing
							if (dBet.IsMissingBet() == 1) {
								plMissingBet = plMissingBet
										.add(dBet.getReturnAmount().multiply(dBet.getMultiplication())
												.multiply(dBet.getBaseConversionRate()));
							} else {
								if(!rejectedStatuses.contains(dBet.getStatus().name())){
									logger.debug("bet id {} has return amount {} and stake {}", dBet.getId(),
											dBet.getReturnAmount().doubleValue(), dBet.getBetStake().doubleValue());

									orderPL = orderPL
											.add(dBet.getReturnAmount().multiply(dBet.getMultiplication()));

									orderStake = orderStake
											.add(dBet.getBetStake().multiply(dBet.getMultiplication()));

								}
							}
						}
						}
						
						//caculate ptpl , get ptpl from cache and cache ptpl after update plpl for agent
						settlePTPLNO(lstCompanyAgentPtpl, oBets, order,true);
						
						List<ModelTransaction> marginTransactions = dbService.findMarginTransactions(Arrays.asList(order.getOrder().getId()),bUser.getUser().getId());
						if(marginTransactions.isEmpty()) {
							isSkip = true;
						} else {
							for (ModelTransaction transaction : marginTransactions) {
								totalStake = totalStake.add(transaction.getCreditAmount());
							}

							BigDecimal refundAmtRejectedBet = totalStake.negate().subtract(orderStake);

							List<ModelTransaction> latestReturnTransToOperator = dbService
									.findLatestReturnTransactionForOperator(order.getOrder().getId(),
											companyAgent.getUser().getId());
							if (!latestReturnTransToOperator.isEmpty()) {

								BigDecimal revAmountOperator = latestReturnTransToOperator.get(0).getCreditAmount();

								//priorCredit = companyAgent.getCredit();
								BigDecimal[] credits = companyAgent.addCredit(revAmountOperator.negate());
								priorCredit = credits[0];
								postCredit = credits[1];
								// revert credit
								tBuilder.populateSettleModifiedOrderTransactionsOperator(order, companyAgent,
										revAmountOperator, priorCredit, postCredit);
							}

							// settle for case missing bet to operator
							settleMissingBet(order, plMissingBet, companyAgent,orderType);

							// check reason status's bet change make order isModifiy
							ModelTransaction latestReturnTrans = dbService
									.findLatestReturnTransaction(order.getOrder().getId(), bUser.getUser().getId());
							if (latestReturnTrans != null) {
								BigDecimal revAmount = latestReturnTrans.getCreditAmount();

								BigDecimal settledAmt = orderPL.add(orderStake);
								BigDecimal newRevAmount = settledAmt.add(refundAmtRejectedBet);

								logger.info("OrderId: {},total stake base on transaction {} , total stake base on bet {},settlement amnt {} , return amount {} , new return amount {}"
										,order.getOrder().getId(),totalStake,orderStake,settledAmt,revAmount,newRevAmount);


								//BigDecimal sum = revAmount.add(newRevAmount);

								if (revAmount.compareTo(newRevAmount) != 0 || checkResettle(order)) {

									logger.info("Modified with order id {}", order.getOrder().getId());

									// revert the previous credit amount and add the
									// new return amount

									// bUser.addCredit(revAmount.negate().add(orderPL.add(orderStake)));
									BigDecimal[] revAmtCredit = bUser.addCredit(revAmount.negate(), order.getOrder().getId());
									priorCredit = revAmtCredit[0];
									logger.info("orderPL {}, orderStake {}, revAmout {}, creditRev {} , orderId {}", orderPL, orderStake,
											revAmount, bUser.getCredit(), order.getOrder().getId());

									postCredit = revAmtCredit[1];

									logger.info("revert order creditBefore {}, returnAmount {}, creditAfter {}, orderId {}",
											priorCredit, revAmount.negate(), postCredit, order.getOrder().getId());

									//BigDecimal settledAmt = orderPL.add(orderStake);

									order.setReturnAmount(newRevAmount);

									if (isRejected) {
										order.setOrderSettlement(NormalOrderStatus.REFUNDED.name());
									} else {
										if (orderPL.compareTo(BigDecimal.ZERO) > 0) {
											order.setOrderSettlement(NormalOrderStatus.WIN.name());
										} else if (orderPL.compareTo(BigDecimal.ZERO) == 0) {
											order.setOrderSettlement(NormalOrderStatus.DRAW.name());
										} else {
											order.setOrderSettlement(NormalOrderStatus.LOSS.name());
										}
									}
									// revert credit
									tBuilder.populateSettleModifiedOrderTransactions(order, bUser, revAmount, priorCredit,
											postCredit);


									BigDecimal[] revertedCreidt = bUser.addCredit(order.getReturnAmount(), order.getOrder().getId());
									BigDecimal creditBefore = revertedCreidt[0];
									BigDecimal creditAfter = revertedCreidt[1];

									logger.info("re-settled order creditBefore {}, returnAmount {}, creditAfter {}, orderId {}",
											creditBefore, order.getReturnAmount(), creditAfter, order.getOrder().getId());

									// TODO: populate transactions for re-settle
									// order

									String message = "Order Re-Settled";

									tBuilder.populateSettleOrderTransactions(order, bUser, creditBefore, creditAfter,
											orderPL, orderStake, message);
								}
							}
						}
					}

					if(!isSkip) {
						logger.info("resetting is_modified orderId: {}",order.getOrder().getId());
						order.getOrder().setIsModified((byte) 0);

						persistenceModerator.saveOrder(order.getOrder());
					}else {
						order.setIsSettling(false);
						logger.debug("OrderId {} - IsSettling {}",order.getOrder().getId(),order.getIsSettling());
					}
				}
				// handle the positional order settlement
				else if (orderType.equals("VOClose") || orderType.equals(VOddsModelType.POSITIONAL_CLOSE.name())
						|| orderType.equals("VOFutureClose")
						|| orderType.equals(VOddsModelType.FUTURE_POSITIONAL_CLOSE.name())) {

					BigDecimal orderPL = new BigDecimal(0);
					BigDecimal openPL = new BigDecimal(0);
					BigDecimal closePL = new BigDecimal(0);
					BigDecimal openStake = new BigDecimal(0);
					BigDecimal closeStake = new BigDecimal(0);
					BigDecimal orderMargin = new BigDecimal(0);
					// BigDecimal reserveStake = new BigDecimal(0);
					BigDecimal orderPLCompany = new BigDecimal(0);
					BigDecimal plMissingBet = new BigDecimal(0);
					BigDecimal stopLoss = new BigDecimal(0);

					
					BetUserNode betUser = uManager.findBetUserNode(order.getOrder().getOwner().getUser().getId());
					
					List<AgentUserNode> lstCompanyAgentPtpl = betUser.getAgentUsers().stream()
							.filter(aNode -> aNode.getUser().getAgentType().getLevel() != 0)
							.collect(Collectors.toList());
					
					// Set<BetWrapper> oBets =
					// oManager.findOrderBets(order.getOrder().getId());

					// fetch the bets related to the positional order, the
					// closing order id will contain both opening and closing
					// order bets
					// type cast into positional wrapper when the settled order
					// is close
					PositionalOrderWrapper pOrder = (PositionalOrderWrapper) order;

					Set<BetWrapper> oBets = order.getBetWrappers();
					
					PositionalModel pModel = pOrder.getpModel();

					// odd buffer for guranteed stop orders
					BigDecimal bufferOdd = pModel.getBufferOdd();
					if(pModel.getIsGstop() == 1){
						if(pModel.getClosingConditionType() == 1){
							BettingModelExtraVodds openVModel = dbService.getModelExtraEntity(pModel.getOpenModelId().getId());
							BigDecimal confirmStake = openVModel.getConfirmedStake();
							BigDecimal confirmOdds = openVModel.getConfirmedAvgOdd();
						
							BigDecimal value1 = confirmStake.multiply(confirmOdds);
							BigDecimal value2 = (confirmStake.multiply(new BigDecimal(1).add(confirmOdds))).divide(new BigDecimal(1).add(pModel.getStopLoss()),new MathContext(20, RoundingMode.CEILING));
							stopLoss = value1.subtract(value2);
						}else{
						// stop loss amount of the positional order
							stopLoss = pModel.getStopLoss();
						}
						pOrder.getpModel().setStopLoss(stopLoss);
					}
					// refund amount for the user when the effective pl falls
					// below stop loss value
					BigDecimal gRefund = new BigDecimal(0);
					
					
					//calulate mpl for case VODING by OE 
					BigDecimal mpl = BigDecimal.ZERO;
					Boolean isVoidingByOE = false;
					if (pOrder.getpModel().getExecutionStage().equals(PositionalOrderStage.VOIDING.name())) {
						for (BetWrapper oBet : oBets) {
							Bet dBet = oBet.getBet();
							logger.debug("OrderId {} of betId {} - openOrderId {}",dBet.getModel().getId(),dBet.getId(),pOrder.getoModel().getId());
							if(dBet.getModel().getId() == pOrder.getoModel().getId()) {
								if (dBet.IsMissingBet() != 1) {
									mpl = mpl.add(dBet.getSubmittedStake());
									isVoidingByOE = true;
									logger.debug("MPL {} after add stake {} of bet id {} orderId {}",mpl,dBet.getSubmittedStake(),dBet.getId(),pOrder.getoModel().getId());
								}
							}else {
								isVoidingByOE = false;
								mpl = BigDecimal.ZERO;
								logger.debug("MPL {} when voiding by plm",mpl);
								break;
							}
						}
						
						logger.debug("MPL for VOIDING order {} - with case isVoidingByOE {}",mpl,isVoidingByOE);
					}
					
					
					
					

					for (BetWrapper oBet : oBets) {
						Bet dBet = oBet.getBet();
						if (dBet.getReturnAmount() != null){
							logger.debug("bet id {} has return amount {} and stake {} is missing {}", dBet.getId(),
									dBet.getReturnAmount().doubleValue(), dBet.getBetStake().doubleValue(), dBet.IsMissingBet());
							if (dBet.IsMissingBet() == 0) {
								orderPLCompany = orderPLCompany.add(genUtil.getReturnAmountCompany(dBet));

								BigDecimal returnAmount = dBet.getReturnAmount().multiply(dBet.getMultiplication());

								// calculate return amount for voiding order
								BigDecimal serverOdd = dBet.getServerOdd();
								if (bufferOdd != null && stopLoss != null && pModel.getIsGstop() == 1) {
									// buffered odd is only applicable to closing
									// order related bets
									if (dBet.getModel().getId() == order.getOrder().getId()) {
										// use the buffer odd only when the return
										// amount is positive
										if (returnAmount.compareTo(new BigDecimal(0)) > 0) {
											// BigDecimal effectiveReturn =
											// returnAmount.subtract(returnAmount.abs().multiply(bufferOdd).setScale(5,
											// RoundingMode.CEILING));
											// BigDecimal effectiveReturn =
											// closeStake.multiply(serverOdd.subtract(bufferOdd));

											BigDecimal effectiveReturn = returnAmount
													.divide(serverOdd, 10, RoundingMode.CEILING)
													.multiply(serverOdd.subtract(bufferOdd));

											orderPL = orderPL.add(effectiveReturn);
											logger.debug(
													"new return amount {} calculated from return amount  {} with server odd {} and buffered difference {}",
													effectiveReturn.doubleValue(), returnAmount.doubleValue(),
													serverOdd.doubleValue(), bufferOdd.doubleValue());

										}
										// when the result of close order is
										// draw/loss, use the original amount
										else {
											orderPL = orderPL.add(returnAmount);
										}
										closePL = closePL.add(returnAmount);

										//calculate stake for close order
										closeStake = calculateStakeForPO(dBet,closeStake);
									}
									// open order bets still use the same odd
									else {
										orderPL = orderPL.add(returnAmount);
										openPL = openPL.add(returnAmount);

										//calculate stake for open order
										openStake = calculateStakeForPO(dBet,openStake);
									}

								} else {
									orderPL = orderPL.add(returnAmount);
									if (dBet.getModel().getId() == order.getOrder().getId()) {
										closePL = closePL.add(returnAmount);

										//calculate stake for close order
										closeStake = calculateStakeForPO(dBet,closeStake);
									} else {
										openPL = openPL.add(returnAmount);

										//calculate stake for open order
										openStake = calculateStakeForPO(dBet,openStake);

									}
								}
							} else {
								BigDecimal returnAmountMisingBet = dBet.getReturnAmount()
										.multiply(dBet.getMultiplication())
										.multiply(dBet.getBaseConversionRate());
								plMissingBet = plMissingBet.add(returnAmountMisingBet);
							}
						}
					}

					// when the effective pl of the user is less than the stop
					// loss of a guranteed stop order
					// the difference has to be stored to refund back the user
					if (stopLoss != null && pModel.getIsGstop() == 1 && orderPL.compareTo(stopLoss) < 0) {
						gRefund = stopLoss.subtract(orderPL);
						logger.debug(
								"refunded credit amount {} since the effective pl of the order {} is less than the stop loss amount {}",
								gRefund.doubleValue(), orderPL.doubleValue(), stopLoss.doubleValue());

					}

					// calculate the total amount of margin reserved at the end
					// of fulfilling closing order

					BettingModel openModel = pOrder.getoModel();



					// BettingModel openModel =
					// dbService.findOpeningOrder(order.getOrder().getId());
					logger.debug("opening model id {} found for closing model {}", openModel.getId(),
							order.getOrder().getId());

					List<ModelTransaction> marginTransactions = dbService.findMarginTransactions(Arrays.asList(openModel.getId()),betUser.getUser().getId());
					logger.debug("margin transactions of size {} fetched for open order", marginTransactions.size());
					// difference between margin and max potential loss is
					// returned after the closing order is stopped, hence taken
					// into account
					List<ModelTransaction> cMarginTransactions = dbService
							.findMarginTransactions(Arrays.asList(order.getOrder().getId()),betUser.getUser().getId());
					logger.debug("margin transactions of size {} fetched for close order", cMarginTransactions.size());

					marginTransactions.addAll(cMarginTransactions);

					for (ModelTransaction transaction : marginTransactions) {
						orderMargin = orderMargin.add(transaction.getCreditAmount());
					}

					// orderMargin = pModel.getTotalMargin();

					logger.debug("margin {} and pl {} for order {}", orderMargin.doubleValue(), orderPL.doubleValue(),
							order.getOrder().getId());
					// add the values to the user wrapper
					BetUserNode bUser = uManager.findBetUserNode(order.getOrder().getOwner().getUser().getId());
					AgentUserNode aUser = bUser.getAgentUsers().stream()
							.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1)
							.collect(Collectors.toList()).get(0);

					BigDecimal settledCreditUser = (orderPL.add(orderMargin.negate()));

					if (order.getOrderSettlement().equals(PositionalCloseStatus.MODIFIED.name())
							|| openModel.IsModified() == 1) {


						// revert all credit amount for Vodds company
						List<ModelTransaction> lst = dbService.findLatestReturnTransactionForOperator(
								order.getOrder().getId(), aUser.getUser().getId());
						BigDecimal revAmountOperator = new BigDecimal(0);
						if (!lst.isEmpty()) {
							for (ModelTransaction modelTransaction : lst) {
								revAmountOperator = revAmountOperator.add(modelTransaction.getCreditAmount());
							}


							BigDecimal[] revAmnt = aUser.addCredit(revAmountOperator.negate());
							BigDecimal priorCreditOperator = revAmnt[0];
							BigDecimal postCreditOperator = revAmnt[1];
							logger.info(
									"last revert amount of operator {} , {} for the order is found , credit before {} , credit after {}",
									aUser.getUser().getId(), revAmountOperator.doubleValue(), priorCreditOperator,
									postCreditOperator);
							tBuilder.populateSettleModifiedOrderTransactionsForOperator(order, aUser, revAmountOperator,
									priorCreditOperator, postCreditOperator);

						}


						// settle voiding order to operator
						if (pOrder.getpModel().getExecutionStage().equals(PositionalOrderStage.VOIDING.name())
								|| pOrder.getpModel().getExecutionStage().equals(PositionalOrderStage.VOIDED.name())) {

							// settle for case missing bet to operator
							settleMissingBet(order, plMissingBet, aUser, orderType);

							settleVoidingOrder(order, orderPLCompany, aUser, bUser, orderMargin, openPL,closePL,pOrder.getpModel().getExecutionStage(),true,mpl,isVoidingByOE);
						} else {

							//caculate ptpl , get ptpl from cache and cache ptpl after update plpl for agent
							settlePTPLPO(lstCompanyAgentPtpl, oBets, order,openModel,true);

							// settle for case missing bet to operator
							settleMissingBet(order, plMissingBet, aUser,orderType);

							ModelTransaction latestReturnTrans = dbService
									.findLatestReturnTransaction(order.getOrder().getId(), bUser.getUser().getId());
							if(latestReturnTrans != null){
								BigDecimal revAmount = latestReturnTrans.getCreditAmount();

								//BigDecimal sum = revAmount.add(settledCreditUser);
								// check settled credit is equal return amount , If
								// it is equal
								if (revAmount.compareTo(settledCreditUser) != 0) {
									logger.info("last return amount {} for the order is found", revAmount.doubleValue());

									// revert credit for user
									BigDecimal priorRevertCredit = bUser.getCredit();
									bUser.addCredit(revAmount.negate(), order.getOrder().getId());
									BigDecimal postRevertCredit = bUser.getCredit();
									tBuilder.populateSettleModifiedOrderTransactions(order, bUser, revAmount,
											priorRevertCredit, postRevertCredit);

									// re-settle order to user
									priorCredit = bUser.getCredit();
									bUser.addCredit(orderPL.add(orderMargin.negate()), order.getOrder().getId());
									postCredit = bUser.getCredit();

									BigDecimal priorRefundCredit = bUser.getCredit();
									bUser.addCredit(gRefund, order.getOrder().getId());
									BigDecimal postRefundCredit = bUser.getCredit();

									order.setReturnAmount(orderPL.add(orderMargin.negate()));

									// settle GS to operator
									settleOperatorWithGs(pOrder, orderPL);

									String message = "Positional Order Re-Settled";

									tBuilder.populateSettleOrderTransactions(order, bUser, priorCredit, postCredit,
											priorRefundCredit, postRefundCredit, gRefund, stopLoss, openPL, closePL,
											orderPL, false,openStake,closeStake,message);


								}

							}

						}

					} else if (order.getOrderSettlement().equals(PositionalCloseStatus.STOPPED.name())
							|| order.getOrderSettlement().equals(PositionalCloseStatus.CONFIRMED.name())
							|| order.getOrderSettlement().equals(PositionalCloseStatus.PARTIAL.name())
							|| order.getOrderSettlement().equals(PositionalCloseStatus.REJECTED.name())
							|| order.getOrderSettlement().equals(PositionalCloseStatus.UNFULFILLED.name())) {


						// settle voiding order to operator
						if (pOrder.getpModel().getExecutionStage().equals(PositionalOrderStage.VOIDING.name())
								|| pOrder.getpModel().getExecutionStage().equals(PositionalOrderStage.VOIDED.name())) {

							// settle for case missing bet to operator
							settleMissingBet(order, plMissingBet, aUser, orderType);

							settleVoidingOrder(order, orderPLCompany, aUser, bUser, orderMargin, openPL,closePL,pOrder.getpModel().getExecutionStage(),false,mpl,isVoidingByOE);
						} else {

							//caculate ptpl , get ptpl from cache and cache ptpl after update plpl for agent
							settlePTPLPO(lstCompanyAgentPtpl, oBets, order,openModel,false);

							// settle for case missing bet to operator
							settleMissingBet(order, plMissingBet, aUser,orderType);

							// settle order to user
							priorCredit = bUser.getCredit();
							bUser.addCredit(orderPL.add(orderMargin.negate()), order.getOrder().getId());
							postCredit = bUser.getCredit();

							BigDecimal priorRefundCredit = bUser.getCredit();
							bUser.addCredit(gRefund, order.getOrder().getId());
							BigDecimal postRefundCredit = bUser.getCredit();

							order.setReturnAmount(orderPL.add(orderMargin.negate()));

							// settle GS to operator
							settleOperatorWithGs(pOrder, orderPL);

							String message = "Positional Order Settled";

							tBuilder.populateSettleOrderTransactions(order, bUser, priorCredit, postCredit,
									priorRefundCredit, postRefundCredit, gRefund, stopLoss, openPL, closePL, orderPL, true,openStake,closeStake,message);


						}

					}

					order.getOrder().setIsModified((byte)0);
					pOrder.getoModel().setIsModified((byte)0);
					persistenceModerator.saveOrder(order.getOrder());
					persistenceModerator.saveOrder(pOrder.getoModel());
				}


			} catch (Exception e) {
				logger.error("error while settling the orders ", e);
				continue;
			} finally {
				// remove order from the queue
				order = this.getOrderQueue().poll();
				logger.debug("Size of orderQueue {} after remove orderId {}",this.getOrderQueue().size(),order.getOrder().getId());
			}

		}

	}

	private void settleMissingBet(OrderWrapper oWrapper, BigDecimal plMissingBet, AgentUserNode aUser,String orderType)
			throws Exception {
		// there is missing bet pl => persist transaction
		if (plMissingBet.compareTo(BigDecimal.ZERO) != 0) {
			BigDecimal[] creditOperators = aUser.addCredit(plMissingBet);
			BigDecimal preCreditOperator = creditOperators[0];
			BigDecimal postCreditOperator = creditOperators[1];
			logger.info("return amount missing bet of operator {} , {} for the order is found , credit before {} , credit after {}",
					aUser.getUser().getId(), plMissingBet.doubleValue(), preCreditOperator, postCreditOperator);
			tBuilder.populateSettleMissingBetTransactions(oWrapper, aUser, plMissingBet, preCreditOperator,
					postCreditOperator, orderType);
		}
	}

	private void settleVoidingOrder(OrderWrapper oWrapper, BigDecimal retAmountOperator, AgentUserNode aUser,
			BetUserNode bUser, BigDecimal refundMargin, BigDecimal openPL,BigDecimal closePL,String status,Boolean isModified,BigDecimal mpl,Boolean isVoidingByOE) throws Exception {
		BigDecimal priorCredit = new BigDecimal(0);
		BigDecimal postCredit = new BigDecimal(0);

		PositionalOrderWrapper pOrder = (PositionalOrderWrapper) oWrapper;

		if(oWrapper.getOrderSettlement().equals(PositionalCloseStatus.UNFULFILLED.name()) && (pOrder.getoModel().getModelStatus().equals(PositionalOpenStatus.WIN.name())
				|| pOrder.getoModel().getModelStatus().equals(PositionalOpenStatus.LOSS.name()) || pOrder.getoModel().getModelStatus().equals(PositionalOpenStatus.DRAW.name()))){
			logger.info("status of open order {} , status of close order {}",pOrder.getoModel().getModelStatus(),oWrapper.getOrderSettlement());
		}else{

			if(status.equals(PositionalOrderStage.VOIDING.name())){

				if(isModified){


					ModelTransaction lastestRefundOfVoidOrder = dbService.findLastestRefundTransaction(oWrapper.getOrder().getId(), bUser.getUser().getId());

					if(lastestRefundOfVoidOrder == null){

						ModelTransaction latestReturnTrans = dbService
								.findLatestReturnTransaction(oWrapper.getOrder().getId(), bUser.getUser().getId());

						if(latestReturnTrans != null){
							BigDecimal revAmount = latestReturnTrans.getCreditAmount();

							logger.info("last return amount {} for the order is found", revAmount.doubleValue());

							// revert credit for user
							BigDecimal priorRevertCredit = bUser.getCredit();
							bUser.addCredit(revAmount.negate(), oWrapper.getOrder().getId());
							BigDecimal postRevertCredit = bUser.getCredit();
							tBuilder.populateSettleModifiedOrderTransactions(oWrapper, bUser, revAmount,
									priorRevertCredit, postRevertCredit);

							//refund margin for user
							priorCredit = bUser.getCredit();
							logger.info("credit of {} before {}", bUser.getUser().getId(), priorCredit);
							bUser.addCredit(refundMargin.negate(), oWrapper.getOrder().getId());
							postCredit = bUser.getCredit();
							logger.info("credit of {} after {} with refund margin {}", bUser.getUser().getId(), postCredit,
									refundMargin);

						}

					}
				}else{

					//refund margin for user
					priorCredit = bUser.getCredit();
					logger.info("credit of {} before {}", bUser.getUser().getId(), priorCredit);
					bUser.addCredit(refundMargin.negate(), oWrapper.getOrder().getId());
					postCredit = bUser.getCredit();
					logger.info("credit of {} after {} with refund margin {}", bUser.getUser().getId(), postCredit,
							refundMargin);

				}
			}

			//BigDecimal preCreditOperator = aUser.getCredit();

			BigDecimal[] creditOperators = aUser.addCredit(retAmountOperator);
			BigDecimal preCreditOperator = creditOperators[0];
			BigDecimal postCreditOperator = creditOperators[1];
			logger.info(
					"return amount voiding order of operator {} , {} for the order is found , credit before {} , credit after {}",
					aUser.getUser().getId(), retAmountOperator.doubleValue(), preCreditOperator, postCreditOperator);
			tBuilder.populateSettleVoidingOrderTransactions(oWrapper, bUser, refundMargin, priorCredit, postCredit, aUser,
					retAmountOperator, preCreditOperator, postCreditOperator, openPL,closePL,status,isModified,mpl,isVoidingByOE);
			// tBuilder.populateSettleVoidingOrderTransactions(oWrapper, aUser,
			// retAmountOperator, preCreditOperator, postCreditOperator);
		}

	}

	private void settleOperatorWithGs(PositionalOrderWrapper poWrapper, BigDecimal poPl) throws Exception {
		// There is no guarantee stop, exit method
		if (poWrapper.getpModel().getIsGstop() == 0)
			return;

		poWrapper.getpModel().setOperatorGain(BigDecimal.ZERO);
		poWrapper.getpModel().setOperatorLoss(BigDecimal.ZERO);

		BettingModel closeBm = poWrapper.getpModel().getCloseModelId();

		// Settlement
		// Calculate PL of bets in CLOSE order as well as operator gain
		BigDecimal closePl = new BigDecimal(0);
		BigDecimal opGain = new BigDecimal(0);
		Set<BetWrapper> bws = poWrapper.getBetWrappers();
		BigDecimal bufferOdd = poWrapper.getpModel().getBufferOdd();
		BigDecimal baseConvRate = BigDecimal.ONE;
		for (BetWrapper bw : bws) {
			Bet bet = bw.getBet();
			if (bet.getReturnAmount() != null){
				if (bet.getModel().getId() == closeBm.getId() && bet.IsMissingBet() == 0) {
					BigDecimal userPl = bet.getReturnAmount().multiply(bet.getMultiplication());
					closePl = closePl.add(userPl);
					// convert to base conversion rate
					baseConvRate = bet.getBaseConversionRate();
					opGain = opGain.add((userPl.multiply(baseConvRate).multiply(bufferOdd).divide(bet.getServerOdd(),
							new MathContext(20, RoundingMode.CEILING))));
				}
			}
		}

		logger.info("Close PL {} of Close order {}", closePl, closeBm.getId());
		// BigDecimal closePl = new BigDecimal(dClosePl);

		if (closePl.compareTo(BigDecimal.ZERO) > 0) {// Bets in CLOSE order are
														// win, operator will
														// gain a part of
														// closePl

			BetUserNode bUser = uManager.findBetUserNode(closeBm.getOwner().getUser().getId());
			// retrieve the company agent of the bet user
			AgentUserNode companyAgent = bUser.getAgentUsers().stream()
					.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1).collect(Collectors.toList())
					.get(0);
			// BigDecimal opGain = new BigDecimal(dOpGain);

			// Add Operator gain to company's credit
			 BigDecimal[]creditOpGains = companyAgent.addCredit(opGain);

			 BigDecimal preCredit = creditOpGains[0];
			 BigDecimal postCredit = creditOpGains[1];

			logger.info("Adding {} to company agent {} with current credit {}", opGain,
					companyAgent.getUser().getUsername(), preCredit);

			// Set operator gain to p.o
			poWrapper.getpModel().setOperatorGain(opGain);


			tBuilder.populateGsTransactions(companyAgent, opGain, preCredit, postCredit,
					ModelTransactionType.RETURN, poWrapper);
		}

		// If poPl is loss and the loss exceeds stop_loss, Operator bears the
		// exceed amount
		if (poPl.compareTo(BigDecimal.ZERO) < 0) {
			BigDecimal opLoss = poPl.subtract(poWrapper.getpModel().getStopLoss());
			if (opLoss.compareTo(BigDecimal.ZERO) < 0) {
				// Convert opLoss to base conversion rate
				opLoss = opLoss.multiply(baseConvRate);
				BetUserNode bUser = uManager.findBetUserNode(closeBm.getOwner().getUser().getId());
				// retrieve the company agent of the bet user
				AgentUserNode companyAgent = bUser.getAgentUsers().stream()
						.filter(aNode -> aNode.getUser().getAgentType().getLevel() == 1).collect(Collectors.toList())
						.get(0);
				BigDecimal preCredit = companyAgent.getCredit();
				logger.info("Subtracting {} to company agent {} with current credit {}", opLoss,
						companyAgent.getUser().getUsername(), preCredit);

				companyAgent.addCredit(opLoss);


				// Set operator gain to p.o
				poWrapper.getpModel().setOperatorLoss(opLoss);

				tBuilder.populateGsTransactions(companyAgent, opLoss, preCredit, companyAgent.getCredit(),
						ModelTransactionType.RETURN, poWrapper);
			}
		}

	}

	private Map<String, Map<Long, BigDecimal>> getPtPercents(List<AgentUserNode> agentUserNodes, String ptPercentJson){
		Map<String, Map<Long, BigDecimal>> map = new HashMap<>();
		if (ptPercentJson == null || ptPercentJson.isEmpty()){
			return map;
		}
		try{

			ObjectMapper mapper = new ObjectMapper();
			// parse {"sbc":"[30, 30, 30]","xtx":null} -> map of sport book, string
			Map<String, String> sbPercents = mapper.readValue(ptPercentJson, Map.class);

			for (String sb : sbPercents.keySet()) {
				Map<Long, BigDecimal> agentPercents = new HashMap<>();
				// parse "[30, 30, 30]" to array list
				List<Integer> percents = Arrays.asList(0, 0, 0);
				if(sbPercents.get(sb) != null && !sbPercents.get(sb).equals("") && !sbPercents.get(sb).equals("null")) {
					percents = mapper.readValue(sbPercents.get(sb), ArrayList.class);
				}

				/**
				 * Get the pt percent for certain sportbook
				 *
				 * [0,10,20]
				 * index 0: company
				 * index 1: master
				 * index 2: agent
				 */
				for (AgentUserNode agentUserNode : agentUserNodes) {
					agentPercents.put(agentUserNode.getUser().getId(), BigDecimal.valueOf(percents.get(agentUserNode.getUser().getAgentType().getLevel() - 1)));
				}
				map.put(sb, agentPercents);
			}

			return map;
		}catch (Exception e) {
			logger.error("Fail to get ptpl for {} ", ptPercentJson, e);
			return map;
		}
	}

	private boolean revertTransactionPTPL(OrderWrapper order,AgentUserNode aUser,BigDecimal newPtpl){
		try{
			ModelTransaction latestReturnTransPTPL = dbService
					.findLatestReturnPTPLTransaction(order.getOrder().getId(), aUser.getUser().getId());
			if(latestReturnTransPTPL != null){
				BigDecimal retAmountPtplTrans = latestReturnTransPTPL.getCreditAmount();
				logger.info("OrderId: {} with last ptpl amount on transaction {} , new ptpl {} , agent user {}",
						order.getOrder().getId(),retAmountPtplTrans,newPtpl,aUser.getUser().getUsername());
				//if (retAmountPtplTrans.compareTo(newPtpl) != 0) {
					BigDecimal[] creditPtpls = aUser.addCreditPtpl(retAmountPtplTrans.negate());
					BigDecimal ptplBefore = creditPtpls[0];
					BigDecimal ptplAfter = creditPtpls[1];
					PTPLSettlementInfo ptplInfo = new PTPLSettlementInfo(aUser.getUser(), ptplBefore, ptplAfter, retAmountPtplTrans, order.getOrder().getId());
					tBuilder.populateSettleModifiedPtplTransactionsForAgent(ptplInfo);
					return true;

//				}else{
//					return false;
//				}
			}else{
				return false;
			}
			
		}catch(Exception ex){
			logger.error("Fail to revert ptpl for order {}, agent {} ", order.getOrder().getId(),aUser.getUser().getUsername(), ex);
			return false;
		}
	}

	private boolean checkResettle(OrderWrapper order){
		if (order.getOrder().getModelStatus().equals(NormalOrderStatus.CONFIRMED.name()) && order.getOrder().IsModified() == 1 ||
				order.getOrder().getModelStatus().equals(NormalOrderStatus.PARTIAL.name()) && order.getOrder().IsModified() == 1){
			logger.info("resettle case with order CONFIRMED/PARTIAL and ismodified with orderId ",order.getOrder().getId());
			return true;
		}
		return false;
	}

	private void settlePTPLNO (List<AgentUserNode>lstCompanyAgentPtpl, Set<BetWrapper> oBets, OrderWrapper order,boolean isModified) {

		try {
			Map<Long, PTPLSettlementInfo> ptplSettlementInfos = new HashMap<>();

			// Get json string of the pt percent from betting model extra
			BettingModelExtraVodds extraVModel = dbService.getModelExtraEntity(order.getOrder().getId());
			Map<String, Map<Long, BigDecimal>> ptPercentMap = getPtPercents(lstCompanyAgentPtpl, extraVModel.getPtPercent());
			List<AgentPtPlInfo> ptPlInfos = new ArrayList<>();
			List<BetAgentPtPlInfo> betPtPlInfos = new ArrayList<>();

			// loop through all the agents (company, master, agent)
			for (AgentUserNode agentUserNode:lstCompanyAgentPtpl) {

				// check if agent has pt taking
				if(!hasPtTaking(oBets, agentUserNode, ptPercentMap, order.getOrder().getId())) {
					logger.info("aId {} oId {} has no pt taking", agentUserNode.getUser().getId(), order.getOrder().getId());
					continue;
				}

				// initialize the ptpl to 0, sum of ptpl for all the bets
				BigDecimal ptplChangeTotal = BigDecimal.ZERO; // ptpl in user currency

				
				for (BetWrapper oBet:oBets) {
					Bet dBet = oBet.getBet();
					if (dBet.getReturnAmount() != null){
						// only calculate ptpl when bet is not missing bet
						if (dBet.IsMissingBet() != 0)
							continue;

						BigDecimal ptPercents = getPtPercents(ptPercentMap, agentUserNode, dBet);

						BigDecimal betPL = dBet.getReturnAmount().multiply(dBet.getMultiplication()).multiply(BigDecimal.valueOf(-1));

						// bet pl must be in agent currency, if the agent is company agent, it should be converted to base currency
						if (agentUserNode.getUser().getAgentType().getLevel() == 1) {
							betPL = betPL.multiply(dBet.getBaseConversionRate());
						}

						// betPL * percent of  (company/master/agent)
						BigDecimal ptpl = betPL.multiply(ptPercents).divide(BigDecimal.valueOf(100));

						logger.debug("order id {} agent id {} with ptpl percentage {} calculated ptpl {} from bet id {} with pl {} for sportbook {} ", order.getOrder().getId(),
								agentUserNode.getUser().getId(), ptPercents, ptpl, dBet.getId(), betPL, dBet.getSportbook().getName().toLowerCase());

						// sum ptpl for bets
						ptplChangeTotal = ptplChangeTotal.add(ptpl);

						//bet ptpl
						if (ptpl.compareTo(BigDecimal.ZERO) != 0) {
							betPtPlInfos.add(new BetAgentPtPlInfo(agentUserNode, extraVModel, ptpl, dBet.getId()));
						}
					}
				}

				if(isModified){
					//revert all transaction ptpl before
					boolean isResettled = revertTransactionPTPL(order, agentUserNode, ptplChangeTotal);
					if(isResettled){
						createPtPlSettlementInfo(order, ptplSettlementInfos, agentUserNode, ptplChangeTotal);
					}
				}else{
					createPtPlSettlementInfo(order, ptplSettlementInfos, agentUserNode, ptplChangeTotal);
				}

				// store agent pt pl info
				ptPlInfos.add(new AgentPtPlInfo(agentUserNode, extraVModel, ptplChangeTotal));

			}

			// persist agent pt pl
			Map<Long, List<AgentPtPlInfo>> ptPlInfoMap = new HashMap<>();
			ptPlInfoMap.put(order.getOrder().getId(), ptPlInfos);
			persistAgentPtpl(ptPlInfoMap);

			// persist bet agent pt pl
			persistBetOfAgentPtPl(betPtPlInfos);

			// Populate the settlement only where size is > 0
			if (ptplSettlementInfos.size() > 0)
				tBuilder.populateSettlePTPLTransaction(ptplSettlementInfos);
		} catch (Exception e) {
			logger.error("Fail to calculate ptpl for order id {} ", order.getOrder().getId(), e);
		}
	}

	private BigDecimal getPtPercents(Map<String, Map<Long, BigDecimal>> ptPercentMap, AgentUserNode agentUserNode, Bet dBet) {
		try {
			BigDecimal ptPercent = ptPercentMap.get(dBet.getSportbook().getName().toLowerCase()).get(agentUserNode.getUser().getId());
			return ptPercent;
		} catch (Exception ex) {
			logger.info("get pt percent exception for sportbook {} agent id {} order id {}", dBet.getSportbook().getName(), agentUserNode.getUser().getId(),
					dBet.getModel().getId());
		}
		return BigDecimal.ZERO;
	}

	/**
	 * This method aims to check if agent has pt taking. Pt taking is defined that an agent has at least a bet has non-zero pt.
	 * @param oBets
	 * @return true if agent has at least one order bet with non-zero pt
	 */
	private boolean hasPtTaking(Set<BetWrapper> oBets, AgentUserNode agentUserNode, Map<String, Map<Long, BigDecimal>> ptPercentMap, Long modelId) {
		for (BetWrapper dBet : oBets) {
			// skip checking a bet if it's belonged to different model
			// missing bet is counted for testing purpose
			if(dBet.getBet().getModel().getId() != modelId) {
				continue;
			}

			if(getPtPercents(ptPercentMap, agentUserNode, dBet.getBet()).compareTo(BigDecimal.ZERO) != 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * persist agent ptpl info to betting_model_agent_pt_pl table
	 * @param ptplInfos modelId -> list of agent ptpl
	 */
	private void persistAgentPtpl(Map<Long, List<AgentPtPlInfo>> ptplInfos) {
		Map<Long, BettingModelAgentPtPl> agentPtPlMap = new HashMap<>();
		for (Long modelId : ptplInfos.keySet()) {
			for (AgentPtPlInfo agentPtPlInfo : ptplInfos.get(modelId)) {
				BettingModelAgentPtPl agentPtPl = agentPtPlMap.computeIfAbsent(modelId, k -> new BettingModelAgentPtPl(modelId,
						null, null, null, agentPtPlInfo.getModelExtra().getStatementDate(), agentPtPlInfo.getModelExtra().getModelId().getOwner().getId()));

				BigDecimal ptpl = agentPtPlInfo.getTotalPtPl();
				// if agent is company, totalPtPl is base currency -> need to convert to user currency
				if (agentPtPlInfo.getAgentUserNode().getUser().getAgentType().getLevel() == 1) {
					ptpl = ptpl.divide(agentPtPlInfo.getModelExtra().getBaseConversionRate());
				}
				BigDecimal basePtpl = ptpl.multiply(agentPtPlInfo.getModelExtra().getBaseConversionRate());

				// set ptpl, base ptpl for each agent
				if(agentPtPlInfo.getAgentUserNode().getUser().getAgentType().getLevel() == 1) { // company agent
					agentPtPl.setPl2(ptpl);
					agentPtPl.setBasePl2(basePtpl);
				} else if(agentPtPlInfo.getAgentUserNode().getUser().getAgentType().getLevel() == 2) { // master agent
					agentPtPl.setPl3(ptpl);
					agentPtPl.setBasePl3(basePtpl);
				} else { // normal agent
					agentPtPl.setPl4(ptpl);
					agentPtPl.setBasePl4(basePtpl);
				}
				logger.info("aId {} oId {} ptpl {} base ptpl {} put to agent ptpl map", agentPtPlInfo.getAgentUserNode().getUser().getId(),
						modelId, ptpl, basePtpl);
			}
		}

		if(!agentPtPlMap.isEmpty()) {
			for (BettingModelAgentPtPl agentPtPl : agentPtPlMap.values()) {
				persistenceModerator.persistBettingModelAgentPtPl(agentPtPl);
				logger.debug("persist order id {} pl2 {} pl3 {} pl4 {} for user id {}", agentPtPl.getModelId(),
						agentPtPl.getPl2(), agentPtPl.getPl3(), agentPtPl.getPl4(), agentPtPl.getUserId());
			}
		}
	}

	/**
	 * persist agent ptpl info to bet_agent_pt_pl table
	 * @param betAgentPtPlInfoList modelId -> list of agent ptpl
	 */
	private void persistBetOfAgentPtPl(List<BetAgentPtPlInfo> betAgentPtPlInfoList) {
		Map<String, BetAgentPtPl> betAgentPtPlMap = new HashMap<>();
		for (BetAgentPtPlInfo betAgentPtPlInfo : betAgentPtPlInfoList) {
			BetAgentPtPl betAgentPtPl = betAgentPtPlMap.computeIfAbsent(betAgentPtPlInfo.getBetId(), k -> new BetAgentPtPl(betAgentPtPlInfo.getBetId(),
					betAgentPtPlInfo.getModelExtra().getModelId(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, betAgentPtPlInfo.getModelExtra().getStatementDate(), betAgentPtPlInfo.getModelExtra().getModelId().getOwner()));

			BigDecimal ptpl = betAgentPtPlInfo.getPtPl();
			// if agent is company, totalPtPl is base currency -> need to convert to user currency
			if (betAgentPtPlInfo.getAgentUserNode().getUser().getAgentType().getLevel() == 1) {
				ptpl = ptpl.divide(betAgentPtPlInfo.getModelExtra().getBaseConversionRate());
			}
			BigDecimal basePtpl = ptpl.multiply(betAgentPtPlInfo.getModelExtra().getBaseConversionRate());

			// set ptpl, base ptpl for each agent
			if(betAgentPtPlInfo.getAgentUserNode().getUser().getAgentType().getLevel() == 1) { // company agent
				betAgentPtPl.setBasePl2(basePtpl);
			} else if(betAgentPtPlInfo.getAgentUserNode().getUser().getAgentType().getLevel() == 2) { // master agent
				betAgentPtPl.setBasePl3(basePtpl);
			} else { // normal agent
				betAgentPtPl.setBasePl4(basePtpl);
			}
			logger.info("aId {} oId {} ptpl {} base ptpl {} put to agent ptpl map", betAgentPtPlInfo.getAgentUserNode().getUser().getId(),
					betAgentPtPl.getModel().getId(), ptpl, basePtpl);
		}

		if(!betAgentPtPlMap.isEmpty()) {
			for (BetAgentPtPl betAgentPtPl : betAgentPtPlMap.values()) {
				persistenceModerator.persistBetOfAgentPtPl(betAgentPtPl);
				logger.debug("persist bet id {} model id {} base_pl2 {} base_pl3 {} base_pl4 {} for user id {}", betAgentPtPl.getId(), betAgentPtPl.getModel().getId(),
						betAgentPtPl.getBasePl2(), betAgentPtPl.getBasePl3(), betAgentPtPl.getBasePl4(), betAgentPtPl.getUser().getId());
			}
		}
	}

	private void createPtPlSettlementInfo(OrderWrapper order, Map<Long, PTPLSettlementInfo> ptplSettlementInfos, AgentUserNode agentUserNode, BigDecimal ptplChangeTotal) throws InterruptedException {
		BigDecimal[] creditPtpls = agentUserNode.addCreditPtpl(ptplChangeTotal);
		// get from cache
		BigDecimal ptplBefore = creditPtpls[0];
		// ptpl before + sum ptpl for all bets
		BigDecimal ptplAfter = creditPtpls[1];

		// Only where there is change
		//if (ptplBefore.compareTo(ptplAfter) != 0) {
		PTPLSettlementInfo ptplInfo = new PTPLSettlementInfo(agentUserNode.getUser(), ptplBefore, ptplAfter, ptplChangeTotal, order.getOrder().getId());
		ptplSettlementInfos.put(agentUserNode.getUser().getId(), ptplInfo);
		//}
		logger.debug("order id {} agent id {} ptpl before {} calculated ptpl {} ptpl after {} ", order.getOrder().getId(),
				agentUserNode.getUser().getId(), ptplBefore, ptplChangeTotal, ptplAfter);
	}

	private BigDecimal calculateStakeForPO(Bet dBet,BigDecimal stake){
		if(!rejectedStatuses.contains(dBet.getStatus().name())){
			stake = stake
					.add(dBet.getBetStake().multiply(dBet.getMultiplication()));
		}
		return stake;
	}
	
	
	private void settlePTPLPO (List<AgentUserNode>lstCompanyAgentPtpl, Set<BetWrapper> oBets, OrderWrapper order,BettingModel openModel,boolean isModified) {
		try {
			Map<Long, PTPLSettlementInfo> ptplSettlementInfos = new HashMap<>();

			PositionalOrderWrapper pOrder = (PositionalOrderWrapper) order;
			PositionalModel pModel = pOrder.getpModel();
			// get pt percents of open model
			BettingModelExtraVodds openExtra = dbService.getModelExtraEntity(pModel.getOpenModelId().getId());
			Map<String, Map<Long, BigDecimal>> openPtPercent = getPtPercents(lstCompanyAgentPtpl, openExtra.getPtPercent());

			// get pt percents of close model
			BettingModelExtraVodds closeExtra = dbService.getModelExtraEntity(pModel.getCloseModelId().getId());
			Map<String, Map<Long, BigDecimal>> closePtPercent = getPtPercents(lstCompanyAgentPtpl, closeExtra.getPtPercent());

			// modelId -> list of agent ptpl
			Map<Long, List<AgentPtPlInfo>> ptPlInfos = new HashMap<>();
			List<BetAgentPtPlInfo> betPtPlInfos = new ArrayList<>();

			// loop through all the agents (company, master, agent)
			for (AgentUserNode agentUserNode:lstCompanyAgentPtpl) {
								
				// initialize the ptpl to 0, sum of ptpl for all the bets
				BigDecimal ptplChangeTotal = BigDecimal.ZERO; // pt pl in agent currency

				Map<Long, AgentPtPlInfo> ptplModels = new HashMap<>();
				
				for (BetWrapper oBet:oBets) {
					Bet dBet = oBet.getBet();
					if (dBet.getReturnAmount() != null){
						// create agent ptpl info
						ptplModels.computeIfAbsent(dBet.getModel().getId(), k -> new AgentPtPlInfo(agentUserNode, dBet.getModel().getId() == openModel.getId() ? openExtra : closeExtra, BigDecimal.ZERO));

						// only calculate ptpl when bet is not missing bet
						if (dBet.IsMissingBet() != 0)
							continue;

						BigDecimal ptPercents;
						if (dBet.getModel().getId() == order.getOrder().getId()) { // close order
							ptPercents = getPtPercents(closePtPercent, agentUserNode, dBet);
						}else{
							ptPercents = getPtPercents(openPtPercent, agentUserNode, dBet);
						}


						BigDecimal betPL;

						if(pModel.getIsGstop() == 1){
							if(dBet.getStatus().name().equals(BetStatus.WIN.name()) && dBet.getModel().getId() == pOrder.getOrder().getId()){
								BigDecimal newReturnAmount;
								BigDecimal actualOdd = dBet.getSubmittedOdd().subtract(pModel.getBufferOdd());
								// handle case win
								if(dBet.getReturnAmount().setScale(0, RoundingMode.HALF_UP).compareTo(dBet.getBetStake().multiply(dBet.getSubmittedOdd()).setScale(0, RoundingMode.HALF_UP)) == 0){
									newReturnAmount = actualOdd.multiply(dBet.getBetStake());
									logger.info("Order {} - user {} with new return amount {} for win, old return amount 1 {} - old return amount 2 {}",
											order.getOrder().getId(),agentUserNode.getUser().getId(),newReturnAmount,dBet.getReturnAmount().setScale(0, RoundingMode.HALF_UP),dBet.getBetStake().multiply(dBet.getSubmittedOdd()).setScale(0, RoundingMode.HALF_UP));
								}else{
									newReturnAmount = actualOdd.multiply(dBet.getBetStake().divide(new BigDecimal(2)));
									logger.info("Order {} - user {} with new return amount {} for haft win, old return amount 1 {} - old return amount 2 {}",
											order.getOrder().getId(),agentUserNode.getUser().getId(),newReturnAmount,dBet.getReturnAmount().setScale(0, RoundingMode.HALF_UP),dBet.getBetStake().multiply(dBet.getSubmittedOdd()).setScale(0, RoundingMode.HALF_UP));
								}
								betPL = newReturnAmount.multiply(dBet.getMultiplication()).multiply(BigDecimal.valueOf(-1));

							}else{
								betPL = dBet.getReturnAmount().multiply(dBet.getMultiplication()).multiply(BigDecimal.valueOf(-1));
							}
						}else{
							betPL = dBet.getReturnAmount().multiply(dBet.getMultiplication()).multiply(BigDecimal.valueOf(-1));
						}

						// bet pl must be in agent currency, if the agent is company agent, it should be converted to base currency
						if (agentUserNode.getUser().getAgentType().getLevel() == 1) {
							betPL = betPL.multiply(dBet.getBaseConversionRate());
						}

						// betPL * percent of  (company/master/agent)
						BigDecimal ptpl = betPL.multiply(ptPercents).divide(BigDecimal.valueOf(100));

						logger.debug("agent id {} with ptpl percentage {} calculated ptpl {} from bet id {} with pl {} for sportbook {} ",
								agentUserNode.getUser().getId(), ptPercents, ptpl, dBet.getId(), betPL, dBet.getSportbook().getName().toLowerCase());

						// sum ptpl for bets
						ptplChangeTotal = ptplChangeTotal.add(ptpl);

						// accumulate ptpl for each model
						ptplModels.get(dBet.getModel().getId()).addPtPl(ptpl);

						//bet ptpl
						if (ptpl.compareTo(BigDecimal.ZERO) != 0) {
							betPtPlInfos.add(new BetAgentPtPlInfo(agentUserNode, dBet.getModel().getId() == openModel.getId() ? openExtra : closeExtra, ptpl, dBet.getId()));
						}
					}
				}

				// skip ptpl transaction if agent is without taking pt for both open, close model
				if(!hasPtTaking(oBets, agentUserNode, openPtPercent, openModel.getId()) && !hasPtTaking(oBets, agentUserNode, closePtPercent, closeExtra.getModelId().getId())) {
					logger.info("aId {} open id {} close id {} has no pt taking", agentUserNode.getUser().getId(), openModel.getId(), closeExtra.getModelId().getId());
					continue;
				}

				// there is pt taking in open or close model => need to save ptpl transaction
				if(isModified){
					//revert all transaction ptpl before
					boolean isResettled = revertTransactionPTPL(order, agentUserNode, ptplChangeTotal);
					if(isResettled){
						createPtPlSettlementInfo(order, ptplSettlementInfos, agentUserNode, ptplChangeTotal);
					}
				}else{
					createPtPlSettlementInfo(order, ptplSettlementInfos, agentUserNode, ptplChangeTotal);

				}

				// copy agent ptpl of each model
				for (Long modelId : ptplModels.keySet()) {
					ptPlInfos.computeIfAbsent(modelId, k -> new ArrayList<>());
					if(hasPtTaking(oBets, agentUserNode, modelId == openModel.getId() ? openPtPercent : closePtPercent, modelId)) {
						ptPlInfos.get(modelId).add(ptplModels.get(modelId));
					}
				}
			}

			// persist agent ptpl
			persistAgentPtpl(ptPlInfos);

			// persist bet agent pt pl
			persistBetOfAgentPtPl(betPtPlInfos);

			// Populate the settlement only where size is > 0
			if (ptplSettlementInfos.size() > 0)
				tBuilder.populateSettlePTPLTransaction(ptplSettlementInfos);
		} catch (Exception e) {
			logger.error("Fail to calculate ptpl for bet id {} ", order.getOrder().getId(), e);
		}
	}
}
