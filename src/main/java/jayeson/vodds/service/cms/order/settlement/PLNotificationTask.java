package jayeson.vodds.service.cms.order.settlement;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.BetUser;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.events.UserPlChangeEvent;

@Singleton
public class PLNotificationTask{
	
	@Inject
	UserManager uManager;
	
	@Inject
	IDBService dbService;
	
	ConcurrentHashMap<Long, BetUser> userIds; 
	
	@Inject
	public PLNotificationTask(){
		userIds = new ConcurrentHashMap<>();
	}
	
	static Logger logger = LoggerFactory.getLogger(PLNotificationTask.class.getName());
	
	public void saveUserHavePlChange(BetUser bu){
		userIds.put(bu.getId(), bu);
	}

	public void notifyPLChanges() {
		logger.info("count users {}",userIds.size());
		Map<Long, BetUser> tmpUserIds = new HashMap<>();
		tmpUserIds.putAll(userIds);
		userIds = new ConcurrentHashMap<>();
		if(!tmpUserIds.isEmpty()){
			logger.info("User to notify pl changes {}", tmpUserIds.keySet().toString());
			for (BetUser item : tmpUserIds.values()) {
				BigDecimal plValue = dbService.getCachedPlOfBetUser(item.getId());
				logger.info("pl update message posted to event bus for user id {} and value {}",item.getId(),plValue.doubleValue());
				// post the message to event bus, to be listened by thrift server
				this.uManager.geteBus().post(new UserPlChangeEvent(item.getUser().getUsername(),plValue.doubleValue(),item.getId()));
			}
		
		}
		
	}

}
