package jayeson.vodds.service.cms.order.result;

public abstract class IActionResult {
	
	// optional message to describe the result 

	long modelId;
	String message;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String rMessage) {
		this.message = rMessage;
	}
	public long getModelId() {
		return modelId;
	}
	public void setModelId(long modelId) {
		this.modelId = modelId;
	}


}
