package jayeson.vodds.service.cms.order.action;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.concurrent.Callable;
import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;


import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/***
 * Action executed when the user's credit is intended to be reserved for executing a normal order
 * The action will contain the logic to deduct credit and commission from the user credit
 * @author Praveen
 *
 */

public class ReserveNormalCreditAction implements IOrderAction,Callable<ReserveCreditResult> {
	
	private static Logger logger = LoggerFactory.getLogger(ReserveNormalCreditAction.class);
	
	private static String TX_RESERVE_STAKE = "tx_reserve_stake";
	
	private static String TX_PARTIALLY__RESERVE_STAKE = "tx_partially_reserve_stake";
	
	private static String TX_OUTSTANDING_AMT_NEGATIVE = "tx_outstanding_amount_negative";
	
	// user manager instance to manager the bet user node
	UserManager uManager;

	// information required to process the credit request
	NormalReserveInfo resInfo;
	// transaction builder to populate transactions
	TransactionBuilder tBuilder;
	
	VoddsCommonUtility voddsCommonUtility;
	
	ThriftCreditRequestInfo reqInfo;
	GeneralUtility genUtil;
	
	@Inject
	public ReserveNormalCreditAction(UserManager umanager, TransactionBuilder builder, NormalReserveInfo info, ThriftCreditRequestInfo thriftReqInfo,
									 VoddsCommonUtility voddsCommonUtility, GeneralUtility genUtil){
		uManager = umanager;
		reqInfo = thriftReqInfo;
		resInfo = info;
		tBuilder = builder;
		this.voddsCommonUtility = voddsCommonUtility;
		this.genUtil = genUtil;
	}
	
	@Override
	public ReserveCreditResult call()  {
		return processCreditAction();
	}

	@Override
	public ReserveCreditResult processCreditAction() {
		
		BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
		BigDecimal reqCredit = new BigDecimal(resInfo.creditAmount);
		
		// compute total credit after adding in commission percentage of user
		BigDecimal commission = reqCredit.multiply(userNode.getUser().getCommission().negate());
		BigDecimal totalCredit = reqCredit.add(commission);
		
		
		ReserveCreditResult aResult = new ReserveCreditResult();
		aResult.setModelId(resInfo.modelId);

		TradeLimitData tradeLimitData = new TradeLimitData(reqInfo.getMatchId(),reqInfo.getSportType(), reqInfo.getOrderStatus(),
				reqInfo.getExecutionStage(), new Timestamp(reqInfo.getStartTime()), BigDecimal.ZERO,
				BigDecimal.valueOf(resInfo.creditAmount),BigDecimal.ZERO, ThriftTransactionType.RESERVE.name());

		tradeLimitData.setTradeLimitCheck(userNode.isCheckTradeLimit());
		tradeLimitData.setoType(reqInfo.oType);
		
		logger.info("trying to reserve a credit of {} and commission {} from user {} - matchId {} - sportType {} - orderStatus {} - executeStage {}",
				reqCredit,commission,reqInfo.userId,reqInfo.matchId,reqInfo.sportType,reqInfo.orderStatus,reqInfo.executionStage);
		
		if(!genUtil.isValidOutstanding(userNode.getOutstandingAmount())) {
			aResult.setReservedCredit(Double.NaN);
			aResult.setCommissionAmount(Double.NaN);
			logger.info("The outstanding {} of user {} is negative !",userNode.getOutstandingAmount(),userNode.getUser().getId());
			aResult.setMessage(this.voddsCommonUtility.stringsToDescription(TX_OUTSTANDING_AMT_NEGATIVE,tBuilder.format(userNode.getOutstandingAmount())));
		}else {		
			// when the user has sufficient credit that is requested
			if(userNode.getCredit().compareTo(totalCredit) > -1){
				// set the result to the effective credit reserved, without commission value
				aResult.setReservedCredit(reqCredit.doubleValue());
				aResult.setCommissionAmount(commission.doubleValue());
				
				//aResult.setMessage(resInfo.remark);
				
				String remark = String.format("Reserve for normal order with stake = %s",
						tBuilder.format(new BigDecimal(aResult.getReservedCredit())));
				
				aResult.setMessage(this.voddsCommonUtility.stringsToDescription(TX_RESERVE_STAKE,tBuilder.format(new BigDecimal(aResult.getReservedCredit()))));
				
				// populate the reserve credit related db transactions
				tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.NORMAL.name(),tradeLimitData);
	
	
			}
			
			// when user didn't have enough credit, but the option to use all credit is set true in the request
			else if (userNode.getCredit().compareTo(totalCredit) < 0 && userNode.getCredit().compareTo(new BigDecimal(0)) > 0 && resInfo.useAllCredit){
				
				// compute the effective credit, after taking care of commission
				BigDecimal eCredit = userNode.getCredit().divide(new BigDecimal(1).subtract(userNode.getUser().getCommission()) ,
						new MathContext(20, RoundingMode.CEILING)).setScale(10, RoundingMode.CEILING);
				
				commission = eCredit.multiply(userNode.getUser().getCommission().negate());
				
				logger.info("trying to reserve a credit of {} and commission {} from user {}",eCredit,commission,this.reqInfo.userId);
				
				aResult.setReservedCredit(eCredit.doubleValue());
				aResult.setCommissionAmount(commission.doubleValue());
				//Result.setMessage("credit partially reserved due to insufficient funds");
				
				aResult.setMessage(this.voddsCommonUtility.stringsToDescription(TX_PARTIALLY__RESERVE_STAKE,null));
	
				// populate the reserve credit related db transactions
	
				tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.NORMAL.name(),tradeLimitData);
				
	
			}
			
			else{
				// set invalid values for error cases
				aResult.setReservedCredit(-1);
				aResult.setCommissionAmount(Double.NaN);
				
				aResult.setMessage("credit reservation failed due to insufficient funds");
			}
		}

		return aResult;
		
	}
		
	

}
