package jayeson.vodds.service.cms.order.worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.apache.thrift.observable.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.vodds.service.cms.data.ServiceEmission;
import jayeson.vodds.service.cms.events.CMSEvent;
import jayeson.vodds.service.cms.events.POSettledEvent;
import jayeson.vodds.service.cms.events.UserCreditChangeEvent;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.events.UserPlChangeEvent;
import jayeson.vodds.service.cms.service.CMSThriftServer;
import jayeson.vodds.service.cms.thrift.ThriftCreditUpdate;
import jayeson.vodds.service.cms.thrift.ThriftMplUpdate;
import jayeson.vodds.service.cms.thrift.ThriftUserUpdate;

@Singleton
public class CMSThriftHandler implements Function<List<ServiceEmission>, CompletableFuture<Boolean>>{
	
	private static Logger logger = LoggerFactory.getLogger(CMSThriftHandler.class);
	
	CMSThriftServer cmsThriftServer;
	
	@Inject
	public CMSThriftHandler(CMSThriftServer server){
		this.cmsThriftServer = server;
	}
	
	private void handleUpdateUserOutStandingEvent(CMSEvent cmsEvent,ThriftUserUpdate update){
		long userId = cmsEvent.getUserId();
		String userName = cmsEvent.getUserName();
		double outStanding = ((UserOutstandingChangeEvent) cmsEvent).getOutstandingValue();
		logger.info("Handle outstanding event for user id {} - outstanding {}",userId,outStanding);
		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();
		cUpdate.setUserName(userName);
		cUpdate.setOutStanding(outStanding);
		update.cUpdates.add(cUpdate);
	}
	
	private void handleUpdateCreditEvent(CMSEvent cmsEvent,ThriftUserUpdate update){
		long userId = cmsEvent.getUserId();
		String userName = cmsEvent.getUserName();
		double credit = ((UserCreditChangeEvent) cmsEvent).getCreditValue();
		logger.info("Handle credit event for user id {} - outstanding {}",userId,credit);
		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();
		cUpdate.setUserName(userName);
		cUpdate.setCredit(credit);
		update.cUpdates.add(cUpdate);
		
	}
	
	private void handleChangePlEvent(CMSEvent cmsEvent,ThriftUserUpdate update){
		long userId = cmsEvent.getUserId();
		String userName = cmsEvent.getUserName();
		double pl = ((UserPlChangeEvent) cmsEvent).getPl();
		logger.info("Handle pl event for user id {} - pl {}",userId,pl);
		ThriftCreditUpdate cUpdate = new ThriftCreditUpdate();
		cUpdate.setUserName(userName);
		cUpdate.setProfitLoss(pl);
		update.cUpdates.add(cUpdate);
	}
	
	private void handleUpdatePOSettledEvent(CMSEvent cmsEvent,ThriftUserUpdate update){
		long userId = cmsEvent.getUserId();
		String userName = cmsEvent.getUserName();
		long closeId = ((POSettledEvent) cmsEvent).getCloseId();
		double mpl = ((POSettledEvent) cmsEvent).getMplValue();
		long pId = ((POSettledEvent) cmsEvent).getpId();
		logger.info("Handle po-settlement event for user id {} - pId {} - closedId {} - mpl {}",userId,pId,closeId,mpl);
		ThriftMplUpdate mUpdate = new ThriftMplUpdate();
		mUpdate.setUserName(userName);
		mUpdate.setCloseId(closeId);
		mUpdate.setMplValue(mpl);
		mUpdate.setPId(pId);
		update.mUpdates.add(mUpdate);
	}
	
	@Override
	public CompletableFuture<Boolean> apply(List<ServiceEmission> list) {
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		HashMap<Long, ThriftUserUpdate> hm = new HashMap<>();
		try{
			logger.trace("Total item in batch: {} data {}", list.size(),list.toString());
			String service = null;
			for (ServiceEmission serviceEmission : list) {
				service = serviceEmission.getServiceId();
				ThriftUserUpdate thriftUserUpdate = hm.computeIfAbsent(serviceEmission.getCmsEvent().getUserId(), r -> new ThriftUserUpdate());
				thriftUserUpdate.cUpdates = (thriftUserUpdate.cUpdates == null) ? new ArrayList<>() : thriftUserUpdate.cUpdates;
				thriftUserUpdate.mUpdates = (thriftUserUpdate.mUpdates == null) ? new ArrayList<>() : thriftUserUpdate.mUpdates;
				if(serviceEmission.getCmsEvent() instanceof UserOutstandingChangeEvent){
					handleUpdateUserOutStandingEvent(serviceEmission.getCmsEvent(),thriftUserUpdate);
				}else if(serviceEmission.getCmsEvent() instanceof UserPlChangeEvent){
					handleChangePlEvent(serviceEmission.getCmsEvent(),thriftUserUpdate);
				}else if(serviceEmission.getCmsEvent() instanceof UserCreditChangeEvent){
					handleUpdateCreditEvent(serviceEmission.getCmsEvent(),thriftUserUpdate);
				}else if(serviceEmission.getCmsEvent() instanceof POSettledEvent){
					handleUpdatePOSettledEvent(serviceEmission.getCmsEvent(), thriftUserUpdate);				
				}
			}
			//emit thrift user update
			ThriftUserUpdate lstUserUpdate = new ThriftUserUpdate();
			lstUserUpdate.cUpdates = new ArrayList<>();
			lstUserUpdate.mUpdates = new ArrayList<>();
			logger.info("Size of map thrift user update {}",hm.size());
			hm.forEach((userId,thriftUserUpdate)->{
				lstUserUpdate.cUpdates.addAll(thriftUserUpdate.getCUpdates());
				lstUserUpdate.mUpdates.addAll(thriftUserUpdate.getMUpdates());
			});
			logger.info("Emit thrift user update {} for service {}",lstUserUpdate,service);
			return cmsThriftServer.emitThriftUserUpdate(lstUserUpdate, service).thenApply(k -> true);
		}catch(Exception ex){
			logger.error("Cannot emit data ", ex);
			return CompletableFuture.completedFuture(false);
		}
	}
}
