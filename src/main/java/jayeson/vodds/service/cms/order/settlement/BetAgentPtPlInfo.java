package jayeson.vodds.service.cms.order.settlement;

import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.vodds.service.cms.data.AgentUserNode;

import java.math.BigDecimal;

public class BetAgentPtPlInfo {
	private String betId;
	private AgentUserNode agentUserNode;
	private BettingModelExtraVodds modelExtra;
	private BigDecimal ptPl;

	public BetAgentPtPlInfo() {
	}

	public BetAgentPtPlInfo(AgentUserNode agentUserNode, BettingModelExtraVodds modelExtra, BigDecimal ptPl, String betId) {
		this.agentUserNode = agentUserNode;
		this.modelExtra = modelExtra;
		this.ptPl = ptPl;
		this.betId = betId;
	}

	public BigDecimal getPtPl() {
		return ptPl;
	}
	public void setPtPl(BigDecimal ptPl) {
		this.ptPl = ptPl;
	}

	public AgentUserNode getAgentUserNode() {
		return agentUserNode;
	}
	public void setAgentUserNode(AgentUserNode agentUserNode) {
		this.agentUserNode = agentUserNode;
	}
	
	public void addPtPl (BigDecimal ptpl) {
		this.ptPl = ptPl.add(ptpl);
	}

	public BettingModelExtraVodds getModelExtra() {
		return modelExtra;
	}

	public void setModelExtra(BettingModelExtraVodds modelExtra) {
		this.modelExtra = modelExtra;
	}

	public String getBetId() {
		return betId;
	}

	public void setBetId(String betId) {
		this.betId = betId;
	}
}
