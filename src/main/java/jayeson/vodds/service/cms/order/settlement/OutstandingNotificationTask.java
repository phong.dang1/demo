package jayeson.vodds.service.cms.order.settlement;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.order.worker.OutstandingWorker;
import jayeson.vodds.service.cms.service.NoQPersister;

@Singleton
public class OutstandingNotificationTask{
	
	private static Logger logger = LoggerFactory.getLogger(OutstandingNotificationTask.class);
	
	private OutstandingWorker worker;

	@Inject
	public OutstandingNotificationTask(OutstandingWorker worker) {
		this.worker = worker;
	}
	
	public void submitOustanding(UserOutstandingChangeEvent event){
		worker.submitOutstanding(event);
	}


}
