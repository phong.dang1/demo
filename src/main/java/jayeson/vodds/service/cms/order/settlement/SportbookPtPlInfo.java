package jayeson.vodds.service.cms.order.settlement;

import java.math.BigDecimal;

import jayeson.database.BettingModel;
import jayeson.vodds.service.cms.data.AgentUserNode;

public class SportbookPtPlInfo {
	private String sportbook;
	private BigDecimal totalPtPl;
	private BigDecimal ptPercent;
	private BigDecimal conversionRate;
	private BettingModel model;
	private AgentUserNode agentUserNode;
	
	public SportbookPtPlInfo(String sportbook, BigDecimal ptPercent, BigDecimal conversionRate,
			BettingModel model, AgentUserNode agentUserNode) {
		super();
		this.sportbook = sportbook;
		this.ptPercent = ptPercent;
		this.conversionRate = conversionRate;
		this.model = model;
		this.agentUserNode = agentUserNode;
		this.totalPtPl = BigDecimal.ZERO;
	}
	public String getSportbook() {
		return sportbook;
	}
	public void setSportbook(String sportbook) {
		this.sportbook = sportbook;
	}
	public BigDecimal getTotalPtPl() {
		return totalPtPl;
	}
	public void setTotalPtPl(BigDecimal totalPtPl) {
		this.totalPtPl = totalPtPl;
	}
	public BigDecimal getPtPercent() {
		return ptPercent;
	}
	public void setPtPercent(BigDecimal ptPercent) {
		this.ptPercent = ptPercent;
	}
	public BigDecimal getConversionRate() {
		return conversionRate;
	}
	public void setConversionRate(BigDecimal conversionRate) {
		this.conversionRate = conversionRate;
	}
	public BettingModel getModel() {
		return model;
	}
	public void setModel(BettingModel model) {
		this.model = model;
	}
	public AgentUserNode getAgentUserNode() {
		return agentUserNode;
	}
	public void setAgentUserNode(AgentUserNode agentUserNode) {
		this.agentUserNode = agentUserNode;
	}
	
	public void addPtPl (BigDecimal ptpl) {
		this.totalPtPl = totalPtPl.add(ptpl);
	}
}
