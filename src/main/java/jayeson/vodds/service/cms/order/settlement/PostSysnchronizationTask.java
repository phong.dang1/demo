package jayeson.vodds.service.cms.order.settlement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.Bet;
import jayeson.database.BettingModel;
import jayeson.lib.betting.datastructure.BetStatus;
import jayeson.vodds.service.cms.data.BetWrapper;
import jayeson.vodds.service.cms.data.OrderWrapper;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;

@Singleton
public class PostSysnchronizationTask {

	UserManager uManager;
	
	IDBService dbService;
	
	OrderWrapperManager owManger;
	
	private static Logger logger = LoggerFactory.getLogger(PostSysnchronizationTask.class);

	
	
	@Inject
	public PostSysnchronizationTask(UserManager userManager,IDBService service, OrderWrapperManager manger) {
		uManager = userManager;
		dbService = service;
		owManger = manger;
	}
	
	public void checkSysnDataOfOrders(Set<BettingModel> setBM){
		if(setBM.size() >0){
			try{
				logger.info("check sysn data of order running");
				for (Iterator<BettingModel> iterator = setBM.iterator(); iterator.hasNext();) {
					BettingModel bettingModel = null;
					try{
						List<String> lstBetIds = new ArrayList<>();
						bettingModel = iterator.next();
						logger.info("sysn data for order {}",bettingModel.getId());
						OrderWrapper oWrapper = owManger.findStoppedOrderById(bettingModel.getId());
						Set<BetWrapper> oBets = oWrapper.getBetWrappers();
						HashMap<String, Bet> hash = convertSetToHashMap(oBets,oWrapper,lstBetIds);
						List<Bet>lstBet = dbService.findBetListById(lstBetIds);
						if(!isAllBetsChanged(lstBet, hash)){
							owManger.removeOrderFromCache(bettingModel.getId());
							iterator.remove();
						}
					}catch(Exception ex){
						logger.error("Cannot sysn data with order {}",bettingModel.getId(),ex);
					}
				}
				dbService.updateBMResults(setBM,owManger);
			}catch(Exception ex){
				logger.error("Error in checkSysnDataofOrders ",ex);
			}
			
		}
	}
	
	private HashMap<String, Bet> convertSetToHashMap(Set<BetWrapper> betWrapper,OrderWrapper oWrapper,List<String> lstBetIds){
		HashMap<String, Bet> hm = new HashMap<>();
		for (BetWrapper bWrapper : betWrapper) {
			if(bWrapper.getBet().getModel().getId() == oWrapper.getOrder().getId()){
				logger.info("Info bet id {}",bWrapper.getBet().getModel().getId());
				hm.put(bWrapper.getBet().getId(), bWrapper.getBet());
				lstBetIds.add(bWrapper.getBet().getId());
			}
		}
		return hm;
	}
	
	public boolean isAllBetsChanged(List<Bet>lstBet,HashMap<String, Bet> hash){
		boolean isChanged=false;
		for (Bet bet : lstBet) {
			BigDecimal returnAmount = hash.get(bet.getId()).getReturnAmount();
			String status =  hash.get(bet.getId()).getStatus().name().toString();
			logger.info("amt on cache {} , amt on db {} , status on cache {} , status on db {} bet id {}",returnAmount,bet.getReturnAmount(),status,bet.getStatus().name().toString(), bet.getId());
			if((returnAmount != null && bet.getReturnAmount() != null && returnAmount.compareTo(bet.getReturnAmount()) != 0) || !status.equalsIgnoreCase(bet.getStatus().name().toString())){
				isChanged = true;
			}
		}
		
		return isChanged;
	}
}
