package jayeson.vodds.service.cms.order.settlement;

import jayeson.database.BetUser;

public class MPLChangeInfo{
	private Long pId;
	private Long closeId;
	//value before change
	private Double updatedMplValue;
	private BetUser betUser;
	
	public MPLChangeInfo(Long pId, Long closeId, Double updatedMplValue,
			BetUser betUser) {
		super();
		this.pId = pId;
		this.closeId = closeId;
		this.updatedMplValue = updatedMplValue;
		this.betUser = betUser;
	}
	
	public BetUser getBetUser() {
		return betUser;
	}
	public void setBetUser(BetUser betUser) {
		this.betUser = betUser;
	}
	public Long getpId() {
		return pId;
	}
	public void setpId(Long pId) {
		this.pId = pId;
	}
	public Long getCloseId() {
		return closeId;
	}
	public void setCloseId(Long closeId) {
		this.closeId = closeId;
	}
	public Double getUpdatedMplValue() {
		return updatedMplValue;
	}
	public void setUpdatedMplValue(Double updatedMplValue) {
		this.updatedMplValue = updatedMplValue;
	}
	
	
	
}
