package jayeson.vodds.service.cms.order.settlement;

import com.google.inject.Inject;
import jayeson.database.Bet;
import jayeson.vodds.service.cms.data.OrderWrapperManager;
import jayeson.vodds.service.cms.database.IDBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/***
 * Implements the logic to update the status of order and corresponding betwrappers when updated by PLM
 * @author Praveen
 *
 */
public class UpdateOrderStatusTask implements Runnable {
	
	IDBService dbHelper;
	OrderWrapperManager oManager;
	List<String> idList;
	static Logger logger = LoggerFactory.getLogger(UpdateOrderStatusTask.class.getName());

	@Inject
	public UpdateOrderStatusTask(IDBService helper, OrderWrapperManager manager,List<String> idList){
		dbHelper = helper;
		oManager = manager;
		this.idList = idList;
	}

	public void run(){
		
		logger.debug("updating bet list of size {}, element {}",idList.size(),idList);
		if(!idList.isEmpty()){
			List<Bet> betList = dbHelper.findBetListById(idList);
			for(Bet bet:betList){
			
				try{
					if(bet.getModel() != null){
						logger.debug("status update for bet id {} with order id {}",bet.getId(),bet.getModel().getId());
						
						if(oManager.existOrderCache(bet.getModel().getId())){
							logger.info("Order {} have in cache",bet.getModel().getId());
						}
					}
	
					
				}
				catch(Exception e){
					logger.error("error while updating staus of betwrapper",e);
				}
				
			}
			
			logger.debug("finished updating bet wrappers of size {}",betList.size());
		}

		
	}
	
}
