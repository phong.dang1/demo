package jayeson.vodds.service.cms.order.settlement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import jayeson.database.BetUser;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.events.CMSEvent;
import jayeson.vodds.service.cms.events.POSettledEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
@Singleton
public class MPLNotificationTask implements Runnable{
	private static Logger logger = LoggerFactory.getLogger(MPLNotificationTask.class);
	//GUICE injected
	private UserManager uManager;
	
	
	/**
	 * pid -> mpl change info
	 */
	private Map<Long, MPLChangeInfo> poHasMPLChangeAndPersisted;
	/**
	 * positional model id that has MPL changed but not yet persisted to db
	 */
	private Map<Long, MPLChangeInfo> poHasMPLChangeButNotPersisted;
	
	@Inject
	public MPLNotificationTask(UserManager uManager) {
		this.uManager = uManager;
		this.poHasMPLChangeAndPersisted = new ConcurrentHashMap<>();
		this.poHasMPLChangeButNotPersisted = new ConcurrentHashMap<>();
	}

	
	public void addOrderHaveMPlChange(long pId, long closeId, double mplValue, BetUser betUser) {
		try{
			MPLChangeInfo info = new MPLChangeInfo(pId, closeId, Math.abs(mplValue), betUser);
			this.poHasMPLChangeButNotPersisted.put(info.getpId(), info);
			
			logger.debug("Start monitoring addOrderHaveMPlChange: user {}, pid {}, updated mpl {}",
					info.getBetUser().getId(), info.getpId(), info.getUpdatedMplValue());
		}catch(Exception ex){
			logger.error("Error in addOrderHaveMPlChange, pid {}, updated mpl {} ", pId, mplValue, ex);
		}
		
	}
	
	public void onPositionalOrderPersisted(long pidPersisted) {
		try{
			MPLChangeInfo info = this.poHasMPLChangeButNotPersisted.get(pidPersisted);
			if (info != null){//previously not persisted PO already persisted
				this.poHasMPLChangeAndPersisted.put(info.getpId(), info);
				this.poHasMPLChangeButNotPersisted.remove(pidPersisted);
				
				logger.debug("Sumitted mpl change info to sender: user {}, pid {}, updated mpl {}",
						info.getBetUser().getId(), info.getpId(), info.getUpdatedMplValue());
			}
			
		}catch(Exception ex){
			logger.error("Error in onPositionalOrderPersisted, pidPersisted {} ", pidPersisted, ex);
		}
		
	}
	
	@Override
	public void run() {
		try{
			logger.trace("MPL notification running");
			List<MPLChangeInfo> toBeSent = new ArrayList<>(this.poHasMPLChangeAndPersisted.values());
			this.poHasMPLChangeAndPersisted.clear();
			
			for (MPLChangeInfo info: toBeSent){
				logger.debug("Sending mpl change notification: user {}, pid {}, updated mpl {}",
						info.getBetUser().getId(), info.getpId(), info.getUpdatedMplValue());
				POSettledEvent event = new POSettledEvent(info.getBetUser().getUsername(), info.getpId(), info.getCloseId(), info.getUpdatedMplValue(), info.getBetUser().getId());
				try{
					long sleep = 300;
					this.uManager.geteBus().post(event);
					logger.debug("Sent mpl change notification: user {}, pid {}, updated mpl {}, sleep for {}",
							info.getBetUser().getId(), info.getpId(), info.getUpdatedMplValue(), sleep);
					Thread.sleep(sleep);
				}catch(Exception ex){
					logger.error("Cannot send mpl change notification: user {}, pid {}, updated mpl {}",
							info.getBetUser().getId(), info.getpId(), info.getUpdatedMplValue());
				}
				
			}
		}catch(Exception ex){
			logger.error("Cannot sent MPL change notification in batch", ex);
		}
	}

}
