package jayeson.vodds.service.cms.order.action;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.json.LeverageReserveInfo;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;


import jayeson.vodds.service.cms.util.GeneralUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/***
 * Action executed when the user's credit is intended to be reserved for executing a positional open order
 * The action will contain the logic to deduct margin amount using leverage ratio and commission based on original creditAmount from the user credit
 * @author Praveen
 *
 */
public class ReserveOpenCreditAction implements IOrderAction,Callable<ReserveCreditResult> {

	private static Logger logger = LoggerFactory.getLogger(ReserveOpenCreditAction.class);
	
	// user manager instance to manager the bet user node
	UserManager uManager;
	
	// information required to process the credit request
	LeverageReserveInfo resInfo;
	// transaction builder to populate transactions
	TransactionBuilder tBuilder;
	
	ThriftCreditRequestInfo reqInfo;
	
	private static String TX_RESERVE_MARGIN_OPEN = "tx_reserve_margin_open";
	
	private static String TX_PARTIALLY_RESERVE_MARGIN_OPEN = "tx_partially_reserve_margin_open";
	
	private static String TX_OUTSTANDING_AMT_NEGATIVE = "tx_outstanding_amount_negative";
	
	VoddsCommonUtility voddsCommonUtility;
	GeneralUtility genUtil;
	
	@Inject
	public ReserveOpenCreditAction(UserManager umanager, TransactionBuilder builder, LeverageReserveInfo info, ThriftCreditRequestInfo thriftReqInfo,
								   VoddsCommonUtility voddsCommonUtility, GeneralUtility genUtil){
		uManager = umanager;
		reqInfo = thriftReqInfo;
		resInfo = info;
		tBuilder = builder;
		this.voddsCommonUtility = voddsCommonUtility;
		this.genUtil = genUtil;
	}
	
	
	@Override
	public ReserveCreditResult call()  {
		return processCreditAction();
	}

	@Override
	public ReserveCreditResult processCreditAction() {
		
		BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
		BigDecimal reqCredit = new BigDecimal(resInfo.creditAmount);
		BigDecimal leverage = new BigDecimal(resInfo.leverageRatio);
		
		
		ReserveCreditResult aResult = new ReserveCreditResult();
		aResult.setModelId(resInfo.modelId);
		
		TradeLimitData tradeLimitData = new TradeLimitData(reqInfo.getMatchId(),reqInfo.getSportType(), reqInfo.getOrderStatus(),
				reqInfo.getExecutionStage(), new Timestamp(reqInfo.getStartTime()), BigDecimal.ZERO,
				BigDecimal.valueOf(resInfo.creditAmount),BigDecimal.ZERO, ThriftTransactionType.RESERVE.name());
		
		tradeLimitData.setTradeLimitCheck(userNode.isCheckTradeLimit());
		tradeLimitData.setoType(reqInfo.oType);
		
		String remark = "";
		
		if(!genUtil.isValidOutstanding(userNode.getOutstandingAmount())) {
			aResult.setReservedCredit(Double.NaN);
			aResult.setCommissionAmount(Double.NaN);
			logger.info("The outstanding {} of user {} is negative !",userNode.getOutstandingAmount(),userNode.getUser().getId());
			aResult.setMessage(this.voddsCommonUtility.stringsToDescription(TX_OUTSTANDING_AMT_NEGATIVE,tBuilder.format(userNode.getOutstandingAmount())));
		}else {
		
			// handle the margin topup case
			if(resInfo.isMarginTopup){
				
				// handle the case when user has sufficient credit
				if(userNode.getCredit().compareTo(reqCredit) > -1){
					// set the result to the effective credit reserved, without commission value
					aResult.setReservedCredit(reqCredit.doubleValue());
					// no commission involved in the margin topup
					aResult.setCommissionAmount(0);
					aResult.setMarginTopup(true);
					
					aResult.setMessage(resInfo.remark);
	//				remark = String.format("Reserve margin of %s for open order with stake = %s , leverage ratio = %s",
	//						(aResult.getReservedCredit()/aResult.getLeverageRatio()),aResult.getReservedCredit(),aResult.getLeverageRatio());
	//				aResult.setMessage(remark);
					// populate the reserve credit related db transactions
					tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.POSITIONAL_OPEN.name(),null);
				}
				// when there is not enough credit to topup , consider the credit request as failed
				// for margin topup, the useAllCredit flag is not valid
				else{
					// set invalid values for error cases
					aResult.setReservedCredit(-1);
					aResult.setCommissionAmount(-1);
					
					aResult.setMessage("margin topup failed due to insufficient funds");
					
				}
				
			}
			// handle the positional reserve open credit case
			else{
				
				// calculate the margin credit that need to be deducted from the user credit
				BigDecimal marginCredit = reqCredit.divide(leverage,new MathContext(20, RoundingMode.CEILING)).setScale(10, RoundingMode.CEILING);
				
				// commission is calculated from requested credit
				BigDecimal commission = reqCredit.multiply(userNode.getUser().getCommission().negate());
				// compute total credit after adding in commission percentage of user
				BigDecimal totalCredit = marginCredit.add(commission);
	
				
				logger.info("trying to reserve a credit of {} with leverage ratio {} and commission {} from user {} and creidt of user {} - matchId {} - sportType {} - orderStatus {} - executeStage {} - startTime {}",
						marginCredit,leverage,commission,reqInfo.userId,userNode.getCredit(),reqInfo.getMatchId(),reqInfo.getSportType(),reqInfo.getOrderStatus(),reqInfo.getExecutionStage(),reqInfo.getStartTime());
				
				// when the user has sufficient credit that is requested
				if(userNode.getCredit().compareTo(totalCredit) > -1){
					
					// set the result to the effective credit reserved, without commission value
					aResult.setReservedCredit(reqCredit.doubleValue());
					aResult.setCommissionAmount(commission.doubleValue());
					aResult.setLeverageRatio(leverage.doubleValue());
					
					//aResult.setMessage(resInfo.remark);
					remark = this.voddsCommonUtility.stringsToDescription(TX_RESERVE_MARGIN_OPEN,tBuilder.format(new BigDecimal(aResult.getReservedCredit()/aResult.getLeverageRatio())),tBuilder.format(new BigDecimal(aResult.getReservedCredit())),tBuilder.format(new BigDecimal(aResult.getLeverageRatio())));
	//				remark = String.format("Reserve margin of %s for open order with stake = %s , leverage ratio = %s",
	//					tBuilder.format(new BigDecimal(aResult.getReservedCredit()/aResult.getLeverageRatio())),tBuilder.format(new BigDecimal(aResult.getReservedCredit())),tBuilder.format(new BigDecimal(aResult.getLeverageRatio())));
					aResult.setMessage(remark);
					
					// populate the reserve credit related db transactions
					tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.POSITIONAL_OPEN.name(),tradeLimitData);
				}
				// when user didn't have enough credit, but the option to use all credit is set true in the request
				else if (userNode.getCredit().compareTo(totalCredit) < 0 && userNode.getCredit().compareTo(new BigDecimal(0)) > 0 && resInfo.useAllCredit){
					
					// compute the effective credit, after taking care of commission and leverage margin
					// user credit = x*(1/leverage)+x* commission
					
					BigDecimal eCredit = userNode.getCredit().divide(userNode.getUser().getCommission().negate().add(new BigDecimal(1).divide(leverage,new MathContext(20, RoundingMode.CEILING)).setScale(10, RoundingMode.CEILING)) ,
							new MathContext(20, RoundingMode.CEILING)).setScale(10, RoundingMode.CEILING);
					
					commission = eCredit.multiply(userNode.getUser().getCommission().negate());
					
					aResult.setReservedCredit(eCredit.doubleValue());
					aResult.setCommissionAmount(commission.doubleValue());
					aResult.setLeverageRatio(leverage.doubleValue());
					
					remark = this.voddsCommonUtility.stringsToDescription(TX_PARTIALLY_RESERVE_MARGIN_OPEN);
					aResult.setMessage("credit partially reserved due to insufficient funds");
					aResult.setMessage(remark);
	
					// populate the reserve credit related db transactions
					tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.POSITIONAL_OPEN.name(),tradeLimitData);
				}
				
				
				else{
					// set invalid values for error cases
					aResult.setReservedCredit(-1);
					aResult.setCommissionAmount(Double.NaN);
					
					aResult.setMessage("credit reservation failed due to insufficient funds");
				}
			}
			
		}
		
		return aResult;

	}
}
