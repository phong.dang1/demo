package jayeson.vodds.service.cms.order.settlement;

import jayeson.database.BettingModel;
import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.vodds.service.cms.data.AgentUserNode;

import java.math.BigDecimal;

public class AgentPtPlInfo {
	private AgentUserNode agentUserNode;
	private BettingModelExtraVodds modelExtra;
	private BigDecimal totalPtPl;

	public AgentPtPlInfo() {
	}

	public AgentPtPlInfo(AgentUserNode agentUserNode, BettingModelExtraVodds modelExtra, BigDecimal totalPtPl) {
		this.agentUserNode = agentUserNode;
		this.modelExtra = modelExtra;
		this.totalPtPl = totalPtPl;
	}

	public BigDecimal getTotalPtPl() {
		return totalPtPl;
	}
	public void setTotalPtPl(BigDecimal totalPtPl) {
		this.totalPtPl = totalPtPl;
	}

	public AgentUserNode getAgentUserNode() {
		return agentUserNode;
	}
	public void setAgentUserNode(AgentUserNode agentUserNode) {
		this.agentUserNode = agentUserNode;
	}
	
	public void addPtPl (BigDecimal ptpl) {
		this.totalPtPl = totalPtPl.add(ptpl);
	}

	public BettingModelExtraVodds getModelExtra() {
		return modelExtra;
	}

	public void setModelExtra(BettingModelExtraVodds modelExtra) {
		this.modelExtra = modelExtra;
	}
}
