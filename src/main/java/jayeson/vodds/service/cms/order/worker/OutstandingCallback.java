package jayeson.vodds.service.cms.order.worker;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.BetUser;
import jayeson.utility.concurrent.worker.batch.SharedExecutorBatchWorker;
import jayeson.utility.concurrent.worker.batch.SharedExecutorBatchWorker.BatchWorkerCallback;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.events.CMSEvent;
import jayeson.vodds.service.cms.events.POSettledEvent;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.events.UserPlChangeEvent;

@Singleton
public class OutstandingCallback implements BatchWorkerCallback<UserOutstandingChangeEvent, Void>{
	
	private static Logger logger = LoggerFactory.getLogger(OutstandingCallback.class);
	
	EventBus eBus;
	
	public OutstandingCallback(EventBus eBus){
		this.eBus = eBus;
	}

	@Override
	public void batchProcessed(SharedExecutorBatchWorker<UserOutstandingChangeEvent, Void>batchWorker,
			List<UserOutstandingChangeEvent> data, Void result, Throwable ex) {
		if(ex != null){
			logger.error("error from outstanding call back: {}", ex);
		}else{
			if(!data.isEmpty())
			{
				
				List<UserOutstandingChangeEvent> lst = caculateOutstandingForEachUser(data);
				for (UserOutstandingChangeEvent item : lst) {
					
					logger.debug("Sending outstanding change notification: user {}, outstanding {}",
							item.getUserName(),item.getOutstandingValue());
					UserOutstandingChangeEvent event = new UserOutstandingChangeEvent(item.getUserName(),item.getOutstandingValue(),item.getUserId());
					try{
						this.eBus.post(event);
						logger.debug("Sent outstanding change notification: user {}, outstanding {}",
								item.getUserName(),item.getOutstandingValue());
					}catch(Exception e){
						logger.error("Can not sent outstanding change notification: user {}, outstanding {} with exception {}",
								item.getUserName(),item.getOutstandingValue(),e.getMessage());
					}
				}
			}
			logger.info("success to post message: {}", ex);
		}
		
	}
	
	List<UserOutstandingChangeEvent> caculateOutstandingForEachUser(List<UserOutstandingChangeEvent> data){
		HashMap<Long, UserOutstandingChangeEvent> hm = new HashMap<>();
		List<UserOutstandingChangeEvent> list = new ArrayList<>();
		for (UserOutstandingChangeEvent item : data) {
			if(hm.containsKey(item.getUserId())){
				double outStanding = item.getOutstandingValue() + hm.get(item.getUserId()).getOutstandingValue();
				hm.get(item.getUserId()).setOutstandingValue(outStanding);
			}else{
				hm.put(item.getUserId(), item);
			}
		}
		list.addAll(hm.values());
		return list;
		
	}


}
