package jayeson.vodds.service.cms.order.result;

public class RefundCreditResult extends IActionResult {
	
	// amount of credit that is succesfully reserved
	double refundedCredit;
	
	// amount of commission value deducted from user
	double refundedCommission;
	
	// whether to or not use only the commission amount in adding to user credit, true for closing order reservation
	boolean useOnlyCommission=false;
	
	// whether the refunded credit is related to margin, true for close order refund
	boolean marginRefund = false;
	
	//default leverage ratio for the result, values set to corresponding ratio in positional opening order 
	double leverageRatio=1;
	
	public double getRefundedCredit() {
		return refundedCredit;
	}

	public void setRefundedCredit(double refundedCredit) {
		this.refundedCredit = refundedCredit;
	}

	public double getRefundedCommission() {
		return refundedCommission;
	}

	public void setRefundedCommission(double refundedCommission) {
		this.refundedCommission = refundedCommission;
	}

	public boolean isUseOnlyCommission() {
		return useOnlyCommission;
	}

	public void setUseOnlyCommission(boolean useOnlyCommission) {
		this.useOnlyCommission = useOnlyCommission;
	}

	public double getLeverageRatio() {
		return leverageRatio;
	}

	public void setLeverageRatio(double leverageRatio) {
		this.leverageRatio = leverageRatio;
	}

	public boolean isMarginRefund() {
		return marginRefund;
	}

	public void setMarginRefund(boolean marginRefund) {
		this.marginRefund = marginRefund;
	}

	

}
