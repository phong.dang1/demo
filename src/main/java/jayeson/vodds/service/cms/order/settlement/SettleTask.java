package jayeson.vodds.service.cms.order.settlement;

import com.google.inject.Inject;

public class SettleTask implements Runnable{

	@Inject
	UnsettledQueue oQueue;
	
	@Override
	public void run() {

		oQueue.settleOrders();
		oQueue.settleDPoolBets();
		oQueue.revertSettlementForAcceptedBet();
	}
	

}
