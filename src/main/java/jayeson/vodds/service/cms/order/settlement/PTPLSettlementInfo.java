package jayeson.vodds.service.cms.order.settlement;

import java.math.BigDecimal;

import jayeson.database.AgentUser;

/**
 * Class that hold all the PTPL settlement info
 * @author Anthony
 *
 */
public class PTPLSettlementInfo {
	private AgentUser agentUser;
	private BigDecimal creditPTPLBefore;
	private BigDecimal creditPTPLAfter;
	private BigDecimal creditPTPL;
	private long orderId;
	
	public PTPLSettlementInfo(AgentUser agentUser, BigDecimal creditPTPLBefore, BigDecimal creditPTPLAfter,
			BigDecimal creditPTPL, long orderId) {
		super();
		this.agentUser = agentUser;
		this.creditPTPLBefore = creditPTPLBefore;
		this.creditPTPLAfter = creditPTPLAfter;
		this.creditPTPL = creditPTPL;
		this.orderId = orderId;
	}
	public AgentUser getAgentUser() {
		return agentUser;
	}
	public void setAgentUser(AgentUser agentUser) {
		this.agentUser = agentUser;
	}
	public BigDecimal getCreditPTPLBefore() {
		return creditPTPLBefore;
	}
	public void setCreditPTPLBefore(BigDecimal creditPTPLBefore) {
		this.creditPTPLBefore = creditPTPLBefore;
	}
	public BigDecimal getCreditPTPLAfter() {
		return creditPTPLAfter;
	}
	public void setCreditPTPLAfter(BigDecimal creditPTPLAfter) {
		this.creditPTPLAfter = creditPTPLAfter;
	}
	public BigDecimal getCreditPTPL() {
		return creditPTPL;
	}
	public void setCreditPTPL(BigDecimal creditPTPL) {
		this.creditPTPL = creditPTPL;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	
	
}
