package jayeson.vodds.service.cms.order.action;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.database.IDBService;
import jayeson.vodds.service.cms.json.LeverageRefundInfo;
import jayeson.vodds.service.cms.order.result.IActionResult;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/***
 * Action executed when the user's credit is refunded after the completion/rejection of positional open order execution
 * only the margin corresponding to the refunded credit and the commission on overall refunded credit is added to user credit
 * @author Praveen
 *
 */
public class RefundOpenCreditAction  implements IOrderAction,Callable<RefundCreditResult> {
	
	@Inject
	IDBService helper;
	
	// user manager instance to manager the bet user node
	UserManager uManager;

	ThriftCreditRequestInfo reqInfo;

	// information required to process the credit refund
	LeverageRefundInfo refInfo;

	TransactionBuilder tBuilder;
	
	private VoddsCommonUtility voddsCommonUtility;
	
	private static String TX_REFUND_MARGIN_OPEN = "tx_refund_margin_open";

	private static Logger logger = LoggerFactory.getLogger(RefundOpenCreditAction.class);

	@Inject
	public RefundOpenCreditAction(UserManager manager,TransactionBuilder builder, LeverageRefundInfo info, ThriftCreditRequestInfo thriftReqInfo,VoddsCommonUtility voddsCommonUtility){
		uManager = manager;
		refInfo = info;
		reqInfo = thriftReqInfo;
		tBuilder = builder;
		this.voddsCommonUtility = voddsCommonUtility;
	}
	
	@Override
	public RefundCreditResult call() throws Exception {
		return processCreditAction();
	}

	@Override
	public RefundCreditResult processCreditAction() {
		
		BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
		BigDecimal refCredit = new BigDecimal(refInfo.refundAmount);
		BigDecimal leverage = new BigDecimal(refInfo.leverageRatio);

		//calculate the margin credit amount based on the leverage ratio
		BigDecimal marginCredit = refCredit.divide(leverage,new MathContext(20, RoundingMode.CEILING)).setScale(10, RoundingMode.CEILING);

		
		// compute total credit after adding in commission percentage of user
		BigDecimal commission = refCredit.multiply(userNode.getUser().getCommission());
				
		logger.info("refunding a credit of {} and commission {} from user {} with leverage ratio {}",marginCredit,commission,reqInfo.getUserId(),leverage);

		RefundCreditResult result = new RefundCreditResult();
		result.setModelId(refInfo.modelId);
		result.setRefundedCommission(commission.doubleValue());
		result.setRefundedCredit(refCredit.doubleValue());
		result.setLeverageRatio(refInfo.leverageRatio);


//		String remark = String.format("Refund margin of %s since open order has confirmed stake less than stake placed",
//			tBuilder.format(new BigDecimal(result.getRefundedCredit()/result.getLeverageRatio())));
		
		TradeLimitData tradeLimitData = new TradeLimitData(reqInfo.getMatchId(),reqInfo.getSportType(), reqInfo.getOrderStatus(),
				reqInfo.getExecutionStage(), new Timestamp(reqInfo.getStartTime()), BigDecimal.ZERO,
				BigDecimal.valueOf(refInfo.refundAmount),BigDecimal.ZERO, ThriftTransactionType.REFUND.name());
		
		tradeLimitData.setTradeLimitCheck(userNode.isCheckTradeLimit());
		tradeLimitData.setRejectedStake(BigDecimal.valueOf(refInfo.rejectedStake));
		tradeLimitData.setoType(reqInfo.oType);

		String remark = voddsCommonUtility.stringsToDescription(TX_REFUND_MARGIN_OPEN,tBuilder.format(new BigDecimal(result.getRefundedCredit()/result.getLeverageRatio())));
		
		//result.setMessage(refInfo.remark);
		result.setMessage(remark);
		
		logger.info("model id {} of user id {} have refund credit {} and leverage ratio {}",refInfo.modelId,reqInfo.getUserId(),result.getRefundedCredit(),leverage);
		
		tBuilder.populateRefundCreditTransactions(result, userNode,ThriftOrderType.POSITIONAL_OPEN.name(),tradeLimitData);

	
		return result;

	}
	
	

}
