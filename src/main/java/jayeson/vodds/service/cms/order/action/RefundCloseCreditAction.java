package jayeson.vodds.service.cms.order.action;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.json.LeverageRefundInfoCloseOrder;
import jayeson.vodds.service.cms.json.NormalRefundInfo;
import jayeson.vodds.service.cms.order.result.IActionResult;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/***
 * Action executed when the user's credit is refunded after the completion/rejection of positional closing order execution
 * only the commission amount of refunded credit is added back to user credit
 * @author Praveen
 *
 */

public class RefundCloseCreditAction implements IOrderAction,Callable<RefundCreditResult> {
	
	// user manager instance to manager the bet user node
		UserManager uManager;

		// information required to process the credit refund
		LeverageRefundInfoCloseOrder refInfo;

		TransactionBuilder tBuilder;
		
		ThriftCreditRequestInfo reqInfo;

		private static Logger logger = LoggerFactory.getLogger(RefundCloseCreditAction.class);

		@Inject
		public RefundCloseCreditAction(UserManager manager,TransactionBuilder builder, LeverageRefundInfoCloseOrder info, ThriftCreditRequestInfo thriftReqInfo){
			uManager = manager;
			refInfo = info;
			reqInfo = thriftReqInfo;
			tBuilder = builder;
		}
		
		@Override
		public RefundCreditResult call() throws Exception {
			return processCreditAction();
		}

		// refund credit of closing order shall be the difference between margin topup and upl, so no commission shall be involved 
		@Override
		public RefundCreditResult processCreditAction() {

			BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
			BigDecimal refCredit = new BigDecimal(refInfo.refundAmount);
					
			logger.info("refunding a credit of {} and commission {} from user {}",refCredit,0,reqInfo.getUserId());

			RefundCreditResult result = new RefundCreditResult();
			result.setModelId(refInfo.modelId);
			result.setRefundedCommission(0);
			result.setRefundedCredit(refCredit.doubleValue());
			result.setMarginRefund(true);
			
			result.setMessage(refInfo.remark);
			
			TradeLimitData tradeLimitData = new TradeLimitData(reqInfo.getMatchId(),reqInfo.getSportType(), reqInfo.getOrderStatus(),
					reqInfo.getExecutionStage(), new Timestamp(reqInfo.getStartTime()), BigDecimal.ZERO,
					BigDecimal.ZERO,BigDecimal.valueOf(refInfo.mpl), ThriftTransactionType.REFUND.name());
			tradeLimitData.setConfirmedStakeOpenOrder(BigDecimal.valueOf(refInfo.confirmedStakeOpenOrder));

			tradeLimitData.setTradeLimitCheck(userNode.isCheckTradeLimit());
			tradeLimitData.setoType(reqInfo.oType);

			tBuilder.populateRefundCreditTransactions(result, userNode,ThriftOrderType.POSITIONAL_CLOSE.name(),tradeLimitData);

			
			return result;
		}


}
