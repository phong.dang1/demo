package jayeson.vodds.service.cms.order.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.events.UserOutstandingChangeEvent;
import jayeson.vodds.service.cms.json.NormalRefundInfo;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.order.result.IActionResult;
import jayeson.vodds.service.cms.order.result.RefundCreditResult;
import jayeson.vodds.service.cms.order.worker.OutstandingHandler;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;

import com.google.inject.Inject;


/***
 * Action executed when the user's credit is refunded after the completion/rejection of normal order execution
 * @author Praveen
 *
 */
public class RefundNormalCreditAction implements IOrderAction,Callable<RefundCreditResult> {

	// user manager instance to manager the bet user node
	UserManager uManager;


	// information required to process the credit refund
	NormalRefundInfo refInfo;

	TransactionBuilder tBuilder;
	
	private VoddsCommonUtility voddsCommonUtility;

	private static Logger logger = LoggerFactory.getLogger(RefundNormalCreditAction.class);
	
	private static String TX_REFUND_STAKE = "tx_refund_stake";
	
	ThriftCreditRequestInfo reqInfo;
	

	@Inject
	public RefundNormalCreditAction(UserManager manager,TransactionBuilder builder, NormalRefundInfo info, ThriftCreditRequestInfo thriftReqInfo,VoddsCommonUtility voddsCommonUtility){
		uManager = manager;
		refInfo = info;
		reqInfo = thriftReqInfo;
		tBuilder = builder;
		this.voddsCommonUtility = voddsCommonUtility;
	}
	
	@Override
	public RefundCreditResult call() throws Exception {
		return processCreditAction();
	}

	@Override
	public RefundCreditResult processCreditAction() {
		
		BetUserNode userNode = uManager.findBetUserNode(this.reqInfo.userId);
		BigDecimal refCredit = new BigDecimal(refInfo.refundAmount);
		RefundCreditResult result = new RefundCreditResult();

		// compute total credit after adding in commission percentage of user
		BigDecimal commission = refCredit.multiply(userNode.getUser().getCommission());
		
		logger.info("refunding a credit of {} and commission {} from user {} - matchId {} - sportType {} - orderStatus {} - executeStage {} - startTime {}",
				refCredit,commission,reqInfo.userId,reqInfo.getMatchId(),reqInfo.getSportType(),reqInfo.getOrderStatus(),reqInfo.getExecutionStage(),reqInfo.getStartTime());

		
		result.setModelId(refInfo.modelId);
		result.setRefundedCommission(commission.doubleValue());
		result.setRefundedCredit(refCredit.doubleValue());
			
		
			
		TradeLimitData tradeLimitData = new TradeLimitData(reqInfo.getMatchId(),reqInfo.getSportType(), reqInfo.getOrderStatus(),
				reqInfo.getExecutionStage(), new Timestamp(reqInfo.getStartTime()), BigDecimal.valueOf(refInfo.rejectedStake),
				BigDecimal.valueOf(refInfo.refundAmount),BigDecimal.ZERO, ThriftTransactionType.REFUND.name());

		tradeLimitData.setTradeLimitCheck(userNode.isCheckTradeLimit());
		tradeLimitData.setoType(reqInfo.oType);
		
		
		//result.setMessage(refInfo.remark);
		
		//		String remark = String.format("Refund for normal order with unused stake = %s",
		//			 tBuilder.format(new BigDecimal(result.getRefundedCredit())));
		
		String remark = this.voddsCommonUtility.stringsToDescription(TX_REFUND_STAKE,tBuilder.format(new BigDecimal(result.getRefundedCredit())));
		result.setMessage(remark);

		
		tBuilder.populateRefundCreditTransactions(result, userNode,ThriftOrderType.NORMAL.name(),tradeLimitData);
		
		
		return result;
	}

}
