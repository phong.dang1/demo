package jayeson.vodds.service.cms.order.action;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jayeson.vodds.common.utility.VoddsCommonUtility;
import jayeson.vodds.orders.positional.PositionalOrderStage;
import jayeson.vodds.service.cms.data.BetUserNode;
import jayeson.vodds.service.cms.data.UserManager;
import jayeson.vodds.service.cms.json.LeverageReserveInfo;
import jayeson.vodds.service.cms.json.NormalReserveInfo;
import jayeson.vodds.service.cms.order.result.IActionResult;
import jayeson.vodds.service.cms.order.result.ReserveCreditResult;
import jayeson.vodds.service.cms.thrift.ThriftCreditRequestInfo;
import jayeson.vodds.service.cms.thrift.ThriftOrderType;
import jayeson.vodds.service.cms.thrift.ThriftTransactionType;
import jayeson.vodds.service.cms.tradelimit.TradeLimitData;
import jayeson.vodds.service.cms.transactions.TransactionBuilder;


import com.google.inject.Inject;

/***
 * Action executed when the user's credit is intended to be reserved for executing a positional open order
 * The action will contain the logic to deduct only the commission of order credit amount from user credit
 * 
 * @author Praveen
 *
 */
public class ReserveCloseCreditAction implements IOrderAction,Callable<ReserveCreditResult>{
	
	private static Logger logger = LoggerFactory.getLogger(ReserveCloseCreditAction.class);

	
	// user manager instance to manager the bet user node
	UserManager uManager;
	// id of the bet user

	// information required to process the credit request
	LeverageReserveInfo resInfo;
	// transaction builder to populate transactions
	TransactionBuilder tBuilder;
	
	VoddsCommonUtility voddsCommonUtility;
	
	ThriftCreditRequestInfo reqInfo;
	
	private static String TX_PARTIALLY_RESERVE_MARGIN_CLOSE = "tx_partially_reserve_margin_close";
	
	@Inject
	public ReserveCloseCreditAction(UserManager umanager, TransactionBuilder builder, LeverageReserveInfo info, ThriftCreditRequestInfo thriftReqInfo,VoddsCommonUtility voddsCommonUtility){
		uManager = umanager;
		reqInfo = thriftReqInfo;
		resInfo = info;
		tBuilder = builder;
		this.voddsCommonUtility = voddsCommonUtility;
				
	}
	
	
	@Override
	public ReserveCreditResult call()  {
		return processCreditAction();
	}


	@Override
	public ReserveCreditResult processCreditAction() {

		BetUserNode userNode = uManager.findBetUserNode(reqInfo.getUserId());
		BigDecimal reqCredit = new BigDecimal(resInfo.creditAmount);

		
		// compute total credit after adding in commission percentage of user
		BigDecimal commission = reqCredit.multiply(userNode.getUser().getCommission().negate());

		ReserveCreditResult aResult = new ReserveCreditResult();
		aResult.setModelId(resInfo.modelId);
		
		TradeLimitData tradeLimitData = new TradeLimitData(reqInfo.getMatchId(),reqInfo.getSportType(), reqInfo.getOrderStatus(),
				reqInfo.getExecutionStage(), new Timestamp(reqInfo.getStartTime()), BigDecimal.ZERO,
				BigDecimal.ZERO,BigDecimal.valueOf(resInfo.mpl), ThriftTransactionType.RESERVE.name());
		
		tradeLimitData.setTradeLimitCheck(userNode.isCheckTradeLimit());
			
		tradeLimitData.setConfirmedStakeOpenOrder(BigDecimal.valueOf(resInfo.confirmedStakeOpenOrder));
		tradeLimitData.setoType(reqInfo.oType);
		
		
		// handle the margin topup case
		if(resInfo.isMarginTopup){
			logger.info("trying to margin topup with MPL > Margin with margin {} from user {}",reqCredit,reqInfo.getUserId());
			aResult.setReservedCredit(reqCredit.doubleValue());
			aResult.setCommissionAmount(0);
			aResult.setMarginTopup(true);
			
			aResult.setMessage(resInfo.remark);

			tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.POSITIONAL_CLOSE.name(),reqInfo.getExecutionStage().equalsIgnoreCase(PositionalOrderStage.CLOSED.name().toString()) == true ? tradeLimitData : null);
		}else{
			logger.info("trying to reserve credit of commission value {} from user {}",commission,reqInfo.getUserId());
	
			// when the user has sufficient credit that is requested
			if(userNode.getCredit().compareTo(commission) > -1){
	
				// set the result to the effective credit reserved, without commission value
				aResult.setReservedCredit(reqCredit.doubleValue());
				aResult.setCommissionAmount(commission.doubleValue());
				aResult.setUseOnlyCommission(true);
				
				aResult.setMessage(resInfo.remark);
				// populate the reserve credit related db transactions
				tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.POSITIONAL_CLOSE.name(),tradeLimitData);
			}
			
			// when user didn't have enough credit, but the option to use all credit is set true in the request
			else if (userNode.getCredit().compareTo(commission) < 0 && userNode.getCredit().compareTo(new BigDecimal(0)) > 0 && resInfo.useAllCredit){
				
				// compute the effective credit, after taking care of commission
				BigDecimal eCredit = userNode.getCredit().divide(userNode.getUser().getCommission().negate(),
						new MathContext(20, RoundingMode.CEILING)).setScale(10, RoundingMode.CEILING);			
				commission = eCredit.multiply(userNode.getUser().getCommission().negate());	
				
				aResult.setReservedCredit(eCredit.doubleValue());
				aResult.setCommissionAmount(commission.doubleValue());
				// set the commission only flag to true so that the transactions inside database will only record for commission
				aResult.setUseOnlyCommission(true);
				String remark = this.voddsCommonUtility.stringsToDescription(TX_PARTIALLY_RESERVE_MARGIN_CLOSE);
				//aResult.setMessage("credit partially reserved due to insufficient funds");

				aResult.setMessage(remark);
				
				// populate the reserve credit related db transactions
				tBuilder.populateReserveCreditTransactions(aResult, userNode,ThriftOrderType.POSITIONAL_CLOSE.name(),tradeLimitData);
			}
			
			
			else{
				// set invalid values for error cases
				aResult.setReservedCredit(-1);
				aResult.setCommissionAmount(Double.NaN);
				
				aResult.setMessage("credit reservation failed due to insufficient funds");
			}
		}

		return aResult;
	}


}
