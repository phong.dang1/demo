package jayeson.vodds.service.cms.config;

import com.google.inject.Singleton;

@Singleton
public class ConfigBean {
	
	private KafkaConfig kafkaConfig;
	// The service id
	private String serviceId;
	// The IP of the host machine
	private String host; 
	// The thrift service listening port
	private int port;
	
	// Number of selector threads for thrift server
	private int selectorThreads;
	// Number of worker threads
	private int workerThreads;
	
	// number of orders to be settled at one go
	private int settleOrderPerBlock;
	
	// interval at which the transaction queue will be processed
	private int transactionProcessInterval;
	
	// interval at which orders are added into the unsettled queue
	private int unsettledOrderQInterval;
	
	// interval at which unsettled queues is processed
	private int settleOrderQInterval;
	
	// expirty interval for order cache, after which the order and its bets are refreshed from db
	private int orderExpiryInterval;
	
	// number of unsettled orders to scan from the database in one iteration
	private int populateOrdersPerInterval;
	
	// interval at which pl notification
	private int plNotificationInterval;
	
	//size of batch 
	private int batchSize;
	
	private int batchSizeOfThriftUserUpdate;

	//config service config
	private ConfigConfig configConfig;
	// credit tally threadhold
	private double creditTallyThreshold;
	// negative outstanding threshold -0.0002 is valid
	private double outstandingThreshold;
	
	public ConfigBean() {
		super();
	}

	public int getBatchSizeOfThriftUserUpdate() {
		return batchSizeOfThriftUserUpdate;
	}

	public void setBatchSizeOfThriftUserUpdate(int batchSizeOfThriftUserUpdate) {
		this.batchSizeOfThriftUserUpdate = batchSizeOfThriftUserUpdate;
	}

	public int getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}

	public int getPlNotificationInterval() {
		return plNotificationInterval;
	}

	public void setPlNotificationInterval(int plNotificationInterval) {
		this.plNotificationInterval = plNotificationInterval;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getSelectorThreads() {
		return selectorThreads;
	}

	public void setSelectorThreads(int selectorThreads) {
		this.selectorThreads = selectorThreads;
	}

	public int getWorkerThreads() {
		return workerThreads;
	}

	public void setWorkerThreads(int workerThreads) {
		this.workerThreads = workerThreads;
	}

	public int getSettleOrderPerBlock() {
		return settleOrderPerBlock;
	}

	public void setSettleOrderPerBlock(int settleOrderPerBlock) {
		this.settleOrderPerBlock = settleOrderPerBlock;
	}

	public int getTransactionProcessInterval() {
		return transactionProcessInterval;
	}

	public void setTransactionProcessInterval(int transactionProcessInterval) {
		this.transactionProcessInterval = transactionProcessInterval;
	}

	public int getUnsettledOrderQInterval() {
		return unsettledOrderQInterval;
	}

	public void setUnsettledOrderQInterval(int unsettledOrderQInterval) {
		this.unsettledOrderQInterval = unsettledOrderQInterval;
	}

	public int getSettleOrderQInterval() {
		return settleOrderQInterval;
	}

	public void setSettleOrderQInterval(int settleOrderQInterval) {
		this.settleOrderQInterval = settleOrderQInterval;
	}

	public int getOrderExpiryInterval() {
		return orderExpiryInterval;
	}

	public void setOrderExpiryInterval(int orderExpiryInterval) {
		this.orderExpiryInterval = orderExpiryInterval;
	}

	public int getPopulateOrdersPerInterval() {
		return populateOrdersPerInterval;
	}

	public void setPopulateOrdersPerInterval(int populateOrdersPerInterval) {
		this.populateOrdersPerInterval = populateOrdersPerInterval;
	}

	public KafkaConfig getKafkaConfig() {
		return kafkaConfig;
	}

	public void setKafkaConfig(KafkaConfig kafkaConfig) {
		this.kafkaConfig = kafkaConfig;
	}
	
	

	public ConfigConfig getConfigConfig() {
		return configConfig;
	}

	public void setConfigConfig(ConfigConfig configConfig) {
		this.configConfig = configConfig;
	}

	public double getCreditTallyThreshold() {
		return creditTallyThreshold;
	}

	public void setCreditTallyThreshold(double creditTallyThreshold) {
		this.creditTallyThreshold = creditTallyThreshold;
	}

	public double getOutstandingThreshold() {
		return outstandingThreshold;
	}

	public void setOutstandingThreshold(double outstandingThreshold) {
		this.outstandingThreshold = outstandingThreshold;
	}
}
