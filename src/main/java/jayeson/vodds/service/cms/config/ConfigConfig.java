package jayeson.vodds.service.cms.config;

public class ConfigConfig {
    private String host;
    private String loadTradeLimitUrl;
    private String loadConstantUrl;

	public ConfigConfig() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getLoadTradeLimitUrl() {
        return loadTradeLimitUrl;
    }

    public void setLoadTradeLimitUrl(String loadTradeLimitUrl) {
        this.loadTradeLimitUrl = loadTradeLimitUrl;
    }
    
    public String getLoadConstantUrl() {
		return loadConstantUrl;
	}

	public void setLoadConstantUrl(String loadConstantUrl) {
		this.loadConstantUrl = loadConstantUrl;
	}
}
