package jayeson.vodds.service.cms.config;

public class KafkaConfig {
	private String topic;
	private Long pollDelay; // delay before polling from kafka again 
	private Long startOffset;
	
	public KafkaConfig() {
		this.topic = "orderlog";
		this.pollDelay = 5000L;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Long getPollDelay() {
		return pollDelay;
	}

	public void setPollDelay(Long pollDelay) {
		this.pollDelay = pollDelay;
	}

	public Long getStartOffset() {
		return startOffset;
	}

	public void setStartOffset(Long startOffset) {
		this.startOffset = startOffset;
	}
	
}
