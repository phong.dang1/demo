package jayeson.vodds.service.cms.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import jayeson.database.newvodds.BetUserSetting;
import jayeson.database.newvodds.ConstantType;
import jayeson.vodds.service.cms.config.ConfigBean;
import jayeson.vodds.service.cms.config.ConfigConfig;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class ConfigUtil {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private ConfigConfig configConfig;
    private ObjectMapper mapper;
    private boolean firstTradeLimitLoadFinished;
    private boolean firstLoadConstantFinished;
    private Map<String, String> constants;
    public static final String LIMIT_TIME = "LIMIT_TIME";
    public static final String EXCLUDE_TRADE_LIMIT_CHECK = "EXCLUDE_TRADE_LIMIT_CHECK";
    public static final String BITRIX24_AGENT = "BITRIX24_AGENT";
    public static List<String> validConstantNames = Arrays.asList(LIMIT_TIME, EXCLUDE_TRADE_LIMIT_CHECK, BITRIX24_AGENT);

    @Inject
    public ConfigUtil(ConfigBean configBean) {
        this.configConfig = configBean.getConfigConfig();
        this.mapper = new ObjectMapper();
        this.firstTradeLimitLoadFinished = false;
        this.firstLoadConstantFinished = false;
        this.loadConstants();
    }

	public Map<Long, BetUserSetting> loadTradeLimit(List<Long> userIds) {
        Map<Long, BetUserSetting> busMap = new HashMap<>();
        try {
            HttpClient httpclient = HttpClients.createDefault();
            String url = configConfig.getHost().concat(configConfig.getLoadTradeLimitUrl());
            HttpPost httpPost = new HttpPost(url);

            // build parameters
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("users", userIds);
            StringEntity postParameters = new StringEntity(mapper.valueToTree(paramMap).toString());

            httpPost.setEntity(postParameters);
            httpPost.setHeader("Content-type", "application/json");

            //Execute and get the response.
            logger.trace("sending load trade limit request with parameters {}", mapper.valueToTree(paramMap).toString());
            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();

            if (responseEntity != null) {
                logger.trace("received response with status code {}", response.getStatusLine().getStatusCode());
                if (response.getStatusLine().getStatusCode() == 200) {
                    String resultString = this.convertInputStreamToString(responseEntity.getContent());

                    JsonNode resp = mapper.readTree(resultString);
                    
                    if (!resp.get("errorMessage").asText().isEmpty()) {
                    	this.handleFirstTradeLimitLoadFailure(url, resp.get("errorMessage").asText(), userIds, null);
                    }else {
                    	ArrayNode busNodes = (ArrayNode) resp.get("betUserSettings");
                        for (JsonNode busNode : busNodes) {
                        	logger.info("userId {} - max_bet_normal {} - max_bet_positional {} - max_bet_per_match {} - max_loss {} - default_max_bet_normal {} - default_max_bet_positional {} - default_max_bet_per_match {} - default_max_loss {}",
                        			busNode.get("id").asLong(),busNode.get("max_bet_normal"),busNode.get("max_bet_positional"),busNode.get("max_bet_per_match"),busNode.get("max_loss"),busNode.get("default_max_bet_normal"),busNode.get("default_max_bet_positional"),busNode.get("default_max_bet_per_match"),busNode.get("default_max_loss"));
                            BetUserSetting bus = new BetUserSetting();
                            bus.setBetUser(busNode.get("id").asLong());
                            bus.setMaxBetNormal(busNode.get("max_bet_normal") != null ? BigDecimal.valueOf(busNode.get("max_bet_normal").asDouble()) : null);
                            bus.setMaxBetPositional(busNode.get("max_bet_positional") != null ? BigDecimal.valueOf(busNode.get("max_bet_positional").asDouble()) : null);
                            bus.setMaxBetPerMatch(busNode.get("max_bet_per_match") != null ? BigDecimal.valueOf(busNode.get("max_bet_per_match").asDouble()) : null);
                            bus.setMaxLoss(busNode.get("max_loss") != null ? BigDecimal.valueOf(busNode.get("max_loss").asDouble()) : null);
                            
                            bus.setDefaultMaxBetNormal(BigDecimal.valueOf(busNode.get("default_max_bet_normal").asDouble()));
                            bus.setDefaultMaxBetPositional(BigDecimal.valueOf(busNode.get("default_max_bet_positional").asDouble()));
                            bus.setDefaultMaxBetPerMatch(BigDecimal.valueOf(busNode.get("default_max_bet_per_match").asDouble()));
                            bus.setDefaultMaxLoss(BigDecimal.valueOf(busNode.get("default_max_loss").asDouble()));

                            busMap.put(bus.getBetUser(), bus);
                        }
                    }
                }else {
                	handleFirstTradeLimitLoadFailure(url, String.format("%s-%s", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()), userIds, null);
                }
            }else {
            	handleFirstTradeLimitLoadFailure(url, "Response entity is null", userIds, null);
            }
        } catch (Exception ex) {
            handleFirstTradeLimitLoadFailure("loadTradeLimit", null, userIds, ex);
        }

        this.firstTradeLimitLoadFinished = true;
        logger.trace("return bet user setting map with keys {}", busMap.keySet());
        
        return busMap;
    }
	
	public Map<String, String> loadConstants(){
		constants = new HashMap<>();
		try{
            HttpClient httpclient = HttpClients.createDefault();
            String url = configConfig.getHost().concat(configConfig.getLoadConstantUrl());
            HttpPost httpPost = new HttpPost(url);
            
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("constantNames", validConstantNames);
            StringEntity postParameters = new StringEntity(mapper.valueToTree(paramMap).toString());
            
            httpPost.setEntity(postParameters);
            httpPost.setHeader("Content-type", "application/json");

            //Execute and get the response.
            logger.trace("sending load constant request with parameters {}", mapper.valueToTree(paramMap).toString());
            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();
            
            if (responseEntity != null) {
                logger.trace("received response with status code {}", response.getStatusLine().getStatusCode());
                if (response.getStatusLine().getStatusCode() == 200) {
                    String resultString = this.convertInputStreamToString(responseEntity.getContent());
                    JsonNode resp = mapper.readTree(resultString);
                    
                    if (!resp.get("errorMessage").asText().isEmpty()) {
                    	this.handleLoadConstantError(url, resp.get("errorMessage").asText(), null);
                    } else {
                    	constants = this.mapper.readValue(resp.get("voddsConstants").toString(), new TypeReference<Map<String, String>>(){});
                    }
                } else {
                	this.handleLoadConstantError(url, String.format("%s-%s", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()), null);
                }
            } else {
            	this.handleLoadConstantError(url, "Response entity is null", null);
            }
		} catch(Exception ex){
			this.handleLoadConstantError("loadConstants", "Exception at loading constants", ex);
		}
		
        this.firstLoadConstantFinished = true;
        logger.trace("return constant map with keys {}", constants.keySet());
        
        return constants;
	}
	
	private void handleFirstTradeLimitLoadFailure(String url, String errorMessage, List<Long> userIds, Throwable e){
		logger.error("Error while loading {}, first load finished {}, userList{}, errorMessage {} ", url, this.firstTradeLimitLoadFinished, userIds.toString(), errorMessage, e);
		if (!this.firstTradeLimitLoadFinished){
			logger.error("Shut system down because cannot load settings for the first time {}, userIds {}", url, userIds.toString());
			System.exit(-1);
		}
	}
	
	private void handleLoadConstantError(String url, String errorMessage, Throwable e){
		logger.error("Error while loading {}, errorMessage {} ", url, this.firstTradeLimitLoadFinished, errorMessage, e);
		if (!this.firstLoadConstantFinished){
			logger.error("Shut system down because cannot load settings for the first time {}", url);
			System.exit(-1);
		}
	}

    private String convertInputStreamToString(InputStream inputStream) throws Exception{
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString("UTF-8");
    }

    public boolean validBitrix24Agent(Long agentId) {
        try {
            if (constants.containsKey(BITRIX24_AGENT)) {
                List<String> idList = Arrays.asList(constants.get(BITRIX24_AGENT).split(","));
                logger.info("{} is compared to agent id list {}", agentId, idList);
                return idList.contains(String.valueOf(agentId));
            }
        } catch (Exception ex) {
            logger.error("{} verify bitrix agent error", agentId, ex);
        }

        return false;
    }
}
