package jayeson.vodds.service.cms.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import jayeson.database.Bet;
import jayeson.database.newvodds.BettingModelExtraVodds;
import jayeson.database.newvodds.VoddsBetUser;
import jayeson.vodds.common.enums.ServiceType;
import jayeson.vodds.common.enums.UserStatsType;
import jayeson.vodds.common.model.userstats.OrderStats;
import jayeson.vodds.common.model.userstats.UserStats;
import jayeson.vodds.common.utility.LogUtil;
import jayeson.vodds.service.cms.config.ConfigBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Random;

@Singleton
public class GeneralUtility {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private ConfigBean config;
    private ConfigUtil configUtil;
    private Random random;

    @Inject
    public GeneralUtility(ConfigBean config, ConfigUtil configUtil) {
        this.config = config;
        this.configUtil = configUtil;
        random = new Random();
    }

    /**
     * compare first number with second one
     * @param num1
     * @param num2
     * @return true if first number is greater than the second one
     */
    public boolean isLess(BigDecimal num1, BigDecimal num2) {
        if(num1 == null) {
            if(num2 == null) {
                return true;
            }
            return false;
        }

        if (num2 == null) {
            return false;
        }

        return num1.compareTo(num2) == -1;
    }

    public String format(BigDecimal bd) {
    	return this.format(bd, null);
    }
    
    public String format(BigDecimal bd, RoundingMode roundingMode) {
        if(bd == null || bd.compareTo(new BigDecimal(0)) == 0) { return "0"; }

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        df.setGroupingUsed(true);
        if (roundingMode != null) {
        	df.setRoundingMode(roundingMode);
        }

        return df.format(bd);
    }

    public BigDecimal ifnull(BigDecimal num) {
        if(num == null) {
            return BigDecimal.ZERO;
        }

        return num;
    }

    public boolean isValidOutstanding(BigDecimal outstanding) {
        // outstanding = -0.001 > -0.1
        if(outstanding.compareTo(BigDecimal.valueOf(config.getOutstandingThreshold())) <= 0) {
            return false;
        }

        return true;
    }

    public BigDecimal getReturnAmountCompany(Bet bet) {
        return bet.getReturnAmount().multiply(bet.getMultiplication()).multiply(bet.getBaseConversionRate());
    }

    public void logOrderStats(BettingModelExtraVodds extraVModel, BigDecimal newPL, BigDecimal newConfirmedStake) {
        OrderStats orderStats = new OrderStats();
        orderStats.setServiceType(ServiceType.CMS);
        orderStats.setUserId(extraVModel.getModelId().getOwner().getId());
        orderStats.setModelId(extraVModel.getModelId().getId());
        orderStats.setOldPl(ifnull(extraVModel.getProfitLoss()).doubleValue());
        orderStats.setOldConfirmedStake(ifnull(extraVModel.getConfirmedStake()).doubleValue());
        orderStats.setPl(ifnull(newPL).doubleValue());
        orderStats.setConfirmedStake(ifnull(newConfirmedStake).doubleValue());

        LogUtil.logUserStats(orderStats);
    }

    /**
     * Log data to push message to userStatCollector service -> Update Bitrix contact
     * @param betUser
     */
    public void logUserStats(VoddsBetUser betUser) {
        // no need to log for non-bitrix users
        if(!configUtil.validBitrix24Agent(betUser.getParent().getId())) {
            return;
        }
        UserStats userStats = new UserStats(betUser.getId(), System.currentTimeMillis());
        userStats.setServiceType(ServiceType.CMS);
        userStats.setUserStatsType(UserStatsType.CREDIT);
        userStats.setPl(betUser.getpL().doubleValue());
        userStats.setOutstanding(betUser.getOutstandingAmount().doubleValue());
        userStats.setCredit(betUser.getCredit().doubleValue());
        userStats.setCurrency(betUser.getCurrency().getCode());
        LogUtil.logUserStats(userStats);
    }

    public boolean isZero(double d) {
        return Double.compare(d, 0) == 0;
    }

    public String buildGameTransId() {
        return String.format("%s%03d", System.currentTimeMillis(), random.nextInt(1000));
    }
}
