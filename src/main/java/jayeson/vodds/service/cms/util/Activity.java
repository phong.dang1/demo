package jayeson.vodds.service.cms.util;

public enum Activity {
    TOPUP("Top up", "Topup %s%s to %s - transaction id :transId"),
    WITHDRAW("Withdraw", "Withdraw %s%s from %s - transaction id :transId"),
    TRANSFER("Transfer", "Transfer %s%s - transaction id :transId");
    private String name;
    private String description;

    Activity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription(String... params) {
        return String.format(description, params);
    }
}
