# CMS service (Vodds)

## Requirement
- Gradle >= 5.6.1 
- jdk: 1.8.0_256

## CI/CD guideline 

### Deployment

Guideline: [CI/CD guideline](https://docs.google.com/document/d/1A1TvdZ_tL4CUelSHlkrNid-pWeQ_npozw_M7lsocvOU/edit)

Deploy to:
- **Dev** environment:
    ```
      deploy: dev
    ```

- **MyVodds** (or MyTest) environment:
    ```
      deploy: myTest
    ```

- **Staging** environment:
    ```
      deploy: stag
    ```

- **Production** environment:
    ```
      deploy: prod
    ```

### Systemd command for the service
- For verifying service status
    ```
      systemctl status jayeson.vodds.service.cms
    ```

- For restarting service
    ```
      systemctl restart jayeson.vodds.service.cms
    `
